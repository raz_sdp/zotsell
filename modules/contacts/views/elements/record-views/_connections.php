<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:20 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3> Connections</h3>
<hr/>
<div class="note-content">
    <div class="row">
        <div class="col-lg-12">
            <table id="connections-table" class="table table-striped table-bordered table-hover js-view-connections-table">
                <thead>
                <tr>
                    <th>Type</th>
                   <!-- <th>ID</th>-->
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>