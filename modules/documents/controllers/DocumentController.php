<?php

namespace app\modules\documents\controllers;

use app\modules\documents\models\Media;
use app\modules\documents\models\MediaTr;
use app\modules\documents\models\MediaLinks;
use app\modules\documents\models\RepositoryFiles;
use app\modules\documents\models\SearchRepositoryFiles;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * Controller for actions with media
 */
class DocumentController extends \yii\web\Controller
{

    /**
     * View media by id_media from zse_v1_media
     * @param type $id
     * @return type
     */
    public function actionView($id)
    {
        $modelMedia = Media::findOne($id);

        /* @var $dataProviderFolder ActiveDataProvider (Media is also Located in) */
        $dataProviderFolder = new ActiveDataProvider([
            'query' => MediaLinks::find()
                ->joinWith(['folder.father'])
                ->where(['zse_v1_media_links.id_media' => $id])
                ->andWhere(['zse_v1_media_links.context' => 1]),
            'sort' => [
                'attributes' => [
                    'name' => [
                        'asc' => ['zse_v1_docs_tree.label' => SORT_ASC,],
                        'desc' => ['zse_v1_docs_tree.label' => SORT_DESC],
                    ],
                    'path' => [
                        'asc' => ['father.label' => SORT_ASC, 'zse_v1_docs_tree.label' => SORT_ASC],
                        'desc' => ['father.label' => SORT_DESC, 'zse_v1_docs_tree.label' => SORT_DESC],
                    ]
                ]
            ]
        ]);

        $mediaTranslations = $this->buildTranslationArray($modelMedia);

        return $this->render('view', [
            'modelMedia' => $modelMedia,
            'dataProviderFolder' => $dataProviderFolder,
            'mediaTranslations' => $mediaTranslations
        ]);
    }

    /**
     * Create file
     */
    public function actionCreate()
    {
        if (!Yii::$app->getRequest()->getQueryParam('id_folder')) {
            return $this->redirect(['main/list']);
        }

        $idContainer = Yii::$app->getRequest()->getQueryParam('id_folder');

        $media = new Media();

        $media->type = (Yii::$app->getRequest()->getQueryParam('type')) ? Yii::$app->getRequest()->getQueryParam('type') : '';
        if ($media->type == 'gallery') $media->scenario = 'create-gallery';
        else $media->scenario = 'create';

        if ($media->load(Yii::$app->request->post())) {
            $media->idContainer = $idContainer;

            if ($media->validate()) {
                $media->save(false);

                $mediaLink = new MediaLinks();
                $mediaLink->id_container = $idContainer;
                $mediaLink->id_media = $media->getPrimaryKey();
                if (!$mediaLink->validate()) {
                    $errors['mediaLinks'] = $mediaLink->errors;
                }
                $mediaLink->save(false);
                return $this->redirect(['update', 'id' => $media->getPrimaryKey()]);
            } else {
                $errors['media'] = $media->errors;
                Yii::trace("Error. save file: " . print_r($errors, true));
            }
        }

        $media->cansend = true; // sharable checkbox set checked
        $media->icon = ''; // standard icon by default

        $searchModel = new SearchRepositoryFiles();
        $searchModel->load(Yii::$app->request->queryParams, '');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $mediaTranslations = $this->buildTranslationArray($media); // translation for this media

        return $this->render('create', [
            'media' => $media,
            'idContainer' => $idContainer,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mediaTranslations' => $mediaTranslations,
            'dropZoneConfig' => $this->setDropzoneConfig($media->type, $idContainer),
        ]);
    }

    /**
     * Upload files through dropzone
     * @return boolean
     */
    public function actionUpload()
    {
        $fileName = 'file';
        $uploadPath = './uploads';

        if (isset($_FILES[$fileName])) {
            $file = \yii\web\UploadedFile::getInstanceByName($fileName);
            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                if (Yii::$app->getRequest()->getQueryParam('id_folder')) {
                    //save file info to database table
                    $repositoryFile = new RepositoryFiles();
                    $repositoryFile->mimetype = \yii\helpers\FileHelper::getMimeType($uploadPath . '/' . $file->name);
                    $repositoryFile->name = $file->name;
                    $repositoryFile->size = $file->size;

                    if (!$repositoryFile->validate()) {
                        //Yii::trace('validate file error:' . print_r($repositoryFile->errors, true));
                    }
                    $repositoryFile->save(false);

                    echo \yii\helpers\Json::encode($file);
                }
            }
        }

        return false;
    }

    /**
     * Update existing model Media
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->type == 'gallery') $model->scenario = 'create-gallery';
        else $model->scenario = 'create';

        $mediaTranslations = $this->buildTranslationArray($model); // translation for this media

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id_media]);
        }


        $searchModel = new SearchRepositoryFiles();
        $searchModel->type = $model->type;
        $searchModel->load(Yii::$app->request->queryParams, '');
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('create', [
            'media' => $model,
            'idContainer' => $model->mediaLinks[0]->id_container,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'mediaTranslations' => $mediaTranslations,
            'dropZoneConfig' => $this->setDropzoneConfig($model->type, $model->mediaLinks[0]->id_container),
        ]);
    }

    /**
     * Update translation for id_media
     * @param type $id value of field 'media'.'id_media'
     * @param type $lang
     * @return type
     */
    public function actionUpdateTranslation($id, $lang)
    {
        $model = $this->findMediaTrModel($id, $lang);
        if (!$model) {
            $model = new MediaTr();
            $model->id_media = $id;
            $model->lang = $lang;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->type == 'gallery') $model->scenario = 'create-gallery';
            else $model->scenario = 'create';

            if ($model->validate()) {
                $model->save(false);
            } else {
                Yii::trace("Errors when save model Tr: " . print_r($model->errors, true));
            }
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Delete media by primary key $id
     * and delete related media translation models via Media::beforeDelete()
     * @param $id value of database field 'id_media'
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model) $model->delete();

        return $this->redirect(['main/list']);
    }


    /**
     * Delete translation for this media
     * @param $id
     * @param $lang
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDeleteTranslation($id, $lang)
    {
        $model = $this->findMediaTrModel($id, $lang);
        if ($model) $model->delete();

        return $this->redirect(['update', 'id' => $id]);
    }

    /** Find Media model by primary key $id
     * @param $id value of database field 'id_media'
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finde Media translation model
     * @param type $mediaId
     * @param type $lang
     */
    protected function findMediaTrModel($mediaId, $lang)
    {
        if (($modelMediaTr = MediaTr::find()->where(['id_media' => $mediaId, 'lang' => $lang])->one()) !== null) {
            return $modelMediaTr;
        }
        return false;
    }

    /**
     * Build translation array with related models MediaTr (if isset) for current Media
     * @param type $mediaModel
     * @return array translations with added models MediaTr as array element 'model'
     */
    public function buildTranslationArray($mediaModel)
    {
        $translations = Media::getLanguages();
        foreach ($mediaModel->mediaTr as $mediaTr) {
            $issetModel = $mediaTr;
            $issetModel->scenario = 'create';
            $translations[$mediaTr->lang]['model'] = $issetModel;
        }
        foreach ($translations as $key => $translation) {
            if (!isset($translation['model'])) {
                $emptyModel = new MediaTr();
                $emptyModel->file = $mediaModel->file;
                $emptyModel->multimedia = $mediaModel->multimedia;
                $emptyModel->scenario = 'create';
                $translations[$key]['model'] = $emptyModel;
            }
        }
        Yii::trace('buildTranslationArray:' . print_r($translations, true));
        return $translations;
    }

    /**
     * Set dropzone config by media type
     * @param $fileType
     * @param $idContainer
     * @return array
     */
    public function setDropzoneConfig($fileType, $idContainer)
    {
        $config = SearchRepositoryFiles::$dropzoneConfig;
        $config['url'] .= '/?id_folder=' . $idContainer;

        $availableMimeTypes = SearchRepositoryFiles::getMimetypesByType($fileType);
        $mimeTypesString = implode(",", $availableMimeTypes);
        if ($mimeTypesString != "*") $config['acceptedFiles'] = $mimeTypesString;

        return $config;
    }
}
