<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Reports';

$folderLabel = ($model->label) ? "\"{$model->label}\"" : '';
$this->params['headerTitle'] = "Reports Folder {$model->label} Create/update";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$parents = $model->parents()->all();
foreach ($parents as $parentFolder) {
    $this->params['breadcrumbs'][] = [
        'label' => $parentFolder->label,
        'url' => ['main/list', 'id_folder' => $parentFolder->id_report_tree]
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => ($model->label) ? $model->label : 'Folder name',
    'template' => "<li><b>{link}</b></li>\n"
];
?>

    <div class="col-md-3">
        <div class="hpanel panel-group">
            <div class="panel-body">
                <div class="text-center text-muted font-bold">Select tabs</div>

            </div>
            <div class="panel-section">

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Select Area...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i
                            class="glyphicon glyphicon-plus small"></i></button>
                </span>
                </div>
                <button type="button" data-toggle="collapse" data-target="#notes"
                        class="btn-sm visible-xs visible-sm collapsed btn-default btn btn-block m-t-sm">
                    All notes <i class="fa fa-angle-down"></i>
                </button>
            </div>

            <div id="notes" class="collapse">
                <div class="panel-body note-link active_contact_side_bar">
                    <a href="#default" data-toggle="tab">
                        <small class="pull-right text-muted">Active</small>
                        <h5>Basic Info</h5>

                        <div class="small">
                            <strong><?= $model->label ?></strong>
                        </div>
                        <div class="small">
                            <?= $model->description ?>
                        </div>
                    </a>
                </div>

                <div
                    class="panel-body note-link <?php if (!$model->id_report_tree || $model->type == 'folder') echo 'disabledTab'; ?>">
                    <a href="#users" data-toggle="tab">
                        <small class="pull-right text-muted">
                            <?php if (!$model->id_report_tree || $model->type == 'folder'): ?>
                                Disabled
                            <?php else: ?>
                                Active
                            <?php endif; ?>
                        </small>
                        <h5>Users</h5>

                        <div class="small">
                            <strong>User names</strong>
                        </div>
                        <div class="small">
                            Users that can browse documents
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="hpanel">

            <div class="panel-body">

                <div class="text-center hidden">
                    We couldn't find any Document. Add one
                </div>

                <div class="tab-content">
                    <div id="default" class="tab-pane active">
                        <div class="pull-right text-muted m-l-lg">
                            Active
                        </div>
                        <h3>Basic Info</h3>
                        <hr/>
                        <div class="note-content">
                            <div class="row">

                                <?=
                                $this->render('_createForm', [
                                    'model' => $model,
                                    'idContainer' => $idContainer,
                                ]);
                                ?>
                            </div>
                        </div>
                        <?php $formDelete = ActiveForm::begin(['id' => 'delete-folder', 'method' => 'GET', 'action' => ['delete']]); ?>
                        <?= Html::hiddenInput('id', $model->id_report_tree); ?>
                        <?php ActiveForm::end(); ?>

                        <div class="btn-group">
                            <button class="btn btn-sm btn-default" form="create-folder"><i
                                    class="fa fa-thumbs-o-up"></i> Save
                            </button>
                            <?php if ($model->id_report_tree): ?>
                                <button class="btn btn-sm btn-default" form="delete-folder"><i class="fa fa-trash"></i>
                                    Remove
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>


                    <div id="users" class="tab-pane">
                        <div class="pull-right text-muted m-l-lg">
                            Active
                        </div>
                        <h3>Users</h3>
                        <hr/>
                        <div class="note-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php if ($model->type == 'userFolder'): ?>
                                        <?=
                                        $this->render('_moveUsers', [
                                            'model' => $model,
                                            'usersAll' => $usersAll,
                                            'selectedUsers' => $selectedUsers,
                                        ]);
                                        ?>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
                        <?php $formDelete = ActiveForm::begin(['id' => "delete-users", 'method' => 'GET', 'action' => ['delete-users', 'id' => $model->id_report_tree]]); ?>
                        <?= Html::hiddenInput('id', $model->id_report_tree); ?>
                        <?php ActiveForm::end(); ?>

                        <div class="btn-group">
                            <button class="btn btn-sm btn-default" form="move-users"><i
                                    class="fa fa-thumbs-o-up"></i> Save
                            </button>
                            <? ?>
                            <?php if ($model->id_report_tree): ?>
                                <button class="btn btn-sm btn-default" form="delete-users"><i
                                        class="fa fa-trash"></i>
                                    Remove
                                </button>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

<?php
$js = <<<JS
/* bind clicks on the labels */
/*$('#icon_block').find('label').click(function(e) {
    //e.preventDefault();
    console.log(this);
    $(this).closest('.note-content').find('.create-update-media').yiiActiveForm('validateAttribute', 'icon_block');
});
*/
// event after upoading icon image
function eventAfterIconUpload(file){
$('#radio_custom').val(file.name);
//$('#create-media').yiiActiveForm('validateAttribute', 'icon_block'); // before
$('#icon_block').closest('.note-content').find('.create-update-folder').yiiActiveForm('validateAttribute', 'icon_block'); // validate Icon radio input after uploading
}

// active current language in tranlsation block
$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
});
JS;

$this->registerJs($js, \yii\web\View::POS_READY); //, 'projects-filter-form-script'

if ($model->type == 'userFolder') {

    $this->registerCssFile("/homer/js/lou-multi-select-05449b3/css/multi-select.css", [
        'depends' => [\yii\bootstrap\BootstrapAsset::className()],
    ]);
    $this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/quicksearch/jquery.quicksearch.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/lou-multi-select-05449b3/js/jquery.multi-select.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
}
?>

<?php
if (isset($activeTab) && !is_null($activeTab)) Yii::$app->view->registerJs('var tab = "' . $activeTab . '"', \yii\web\View::POS_HEAD); ?>

<?php
$js = <<<JS
function activeTab(tab){
if (tab!=''){
    $('#notes a[href="#' + tab + '"]').tab('show');
    $('#notes a[href="#' + tab + '"]').parent().addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
}
};

$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
    if (typeof tab != 'undefined') activeTab(tab);
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>