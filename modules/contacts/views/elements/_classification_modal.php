<?php
/**
 * Created by PhpStorm.
 * User: Raaz
 * Date: 2/27/16
 * Time: 12:27 PM
 */
?>

<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Define Classification Types</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <input type="hidden" name="classification_id" value="">
                <div class="form-group">
                    <label for="name">Classification Color [Beta] </label>
                    <input class="form-control js-text-color" type="color" placeholder="Choose color" required/>
                    <!--<input type="text" id="name" name="name" placeholder="enter hex color #ff45ff" class="form-control js-text-color" required>-->
                </div>
                <div class="form-group">
                    <label for="name"> Classification Label </label>
                    <input type="text" id="name" name="name" placeholder="enter label" class="form-control js-text-label">
                    <input type="hidden" class="form-control js-text-id">
                </div>

                <div>
                    <button class="btn btn-block btn-success m-t-n-xs js-classification_submit_btn">Add Classification Type</button>
                </div>

                <div class="table-responsive">
                    <div class="item-view slim_scroll">
                      <table class="table table-striped js-classifications-table">
                        <thead>
                        <tr>
                            <th>Color</th>
                            <th>Label</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if(isset($classifications) && !empty($classifications)) {
                            foreach ($classifications as $key => $item) { ?>
                                <tr id="row_id_<?= $item['id_classification'] ?>">
                                    <td><span class="text-danger font-bold"
                                              id="row_color_id_<?= $item['id_classification'] ?>"><?=$item['color']?></span></td>
                                    <td id="row_label_id_<?= $item['id_classification'] ?>"><?= $item['label']; ?></td>
                                    <td>
                                        <input id="record_status_<?= $item['id_classification'] ?>" type="hidden"
                                               value="<?= $item['record_status'] ?>">
                                        <span style="cursor: pointer" class="classification_edit_btn"
                                              index="<?= $item['id_classification'] ?>"
                                              id="edit_classifications_<?= $item['id_classification'] ?>"
                                              style="margin-right: 10px;"><i class="fa fa-pencil"></i></span>
                                        <span style="cursor: pointer" class="classification_del_btn"
                                              index="<?= $item['id_classification'] ?>"
                                              id="delete_classifications_<?= $item['id_classification'] ?>"><i
                                                class="fa fa-trash"></i></span>
                                    </td>
                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default myModal4_close" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>