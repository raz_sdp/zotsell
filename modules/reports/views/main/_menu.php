<div class="panel-body">
    <div class="dropdown">
        <a class="dropdown-toggle btn btn-success btn-block <?php if(!$this->params['newEntities']) echo 'disabled'; ?>" href="#" data-toggle="dropdown">
            NEW
        </a>

        <?php
        echo yii\widgets\Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'dropdown-menu filedropdown m-l',
            ],
            'items' => ($this->params['newEntities']) ? $this->params['newEntities'] : [],
        ]);
        ?>
    </div>

    <?php echo yii\bootstrap\Nav::widget([
        'options' => ['class' => 'h-list m-t'],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<i class="fa fa-list"></i> List',
                'url' => ['main/list'],
            ],
            [
                'label' => '<i class="fa fa-folder"></i> Folders',
                'url' => ['main/folders'],
            ],
            [
                'label' => '<i class="fa fa fa-th"></i> Flow',
                'url' => ['main/flow'],
            ]
        ],
    ]);
    ?>
</div>
