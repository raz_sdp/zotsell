<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchZseV1Records */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zse V1 Records';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zse-v1-records-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Zse V1 Records', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_record',
            'id_locality',
            'id_classification',
            'code',
            'code_destination',
            // 'business_name',
            // 'legal_status',
            // 'type',
            // 'address',
            // 'email:email',
            // 'phone',
            // 'fax',
            // 'mobile',
            // 'inserted_by',
            // 'assigned_to',
            // 'origin',
            // 'is_contact',
            // 'is_customer',
            // 'is_supplier',
            // 'is_prospect',
            // 'is_destination',
            // 'is_reseller',
            // 'is_intrested',
            // 'authorize_send',
            // 'coordinates',
            // 'external_code',
            // 'icon',
            // 'country',
            // 'province',
            // 'city',
            // 'postal_code',
            // 'vat_code',
            // 'fiscal_code',
            // 'pricelist',
            // 'options:ntext',
            // 'person_in_charge',
            // 'role',
            // 'direct_mail',
            // 'direct_phone',
            // 'date_modification',
            // 'record_status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
