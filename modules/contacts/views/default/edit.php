<?php
    $this->title = 'Edit Contact ';
    
      $this->params['breadcrumbs']   [] = $this->title; 
?>

  <h1>New/Edit Contact</h1>

  <p class="lead">Contacts creation/update</p>

  
<button  class="btn btn-success btn-block" type="button"  ><i class="fa fa-pencil"></i> Update Contact </button>

<p >

<table class="table table-condensed">


<tr class="even"><th>Icon<br><img class="media-object img-thumbnail " data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;"></th> 
<td>
<button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> 	
</td> 
</tr>



<tr class="even"><th>Company name</th><td><input  type="text" class="form-control" placeholder="Twitter, Inc."></td></tr>
<tr class="odd"><th>Address</th><td><input  type="text" class="form-control" placeholder="1355 Market Street, Suite 900."></td></tr>
<tr class="even"><th>City</th><td><input  type="text" class="form-control" placeholder=" San Francisco "></td></tr>
<tr class="even"><th>State</th><td><input  type="text" class="form-control" placeholder="  CA  ."></td></tr>
<tr class="even"><th>zip</th><td><input  type="text" class="form-control" placeholder=" 94103."></td></tr>
<tr class="odd"><th>Phone</th><td><input  type="text" class="form-control" placeholder="(123) 456-7890.">



    
</tr> 

</table>



<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Info</a></li>
    <li role="presentation"><a href="#connections" aria-controls="profile" role="tab" data-toggle="tab">Connections</a></li>
    <li role="presentation"><a href="#documents" aria-controls="messages" role="tab" data-toggle="tab">Docs</a></li>
    <li role="presentation"><a href="#sales" aria-controls="settings" role="tab" data-toggle="tab">Sales</a></li>
     <li role="presentation"><a href="#sent" aria-controls="settings" role="tab" data-toggle="tab">Sent</a></li>
      <li role="presentation"><a href="#map" aria-controls="settings" role="tab" data-toggle="tab">Map</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="info">
    
   
   
   <table class="detail-view table table-striped table-condensed" id="yw6"><tbody>

<tr class="even"><th>Provincia</th><td><input  type="text" class="form-control" placeholder="WA"></td></tr>
<tr class="odd"><th>Phone</th><td><input  type="text" class="form-control" placeholder="0445-575870"></td></tr>
<tr class="even"><th>State</th><td><input  type="text" class="form-control" placeholder="WA"></td></tr>
<tr class="odd"><th>Codice Postale</th><td><input  type="text" class="form-control" placeholder="10024"></td></tr>
<tr class="even"><th>Partita IVA</th><td><input  type="text" class="form-control" placeholder="01234556755"></td></tr>
<tr class="odd"><th>Codice Fiscale</th><td><input  type="text" class="form-control" placeholder="01234556755"></td></tr>
<tr class="even"><th>Classificazione</th><td><input  type="text" class="form-control" placeholder="Web"></td></tr>
<tr class="odd"><th>Codice Destinazione</th><td><input  type="text" class="form-control" placeholder=""></td></tr>
<tr class="even"><th>Stato Legale</th><td><input  type="text" class="form-control" placeholder=""></td></tr>
<tr class="odd"><th>Tipo</th><td><input  type="text" class="form-control" placeholder=""></td></tr>
<tr class="even"><th>Partitario</th><td><input  type="text" class="form-control" placeholder=""></td> </tr>
<tr class="odd"><th>Stato Registrazione</th><td><input  type="text" class="form-control" placeholder=""></td></tr>
<tr class="even"><th>Statistiche vendita</th><td> <input  type="text" class="form-control" placeholder="">	
<tr class="odd"><th> </th><td><button  class="btn btn-success btn-block" type="button"  ><i class="fa fa-plus"></i> Add new Contact info field </button></td></tr>
</td></tr>
</tbody></table>
   
    
    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="connections">
    
    
    
    
    
    
    
    
    <table class="table table-hover">




<thead> <tr> <th class="col-md-1"></th> <th> </th> <th class="col-md-1"> </th>  </tr> </thead>

<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" src="https://media.licdn.com/mpr/mpr/shrink_100_100/p/2/005/018/3d3/11bf5c3.jpg" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Jack Dorsey</strong> CEO<br>
  1355 Market Street, Suite 900<br>
  San Francisco, CA 94103<br>
</address>
 </td> 
 <td>   
    	
 			 <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> 	
    
</tr> 
  
  
  <tr> 

<td>  
<img class="media-object img-thumbnail pull-left" src="https://media.licdn.com/mpr/mpr/shrink_100_100/p/2/000/1c7/276/27dead6.jpg" style="width: 64px; height: 64px;"></td> 

<td>
<address>
  <strong>Adam Bain</strong> COO<br>
  1355 Market Street, Suite 900<br>
  San Francisco, CA 94103<br>
</address>
 </td> 
 <td>   
    	
 	<button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> 	
    
</tr> 

<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" src="https://media.licdn.com/mpr/mpr/shrink_100_100/p/1/000/005/28a/1771ae7.jpg"  style="width: 64px; height: 64px;"></td> 

<td>
<address>
  <strong>Marissa Mayer</strong> General Counsel<br>
  1355 Market Street, Suite 900<br>
  San Francisco, CA 94103<br>
</address>
 </td> 
 <td>   
    	
 			   <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> 	
    
</tr> 
    
   </table>  
    
    
    
  <button  class="btn btn-success btn-block" type="button"  ><i class="fa fa-plus"></i> Add new Contact connection </button>  
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="documents">
    
    
    
    <table class="table table-striped"><thead>
<tr><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=name" data-sort="name">Name</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=type" data-sort="type">Type</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=sharable" data-sort="sharable">Sharable</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=size" data-sort="size">Size</a></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>reportVendite</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>575 kb</td><td><button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="1"><td>reportmargini</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>2566854 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="2"><td>fatture</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>49400 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
</tbody></table>
    
    
    <button  class="btn btn-success btn-block" type="button"  ><i class="fa fa-plus"></i> Add new document </button>   
    
    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="sales">
    
    
        <table class="table table-striped"><thead>
<tr><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=name" data-sort="name">Order id</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=type" data-sort="type">Order date</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=sharable" data-sort="sharable">Confirmed</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=size" data-sort="size">Value</a></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>ipad4864684</td><td>5/12/2015</td><td><i class="fa fa-check-square-o"></i></td><td>575 &#8364;</td><td><button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="1"><td>ipad4864678</td><td>4/11/2015</td><td><i class="fa fa-check-square-o"></i></td><td>256 &#8364;</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="2"><td>ipad4864344</td><td>7/10/2015</td><td><i class="fa fa-check-square-o"></i></td><td>494 &#8364;</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
</tbody></table>

    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="sent">
    
    
    
    
            <table class="table table-striped"><thead>
<tr><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=name" data-sort="name">Filename</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=type" data-sort="type">Sent date</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=sharable" data-sort="sharable">Opened</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=size" data-sort="size">Size</a></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>Esempio HTML5 Euronda</td><td>5/12/2015</td><td><i class="fa fa-check-square-o"></i></td><td>575 kb</td><td><button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="1"><td>pdf_sfogliabile.zip</td><td>4/11/2015</td><td><i class="fa fa-check-square-o"></i></td><td>2566854 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="2"><td>MuseExport.zip</td><td>7/10/2015</td><td><i class="fa fa-check-square-o"></i></td><td>49400 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
</tbody></table>

    
    
    
    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="map">
    
    
       <table class="detail-view table table-striped table-condensed" id="yw6"><tbody>


<tr class="even"><th>Coordinates</th><td> <input  type="text" class="form-control" placeholder="14.456465465465;34.456464666654">	
<tr class="odd"><th> </th><td><button  class="btn btn-success btn-block" type="button"  ><i class="fa fa-plus"></i> Updtate Coordinates </button></td></tr>
</td></tr>
</tbody></table>
    
    
       <img class=" " src=" http://www.advancedcustomfields.com/wp-content/uploads/2013/11/acf-google-maps-field-2-700x384.png" style="width: 100%; height: 100%;">
       
       
       
    </div>
  </div>




</div>
  
  
  
  
  </p>
  
