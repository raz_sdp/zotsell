<?php

use yii\bootstrap\ActiveForm;

$formVar = 'form' . $key;
?>

<?php $$formVar = ActiveForm::begin([
    'id' => 'create-folderTr-' . $key,
    'action' => ['update-translation', 'id' => $folder->id_folder, 'lang' => $key],
    'options' => ['class' => 'create-update-media', 'role' => 'form']
    ]); ?>
<?= $$formVar->field($model, 'lang')->hiddenInput(['value' => $key])->label(false); ?>
<h3>Add meta</h3>

<div class="form-group">
<?= $$formVar->field($model, 'label')->textInput(['placeholder' => 'Enter your name'])->label('File name') ?>
</div>

<div class="form-group">
<?= $$formVar->field($model, 'description')->textInput(['placeholder' => 'Enter Short Description'])->label('Short Description') ?>
</div>

<?php ActiveForm::end(); ?>