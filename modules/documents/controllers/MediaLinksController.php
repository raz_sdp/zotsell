<?php

namespace app\modules\documents\controllers;

use Yii;
use app\modules\documents\models\MediaLinks;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MediaLinksController implements the CRUD actions for MediaLinks model.
 */
class MediaLinksController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MediaLinks models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => MediaLinks::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MediaLinks model.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return mixed
     */
    public function actionView($id_media, $id_container, $context)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_media, $id_container, $context),
        ]);
    }

    /**
     * Creates a new MediaLinks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MediaLinks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_media' => $model->id_media, 'id_container' => $model->id_container, 'context' => $model->context]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MediaLinks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return mixed
     */
    public function actionUpdate($id_media, $id_container, $context)
    {
        $model = $this->findModel($id_media, $id_container, $context);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_media' => $model->id_media, 'id_container' => $model->id_container, 'context' => $model->context]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MediaLinks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return mixed
     */
    public function actionDelete($id_media, $id_container, $context)
    {
        $this->findModel($id_media, $id_container, $context)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MediaLinks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return MediaLinks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_media, $id_container, $context)
    {
        if (($model = MediaLinks::findOne(['id_media' => $id_media, 'id_container' => $id_container, 'context' => $context])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
