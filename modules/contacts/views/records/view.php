<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\Records */

$this->title = "Contact detail";
$this->subtitle = "Use contact detail view to contact data .";
$this->breadCrums = [['label' => 'Contacts', 'link' => Yii::$app->urlManager->createUrl('contacts/records/list')], 'Contact detail view', $model->business_name];
//$type = Yii::$app->request->getQueryParam('type', 1);
//$type = \app\modules\contacts\components\Enum::getContactType($type_id);
?>
<div class="col-md-3">
    <?php echo Yii::$app->controller->renderPartial('../elements/_contact_tab', ['secondary_tab_disable' => '']); ?>
</div>
<div class="col-md-9">
    <div class="hpanel">
        <div class="panel-body">
            <div class="text-center hidden">
                We couldn't find any Document. Add one
            </div>
            <div class="tab-content">
                <div id="note0" class="tab-pane active">
                    <div class="pull-right text-muted m-l-lg">
                        <?= Html::a('', ['update', 'id' => $model->id_record], ['class' => 'fa fa-pencil']) ?>
                        <?= Html::a('', ['delete', 'id' => $model->id_record], [
                            'class' => 'fa fa-trash',
                            'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],]) ?>
                    </div>
                    <h3>Basic Info</h3>
                    <hr/>
                    <div class="note-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <?php echo $type_icon ?>
                                <h3><a href=""> <?= $model->business_name ?> </a></h3>

                                <div class="text-muted font-bold m-b-xs"> <?= $model->address ?>, <?= $model->city ?>
                                    , <?= $model->province ?>
                                </div>
                            </div>

                            <div class="col-lg-8">
                                <?=
                                DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        [
                                            'label' => 'Field',
                                            'value' => 'Content'
                                        ],
                                        /*'id_record',
                                        'id_locality',
                                        'id_classification',*/
                                        [
                                            'label' => 'Contact ID',
                                            'value' => $model->code
                                        ],
                                        [
                                            'label' => 'Destination ID',
                                            'value' => $model->code_destination
                                        ],
                                        [
                                            'label' => 'Classification',
                                            'value' => $classification
                                        ],
                                        [
                                            'label' => 'Status',
                                            'format' => 'raw',
                                            'value' => Html::label($status)
                                        ],
                                        [
                                            'label' => 'Owner',
                                            'format' => 'raw',
                                            'value' => Html::a($contact_owner_icon,'javascript:void(0)',['onclick'=>'showOwnerList()'])
                                        ],
                                        /*[
                                            'label' => 'Owner ID',
                                            'value' => '-'
                                        ],*/
                                        [
                                            'label' => 'Phone',
                                            'value' => $model->phone
                                        ],
                                        [
                                            'label' => 'Email',
                                            'value' => $model->email
                                        ]
                                        /*'address',
                                        'email:email',
                                        'phone',
                                        'fax',
                                        'mobile',
                                        'inserted_by',
                                        'assigned_to',
                                        'origin',
                                        'is_contact',
                                        'is_customer',
                                        'is_supplier',
                                        'is_prospect',
                                        'is_destination',
                                        'is_reseller',
                                        'is_intrested',
                                        'authorize_send',
                                        'coordinates',
                                        'external_code',
                                        'icon',
                                        'country',
                                        'province',
                                        'city',
                                        'postal_code',
                                        'vat_code',
                                        'fiscal_code',
                                        'pricelist',
                                        'options:ntext',
                                        'person_in_charge',
                                        'role',
                                        'direct_mail',
                                        'direct_phone',
                                        'date_modification',
                                        'record_status',*/
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p>

                                <h3>Additional data</h3></p>
                                <?=
                                DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        [
                                            'label' => 'Field',
                                            'value' => 'Content'
                                        ],
                                        'legal_status',
                                        'fax',
                                        'mobile',
                                        'person_in_charge',
                                        'direct_mail',
                                        'direct_phone',
                                        'external_code',
                                        'province',
                                        'vat_code',
                                        'fiscal_code',
                                        'country',
                                        'pricelist'
                                    ],
                                ]) ?>
                                <p>

                                <h3>Extended data</h3></p>
                                <table class="table table-striped table-bordered detail-view">
                                    <tr>
                                        <th>Field</th>
                                        <td>Content</td>
                                    </tr>
                                    <?php
                                    $options_all = isset($model->options)?json_decode($model->options,1):array();
                                    $options = isset($options_all['note_aggiuntive']) ? $options_all['note_aggiuntive'] : array();

                                    $gruppi = isset($options_all['gruppi'][0]) ? $options_all['gruppi'][0] : "";
                                    $pagamento = isset($options_all['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO']) ? $options_all['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO'] : "";
                                    $resa = isset($options_all['dati_convenzionati']['CONDIZIONE_DI_RESA']) ? $options_all['dati_convenzionati']['CONDIZIONE_DI_RESA'] : "";
                                    $pagamento = \app\modules\contacts\components\Enum::getCatalogLabel($pagamento);
                                    $resa = \app\modules\contacts\components\Enum::getCatalogLabel($resa);

                                    $iva = "";

                                    //print_r($options); die();

                                    if(!empty($options)) {
                                        foreach($options as $item) {
                                            if($item['field_id']=='CONDIZIONE_DI_PAGAMENTO' || $item['field_id']=='CONDIZIONE_DI_RESA') {
                                                continue;
                                            }
                                            if($item['field_id']=='CONDIZIONE_IVA') {
                                                $iva = isset($item['valore']) ? $item['valore'] : "";
                                            }
                                        ?>
                                            <tr>
                                                <th><?= $item['label'];?></th>
                                                <td><?= $item['valore'];?></td>
                                            </tr>
                                        <?php }
                                    }
                                    if(!empty($pagamento)) { ?>
                                        <tr>
                                            <th>Payment Condition</th>
                                            <td><?= $pagamento;?></td>
                                        </tr>
                                    <?php }
                                    if(!empty($resa)) { ?>
                                        <tr>
                                            <th>Incoterms</th>
                                            <td><?= $resa;?></td>
                                        </tr>
                                    <?php }
                                    if(!empty($iva)) { ?>
                                        <tr>
                                            <th>Vat Condition</th>
                                            <td><?= $iva;?></td>
                                        </tr>
                                    <?php }
                                    if(!empty($gruppi)) { ?>
                                        <tr>
                                            <th>Contact Group</th>
                                            <td><?= $gruppi;?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="note1" class="tab-pane">
                    <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_connections'); ?>
                </div>

                <div id="note2" class="tab-pane">
                    <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_documents'); ?>
                </div>

                <div id="note3" class="tab-pane">
                    <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_sales'); ?>
                </div>

                <div id="note4" class="tab-pane">
                    <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_activities'); ?>
                </div>

                <div id="note5" class="tab-pane">
                    <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_maps', ['coordinates' => $model->coordinates, 'type' => 'view']); ?>
                </div>
            </div>
            <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_contact-owners-view-modal',['contact_owner_list_of_this_record' => $contact_owner_list_of_this_record]); ?>
            <?php echo Yii::$app->controller->renderPartial('../elements/record-views/_extended-data-view-modal'); ?>
        </div>
    </div>
</div>


