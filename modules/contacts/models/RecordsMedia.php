<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media_links".
 *
 * @property integer $id_media
 * @property integer $id_container
 * @property integer $context
 * @property integer $weight
 * @property string $date_modification
 * @property integer $record_status
 */
class RecordsMedia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_media', 'id_container', 'context'], 'required'],
            [['id_media', 'id_container', 'context', 'weight', 'record_status'], 'integer'],
            [['date_modification'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'id_container' => 'Id Container',
            'context' => 'Context',
            'weight' => 'Weight',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @return array
     * get all documents of a record
     */
    public function getContactDocuments($id, $filter_where = [], $limit= null, $offset = null, $sort = "") {
        $command = (new \yii\db\Query())
                    #->select(['`zse_v1_media`.*', '`zse_v1_media_links`.`id_media`'])
                    #->select(['`zse_v1_media`.`icon`','`zse_v1_media_links`.`id_media`','`zse_v1_media`.`type`','`zse_v1_media`.`name`','`zse_v1_media`.`description`','`zse_v1_media`.`cansend`'])
                    ->select(['`zse_v1_media`.`type`','`zse_v1_media_links`.`id_media`','`zse_v1_media`.`type`','`zse_v1_media`.`name`','`zse_v1_media`.`description`','`zse_v1_media`.`cansend`'])
                    ->from('`zse_v1_media_links`')
                    ->innerJoin('`zse_v1_media`', '`zse_v1_media_links`.`id_media` = `zse_v1_media`.`id_media`')
                    ->Where(['zse_v1_media_links.id_container' => $id, 'zse_v1_media_links.context'=>3])
                    ->andFilterWhere($filter_where)
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy($sort)
                    ->createCommand();


        return $command->queryAll();
    }

    /**
     * get contact documents count
     * @param $id
     * @param array $filter_where
     * @return int|string
     */
    public function countContactDocuments($id, $filter_where = []) {
        $command = (new \yii\db\Query())
            ->from('`zse_v1_media_links`')
            ->innerJoin('`zse_v1_media`', '`zse_v1_media_links`.`id_media` = `zse_v1_media`.`id_media`')
            ->Where(['zse_v1_media_links.id_container' => $id, 'zse_v1_media_links.context'=>3])
            ->andFilterWhere($filter_where)
            ->count();

        return $command;
    }
    /**
     * @return array
     * search all documents in media table by name field
     */
    public function searchDocumentByName($name) {
        $command = (new \yii\db\Query())
                    ->select(['`zse_v1_media`.*'])
                    ->from('`zse_v1_media`')
                    ->Where(['LIKE', 'zse_v1_media.name', $name])
                    ->createCommand();

        return $command->queryAll(); 
    }

}
