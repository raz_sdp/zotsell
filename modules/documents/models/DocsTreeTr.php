<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "zse_v1_docs_tree_tr".
 *
 * @property integer $id_folder
 * @property string $lang
 * @property string $label
 * @property string $description
 *
 * @property DocsTree $idFolder
 */
class DocsTreeTr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_docs_tree_tr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_folder', 'lang'], 'required'],
            [['id_folder'], 'integer'],
            [['description'], 'string'],
            [['lang', 'label'], 'string', 'max' => 255],
            [['lang', 'id_folder'], 'unique', 'targetAttribute' => ['lang', 'id_folder'], 'message' => 'The combination of Id Folder and Lang has already been taken.'],
            [['label', 'description', 'lang'], 'required', 'on' => ['create']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['label', 'description', 'lang'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_folder' => 'Id Folder',
            'lang' => 'Lang',
            'label' => 'Label',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocsTree()
    {
        return $this->hasOne(DocsTree::className(), ['id_folder' => 'id_folder']);
    }

}
