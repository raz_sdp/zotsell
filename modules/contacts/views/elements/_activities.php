<?php
/**
 * Created by PhpStorm.
 * User: Raaz
 * Date: 2/27/16
 * Time: 12:24 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Disabled
</div>
<h3>Activities </h3>
<hr/>
<div class="note-content">


    <div class="row">
        <?php if(!isset($type)): ?>
        <div class="col-lg-4">

            <p>
            <h3> Choose Activities</h3>
            </p>

            <form role="form" id="form_activity">
                <div class="form-group">
                    <label for="name">Search</label>
                    <input type="text" id="keyword" name="keyword" placeholder="Enter Keyword" class="form-control" required>
                </div>
                <div>
                    <button class="btn btn-block btn-success m-t-n-xs" type="submit"><strong>Search</strong>
                    </button>
                </div>
            </form>

            <div class="table-responsive slim_scroll">
                <table class="table table-striped js-activity-search-result-table">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2">No Result Found</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <?php endif; ?>
        <div class="col-lg-8">
        <?php if(!isset($type)): ?>
                <p>
                <h3> Added Activities</h3>
                </p>
        <?php endif; ?>
            <script type="text/javascript">
                var added_activities = [];
            </script>

            <table id="activities-table-update" class="table table-striped table-bordered table-hover js-activity-table custom_table_brake">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Subject</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($activities)) { ?>
                    <!--<tr class="activity_no_res_msg">
                        <td colspan="6">No Activities.</td>
                    </tr>-->
                <?php } else { ?>
                    <?php foreach($activities as $activity) : ?>
                        <tr id="activity_<?=$activity['id_activity']?>" rel="<?= $activity['id_activity'];?>">
                            <td><?= $activity['date_activity'];?></td>
                            <td><?= $activity['type'];?></td>
                            <td><?= $activity['subject'];?></td>
                            <td><?= $activity['description'];?></td>
                            <td><?= $activity['status'];?></td>
                            <td><a id="<?=$activity['id_activity']?>"><i class="fa fa-minus" onclick="delete_from_activity_table(this)"></i></a></td>
                        </tr>
                        
                        <script type="text/javascript">
                            added_activities.push("<?php echo $activity['id_activity'];?>");
                        </script>
                    <?php endforeach; ?>
                <?php } ?>                
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if(!isset($type)): ?>
<div class="btn-group save_btn_group" id="activity_save_btn_group">
    <button class="btn btn-sm btn-default" id="save_activities"><i class="fa fa-thumbs-o-up"></i> Save</button>
</div>
<?php endif; ?>
<?php
$active = 0;
if(isset($activities) && !empty($activities)){
    $active = 1;
}
?>
<input value="<?=$active?>" id="active_activities" type="hidden">