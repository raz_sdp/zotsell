<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/28/2016
 * Time: 9:01 PM
 */
namespace app\modules\contacts\components;
use app\modules\contacts\models\RecordsClassifications;
use app\modules\contacts\models\CatalogLabels;

class Enum{

    public static function getContactType($typeId)
    {
        $contactType = array(
            '1' => 'Company',
            '2' => 'Destination',
            '3' => 'Prospect',
            '4' => 'Person'
        );

        return isset($contactType[$typeId])?$contactType[$typeId]:'Company';
    }

    public static function getContactTypesSelect()
    {
        $contactType = array(
            'Company' => 'Company',
            'Destination' => 'Destination',
            'Prospect' => 'Prospect',
            'Person' => 'Person'
        );

        return $contactType;
    }


    public static function getPriceList()
    {
        $pricelist =  array(
            ''=>'Choose',
            'Listino test sconto'=>'Listino test sconto',
            'Listino netto caso 1'=>'Listino netto caso 1',
        );

        return $pricelist;
    }

    public static function getCountryList()
    {
        $countryList = array(
            ''=>'Choose',
            'Germany'=>'Germany',
            'Canada'=>'Canada',
            'Spain'=>'Spain',
            'Portugal'=>'Portugal',
            'Poland'=>'Poland',
            'Brazil'=>'Brazil',
            'Italy'=>'Italy',
            'Usa'=>'Usa',
            'Angola'=>'Angola',
            'India'=>'India',
            'China'=>'China',
            'UK'=>'UK',
            'France'=>'France',
            'Australia'=>'Australia',
            'Mexico'=>'Mexico',
            'Argentina'=>'Argentina',
        );

        return $countryList;
    }

    public static function getClassification($id)
    {
        $classification = RecordsClassifications::findOne($id);
        return isset($classification['label'])?$classification['label']:'';
    }
    
    public static function getContactAddress($address,$city,$province)
    {
        if($address != '' && $city != '' && $province != ''){
            $address_ = $address.', '.$city.', '.$province;
        }elseif($address != '' && $city != '' && $province == ''){
            $address_ = $address.', '.$city;
        }elseif($address != '' && $city == '' && $province == ''){
            $address_ = $address;
        }elseif($address != '' && $city == '' && $province != ''){
            $address_ = $address.', '.$province;
        }elseif($address == '' && $city != '' && $province != ''){
            $address_ = $city.', '.$province;
        }elseif($address == '' && $city != '' && $province == ''){
            $address_ = $city;
        }elseif($address == '' && $city == '' && $province != ''){
            $address_ = $province;
        }else{
            $address_ = '';
        }

        return $address_;
    }

    public static function getCatalogLabel($code)
    {
        $data = CatalogLabels::find()->where(['code_external' => $code])->one();
        return  isset($data->label) ? $data->label : "";
    }

    /*public static function getClassification($id)
    {
        $classification = RecordsClassifications::findOne($id);
        return isset($classification['label'])?$classification['label']:'';
    }*/
}