<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_catalog_labels".
 *
 * @property integer $id_label
 * @property string $code_external
 * @property string $code_internal
 * @property string $label
 * @property string $description
 * @property string $icon
 * @property string $context
 * @property string $type_control
 * @property string $date_modification
 * @property integer $record_status
 */
class CatalogLabels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_catalog_labels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_modification'], 'safe'],
            [['record_status'], 'integer'],
            [['code_external', 'code_internal', 'label', 'context'], 'string', 'max' => 64],
            [['description', 'icon', 'type_control'], 'string', 'max' => 255],
            [['code_external', 'context'], 'unique', 'targetAttribute' => ['code_external', 'context'], 'message' => 'The combination of Code External and Context has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_label' => 'Id Label',
            'code_external' => 'Code External',
            'code_internal' => 'Code Internal',
            'label' => 'Label',
            'description' => 'Description',
            'icon' => 'Icon',
            'context' => 'Context',
            'type_control' => 'Type Control',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }
}
