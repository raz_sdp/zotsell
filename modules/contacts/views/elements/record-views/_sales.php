<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:23 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3>Sales </h3>
<hr/>
<div class="note-content">
    <div class="row">
        <div class="col-lg-12">
            <table id=""
                   class="table table-striped table-bordered table-hover js-view-order-table custom_table_brake">
                <thead>
                <tr>
                    <th>Order Code</th>
                    <!--<th>ID</th>-->
                    <th>Date</th>
                    <th>Type</th>
                    <th>Subject</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
            </table>
        </div>
    </div>
</div>