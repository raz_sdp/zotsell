<?php

namespace app\modules\contacts\models;

use Yii;
use app\components\BaseController;

/**
 * This is the model class for table "zse_v1_records_agents".
 *
 * @property string $record_code
 * @property string $destination_code
 * @property string $agent_code
 * @property string $date_modification
 * @property integer $record_status
 */
class RecordsAgents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_records_agents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['record_code', 'destination_code', 'agent_code'], 'required'],
            [['date_modification'], 'safe'],
            [['record_status'], 'integer'],
            [['record_code'], 'string', 'max' => 32],
            [['destination_code'], 'string', 'max' => 64],
            [['agent_code'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'record_code' => 'Record Code',
            'destination_code' => 'Destination Code',
            'agent_code' => 'Agent Code',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @param $data
     * saves the selected record codes against agent code
     */
    public function saveAgentsRecords($data)
    {        
        $agent_code = $data['agent'];
        if (empty($data['my-select']))
            $records = array();
        else
            $records = $data['my-select'];

        foreach ($records as $item) {
            $exploded_data = explode(',', $item);
            $contact_code = $exploded_data[0];
            $destination_code = ($exploded_data[1]=="#") ? '' : $exploded_data[1];
            Yii::$app->db->createCommand()
                ->insert('zse_v1_records_agents', [
                    'agent_code' => $agent_code,
                    'destination_code' => $destination_code,
                    'record_code' => $contact_code,
                ])->execute();
        }
    }

    public static function getContactOwners(){

        $command = (new \yii\db\Query())
            ->select(['DISTINCT(`zse_v1_accounts_services_licenses`.`usercode`)','`zse_v1_accounts`.`username`','`zse_v1_accounts`.`working_code`'])
            ->from('`zse_v1_accounts_services_licenses`')
            //->innerJoin('`zse_v1_records_agents`',  '`zse_v1_records_agents`.`agent_code` = `zse_v1_accounts_services_licenses`.`usercode`')
            ->innerJoin('`zse_v1_accounts`', '`zse_v1_accounts_services_licenses`.`id_account` = `zse_v1_accounts`.`id_account`')
            ->createCommand();

        $result=$command->queryAll();
        #BaseController::_setTrace($result);
        return $result;
    }

    public static function getContactOwnersOfCurrentRecord($id){
        $command = (new \yii\db\Query())
            ->select(['`zse_v1_accounts`.*','`zse_v1_accounts_services_licenses`.usercode'])
            ->from('`zse_v1_records_agents`')
            ->innerJoin('`zse_v1_records`', '`zse_v1_records`.`code` = `zse_v1_records_agents`.`record_code`')
            ->innerJoin('`zse_v1_accounts`', '`zse_v1_accounts`.`working_code` LIKE CONCAT(`zse_v1_records_agents`.`agent_code`, \'%\')')
            ->innerJoin('`zse_v1_accounts_services_licenses`', '`zse_v1_accounts_services_licenses`.`id_account` = `zse_v1_accounts`.`id_account`')
            ->where(['`zse_v1_records`.`id_record`' => $id])
            ->createCommand();

        $result=$command->queryAll();
        #BaseController::_setTrace($result);
        return $result;
    }
}
