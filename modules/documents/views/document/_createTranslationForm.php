<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$formVar = 'form' . $key;
?>

<?php $$formVar = ActiveForm::begin([
    'id' => 'create-mediaTr-' . $key,
    'action' => ['update-translation', 'id' => $media->id_media, 'lang' => $key],
    'options' => ['class' => 'create-update-media', 'role' => 'form']
]); ?>
<?= $$formVar->field($model, 'lang')->hiddenInput(['value' => $key])->label(false); ?>
    <h3>Add meta</h3>

    <div class="form-group">
        <?= $$formVar->field($model, 'name')->textInput(['placeholder' => 'Enter your name'])->label('File name') ?>
    </div>

    <div class="form-group">
        <?= $$formVar->field($model, 'description')->textInput(['placeholder' => 'Enter Short Description'])->label('Short Description') ?>
    </div>

<?= $$formVar->field($model, 'type')->hiddenInput(['value' => $media->type])->label(false); ?>


<?php if ($media->type == 'gallery'): ?>
    <?= $$formVar->field($model, 'multimedia')->textArea(['readonly' => true, 'class' => 'form-control choosen-list', 'style' => 'min-height:120px;'])->label('Choosen Files:') ?>
<?php else: ?>
    <?= $$formVar->field($model, 'file')->textInput(['readonly' => true, 'class' => 'form-control choosen-list'])->label('Choosen File:'); ?>
<?php endif; ?>

<?php ActiveForm::end(); ?>