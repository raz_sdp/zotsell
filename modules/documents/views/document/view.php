<?php
/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['headerTitle'] = "Documents File \"{$modelMedia->name}\" Detail View";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->title = 'Documents';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$fileParentFolder = $modelMedia->folder;
$parentsFolder = $fileParentFolder->parents()->all();

foreach ($parentsFolder as $parentFolder) {
    $this->params['breadcrumbs'][] = [
        'label' => $parentFolder->label,
        'url' => ['folder/view', 'id' => $parentFolder->id_folder]
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => $fileParentFolder->label,
    'url' => ['folder/view', 'id' => $fileParentFolder->id_folder]
];

$this->params['breadcrumbs'][] = [
    'label' => $modelMedia->name,
    'template' => "<li><b>{link}</b></li>\n"
];
?>

    <div class="col-md-3">
        <div class="hpanel panel-group">
            <div class="panel-body">
                <div class="text-center text-muted font-bold">Select translation</div>

            </div>


            <div id="notes" class="collapse">
                <div class="panel-body note-link active_contact_side_bar">
                    <a href="#default" data-toggle="tab">
                        <small class="pull-right text-muted">WORLD</small>
                        <h5>Default</h5>

                        <div class="small">
                            <strong><?= $modelMedia->name ?></strong>
                        </div>
                        <div class="small">
                            <?= $modelMedia->description ?>
                        </div>
                    </a>
                </div>

                <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                    <div
                        class="panel-body note-link <?php if (!isset($languageRelation['model']->id_media)) echo 'disabledTab'; ?>">
                        <a href="#<?= $key ?>" data-toggle="tab">
                            <small class="pull-right text-muted"><?= $languageRelation['country'] ?></small>
                            <h5><?= $languageRelation['language'] ?></h5>

                            <div class="small">
                                <strong><?php
                                    /* if(isset($languageRelation['model'])) */
                                    echo $languageRelation['model']->name;
                                    ?></strong>
                            </div>
                            <div class="small">
                                <?php
                                /* if(isset($languageRelation['model'])) */
                                echo $languageRelation['model']->description;
                                ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="hpanel">
            <div class="panel-heading hbuilt">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                Media <?= $modelMedia->name ?>
            </div>

            <div class="panel-body">
                <div class="tab-content">

                    <div id="default" class="tab-pane active">
                        <div class="pull-right text-muted m-l-lg">
                            WORLD
                        </div>
                        <h3>Default</h3>
                        <hr/>
                        <div class="note-content">
                            <div class="row">
                                <?php
                                echo DetailView::widget([
                                    'model' => $modelMedia,
                                    'attributes' => [
                                        [
                                            'label' => 'Field',
                                            'format' => 'html',
                                            'value' => '<strong>Content</strong>',
                                        ],
                                        'name',
                                        'description',
                                        'type',
                                        [
                                            'label' => 'Sharable',
                                            'format' => 'html',
                                            'value' => ($modelMedia->cansend == 1) ? '<i class="fa fa-check-square-o"></i>' : '<i class="fa fa-square-o"></i>',
                                        ],
                                        [
                                            'label' => 'Size',
                                            'value' => ($modelMedia->fileSize) ? $modelMedia->fileSize . ' kb' : '-',
                                        ],
                                        'icon',
                                        [
                                            'label' => 'Filename',
                                            'attribute' => 'file'
                                        ],
                                    ],
                                ]);
                                ?>

                                <hr>
                                <p class="lead">This Media is also Located in</p>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProviderFolder,
                                    'tableOptions' => [
                                        'class' => 'table table-striped'
                                    ],
                                    'columns' => [
                                        [
                                            'attribute' => 'name',
                                            'label' => 'name',
                                            'format' => 'html',
                                            'value' => function ($model) {
                                                return Html::a($model->folder->label, ['folder/view', 'id' => $model->folder->id_folder], ['class' => 'list-link']);
                                            }
                                        ],
                                        [
                                            'attribute' => 'type',
                                            'value' => function ($model) {
                                                return 'folder';
                                            }
                                        ],
                                        [
                                            'attribute' => 'path',
                                            'value' => function ($model) use ($modelMedia) {
                                                $path = '/';
                                                if ($model->folder->father->id_folder != $model->folder->id_folder) {
                                                    $path .= $model->folder->father->label . '/';
                                                }
                                                $path .= $model->folder->label . '/' . $modelMedia->file;
                                                return $path;
                                            }
                                        ]
                                    ],
                                    'layout' => '{items}',
                                ])
                                ?>


                            </div>
                        </div>
                    </div>

                    <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                        <div id="<?= $key ?>" class="tab-pane">
                            <div class="pull-right text-muted m-l-lg">
                                <?= $languageRelation['country'] ?>
                            </div>
                            <h3><?= $languageRelation['language'] ?></h3>
                            <hr/>
                            <div class="note-content">
                                <div class="row">
                                    <?php
                                    echo DetailView::widget([
                                        'model' => $languageRelation['model'],
                                        'attributes' => [
                                            [
                                                'label' => 'Field',
                                                'format' => 'html',
                                                'value' => '<strong>Content</strong>',
                                            ],
                                            'name',
                                            'description',
                                            'type',
                                            [
                                                'label' => 'Size',
                                                'value' => ($languageRelation['model']->fileSize) ? $languageRelation['model']->fileSize . ' kb' : '-',
                                            ],
                                            [
                                                'label' => 'Filename',
                                                'attribute' => 'file'
                                            ],
                                        ],
                                    ]);
                                    ?>

                                    <hr>
                                    <p class="lead">This Media is also Located in</p>
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProviderFolder,
                                        'tableOptions' => [
                                            'class' => 'table table-striped'
                                        ],
                                        'columns' => [
                                            [
                                                'attribute' => 'name',
                                                'label' => 'name',
                                                'format' => 'html',
                                                'value' => function ($model) {
                                                    return Html::a($model->folder->label, ['folder/view', 'id' => $model->folder->id_folder], ['class' => 'list-link']);
                                                }
                                            ],
                                            [
                                                'attribute' => 'type',
                                                'value' => function ($model) {
                                                    return 'folder';
                                                }
                                            ],
                                            [
                                                'attribute' => 'path',
                                                'value' => function ($model) use ($modelMedia) {
                                                    $path = '/';
                                                    if ($model->folder->father->id_folder != $model->folder->id_folder) {
                                                        $path .= $model->folder->father->label . '/';
                                                    }
                                                    $path .= $model->folder->label . '/' . $modelMedia->file;
                                                    return $path;
                                                }
                                            ]
                                        ],
                                        'layout' => '{items}',
                                    ])
                                    ?>

                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>