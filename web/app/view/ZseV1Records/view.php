<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ZseV1Records */

$this->title = $model->id_record;
$this->params['breadcrumbs'][] = ['label' => 'Zse V1 Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zse-v1-records-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_record], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_record], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_record',
            'id_locality',
            'id_classification',
            'code',
            'code_destination',
            'business_name',
            'legal_status',
            'type',
            'address',
            'email:email',
            'phone',
            'fax',
            'mobile',
            'inserted_by',
            'assigned_to',
            'origin',
            'is_contact',
            'is_customer',
            'is_supplier',
            'is_prospect',
            'is_destination',
            'is_reseller',
            'is_intrested',
            'authorize_send',
            'coordinates',
            'external_code',
            'icon',
            'country',
            'province',
            'city',
            'postal_code',
            'vat_code',
            'fiscal_code',
            'pricelist',
            'options:ntext',
            'person_in_charge',
            'role',
            'direct_mail',
            'direct_phone',
            'date_modification',
            'record_status',
        ],
    ]) ?>

</div>
