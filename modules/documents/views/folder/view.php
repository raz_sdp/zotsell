<?php
/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['headerTitle'] = "Documents Folder \"{$model->label}\" Detail View";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->title = 'Documents';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$parents = $model->parents()->all();
foreach ($parents as $parentFolder) {
    $this->params['breadcrumbs'][] = [
        'label' => $parentFolder->label,
        'url' => ['folder/view', 'id' => $parentFolder->id_folder]
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => $model->label,
    'template' => "<li><b>{link}</b></li>\n"
];


// class for sharable column
switch ($model->sharable) {
    case 0:
        $sharableClass = '<i class="fa fa-square-o"></i>';
        break;
    case 1:
        $sharableClass = '<i class="fa fa-check-square-o"></i>';
        break;
    case 2:
        $sharableClass = '<i class="fa fa-minus-square-o"></i>';
        break;
}
?>

    <div class="col-md-3">
        <div class="hpanel panel-group">
            <div class="panel-body">
                <div class="text-center text-muted font-bold">Select translation</div>

            </div>


            <div id="notes" class="collapse">
                <div class="panel-body note-link active_contact_side_bar">
                    <a href="#default" data-toggle="tab">
                        <small class="pull-right text-muted">WORLD</small>
                        <h5>Default</h5>

                        <div class="small">
                            <strong><?= $model->label ?></strong>
                        </div>
                        <div class="small">
                            <?= $model->description ?>
                        </div>
                    </a>
                </div>

                <?php foreach ($folderTranslations as $key => $languageRelation): ?>
                    <div
                        class="panel-body note-link <?php if (!isset($languageRelation['model']->id_folder)) echo 'disabledTab'; ?>">
                        <a href="#<?= $key ?>" data-toggle="tab">
                            <small
                                class="pull-right text-muted"><?php if (isset($languageRelation['country'])) echo $languageRelation['country']; ?></small>
                            <h5><?php if (isset($languageRelation['language'])) echo $languageRelation['language']; ?></h5>

                            <div class="small">
                                <strong><?= $languageRelation['model']->label ?></strong>
                            </div>
                            <div class="small">
                                <?= $languageRelation['model']->description ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="hpanel">
            <div class="panel-heading hbuilt">
                <div class="panel-tools">
                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                    <a class="closebox"><i class="fa fa-times"></i></a>
                </div>
                <?= $model->label ?>
            </div>

            <div class="panel-body">
                <div class="tab-content">
                    <div id="default" class="tab-pane active">
                        <div class="pull-right text-muted m-l-lg">
                            WORLD
                        </div>
                        <h3>Default</h3>
                        <hr/>
                        <div class="note-content">
                            <div class="row">
                                <?php
                                echo DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        [
                                            'label' => 'Field',
                                            'value' => 'Content',
                                        ],
                                        [
                                            'label' => 'Name',
                                            'attribute' => 'label'
                                        ],
                                        'description',
                                        [
                                            'label' => 'Type',
                                            'value' => 'Folder'
                                        ],
                                        [
                                            'label' => 'Sharable',
                                            'format' => 'html',
                                            'value' => $sharableClass,
                                        ],
                                        [
                                            'label' => 'Size',
                                            'format' => 'html',
                                            'value' => ($model->size) ? $model->size . ' kb' : '-',
                                        ],
                                        'icon',
                                        [
                                            'label' => 'Filename',
                                            'value' => '-',
                                        ],
                                    ],
                                ]);
                                ?>


                                <hr>
                                <p class="lead">This Folder contains these Media</p>
                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProviderMedia,
                                    'tableOptions' => [
                                        'class' => 'table table-striped'
                                    ],
                                    'columns' => [
                                        [
                                            'attribute' => 'name',
                                            'format' => 'html',
                                            'value' => function ($model) {
                                                return Html::a($model->name, ['document/view', 'id' => $model->id_media], ['class' => 'list-link']);
                                            }
                                        ],
                                        'type',
                                    ],
                                    'layout' => '{items}',
                                ])
                                ?>

                            </div>
                        </div>
                    </div>


                    <?php foreach ($folderTranslations as $key => $languageRelation): ?>
                        <div id="<?= $key ?>" class="tab-pane">
                            <div class="pull-right text-muted m-l-lg">
                                <?= $languageRelation['country'] ?>
                            </div>
                            <h3><?= $languageRelation['language'] ?></h3>
                            <hr/>
                            <div class="note-content">
                                <div class="row">
                                    <?php
                                    echo DetailView::widget([
                                        'model' => $languageRelation['model'],
                                        'attributes' => [
                                            [
                                                'label' => 'Field',
                                                'value' => 'Content',
                                            ],
                                            [
                                                'label' => 'Name',
                                                'attribute' => 'label'
                                            ],
                                            'description',
                                            [
                                                'label' => 'Type',
                                                'value' => 'Folder'
                                            ],
                                            [
                                                'label' => 'Sharable',
                                                'format' => 'html',
                                                'value' => $sharableClass,
                                            ],
                                            [
                                                'label' => 'Size',
                                                'format' => 'html',
                                                'value' => ($model->size) ? $model->size . ' kb' : '-',
                                            ],
                                            [
                                                'label' => 'Filename',
                                                'value' => '-',
                                            ],
                                        ],
                                    ]);
                                    ?>


                                    <hr>
                                    <p class="lead">This Folder contains these Media</p>
                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $dataProviderMedia,
                                        'tableOptions' => [
                                            'class' => 'table table-striped'
                                        ],
                                        'columns' => [
                                            [
                                                'attribute' => 'name',
                                                'format' => 'html',
                                                'value' => function ($model) {
                                                    return Html::a($model->name, ['document/view', 'id' => $model->id_media], ['class' => 'list-link']);
                                                }
                                            ],
                                            'type',
                                        ],
                                        'layout' => '{items}',
                                    ])
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php
$js = <<<JS
$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>