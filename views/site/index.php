<?php

/* @var $this yii\web\View */

$this->title = 'Zot 2.0';
?>
<div class="site-index">

<div class="wrapper">

    <div class="jumbotron">
        <h1>Zot 2.0 beta</h1>

        <p class="lead">Welcome to the Yii 2.0  Zot web page</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Go to Admin</a></p>
    </div>

    <div class="body-content">




<table class="table table-condensed"> 
     <thead> 
     
       <tr> <th> </th> <th> </th> <th>  </th> <th> </th> 
     
     <th colspan="9"> l-- Roles  in substitution to the old admin sell buy etc  to be associated for each module in licence store  --l</th>
     
     </tr> 
     
     
     
     
     <tr> <th>#</th> <th>Module Name</th> <th>Developer </th> <th>Due Date</th> 
     
     <th>Developer</th>
     <th>Superuser</th>
     <th>User</th>

     
     </tr> 
          
     </thead> 
     <tbody> 
     <tr> <th scope="row">1</th> <td>Documents</td> <td>Maxim C.</td> <td>Jan 16</td> <td><a href="/zot/web/documents/">Mockup</a><br><a href="/zot/web/documents/">Module</a></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok"></span></td></tr>
     <tr> <th scope="row">2</th> <td>Contacts</td> <td>Thornton</td> <td>Feb 16</td>  <td><a href="/zotsell_xor/_zot-mockup/Contacts/">Mockup</a><br><a href="<?Php echo Yii::$app->homeUrl;?>contacts/records">Module</a></td><td><span class="glyphicon glyphicon-ok" ></span></td><td></td></tr>
     <tr> <th scope="row">3</th> <td>Catalog+Cart</td> <td>Thornton</td> <td>Mar 16</td>  <td><a href="/zot/web/catalog/mockupdeveloper">Mockup</a></span></td><td><a href="/zot/web/catalog/mockupsuperuser">Mockup</a></span></td><td><a href="/zot/web/catalog/mockupuser">Mockup</a></td></tr>
    <tr> <th scope="row">4</th> <td>Maps</td> <td>Thornton</td> <td>Apr 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
    <tr> <th scope="row">5</th> <td>Activities</td> <td>Thornton</td> <td>May 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td></td></tr>
    <tr> <th scope="row">6</th> <td>News</td> <td>Thornton</td> <td>Jun 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
    <tr> <th scope="row">7</th> <td>Pricing</td> <td>Thornton</td> <td>Jul 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td></tr>
    <tr> <th scope="row">8</th> <td>Orders</td> <td>Thornton</td> <td>Aug 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
     <tr> <th scope="row">9</th> <td>Payments</td> <td>Thornton</td> <td>Sep 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
     <tr> <th scope="row">10</th> <td>Promotions</td> <td>Thornton</td> <td>Oct 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
     <tr> <th scope="row">11</th> <td>Users</td> <td>Thornton</td> <td>Nov 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td></tr>
     <tr> <th scope="row">12</th> <td>Notifications</td> <td>Thornton</td> <td>Dec 16</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td></tr>
     <tr> <th scope="row">13</th> <td>Reports</td> <td>Thornton</td> <td>Jan 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>
     <tr> <th scope="row">14</th> <td>Analytics</td> <td>Thornton</td> <td>Feb 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td></tr>
    <tr> <th scope="row">15</th> <td>Admin</td> <td>Thornton</td> <td>Mar 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td></tr>
     <tr> <th scope="row">16</th> <td>Help</td> <td>Thornton</td> <td>Apr 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td></td></tr>
     <tr> <th scope="row">17</th> <td>Licence store</td> <td>Thornton</td> <td>May 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td></td><td></td><td></td></tr>
     <tr> <th scope="row">18</th> <td>Dashboard</td> <td>Thornton</td> <td>Jun 17</td>  <td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></span></td><td><span class="glyphicon glyphicon-ok" ></td></tr>


    
    
    </tbody> 
     </table>


    </div>

    </div>
</div>
