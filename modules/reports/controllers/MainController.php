<?php

namespace app\modules\reports\controllers;

use app\modules\reports\models\ReportsTree;
use app\modules\reports\models\SearchRepositoryFiles;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use dosamigos\arrayquery\ArrayQuery;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;

/**
 * Default controller for module 'Reports'
 */
class MainController extends Controller
{

    public $defaultAction = 'list';
    public static $nestedFolders; //array of nested folders
    public static $relationArray;
    public static $modalContentUri = 'folder/move-files'; //uri for content into Modal (click Massive move button)

    public function init()
    {
        self::$relationArray = [];
    }

    /**
     * @var array list of items (folder, files) and options for menu (New)
     * - icon: icon's style
     * - label: string, optional, specifies the menu item label.
     * - urlNew: array, optional, specifies the URL for creating new item. It will be processed by [[Url::to]].
     */
    public $entities = [
        'userFolder' => [
            'icon' => 'fa fa-folder',
            'label' => 'User Folder',
            'urlNew' => ['folder/create', 'type' => 'userFolder'],
        ],
        'folder' => [
            'icon' => 'fa fa-folder-o',
            'label' => 'Folder',
            'urlNew' => ['folder/create'],
        ],
        'pdf' => [
            'icon' => 'fa fa fa-file-pdf-o',
            'label' => 'Pdf',
            'urlNew' => ['document/create', 'type' => 'pdf'],
        ],
        'movie' => [
            'icon' => 'fa fa-file-movie-o',
            'label' => 'Movie',
            'urlNew' => ['document/create', 'type' => 'movie'],
        ],
        'html5' => [
            'icon' => 'fa fa-file-code-o',
            'label' => 'HTML5',
            'urlNew' => ['document/create', 'type' => 'html5'],
        ],
        'gallery' => [
            'icon' => 'fa fa-file-image-o',
            'label' => 'gallery',
            'urlNew' => ['document/create', 'type' => 'gallery'],
        ],
        'powerpoint' => [
            'icon' => 'fa fa-file-powerpoint-o',
            'label' => 'PowerPoint',
            'urlNew' => ['document/create', 'type' => 'powerpoint'],
        ],
        'excel' => [
            'icon' => 'fa fa-file-excel-o',
            'label' => 'Excel',
            'urlNew' => ['document/create', 'type' => 'excel'],
        ],
        'word' => [
            'icon' => 'fa fa-file-word-o',
            'label' => 'Word',
            'urlNew' => ['document/create', 'type' => 'word'],
        ],
        'default' => [
            'icon' => 'fa fa-file',
            'label' => 'File',
            'urlNew' => ['document/create', 'type' => 'file'],
        ],
    ];

    /**
     * Constuct list datatable
     * @return type
     */
    public function actionList()
    {
        $modelsProvider = [];
        
        $newEntities = false; //block new button by default

        if (Yii::$app->getRequest()->getQueryParam('sharable') || Yii::$app->getRequest()->getQueryParam('type')) { //apply filters
            $itemsList = $this->buildNestedList(); //nested list (before apply filters)
            $modelsProvider = $this->applyFilters($itemsList, Yii::$app->getRequest()->getQueryParams());
        } else { //get array of nested folders and medias (default action (without filters))
            $folderId = (Yii::$app->getRequest()->getQueryParam('id_folder')) ? Yii::$app->getRequest()->getQueryParam('id_folder') : null;
            $currentFolder = (!is_null($folderId)) ? ReportsTree::findOne(['id_report_tree' => $folderId]) : null; //for one folder in root
            $modelsProvider = $this->getFolderContent($currentFolder);
            $newEntities = $this->parseNewEntities($currentFolder);
        }

        $provider = new ArrayDataProvider([
            'allModels' => $modelsProvider,
            'sort' => [
                'attributes' => ['icon', 'name', 'type', 'sharable', 'size'],
            ],
        ]);


        return $this->render('list', [
            'folder' => isset($currentFolder) ? $currentFolder : false,
            'newEntities' => $newEntities,//isset($currentFolder) ? $this->parseNewEntities($currentFolder) : $this->parseNewEntities(), //items for dropdown list (button NEW) -> only inside folder!!!
            'dataProvider' => $provider,
        ]);
    }

    /**
     * build Folders
     * @return type
     */
    public function actionFolders()
    {
        $roots = ReportsTree::find()->roots()->all();

        self::$nestedFolders = [];
        
        foreach ($roots as $rootFolder) {
            self::$nestedFolders[$rootFolder->id_report_tree] = ['label' => '<span class="label h-bg-navy-blue"><i class="fa fa-folder"></i></span> ' . $rootFolder->label.self::setUserFolderBtn($rootFolder->id_report_tree), 'options' => ['data-id' => $rootFolder->id_report_tree]]; //['label' => $rootFolder->label, 'options' => ['data-id' => $rootFolder->id_folder]];

            foreach (SearchRepositoryFiles::$typeRelations as $type => $params) {
                $typeId = "type_{$rootFolder->id_report_tree}_{$type}";
                self::setArray(self::$nestedFolders, "{$rootFolder->id_report_tree}.items.{$typeId}", ['label' => '<span class="label h-bg-navy-blue"><i class="' . $params['icon'] . '"></i></span> ' . $params['label'] . self::setMassiveMoveBtn($rootFolder->id_report_tree, $type, $params['label']), 'options' => ['data-id' => $typeId]]);
            }
            //build recursively nested folder's structure
            Yii::trace('folders nested begin:' . print_r(self::$nestedFolders, true));
            self::folderNestedRecursive($rootFolder->children(1)->all(), "{$rootFolder->id_report_tree}"); //true|false
        }
        
        return $this->render('folders', [
            'folders' => self::$nestedFolders,
            'newEntities' => false, //items for dropdown list (button NEW)
        ]);
    }

    /**
     * Build flow
     * @param type $id_folder
     * @return type
     */
    public function actionFlow($id_folder = null)
    {
        $currentFolder = (!is_null($id_folder)) ? ReportsTree::findOne(['id_report_tree' => $id_folder]) : null;
        $items = $this->getFolderContent($currentFolder, 'text-info');
        return $this->render('flow', [
            'folder' => $currentFolder,
            'columns' => $this->buildFlowGrid($items),
            'newEntities' => false,//$this->parseNewEntities($currentFolder), //items for dropdown list (button NEW)
        ]);
    }

    /**
     * Search by media's or folder's title for actionFolders() and actionFlow()
     * @return type
     */
    public function actionSearch()
    {

        $modelsProvider = [];
        $itemsList = $this->buildNestedList(); //nested list (before apply filters)
        $modelsProvider = $this->applyFilters($itemsList, Yii::$app->request->post());

        $provider = new ArrayDataProvider([
            'allModels' => $modelsProvider,
            'sort' => [
                'attributes' => ['icon', 'name', 'type', 'sharable', 'size'],
            ],
        ]);

        return $this->renderAjax('searchList', [
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Get content for current folder (multiple root available)
     * @param type $folder ReportsTreeObject | null
     * @param type $addIconStyle additional style name
     * @return array
     */
    public function getFolderContent($folder = null, $addIconStyle = null)
    {
        $output = []; //models array for provider (first - folder, then - media for current folder)
        // select root folders
        if (is_null($folder))
            $folders = ReportsTree::find()->roots()->all();
        //select first level children folders
        else
            $folders = $folder->children(1)->all();

        foreach ($folders as $subfolder) {
            $output[] = $this->parseFolder($subfolder, $addIconStyle);
        }

        if (!is_null($folder)) { // set media list for current folder 
            foreach ($folder->media as $media) {
                $output[] = $this->parseMedia($media, $addIconStyle);
            }
        }

        return $output;
    }

    /**
     * Build nested list of folders/subfolders/medias
     * include only unique media (removed the duplicate medias)
     * @return array
     */
    public function buildNestedList()
    {
        $output = [];
        $roots = ReportsTree::find()->roots()->all();
        foreach ($roots as $folder) {
            $output[] = $this->parseFolder($folder);
            foreach ($folder->media as $media) {
                if ($this->mediaAlreadyExists($output, "id", $media->id_media, $media->type))
                    continue; //if media allready in array (earlier added from another linked folder)

                $output[] = $this->parseMedia($media);
            }
            foreach ($folder->children()->all() as $subfolder) {
                $output[] = $this->parseFolder($subfolder);
                foreach ($subfolder->media as $mediaSub) {
                    if ($this->mediaAlreadyExists($output, "id", $mediaSub->id_media, $mediaSub->type))
                        continue; //if media allready in array (earlier added from another linked folder)

                    $output[] = $this->parseMedia($mediaSub);
                }
            }
        }
        return $output;
    }

    /**
     * Apply filters for list of items (folders/subfolders/medias)
     * @param type $input array of items (folders/subfolders/medias)
     * @param type $getParams array of filter params
     * @return type
     */
    public function applyFilters($input, $getParams)
    {
        if (count($input) == 0)
            return $input; //don't apply filter to empty array

        $output = $input;

        if (isset($getParams['sharable'])) {
            $querySharable = new ArrayQuery($output);
            $querySharable->addCondition('sharable', 1); //apply sharable filter
            $output = $querySharable->find();
        }

        if (isset($getParams['type'])) {
            $filterConditionType = $this->getFilterConditionByType($getParams['type']);
            $queryType = new ArrayQuery($output);
            if (isset($filterConditionType) && $filterConditionType) {
                foreach ($filterConditionType as $filter) { //apply type filter
                    if (isset($filter['operator'])) {
                        $queryType->addCondition('type', $filter['value'], $filter['operator']);
                    } else
                        $queryType->addCondition('type', $filter['value']);
                }
                $output = $queryType->find();
            }
        }

        if (isset($getParams['search'])) {
            $querySearch = new ArrayQuery($output);
            $querySearch->addCondition('name', '~' . $getParams['search']); //apply search filter
            $output = $querySearch->find();
        }

        return $output;
    }

    /**
     * Parse ReportsTree object for list table
     * @param type $folder
     * @param type $addClass
     * @return type
     */
    public function parseFolder($folder, $addClass = null)
    {
        //$folderType = ($folder->reportsTreeAccess) ? 'userFolder' : 'folder';
        return [
            'icon' => ($folder->icon != '') ? $this->setImgIcon($folder->icon) : $this->setStyleIcon($folder->type, $addClass),
            'id' => $folder->id_report_tree,
            'name' => $folder->label,
            'type' => $folder->type, //($folderType == 'userFolder') ? 'User folder' : $folderType,
            'sharable' => $folder->sharable,
            'size' => $folder->size,
            'pattern' => $this->buildFolderPattern($folder),
            'repositoryFile' => ''
        ];
    }

    /**
     * Parse Media object for list table
     * @param type $media
     * @param type $addClass
     * @return type
     */
    public function parseMedia($media, $addClass = null)
    {
        return [
            'icon' => ($media->icon != '') ? $this->setImgIcon($media->icon) : $this->setStyleIcon($media->type, $addClass),
            'id' => $media->id_media,
            'name' => $media->name,
            'type' => $media->type,
            'sharable' => $media->cansend,
            'size' => $media->size, //($media->repositoryFile) ? $media->repositoryFile->size : 0,
            'repFile' => ($media->repositoryFile) ? $media->repositoryFile->name : null,
        ];
    }

    /**
     * Build pattern value for folder (column 'pattern' in list view)
     * @param type $folder
     * @return string
     */
    public function buildFolderPattern($folder)
    {
        $patternArray = [];

        $parents = $folder->parents()->all();
        foreach ($parents as $parentFolder) {
            $patternArray[] = $parentFolder->pattern;
        }
        $patternArray[] = $folder->pattern;
        $pattern = implode('_', $patternArray);

        if ($folder->frequency != '')
            $pattern .= '_' . $folder->frequency;

        return $pattern;
    }

    /**
     * Set image icon (if isset)
     * @param type $image
     * @return type
     */
    public function setImgIcon($image)
    {
        return Html::img(Yii::getAlias('@web') . '/images/' . $image, ['width' => '64px', 'height' => '64px']);
        //Html::img(Yii::getAlias('@web').'/images/'. $data['image']
    }

    /**
     * Set icon style by item's type
     * @param type $type
     * @param type $addClass add class name to current name
     * @return type
     */
    public function setStyleIcon($type, $addClass = null)
    {

        if (isset($this->entities[$type]['icon'])) {
            $classStyle = $this->entities[$type]['icon'];
        } else {
            //trick to determine the type of media
            //(i.e. Media with type 'word' can have type in 'zse_v1_media'.'type' =>
            //=>'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
            $trickType = SearchRepositoryFiles::getTypeByMimeType($type); //detect type by mimetype
            if ($trickType)
                $classStyle = $this->entities[$trickType]['icon'];
            else
                $classStyle = $this->entities['default']['icon']; //default icon
        }

        if ($addClass)
            $classStyle .= ' ' . $addClass;

        return "<i class='{$classStyle}'></i>";
    }


    /**
     * Check media exists in provider array
     * @param $array
     * @param $key
     * @param $val
     * @return bool
     */
    public function mediaAlreadyExists($array, $key, $val, $type)
    {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val && $item['type'] == $type)
                return true;
        return false;
    }

    /**
     * Get condition for filter
     * for dosamigos\arrayquery\ArrayQuery addCondition method();
     * @param type $type
     * @return boolean | array
     * return:
     * false => don't apply filter
     * [
     * 'value' => 'filterValue',
     * 'operator' => 'operator' //
     * ]
     */
    public function getFilterConditionByType($type)
    {
        switch ($type) {
            case 'files':
                return [
                    ['value' => '<>folder'],
                    ['value' => '<>userFolder', 'operator' => 'and']
                ];
                break;
            case 'folders':
                return [
                    ['value' => 'folder'],
                    ['value' => 'userFolder', 'operator' => 'or']
                ];
            case 'userFolders':
                return [
                    ['value' => 'userFolder']
                ];
            case 'all':
            case '---':
                return false;
                break;


            default:
                if (array_key_exists($type, $this->entities)) { // $this->iconType
                    //trick to determine the type of media 
                    //(i.e. Media with type 'word' can have type in 'zse_v1_media'.'type' => 
                    // =>'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                    $searchingTypes = SearchRepositoryFiles::getTypeMimeTypeArray($type);
                    $condition = [];
                    $counter = 0;
                    foreach ($searchingTypes as $typeEntity) {
                        $row = [
                            'value' => $typeEntity,
                            'operator' => 'or'
                        ];
                        if ($counter == 0)
                            $row['operator'] = 'and';
                        $condition[] = $row;
                        unset($row);
                        $counter++;
                    }
                    return $condition;
                    //return [
                    //    ['value' => $type],
                    //];
                } else
                    return false;
                break;
        }
    }

    /**
     * Build nested structure of folders and medias
     * @param array $array
     * @param type $path string (array's deep, example: "key1.key2.key3"
     */
    public static function folderNestedRecursive(array $array, $path = null)
    {
        $path = "{$path}.items";
        foreach ($array as $k => $v) {
            $children = $v->children(1)->all(); //children folders
            //set information about folder (build folder menu's item)
            self::setArray(self::$nestedFolders, "{$path}.{$v->id_report_tree}", ['label' => '<span class="label h-bg-navy-blue"><i class="fa fa-folder"></i></span> ' . $v->label, 'options' => ['data-id' => $v->id_report_tree]]);

            //set pannels for moving by types:
            foreach (SearchRepositoryFiles::$typeRelations as $type => $params) {
                $typeId = "type_{$v->id_report_tree}_{$type}";
                //$controller = new MainController('main', 'documents');
                self::setArray(self::$nestedFolders, "{$path}.{$v->id_report_tree}.items.{$typeId}", ['label' => '<span class="label h-bg-navy-blue"><i class="' . $params['icon'] . '"></i></span> ' . $params['label'] . self::setMassiveMoveBtn($v->id_report_tree, $type, $params['label']), 'options' => ['data-id' => $typeId]]);
            }

            if (count($children) > 0) {
                $fullpath = "{$path}.{$v->id_report_tree}";
                self::folderNestedRecursive($children, $fullpath);
            }


            //set information about medias in folder (build media menu's item). Nested structure
            /*
              if (isset($v->media)) {
              if (count($v->media) > 0) { //insert folder's media
              foreach ($v->media as $media) {
              $idMedia = "media_" . $media->id_media;
              $controller = new MainController('main', 'documents');

              //set information about media
              self::setArray(self::$nestedFolders, "{$path}.{$v->id_folder}.items.{$idMedia}", ['label' => '<span class="label h-bg-navy-blue">' . $controller->setStyleIcon($media->type) . '</span> ' . $media->name, 'options' => ['data-id' => $idMedia]]);
              }
              }
              }
             */
        }
    }

    /**
     * Set multidimensional array deep
     * @param $array
     * @param $keys
     * @param $value
     *
     * example:
     * $array = Array();
     * self::setArray($array, "key", Array('value' => 2));
     * self::setArray($array, "5.15.15", 3);
     * print_r($array);
     *
     */
    public static function setArray(&$array, $keys, $value)
    {
        $keys = explode(".", $keys);
        $current = &$array;
        foreach ($keys as $key) {
            $current = &$current[$key];
        }
        $current = $value;
    }

    /**
     * Set style for button "Massive Move" (for opening Modal)
     * @param type $id
     * @param type $label folder's label
     * @return type
     */
    public static function setMassiveMoveBtn($id, $type, $label)
    {
        $url = \yii\helpers\Url::to([self::$modalContentUri, 'id' => $id, 'type' => $type]);
        return '<span class="showModalButton label h-bg-green pull-right"><input id="folder_' . $id . '" type="hidden" value="' . $label . '|' . $url . '"><i class="fa fa-exchange"></i></span>';
    }
    
    /**
     * Set style for link to section 'Users' when click on icon 'User folder' (actionFolders)
     * @param type $id
     * @return type
     */
    public static function setUserFolderBtn($id)
    {
        $a = Html::a('<i class="fa fa-user" style="color:#fff;"></i>', Url::to(['folder/update', 'id' => $id, 'tab' => 'users']));
        return "<span class='label h-bg-green pull-right'>{$a}</span></a>";
    }

    /**
     * Build multidimensional array for flow view
     * @param type $items
     * @param type $columnsLimit
     * @return array $out
     */
    public function buildFlowGrid($items, $columnsLimit = 4)
    {
        $out = [];
        $columnCounter = 0;
        foreach ($items as $item) {
            $out[$columnCounter][] = $item;
            $columnCounter++;

            if ($columnCounter >= $columnsLimit)
                $columnCounter = 0;
        }
        return $out;
    }

    /**
     * Parse $entities array for droppdown list (dropdown when click on NEW button)
     * @param type $folder object ReportsTree
     * @return boolean
     */
    public function parseNewEntities($folder = null)
    {
        if (!$folder) { //not inside folder => block NEW button
            // allow create only user folder
            $newEntities[] = [
                'label' => $this->setStyleIcon('userFolder') . ' ' . $this->entities['userFolder']['label'],
                'url' => $this->entities['userFolder']['urlNew'],
            ];
            return $newEntities;
            //return false;
        }

        $newEntities = [];
        foreach ($this->entities as $key => $entity) {
            $newEntities[] = [
                'label' => $this->setStyleIcon($key) . ' ' . $entity['label'],
                'url' => array_merge($entity['urlNew'], ['id_folder' => $folder->id_report_tree]),
            ];
        }
        return $newEntities;
    }

}
