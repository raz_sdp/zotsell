<?php

use yii\helpers\Html;
use app\themes\Common\AppAssetHomer;
use app\modules\contacts\assets\AppAssetContacts;

/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it.
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    /*echo $this->render(
        'main-login',
        ['content' => $content]
    );*/
} else {

    //if (class_exists('backend\assets\AppAsset')) {
    //    backend\assets\AppAsset::register($this);
    //} else {

    app\assets\AppAsset::register($this);
    $asset = AppAssetHomer::register($this);
    //}
    AppAssetContacts::register($this);
    //$assetPath = $asset->baseUrl;
    //$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <!-- Simple splash screen-->
    <div class="splash">
        <div class="color-line"></div>
        <div class="splash-title"><h1>Zot Admin</h1>

            <p>The Most comprehensive admin </p><img src="<?php echo Yii::$app->homeUrl?>images/loading-bars.svg" width="64" height="64"/></div>
    </div>
    <!--[if lt IE 7]>
    <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a
        href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- Header -->
    <div id="header">
        <?php echo $this->render('header')?>
    </div>

    <!-- Navigation -->
    <aside id="menu">
        <?php echo $this->render('menu')?>
    </aside>

    <?php echo $this->render('hidden-fields')?>

    <div id="wrapper">
        <div class="normalheader transition animated fadeIn">
            <div class="hpanel">
                <?php echo $this->render('hpanel')?>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <?= $content ?>
            </div>
        </div>

        <!-- Footer-->
        <?php //echo $this->render('footer')?>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
