<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = 'Flow views';

// module root
$this->params['breadcrumbs'][] = [
    'label' => "Reports",
    'url' => ['']
];


if (isset($folder['id_report_tree']) && $folder['id_report_tree'] > 0) {
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/flow']];

    $parents = $folder->parents()->all();
    foreach ($parents as $parentFolder) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentFolder->label,
            'url' => ['main/flow', 'id_folder' => $parentFolder->id_report_tree]
        ];
    }

    $this->params['breadcrumbs'][] = [
        'label' => $folder['label'],
        'template' => "<li><b>{link}</b></li>\n"
    ];
} else {
    $this->params['breadcrumbs'][] = $this->title;
}


$this->params['headerTitle'] = "Reports Flow View";
$this->params['headerDescription'] = "Use Reports view to show all documents in a tree .";

$this->params['newEntities'] = $newEntities; // for dropdown list on click button NEW
?>

<div class="col-md-3">
    <div class="hpanel">
        <?=
        $this->render(
            '_menu.php'
        )
        ?>
    </div>
</div>

<div class="col-md-9">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <?= $this->render(
                        '_searchForm.php'
                    ) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="body-result">
        <?php if (count($columns) > 0): ?>
            <?php foreach ($columns as $column): ?>
                <div class="col-md-3">
                    <?php foreach ($column as $object): ?>
                        <div class="hpanel">
                            <div class="panel-body file-body">
                                <?php
                                if (in_array($object['type'], ['folder','userFolder'])) $uri = 'folder';
                                else $uri = 'document';
                                echo Html::a(
                                    $object['icon'],
                                    [$uri . '/view', 'id' => $object['id']]
                                );
                                ?>
                            </div>
                            <div class="panel-footer">
                                <?php
                                if (in_array($object['type'], ['folder','userFolder'])) {
                                    echo Html::a(
                                        $object['name'],
                                        ['main/flow', 'id_folder' => $object['id']],
                                        ['style' => 'text-decoration:underline;']
                                    );
                                } else {
                                    if (isset($object['repFile'])) {
                                        echo Html::a(
                                            $object['name'],
                                            ['/uploads/' . $object['repFile']],
                                            ['style' => 'text-decoration:underline;']
                                        );
                                    } else {
                                        echo Html::tag('span', $object['name']);
                                    }
                                }

                                ?>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="col-md-3">
                <div class="hpanel">
                    <strong>Empty folder</strong>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>