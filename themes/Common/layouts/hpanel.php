<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:32 AM
 */
use yii\widgets\Breadcrumbs;
?>
<div class="panel-body">
    <a class="small-header-action" href="">
        <div class="clip-header">
            <i class="fa fa-arrow-up"></i>
        </div>
    </a>

    <?php
    $breadcrumbs=$this->breadCrums;
    if(!empty($breadcrumbs)){?>
        <div id="hbreadcrumb" class="pull-right m-t-lg">
            <ol class="hbreadcrumb breadcrumb">
                <li><a href="<?php echo $breadcrumbs[0]['link']?>"><?php echo $breadcrumbs[0]['label']?></a></li>
                <li>
                    <span><?php echo $breadcrumbs[1]?></span>
                </li>
                <?php if(isset($breadcrumbs[2])){?>
                    <li class="active">
                        <span><?php echo $breadcrumbs[2]?></span>
                    </li>
                <?php }?>

            </ol>
        </div>
    <?php }?>
    <h2 class="font-light m-b-xs">
        <?php echo $this->title;?>
    </h2>
    <small><?php echo $this->subtitle;?></small>
</div>