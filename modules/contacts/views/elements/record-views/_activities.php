<?php
/**
 * Created by PhpStorm.
 * User: Raaz
 * Date: 2/27/16
 * Time: 12:24 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Disabled
</div>
<h3>Activities </h3>
<hr/>
<div class="note-content">
    <div class="row">
        <div class="col-lg-12">
            <table id=""
                   class="table table-striped table-bordered table-hover js-view-activity-table custom_table_brake">
                <thead>
                <tr>
                    <th>Date</th>
                    <!--<th>ID</th>-->
                    <th>Type</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>