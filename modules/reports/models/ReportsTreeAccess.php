<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_reports_tree_access".
 *
 * @property integer $id_report_tree
 * @property integer $id_account
 * @property string $date_modification
 * @property integer $record_status
 */
class ReportsTreeAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_reports_tree_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_report_tree', 'id_account'], 'required'],
            [['id_report_tree', 'id_account', 'record_status'], 'integer'],
            [['date_modification'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_report_tree' => 'Id Report Tree',
            'id_account' => 'Id Account',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }
}
