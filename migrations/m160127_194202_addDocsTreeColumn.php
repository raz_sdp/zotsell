<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_194202_addDocsTreeColumn extends Migration
{
    public function up()
    {
        $this->addColumn('zse_v1_docs_tree', 'icon_type', 'TINYINT(1) NOT NULL DEFAULT "1"');
    }

    public function down()
    {
        $this->dropColumn('zse_v1_docs_tree', 'icon_type');
    }
}
