<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:14 AM
 */
use app\modules\contacts\components\AppHelper;
$this->title="Contacts Folder View";
$this->subtitle="Use Folder view to show Contacs in a sortable folder .";
$this->breadCrums= [['label'=>'Contacts','link'=>Yii::$app->urlManager->createUrl('contacts/records/list')],'Folder View'];
?>


<div class="col-md-3">
    <?php echo $this->render('../layouts/left-hpanel')?>
    <?php echo $this->render('../elements/_new_folder')?>
</div>

<div class="col-md-9">
    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <form action="<?=yii::$app->request->baseUrl.'/contacts/records/folder'?>" method="post" >
                    <div class="input-group">
                        <input class="form-control" id="serch-contact-records-value-folder" type="text" placeholder="Search Contacts.." name="search" value="<?=$search?>">
                        <div class="input-group-btn">
                            <button class="btn btn-default" id="serch-contact-records-button-folder" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="hpanel">
        <div class="panel-heading">
            <div class="panel-tools">
                    <a class="btn btn-default btn-xs folder_view_expand_btn showhide">Collapse all</a>
            </div>
            User Folders
        </div>
        <div class="panel-body">
            <?php if(!empty($msg)): ?>
                <div class="alert alert-<?php echo ($success) ? "success" : "danger";?> text-center js-update-alert-success alert_msg" role="alert">
                    <?= $msg; ?>
                </div>
            <?php endif; ?>
            <div class="dd" id="nestable2">
                <ol class="dd-list">
                    <?php if(empty($agents_contacts)) { ?>
                        <li class="dd-item" data-id="1">Result Not Found.</li>
                    <?php } else { ?>
                        <?php foreach($agents_contacts as $agent => $agent_contacts) { ?>
                        <?php
                        //print_r($associate_emails);die;
                        if(!empty($associate_emails[$agent])){
                            $agent_info = $agent.' ('.implode(', ', $associate_emails[$agent]).')';
                        } else{
                            $agent_info = $agent;
                        }                       
                        ?>
                        <li class="dd-item" data-id="1">
                            <div class="dd-handle expand_agent">
                                <span class="label h-bg-navy-blue "><i class="fa fa-user"></i></span> <?php echo $agent_info;?>
                            </div>
                            <ol class="dd-list js-contact-list-folder">
                                <?php foreach($agent_contacts as $key => $types) {
                                    $type_text = AppHelper::getPlural($key);
                                    $type_icon = AppHelper::convertTypeIntoImage($key)
                                ?>
                                <li class="dd-item" data-id="2">
                                    <div class="dd-handle expand_type">
                                        <span class="label h-bg-navy-blue"><?php echo $type_icon?></i></span> <?php echo $type_text;?>
                                        <span data-toggle="modal" data-target="#myModal_<?= $agent;?>_<?= $key;?>" class="label h-bg-green pull-right"><i class="fa fa-exchange"></i></span>
                                    </div>
                                    <ol class="dd-list js-contact-list-folder_222">
                                        <?php foreach($types as $key2 => $item) { ?>
                                            <li class="dd-item" data-id="3">
                                                <div class="dd-handle">
                                                    <?= $item['business_name'];?>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ol>
                                </li>
                                <?php } ?>
                            </ol>
                        </li>
                        <?php } ?>
                    <?php } ?>
                </ol>
            </div>
        </div>
    </div>
</div>

<?php foreach($agents_contacts as $agent => $agent_contacts) { ?>
<?php foreach($agent_contacts as $key => $types) { ?>

<!-- myModal5-->
<div class="modal fade" id="myModal_<?= $agent;?>_<?= $key;?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Associate <?= $key;?>  to Users</h4>
                <small class="font-bold">Bulk Associate <?= $key;?> to <?= $agent;?></small>
            </div>
            <form action="<?php echo Yii::$app->homeUrl;?>contacts/records/associate" method="post">
            <div class="modal-body row col-md-12" style="clear: both; margin: auto">
                <!-- <div class="form-group pull-left"> -->
                <div class="col-md-12">
                    <input type="hidden" name="agent" value="<?= $agent;?>">
                    <input type="hidden" name="type" value="<?= $key;?>">
                    <select multiple="multiple" id="my-select" class="searchable" name="my-select[]">
                        <?php foreach($type_contacts[ucfirst($key)] as $contact) { ?>
                            <?php
                            $code_with_dest = $contact->code . ",";
                            $code_with_dest .= (empty($contact->code_destination)) ? "#" : $contact->code_destination;
                            ?>
                            <option value="<?= $code_with_dest;?>" <?php if(in_array($code_with_dest, $agents_contacts_codes[$agent][$key])) echo "selected";?>><?= $contact->business_name;?></option>
                        <?php } ?>
                    </select>
                </div>
                <!-- </div> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php } ?>
<?php } ?>

<style>
    .modal-backdrop {
        z-index: 0 !important;
    }
</style>
