$("#modalSubmit").click(function(e) {
        e.preventDefault();
        //console.log('event submit form');
        var form = $("#formMoveFiles");
        $.ajax({
            url: "/documents/folder/save-moved-files",
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                console.log(result);
                var modalContainer = $('#modal');
                var modalBody = modalContainer.find('#modalContent');
                var insidemodalBody = modalContainer.find('#formMoveFiles');

                if (result == 'true') {
                    insidemodalBody.html(result).hide(); // 
                    //$('#my-modal').modal('hide');
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').append("<strong>Files Moved successfully</strong>");
                    $('#success > .alert-success').append('</div>');


                    setTimeout(function () { // close modal after 2 seconds
                        $("#modal").modal('hide');
                        //console.log('nestable '+nestableFolders);
                        $("#nestableFolders").load(location.href + " #nestableFolders");
                        //document.location.reload();
                    }, 1000);
                }
                else {
                    console.log(modalBody);
                    modalBody.html(result).hide().fadeIn();
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').append("<strong>"+result+"</strong>");
                    $('#success > .alert-danger').append('</div>');

                }
            }
        });
        //$("#myForm").submit();
    });