/**
 * Created by pc on 2/24/16.
 */
var connection_search_data, activity_search_data, document_search_data, order_search_data;
var contact_type_icons = {
    'Company': "<i class=\"fa fa-building-o\"> </i>",
    'Prospect': "<i class=\"fa fa-user-secret\"> </i>",
    'Destination': "<i class=\"fa fa-truck\"> </i>",
    'Person': "<i class=\"fa fa-user\"> </i>"
}
var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[0] + getUrl.pathname.split('/')[1] + "/" + getUrl.pathname.split('/')[2];
var data = {};
var oTable = {};
$(function () {
    /**
     * Datatable firstime initialization for contact list page
     * */
    if($('#action-id').val()=='list'){
        commonDataTableCreation('.js-contact-list-table',baseUrl + "/contacts/ajax/getcontacsrecords");
    }

    /*Js for Individual records view */
    $('[id^=js-tab]').on('click', function(){
        var tab_type = $(this).attr('id').split('-').pop();
        console.log(tab_type);
        var id = location.search.split('id=')[1];
        var url = baseUrl + "/contacts/ajax/get"+ tab_type +"?id="+id;
        if($('#action-id').val()=='view'){
            closeDataTable();
            commonDataTableCreation('.js-view-'+tab_type+'-table',url);
        }
        else if($('#action-id').val()=='update'){
            $('.js-update-alert-success').hide();
            //if($("#active_connections").val() == 1){
                $('#connections-table-update').dataTable();
            //}

            //if($("#active_activities").val() == 1){
                $('#activities-table-update').dataTable();
            //}

            //if($("#active_orders").val() == 1){
                $('#sales-table-update').dataTable();
            //}

            //if($("#active_documents").val() == 1){
                $('#documents-table-update').dataTable();
            //}
        }
    });
    /*End Js for Individual records view*/

    /**
     * filter dataTable for submitting left-filetr form of contact-list page
     * */
    $('#filter-contct-list-form').on('submit', function () {
        data = $(this).serializeObject();
       /* if(!data.favourites){
            data['favourites']='off';
        }*/
        closeDataTable();
        commonDataTableCreation('.js-contact-list-table',baseUrl + "/contacts/ajax/getcontacsrecords");
        //oTable.draw();
    });

    /**
     * First time data load in flow page.
     * */
    $('#serch-contact-records-button-flow').click(function(){
        flowPageSearch();
    });

    $("#serch-contact-records-value-flow").keyup(function (e) {
        if (e.keyCode == 13) {
            flowPageSearch();
        }
    });


    /**
     * firstime load data in flow pge page
     */
    if($('#action-id').val()=='flow'){
        getContactRecordGridData(data);
    }

    /**
     * filter data for flow and list page
     */
    $("#contact-records-filter-button").on('click',function(){
        $('#submitted-by-click').val(1);
        $('#serch-contact-records-search-flow').val(0);
        $('#serch-contact-records-value-flow').val('');
        if($('#action-id').val()=='list'){
            closeDataTable();
        }
    });
    /**
     * filter grid view for submitting left-filetr form of flow page
     * */
    $('#filter-contct-flow-form').on('submit', function () {
        $('#filter-form-submitted').val(1);
        data = $(this).serializeObject();
        /*if(!data.favourites){
         data['favourites']='off';
         }*/
        if($('#submitted-by-click').val()==1){
            $("#pagination-offset").val(0);
        }
        getContactRecordGridData(data);
    });

    $('.searchable').multiSelect({
        selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>",
        selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='search'>",
        afterInit: function (ms) {
            var that = this,
                $selectableSearch = that.$selectableUl.prev(),
                $selectionSearch = that.$selectionUl.prev(),
                selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

            that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                .on('keydown', function (e) {
                    if (e.which === 40) {
                        that.$selectableUl.focus();
                        return false;
                    }
                });

            that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                .on('keydown', function (e) {
                    if (e.which == 40) {
                        that.$selectionUl.focus();
                        return false;
                    }
                });
        },
        afterSelect: function () {
            this.qs1.cache();
            this.qs2.cache();
        },
        afterDeselect: function () {
            this.qs1.cache();
            this.qs2.cache();
        }
    });

    $('#form_connection').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();
        //console.log(data);

        $('#form_connection').find('button').html('Searching...');
        $('#form_connection').find('button').attr('disabled', true);

        $.ajax({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/searchcontact",
            type: "POST",
            data: data,
            success: function (result) {
                $('#form_connection').find('button').html('Search');
                $('#form_connection').find('button').attr('disabled', false);

                var row = "";
                $(".js-connection-search-result-table tbody").empty();
                if(result.success) {
                    connection_search_data = result.data;
                    $.each(result.data, function(key, item){
                        row += "<tr>";
                        row +=      "<td>";
                        row +=          "<a id="+key+" ><i class=\"fa fa-plus\"  onclick=\"add_to_connection_table(this)\"></i></a>&nbsp;&nbsp;";
                        row +=          "<span class=\"text-danger font-bold\">" + item.business_name + "</span>";
                        row +=      "</td>";
                        row +=      "<td>" + item.code + "</td>";
                        row += "</tr>";
                    });
                } else {
                    row += "<tr>";
                    row +=      "<td colspan=\"2\">";
                    row +=          "No Result Found.";
                    row +=      "</td>";
                    row += "</tr>";
                }
                //console.log(row);
                $(".js-connection-search-result-table tbody").html(row);
            }
        });
    });
    $('#save_connections').click(function() {
        var urlString = window.location.href;
        urlParams = parseURLParams(urlString);     
        var _this = $(this);   
        $(this).html('<i class="fa fa-refresh fa-spin fa-2x"></i>').attr('disabled',true);
        $.ajax
        ({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/saveconnections",
            type: "POST",
            data: {id:urlParams['id'][0],connections:added_connection_records},
            success: function (result){
                $('.saving_msg').remove();
                if(result.success){
                    show_alert(true, "Connection saved successfully!");
                } else {
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            },
            error:function(result){
                if(result.status == 500){
                    show_alert(false, "You have already added this contact! Please try another one.");
                }else{
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            }
        });
    });


    $('#form_activity').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();
        //console.log(data);

        $('#form_activity').find('button').html('Searching...');
        $('#form_activity').find('button').attr('disabled', true);

        $.ajax({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/searchactivities",
            type: "POST",
            data: data,
            success: function (result) {
                $('#form_activity').find('button').html('Search');
                $('#form_activity').find('button').attr('disabled', false);

                var row = "";
                $(".js-activity-search-result-table tbody").empty();
                if(result.success) {
                    activity_search_data = result.data;
                    $.each(result.data, function(key, item){
                        row += "<tr>";
                        row +=      "<td>";
                        row +=          "<a id="+key+" ><i class=\"fa fa-plus\" onclick=\"add_to_activity_table(this)\"></i></a>&nbsp;&nbsp;";
                        row +=          "<span class=\"text-danger font-bold\">" + item.date_activity + "</span>";
                        row +=      "</td>";
                        row +=      "<td>" + item.type + "</td>";
                        row += "</tr>";
                    });
                } else {
                    row += "<tr>";
                    row +=      "<td colspan=\"2\">";
                    row +=          "No Result Found.";
                    row +=      "</td>";
                    row += "</tr>";
                }
                //console.log(row);
                $(".js-activity-search-result-table tbody").html(row);
            }
        });
    });

    $('#save_activities').click(function() {
        var urlString = window.location.href;
        urlParams = parseURLParams(urlString);
        //$('#activity_save_btn_group').append("<p class='text-success saving_msg'>Saving To database...</p>");
        $.ajax
        ({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/saveactivities",
            type: "POST",
            data: {id:urlParams['id'][0],activities:added_activities},
            success: function (result){
                $('.saving_msg').remove();
                if(result.success){
                    show_alert(true, "Activities saved successfully!");
                } else {
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            },
            error:function(result){
                if(result.status == 500){
                    show_alert(false, "You have already added this contact! Please try another one.");
                }else{
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            }
        });
    });


    $('#form_order').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();
        //console.log(data);

        $('#form_order').find('button').html('Searching...');
        $('#form_order').find('button').attr('disabled', true);

        $.ajax({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/searchorders",
            type: "POST",
            data: data,
            success: function (result) {
                $('#form_order').find('button').html('Search');
                $('#form_order').find('button').attr('disabled', false);

                var row = "";
                $(".js-order-search-result-table tbody").empty();
                if(result.success) {
                    order_search_data = result.data;
                    console.log(order_search_data)
                    $.each(result.data, function(key, item){
                        row += "<tr>";
                        row +=      "<td>";
                        row +=          "<a id="+key+" ><i class=\"fa fa-plus\" onclick=\"add_to_order_table(this)\"></i></a>&nbsp;&nbsp;";
                        row +=          "<span class=\"text-danger font-bold\">" + item.order_code + "</span>";
                        row +=      "</td>";
                        row +=      "<td>" + item.type + "</td>";
                        row += "</tr>";
                    });
                } else {
                    row += "<tr>";
                    row +=      "<td colspan=\"2\">";
                    row +=          "No Result Found.";
                    row +=      "</td>";
                    row += "</tr>";
                }
                //console.log(row);
                $(".js-order-search-result-table tbody").html(row);
            }
        });
    });

    $('#save_orders').click(function() {
        var urlString = window.location.href;
        urlParams = parseURLParams(urlString);
        var _this = $(this);   
        $(this).html('<i class="fa fa-refresh fa-spin fa-2x"></i>').attr('disabled',true);
        $.ajax
        ({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/saveorders",
            type: "POST",
            data: {id:urlParams['id'][0],orders:added_orders},
            success: function (result){
                $('.saving_msg').remove();
                if(result.success){
                    show_alert(true, "Sales saved successfully!");
                } else {
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            },
            error:function(result){
                if(result.status == 500){
                    show_alert(false, "You have already added this contact! Please try another one.");
                }else{
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            }
        });
    });

    $(".field-records-id_classification").find('label').hide();
    $(".field-records-type").find('label').hide();
    $(".field-records-record_status").find('label').hide();
    $("#records-icon").addClass('btn btn-success');

    $('#form_document').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();
        //console.log(data);

        $('#form_document').find('button').html('Searching...');
        $('#form_document').find('button').attr('disabled', true);

        $.ajax({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/searchdocuments",
            type: "POST",
            data: data,
            success: function (result) {
                $('#form_document').find('button').html('Search');
                $('#form_document').find('button').attr('disabled', false);

                var row = "";
                $(".js-document-search-result-table tbody").empty();
                if(result.success) {
                    document_search_data = result.data;
                    $.each(result.data, function(key, item){
                        row += "<tr>";
                        row +=      "<td>";
                        row +=          "<a id="+key+" ><i class=\"fa fa-plus\" onclick=\"add_to_document_table(this)\"></i></a>&nbsp;&nbsp;";
                        row +=          "<span class=\"text-danger font-bold\">" + item.name + "</span>";
                        row +=      "</td>";
                        row +=      "<td>" + item.type + "</td>";
                        row += "</tr>";
                    });
                } else {
                    row += "<tr>";
                    row +=      "<td colspan=\"2\">";
                    row +=          "No Result Found.";
                    row +=      "</td>";
                    row += "</tr>";
                }
                //console.log(row);
                $(".js-document-search-result-table tbody").html(row);
            }
        });
    });

    $('#save_documents').click(function() {
        var urlString = window.location.href;
        urlParams = parseURLParams(urlString);
        var _this = $(this);   
        $(this).html('<i class="fa fa-refresh fa-spin fa-2x"></i>').attr('disabled',true);        
        $.ajax
        ({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/savedocuments",
            type: "POST",
            data: {id:urlParams['id'][0],documents:added_documents},
            success: function (result){
                $('.saving_msg').remove();
                if(result.success){
                    show_alert(true, "Documents saved successfully!");
                } else {
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            },
            error:function(result){
                if(result.status == 500){
                    show_alert(false, "You have already added this contact! Please try another one.");
                }else{
                    show_alert(false, "Error");
                }
                _this.removeAttr('disabled').html('<i class="fa fa-thumbs-o-up"></i> Save');
            }
        });
    });


    $('#form_map').on('submit', function (e) {
        e.preventDefault();
        /*$('#form_map').find('button').html('Please Wait...');
        $('#form_map').find('button').attr('disabled', true);*/

        data = $(this).serializeObject();
        var coordinates = data.coordinates;
        coordinates = coordinates.split(',');
        var lat_lng = {lat: parseInt(coordinates[0]), lng: parseInt(coordinates[1])};
        loadMap(lat_lng);
        //google.maps.event.trigger(map, 'init');

        /*$('#form_map').find('button').html('Add');
        $('#form_map').find('button').attr('disabled', false);*/
    });


    $('#extended_data_formaaaaaaaaaa').on('submit', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();

        var new_data_field = "";
        new_data_field += "<div class=\"form-group\">";
        new_data_field += "    <label for=\"" + data.extended_data_label + "\">" + data.extended_data_label + "</label>";
        new_data_field += "    <input type=\"hidden\" name=\"Records[options_label][]\" value=\"" + data.extended_data_label + "\">";
        new_data_field += "    <input type=\"hidden\" name=\"Records[options_field_id][]\" value=\"" + data.extended_field_id + "\">";
        if(data.extended_data_type == "s") {
            new_data_field += "    <input type=\"hidden\" name=\"Records[options_type][]\" value=\"TESTO\">";
            new_data_field += "    <input type=\"text\" id=\"" + data.extended_data_label + "_value\" name=\"Records[options][]\" value=\"" + data.extended_data_value_s + "\" class=\"form-control\">";
        } else if(data.extended_data_type == "e") {
            new_data_field += "    <input type=\"hidden\" name=\"Records[options_type][]\" value=\"TESTO_ESTESO\">";
            new_data_field += "    <textarea id=\"" + data.extended_data_label + "_value\" class=\"form-control\" name=\"Records[options][]\" rows=\"2\" style=\"font-family: Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;\">" + data.extended_data_value_e + "</textarea>";
        }
        new_data_field += "</div>";

        $('#extended_datas').append(new_data_field);
    });

    /*Map load*/
    $('#js-map-tab').on('click',function(){
        getGeoLoc();
    });
    function loadMap(lat_lng){
         //console.log(lat_lng)
        $('input[name=coordinates]').val(lat_lng.lat + "," + lat_lng.lng);
         var map = new google.maps.Map(document.getElementById('map-area'), {
                zoom: 14,
                center: lat_lng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
          });

          var marker = new google.maps.Marker({
            position: lat_lng,
            map: map
          });
    }
    function getGeoLoc(){
        //console.log(myLatLng)
        if(!jQuery.isEmptyObject(myLatLng)) {
            //console.log(myLatLng)
            loadMap(myLatLng);
        } else if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var myLatLng = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
              };
              loadMap(myLatLng);
            }, function() {
             alert("Geo Location Error");
            });
       } else {
        alert("Geo Location Error");
       }
    }

    $('#save_map').click(function() {
        var urlString = window.location.href;
        urlParams = parseURLParams(urlString);
        data = $('#form_map').serializeObject();
        coordinates = data.coordinates.replace(/ /g,'');
        $('#map_save_btn_group').append("<p class='text-success saving_msg'>Saving To database...</p>");
        $.ajax
        ({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/savemap",
            type: "POST",
            data: {id:urlParams['id'][0],coordinates:coordinates},
            success: function (result){
                $('.saving_msg').remove();
                if(result.success){
                    alert("successfully saved");
                } else{
                    alert("error");
                }
            }
        });
    });

    $('#form_connection').on('change', function (e) {
        e.preventDefault();
        data = $(this).serializeObject();
        //console.log(data);

        $('#form_connection').find('button').html('Searching...');
        $('#form_connection').find('button').attr('disabled', true);

        $.ajax({
            dataType: 'json',
            url:  baseUrl + "/contacts/ajax/searchcontact",
            type: "POST",
            data: data,
            success: function (result) {
                $('#form_connection').find('button').html('Search');
                $('#form_connection').find('button').attr('disabled', false);

                var row = "";
                $(".js-connection-search-result-table tbody").empty();
                if(result.success) {
                    connection_search_data = result.data;
                    $.each(result.data, function(key, item){
                        row += "<tr>";
                        row +=      "<td>";
                        row +=          "<a id="+key+" ><i class=\"fa fa-plus\"  onclick=\"add_to_connection_table(this)\"></i></a>&nbsp;&nbsp;";
                        row +=          "<span class=\"text-danger font-bold\">" + item.business_name + "</span>";
                        row +=      "</td>";
                        row +=      "<td>" + item.code + "</td>";
                        row += "</tr>";
                    });
                } else {
                    row += "<tr>";
                    row +=      "<td colspan=\"2\">";
                    row +=          "No Result Found.";
                    row +=      "</td>";
                    row += "</tr>";
                }
                //console.log(row);
                $(".js-connection-search-result-table tbody").html(row);
            }
        });
    });

    // side bar activity on contact_create and contact_details
    $('.note-link a').click(function () {
        $('.note-link').each(function () {
            if ($(this).hasClass('active_contact_side_bar')) {
                $(this).removeClass('active_contact_side_bar');
            }
        });
        $(this).parent().addClass('active_contact_side_bar');
    });

    //$('.classification_edit_btn').click(function () {
    $(document).delegate(".classification_edit_btn", "click", function(){
        var id = $(this).attr('index');
        var row_color_id_ = $("#row_color_id_"+id).text();
        var row_label_id_ = $("#row_label_id_"+id).text();
        if(id != ''){
            $(".js-text-color").val(row_color_id_);
            $(".js-text-label").val(row_label_id_);
            $(".js-text-id").val(id);
        }
    });


    $(document).delegate(".classification_del_btn", "click", function(){
        var id = $(this).attr('index');
        var row_id_ = $("#row_id_"+id);
        var confirm_message = confirm("Are you sure you want to delete this?");
        if(confirm_message==true)
        {
                $.ajax
                ({
                    dataType: 'json',
                    url:  baseUrl + "/contacts/ajax/deleteclassification",
                    type: "POST",
                    data: {delete_id:id},
                    success: function (result){
                        if(result.status = true){
                            row_id_.remove();
                        }
                    }
                });
        }
    });

    $(".id_classification").click(function(){
        $("#records-id_classification").val($(this).val());
    });

    $(".status_record").click(function(){
        $("#records-record_status").val($(this).val());
    });

    $(".icon_status_record").click(function(){
        $(".icon_status").val($(this).val());
    });

    $(".custom_image").click(function(){
        $(".custom_image_field").show();
    });

    $(".standard_image").click(function(){
        $(".custom_image_field").hide();
    });

    $('.slim_scroll').slimScroll({
        height: '250px',
        //color: '#62cb31',
        railVisible: true,
        railColor: '#aaa',
        railOpacity: 0.3,
        wheelStep: 20
    });

    $(".js-classification_submit_btn").click(function(){
        var text_color = $(".js-text-color").val();
        var text_label = $(".js-text-label").val();
        var text_id = $(".js-text-id").val();
        if(text_color != '' && text_label != ''){
            $.ajax
            ({
                dataType: 'json',
                url:  baseUrl + "/contacts/ajax/entryclassification",
                type: "POST",
                data: {text_color:text_color,text_label:text_label,text_id:text_id},
                success: function (result){
                    if(result.status == 1){
                        var row = '<tr id="row_id_' + result.id_classification+'"><td><span class="text-danger font-bold" id="row_color_id_'+result.id_classification+'">'+text_color+'</span></td>'+
                            '<td id="row_label_id_'+result.id_classification+'">'+text_label+'</td>'+
                            '<td><input id="record_status_'+result.id_classification+'" type="hidden" value="1"><span index="'+result.id_classification+'" class="classification_edit_btn" style="margin-right: 10px;"><i class="fa fa-pencil"></i></span>'+
                            '<span style="cursor: pointer" class="classification_del_btn" index="'+result.id_classification+'" id="delete_classifications_'+result.id_classification+'"><i class="fa fa-trash"></i></span></td></tr>';
                        $(".js-classifications-table tr:first").after(row);
                        $(".js-text-color").val('');
                        $(".js-text-label").val('');
                    } else if(result.status == 2){
                        $("#row_color_id_"+text_id).text(text_color);
                        $("#row_label_id_"+text_id).text(text_label);
                        $(".js-text-id").val('');
                        $(".js-text-color").val('');
                        $(".js-text-label").val('');
                    } else{

                    }
                }
            });
        } else{
            $('.js-modal-content').html('Please enter required field value');
            $('.js-modal-open').modal('show');
        }
    });

    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
    /*$('#serch-contact-records-button-folder').click(function(){
        var search_key = $('#serch-contact-records-value-folder').val();
        if(search_key !=''){
            window.location = baseUrl + '/contacts/records/folder?search=' + search_key;
        }
    })*/

    /*$('#expand-all').click(function(){
        $(this).hide('slow');
        $('#collapse-all').show('slow');
        $('.js-contact-list-folder').show('slow');
    });

    $('#collapse-all').click(function(){
        $(this).hide('slow');
        $('#expand-all').show('slow');
        $('.js-contact-list-folder').hide('slow');
    });*/

    $('.folder_view_expand_btn').click(function(){
        if($(this).html()=='Expand All')
            $(this).html('Collapse All');
        else
            $(this).html('Expand All');
    });

    $('.expand_agent').on('click', function(){
        $(this).next().slideToggle();
    });

    $('.expand_type').on('click', function(){
        $(this).next().slideToggle();
    });

    $("#contact-owner-list-button").click(function(){
        var data = $("#contact-owner-list").val();
        if(data){
            var tr = '';
            var contact_owner_code_list = '';
            $.each(data, function(key, item){
                var item_data = JSON.parse(item);                
                tr += "<tr><td>"+item_data.username+"</td><td>"+item_data.usercode+"</td></tr>"
                contact_owner_code_list += ','+item_data.working_code;
            });
            contact_owner_code_list = contact_owner_code_list.substring(1);
            $('#tbl-contact-owner-body').html(tr);
            $('#total-contact-owner-list').val(contact_owner_code_list);
            $('#myModal3').modal('hide');
        }else{
            $('#tbl-contact-owner-body').html('');
        }
    });

    type_enable_disable();
    //$('#type_checklist input[type=checkbox]').click(type_enable_disable);    //Shafiq's code
    $('.i-checks').on('ifChanged', function(event){
       type_enable_disable();
    });    
});

function type_enable_disable(){     
    if($('.i-checks').is(':checked')){
        $('.js-recoed-save-btn').removeAttr('disabled');
    } else{
        $('.js-recoed-save-btn').attr('disabled',true);
    }
    var types = $('#type_checklist input[type=checkbox]:checked').map(function () {
        return this.value;
    }).get();    
    $('label[for="records-business_name"]').html("Business Name");

    $('.field-records-person_in_charge').show();
    $('.field-records-role').show();
    $('.field-records-direct_mail').show();
    $('.field-records-direct_phone').show();

    $.each(['Company', 'Destination', 'Prospect', 'Person'], function( index, value ) {        
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="'+value+'"]').removeAttr('disabled');
    });

    $('.alert_msg').click(function() {
        $(this).fadeOut('fast');
    });


    if(jQuery.inArray('Company',types)>=0 || jQuery.inArray('Destination',types)>=0) {
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Prospect"]').attr('disabled', true);
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Person"]').attr('disabled', true);       
    }
    if(jQuery.inArray('Prospect',types)>=0) {
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Company"]').attr('disabled', true);
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Destination"]').attr('disabled', true);
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Person"]').attr('disabled', true);
    }
    if(jQuery.inArray('Person',types)>=0) {
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Company"]').attr('disabled', true);
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Destination"]').attr('disabled', true);
        $('#type_checklist input:checkbox[name="type_options\[\]"][value="Prospect"]').attr('disabled', true);

        $('label[for="records-business_name"]').html("Complete Name");

        $('.field-records-person_in_charge').hide();
        $('.field-records-role').hide();
        $('.field-records-direct_mail').hide();
        $('.field-records-direct_phone').hide();
    }
}

/**
 * For deleting contacts record data from Data table in contact-list page with ajax
 * */
function deleteTableRow(id){
    if(confirm("Do you really want to delete this?")){
        var parent = $(this).closest('tr');
        $.ajax({
            type: 'post',
            url: baseUrl + "/contacts/ajax/deletecontacsrecords",
            data: {id_record:id},
            dataType: 'json',
            success: function(status) {
                if(status.success){
                    location.reload();
                }
            }
        });
    }
    return false;
}
/**
 * get contact records as grid in flow page
 * @param data
 */
function getContactRecordGridData(data){
    //console.log(data);
    data['from_action'] = $('#action-id').val();
    data['limit'] = 24;
    data['offset']=$("#pagination-offset").val();
    $.ajax({
        'type': 'POST',
        'url': baseUrl + "/contacts/ajax/getcontacsrecords",
        'data': data,
        'dataType': 'json',
        'success': function(res){
            if(res.data!='')
                $('.js-contact-records-grid').html(res.data)
        }
    });
}
/**
 * get contact records grid data for pagination
 * @param offset
 */
function getContactRecordGridDataByPagination(offset){
    $("#pagination-offset").val(offset);
    if($('#filter-form-submitted').val() == 1){
        $('#submitted-by-click').val(0);
        $('#filter-contct-flow-form').trigger('submit');
    } else {
        var data = {};
        if($('#submitted-by-search-flow').val() == 1){
             data['search'] = $('#serch-contact-records-value-flow').val();
        }
        getContactRecordGridData(data)
    }
}

function add_to_connection_table(_this) {    
    var key = $(_this).closest('a').prop('id');
    var item = connection_search_data[key];
    var icon = getIconFromArrayVal(item, false, true);
       
    $('#connections-table-update').dataTable().fnAddData([  
        icon,
        item.code,
        item.business_name,
        item.city,
        item.country,
        "<a id="+item.id_record+"><i class=\"fa fa-minus\" onclick=\"delete_from_connection_table(this)\"></i></a>"
    ]);
    added_connection_records.push(item.id_record);
    $(_this).closest('tr').remove();
}

/*function add_to_connection_table2(key) {
    var item = connection_search_data[key];
    var row = "";
    row += "<tr id=\"connections_" + item.id_record + "\" rel=\"" + item.id_record + "\">";
    row +=      "<td>" + contact_type_icons[item.type] + "</td>";
    row +=      "<td>" + item.code + "</td>";
    row +=      "<td>" + item.business_name + "</td>";
    row +=      "<td>" + item.city + "</td>";
    row +=      "<td>" + item.country + "</td>";
    row +=      "<td><i class=\"fa fa-minus\" onclick=\"delete_from_connection_table(" + item.id_record + ")\"></i></td>";
    row += "</tr>";

    if($(".js-connections-table tbody tr:first").hasClass('connecction_no_res_msg')) {
        $(".js-connections-table tbody tr:first").remove();
    }

    $(".js-connections-table tbody").append(row);
    added_connection_records.push(item.id_record);
    $('#connections-table-update').dataTable();
}*/

function delete_from_connection_table2(_this) {
    if(confirm("Do you really want to delete this?")){
        var dataTable = $('#connections-table-update').dataTable();
        dataTable.fnDeleteRow($(_this).closest('tr'));
        var id_link = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deleteconnections?id_link="+id_link, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}

/*function delete_from_connection_table(id_link) {
    added_connection_records = removeItem(added_connection_records, id_link);
    $("#connections_"+id_link).remove();
    if( !($(".js-connections-table tbody tr:first").length) ) {
        var row = "";
        row += "<tr class=\"connecction_no_res_msg\">";
        row +=    "<td colspan=\"6\">No Connections.</td>";
        row += "</tr>";
        $(".js-connections-table tbody").append(row);
    }
}*/
function delete_from_connection_table(_this) {    
    var id_record = $(_this).closest('a').prop('id');
    added_connection_records = removeItem(added_connection_records, id_record);    
    var dataTable = $('#connections-table-update').dataTable();
    dataTable.fnDeleteRow($(_this).closest('tr'));   
}

function delete_from_documents_table(_this) {
    var id_document = $(_this).closest('a').attr('id');
    added_documents = removeItem(added_documents, id_document);
    var dataTable = $('#documents-table-update').dataTable();
    dataTable.fnDeleteRow($(_this).closest('tr'));
}

function delete_from_documents_table2(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_document = $(_this).closest('a').attr('id');
        added_activities = removeItem(added_activities, id_document);
        var dataTable = $('#documents-table-update').dataTable();
        dataTable.fnDeleteRow($(_this).closest('tr'));
        if(typeof id_document != 'undefined'){
            $.get(baseUrl + "/contacts/ajax/deletedocuments?id_document="+id_document, function(status){
                if(status.success){
                    $(_this).closest('tr').remove();
                }
            },'json');
        }else{
            alert(2);
        }

    }
    return false;
}

function delete_connection(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_link = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deleteconnections?id_link="+id_link, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}

function delete_document(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_document = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deletedocuments?id_document="+id_document, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}

function delete_order(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_order = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deleteorders?id_order="+id_order, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}

function delete_activity(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_activity = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deleteactivities?id_activity="+id_activity, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}


/*function add_to_activity_table2(key) {
    var item = activity_search_data[key];
    var row = "";
    row += "<tr id=\"activity_" + item.id_activity + "\" rel=\"" + item.id_activity + "\">";
    row +=      "<td>" + item.date_activity + "</td>";
    row +=      "<td>" + item.type + "</td>";
    row +=      "<td>" + item.subject + "</td>";
    row +=      "<td>" + item.description + "</td>";
    row +=      "<td>" + item.status + "</td>";
    row +=      "<td><i class=\"fa fa-minus\" onclick=\"delete_from_activity_table(" + item.id_activity + ")\"></i></td>";
    row += "</tr>";

    if($(".js-activity-table tbody tr:first").hasClass('activity_no_res_msg')) {
        $(".js-activity-table tbody tr:first").remove();
    }

    $(".js-activity-table tbody").append(row);
    added_activities.push(item.id_activity);
    $('#activities-table-update').dataTable();
}*/

function add_to_activity_table(_this) {   
    var key = $(_this).closest('a').prop('id');
    var item = activity_search_data[key];

    $('#activities-table-update').dataTable().fnAddData([
        item.date_activity,
        item.type,
        item.subject,
        item.description,
        item.status,
        "<a id="+item.id_activity+"><i class=\"fa fa-minus\" onclick=\"delete_from_activity_table(this)\"></i></a>"
    ]);
    added_activities.push(item.id_activity);
    $(_this).closest('tr').remove();
}

/*function delete_from_activity_table2(id_activity) {
    added_activities = removeItem(added_activities, id_activity);
    $("#activity_"+id_activity).remove();

    if( !($(".js-activity-table tbody tr:first").length) ) {
        var row = "";
        row += "<tr class=\"activity_no_res_msg\">";
        row +=    "<td colspan=\"6\">No Activities.</td>";
        row += "</tr>";
        $(".js-activity-table tbody").append(row);
    }
}*/



function delete_from_activity_table(_this) {
    var id_record = $(_this).closest('a').prop('id');
    added_activities = removeItem(added_activities, id_record);
    var dataTable = $('#activities-table-update').dataTable();
    dataTable.fnDeleteRow($(_this).closest('tr'));
}

/*function delete_from_activity_table(_this) {
    if(confirm("Do you really want to delete this?")){
        var id_record = $(_this).closest('a').prop('id');
        added_activities = removeItem(added_activities, id_record);
        var dataTable = $('#activities-table-update').dataTable();
        dataTable.fnDeleteRow($(_this).closest('tr'));
        var id_activity = $(_this).attr('id');
        $.get(baseUrl + "/contacts/ajax/deleteactivities?id_activity="+id_activity, function(status){
            if(status.success){
                $(_this).closest('tr').remove();
            }
        },'json');
    }
    return false;
}*/

function add_to_order_table(_this) {    
    var key = $(_this).closest('a').prop('id');
    var item = order_search_data[key];

    $('#sales-table-update').dataTable().fnAddData([
        item.order_code,
        item.date_order,
        item.type,
        item.subject,
        item.status,
        "<a id="+item.id_order+"><i class=\"fa fa-minus\" onclick=\"delete_from_orders_table(this)\"></i></a>"
    ]);
    added_orders.push(item.id_order);
    $(_this).closest('tr').remove();
}

/*function add_to_order_table(key) {
    var item = order_search_data[key];
    var row = "";
    row += "<tr id=\"sales_" + item.id_order + "\" rel=\"" + item.id_order + "\">";
    row +=      "<td>" + item.order_code + "</td>";
    row +=      "<td>" + item.date_order + "</td>";
    row +=      "<td>" + item.type + "</td>";
    row +=      "<td>" + item.subject + "</td>";
    row +=      "<td>" + item.status + "</td>";
    row +=      "<td><i class=\"fa fa-minus\" onclick=\"delete_from_orders_table(" + item.id_order + ")\"></i></td>";
    row += "</tr>";

    if($(".js-order-table tbody tr:first").hasClass('order_no_res_msg')) {
        $(".js-order-table tbody tr:first").remove();
    }

    $(".js-order-table tbody").append(row);
    added_orders.push(item.id_order);
    $('#sales-table-update').dataTable();
}*/

function delete_from_orders_table(_this) {
    var id_record = $(_this).closest('a').prop('id');
    added_orders = removeItem(added_orders, id_record);
    var dataTable = $('#sales-table-update').dataTable();
    dataTable.fnDeleteRow($(_this).closest('tr'));
}

/*function add_to_document_table2(key) {
    var item = document_search_data[key];
    var row = "";

    var cansend;
    var type_icon;
    switch(item.type) {
        case "gallery":
            type_icon = "<i class=\"fa fa-file-archive-o\"></i>"; break;
        case "txt":
            type_icon = "<i class=\"fa fa-file-text-o\"></i>"; break;
        case "application/msword":
            type_icon = "<i class=\"fa fa-file-word-o\"></i>"; break;
        case "html5":
            type_icon = "<i class=\"fa fa-file-code-o\"></i>"; break;
        case "application/vnd.ms-excel":
            type_icon = "<i class=\"fa fa-file-excel-o\"></i>"; break;
        case "pdf":
            type_icon = "<i class=\"fa fa-file-pdf-o\"></i>"; break;
        case "application/vnd.ms-powerpoint":
            type_icon = "<i class=\"fa fa-file-powerpoint-o\"></i>"; break;
        case "folder":
            type_icon = "<i class=\"fa fa-folder-o\"></i>"; break;
        case "movie":
            type_icon = "<i class=\"fa fa-file-video-o\"></i>"; break;
        case "image":
            type_icon = "<i class=\"fa fa-file-image-o\"></i>"; break;
        default:
            type_icon = "<i class=\"fa fa-file-o\"></i>"; break;
    }

    if(item.cansend) {
        cansend = "<i class=\"fa fa-check-square-o\"";
    } else {
        cansend = "<i class=\"fa fa-minus-square-o\"";
    }
    row += "<tr id=\"documents_" + item.id_media + "\" rel=\"" + item.id_media + "\">";
    row +=      "<td>" + type_icon + "</td>";
    row +=      "<td>" + item.type + "</td>";
    row +=      "<td>" + item.name + "</td>";
    row +=      "<td>" + item.description + "</td>";
    row +=      "<td>" + item.cansend + "</td>";
    row +=      "<td><a id="+item.id_media + "><i class=\"fa fa-minus\" onclick=\"delete_from_documents_table(this)\"></i></a></td>";
    row += "</tr>";

    if($(".js-document-table tbody tr:first").hasClass('document_no_res_msg')) {
        $(".js-document-table tbody tr:first").remove();
    }

    $(".js-document-table tbody").append(row);
    added_documents.push(item.id_media);
    $('#documents-table-update').dataTable();
}*/


function add_to_document_table(_this) {    
    var key = $(_this).closest('a').prop('id');
    var item = document_search_data[key];
    var type_icon;
    switch(item.type) {
        case "gallery":
            type_icon = "<i class=\"fa fa-file-archive-o\"></i>"; break;
        case "txt":
            type_icon = "<i class=\"fa fa-file-text-o\"></i>"; break;
        case "application/msword":
            type_icon = "<i class=\"fa fa-file-word-o\"></i>"; break;
        case "html5":
            type_icon = "<i class=\"fa fa-file-code-o\"></i>"; break;
        case "application/vnd.ms-excel":
            type_icon = "<i class=\"fa fa-file-excel-o\"></i>"; break;
        case "pdf":
            type_icon = "<i class=\"fa fa-file-pdf-o\"></i>"; break;
        case "application/vnd.ms-powerpoint":
            type_icon = "<i class=\"fa fa-file-powerpoint-o\"></i>"; break;
        case "folder":
            type_icon = "<i class=\"fa fa-folder-o\"></i>"; break;
        case "movie":
            type_icon = "<i class=\"fa fa-file-video-o\"></i>"; break;
        case "image":
            type_icon = "<i class=\"fa fa-file-image-o\"></i>"; break;
        default:
            type_icon = "<i class=\"fa fa-file-o\"></i>"; break;
    }

    if(item.cansend) {
        var cansend = "<i class=\"fa fa-check-square-o\"></i>";
    } else {
        var cansend = "<i class=\"fa fa-minus-square-o\"></i>";
    }

    $('#documents-table-update').dataTable().fnAddData([
        type_icon,
        item.type,
        item.name,
        item.description,
        cansend,
        "<a id="+item.id_media+"><i class=\"fa fa-minus\" onclick=\"delete_from_documents_table(this)\"></i></a>"
    ]);
    added_documents.push(item.id_media);
    $(_this).closest('tr').remove();
}


function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd   = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") {
        return;
    }

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=");
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) {
            parms[n] = [];
        }

        parms[n].push(nv.length === 2 ? v : null);
    }
    return parms;
}

function removeItem(array, item){
    for(var i = 0; i < array.length; i++){
        if(array[i]==item) {
            array.splice(i,1);
            i--; // Prevent skipping an item
        }
    }
    return array;
}

function flowPageSearch(){
    var serch_key = $('#serch-contact-records-value-flow').val();
    if(serch_key != ''){
        $('#submitted-by-search-flow').val(1);
        $('#filter-form-submitted').val(0);
        $('#submitted-by-click').val(0);
        var data={};
        data['search'] = serch_key;
        getContactRecordGridData(data);
    }
}

function commonDataTableCreation(selector,url){
     oTable = $(selector).DataTable({
        'bPaginate': true,
        //"bProcessing": true,
        "bServerSide": true,

        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        "iDisplayLength ": 10,
        "iDisplayStart ": 0,
        //"iDisplayEnd ": 0,
        "bSearchable": true,
        "sStart" : 0,
        "sLength" : 10000,
         "columnDefs": [
             /*{
                 "targets": [ 1 ],
                 "visible": false
             },*/
             {
                 orderable: false,
                 targets: -1
             }

         ],
        'fnServerData': function (sSource, aoData, fnCallback) {
            //aoData.push(data);
            //console.log(aoData);
            data['from_action'] = $('#action-id').val();
            data['limit'] = aoData[4].value;
            data['offset'] = aoData[3].value;
            data['search'] = aoData[5].value.value;
            data['sort'] = aoData[2].value[0];
            $.ajax
            ({
                'dataType': 'json',
                'type': 'POST',
                'url': url,
                'data': data,
                'success': fnCallback
            });
        }
        /*Row customization*/
        /*,
         "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
         //console.log(aData)
         $(nRow).attr("rel",aData[1]);
         }*/
    });

}

function closeDataTable(){
    if($.fn.DataTable.isDataTable('.js-contact-list-table') || $.fn.DataTable.isDataTable('.js-view-connections-table') || $.fn.DataTable.isDataTable('.js-view-document-table') || $.fn.DataTable.isDataTable('.js-view-order-table') || $.fn.DataTable.isDataTable('.js-view-activity-table')){
        oTable.destroy();
    }
}

function showOwnerList(){
    $("#contact-owner-view-modal").modal('show');
}

function openExtendedDataViewModal(){
    $('#extended-data-view-modal').modal('show');
}
/**/
 function getIconFromArrayVal(row, only_type, icon_size){
    var type = '';
    var icon = '';
    $.each(row, function(key, val) {
        if (fieldArrayExist(key)) {              
            if(val == 1){
                type = getTypeFromFild(key);
                if(only_type){
                    return type;
                }
                icon = convertTypeIntoImage(type, icon_size);                                                
            }
        }
    }); 
    return icon;    
}
function getTypeFromFild(key){
    var all_tbl_column = {
        'is_customer' : 'company',
        'is_destination' : 'destination',
        'is_prospect' : 'prospect',
        'is_contact' : 'person'
    };
    return all_tbl_column[key];
}
function fieldArrayExist(key){
    var fieldArray = ['is_customer','is_destination','is_prospect','is_contact'];
    if($.inArray(key,fieldArray)!== -1){
        return true;
    }else{
        return false;
    }
}
function convertTypeIntoImage(type, small){
    if(small){
        var type_to_image={
            "company": "<i class='fa fa-building-o'></i>",
            "destination": "<i class='fa fa-truck'></i>",
            "prospect": "<i class='fa fa-user-secret'></i>",
            "person": "<i class='fa fa-user'></i>",
            "gallery": "<i class = 'fa fa-file-archive-o'></i>",
            "txt": "<i class = 'fa fa-file-text-o'></i>",
            "application/msword": "<i class = 'fa fa-file-word-o'></i>",
            "html5": "<i class = 'fa fa-file-code-o'></i>",
            "application/vnd.ms-excel": "<i class = 'fa fa-file-excel-o'></i>",
            "pdf": "<i class = 'fa fa-file-pdf-o'></i>",
            "application/vnd.ms-powerpoint": "<i class = 'fa fa-file-powerpoint-o'></i>",
            "folder": "<i class = 'fa fa-folder-o'></i>",
            "movie": "<i class = 'fa fa-file-video-o'></i>",
            "image": "<i class = 'fa fa-file-image-o'></i>",
            "": "<i class = 'fa fa-file-o'></i>",
            "cansend-1": "<i class = 'fa fa-check-square-o'></i>",
            "cansend-0": "<i class = 'fa fa-minus-square-o'></i>",
            "1": "<i class='fa fa-minus-square-o'>",
            "contact-owner": "<i class='fa fa-arrows-alt fa-2x'></i>"
        };
    } else{
        var type_to_image={
            "company" : "<i class='fa fa-building-o fa-3x'></i>",
            "destination" : "<i class='fa fa-truck fa-3x'></i>",
            "prospect" : "<i class='fa fa-user-secret fa-3x'></i>",
            "person" : "<i class='fa fa-user fa-3x'></i>",
            "" : "<i class = 'fa fa-file fa-3x'></i>"
        };
    }    
    return type_to_image[type.toLowerCase()] ? type_to_image[type.toLowerCase()] : '';
}

function show_alert(success, msg) {
    if(success) {
        $('.js-modal-content').parent().removeClass('alert-danger').addClass('alert-success');
    } else {
        $('.js-modal-content').parent().removeClass('alert-success').addClass('alert-danger');
    }
    $('.js-modal-content').html(msg);
    $('.js-modal-open').modal('show');
}
