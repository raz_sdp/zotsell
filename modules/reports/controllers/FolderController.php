<?php

namespace app\modules\reports\controllers;

use app\modules\reports\models\ReportsTree;
use app\modules\reports\models\Media;
use app\modules\reports\models\MediaLinks;
use app\modules\reports\models\SearchRepositoryFiles;
use app\modules\reports\models\Accounts;
use app\modules\reports\models\ReportsTreeAccess;
//use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Controller for actions with folder
 */
class FolderController extends \yii\web\Controller
{

    /**
     * View information about this folder
     * @param type $id
     * @return type
     */
    public function actionView($id)
    {
        
        $model = ReportsTree::findOne($id);
        $mediasInFolder = [];
        foreach ($model->media as $media) {
            $mediasInFolder[] = [
                'id_media' => $media->id_media,
                'media-name' => $media->name,
                'media-type' => $media->type
            ];
        }

        $dataProviderMedia = new ArrayDataProvider([
            'allModels' => $mediasInFolder,
            'sort' => [
                'attributes' => ['media-name', 'media-type'],
            ],
        ]);

        // data for 'Users' tab
        if ($model->type == 'userFolder') {
            
            $users = [];
            foreach ($model->accounts as $account) {
                $user['id'] = $account->id_account;
                $user['user-username'] = $account->username;
                $user['user-usercode'] = $account->usercode;

                $users[] = $user;
                unset($user);
            }
            $dataProviderUsers = new ArrayDataProvider([
                'allModels' => $users,
                'sort' => [
                    'attributes' => ['user-username', 'user-usercode'],
                ],
            ]);
        }
        
        $activeTab = $this->detectActiveTab(Yii::$app->getRequest()->getQueryParams());
       
        return $this->render('view', [
            'model' => $model,
            'dataProviderMedia' => $dataProviderMedia,
            'dataProviderUsers' => (isset($dataProviderUsers)) ? $dataProviderUsers : null,
            'activeTab' => (isset($activeTab)) ? $activeTab : null,
        ]);
    }
    
    /**
     * Create folder
     */
    public function actionCreate()
    {
        if (!Yii::$app->getRequest()->getQueryParam('id_folder') && (Yii::$app->getRequest()->getQueryParam('type') != 'userFolder')) {
            return $this->redirect(['main/list']);
        }
        
        $idContainer = (Yii::$app->getRequest()->getQueryParam('id_folder')) ? Yii::$app->getRequest()->getQueryParam('id_folder') : 'newRoot';
        $folder = new ReportsTree();
        $folder->icon = ''; // standard icon by default
        $folder->scenario = 'create';

        if ($folder->load(Yii::$app->request->post())) {
            if ($folder->validate()) {
                if (Yii::$app->getRequest()->getQueryParam('type') && Yii::$app->getRequest()->getQueryParam('type') == 'userFolder') { //create root folder
                    $folder->makeRoot();
                } else { //create normal folder inside current folder
                    $parentFolder = ReportsTree::findOne(['id_report_tree' => $idContainer]);
                    $folder->appendTo($parentFolder);
                }
                //$folder->save(false);

                return $this->redirect(['update', 'id' => $folder->getPrimaryKey()]);
            } else {
                $errors['folder'] = $folder->errors;
                Yii::trace("Errors when save folder: " . print_r($errors, true));
            }
        }

        return $this->render('create', [
            'model' => $folder,
            'idContainer' => $idContainer,
        ]);
    }

    /**
     * Update existing model ReportsTree
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        Yii::trace('get params:'.print_r(Yii::$app->getRequest()->getQueryParams(),true));
        $model = $this->findModel($id);

        $model->scenario = 'create';

        if ($model->type == 'userFolder') { // build arrays for moving users into folder
            //isset tab param => to do active tab
            if(Yii::$app->getRequest()->getQueryParam('tab') && in_array(Yii::$app->getRequest()->getQueryParam('tab'), ['default', 'users'])) {
                $activeTab = Yii::$app->getRequest()->getQueryParam('tab');
                Yii::trace('activeTab:'.print_r($activeTab,true));
            }

            $users = Accounts::find()->orderBy(['username' => SORT_ASC])->asArray()->all();

            $selectedUsers = [];
            ArrayHelper::multisort($users, 'username', SORT_ASC); //sort users by username

            $usersAll = ArrayHelper::map($users, 'id_account', 'username');

            foreach ($model->accounts as $account) {
                $selectedUsers[$account->id_account] = $account->username;
            }
        }


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id_report_tree]);
        }


        $folderParent = $model->parents(1)->one();

        return $this->render('create', [
            'model' => $model,
            'idContainer' => ($folderParent) ? $folderParent->id_report_tree : $model->id_report_tree, // parent folder id or current folder's id
            'usersAll' => (isset($usersAll)) ? $usersAll : null,
            'selectedUsers' => (isset($selectedUsers)) ? $selectedUsers : null,
            'activeTab' => (isset($activeTab)) ? $activeTab : null,
        ]);
    }

    /**
     * Update user's list in current userFolder
     * @return type
     */
    public function actionUpdateReportsAccess()
    {
        $postData = Yii::$app->getRequest()->post();

        $idFolder = (isset($postData['idFolder'])) ? $postData['idFolder'] : null;

        if (is_null($idFolder))
            return $this->redirect(['main/list']);

        $model = $this->findModel($idFolder);

        $selectedUsersBefore = []; // array of users in this folder before update (move)
        foreach ($model->accounts as $account) {
            $selectedUsersBefore[$account->id_account] = $account->username;
        }

        $errors = [];

        $selectedUsersAfter = (isset($postData['multiForm'])) ? $postData['multiForm'] : []; //array of users after move

        foreach ($selectedUsersBefore as $id => $name) {
            if (!in_array($id, $selectedUsersAfter)) { //user doesn't exist in folder after moving => delete user from folder
                $modelDel = $this->findReportsTreeAccess($idFolder, $id);
                if ($modelDel)
                    $modelDel->delete();
                else
                    $errors[] = 'Don\'t remove from folder user with id =' . $id;
            } else
                $selectedUsersAfter = $this->removeArrayElement($selectedUsersAfter, $id); //remove deleted user id
        }

        if (count($selectedUsersAfter) > 0) {//need to save users to folder
            foreach ($selectedUsersAfter as $userId) {
                $model = new ReportsTreeAccess();
                $model->id_report_tree = $idFolder;
                $model->id_account = $userId;

                if (!$model->save())
                    $errors[] = 'Failed to save the user with id =' . $idMediaAdd . ' to folder ' . $idFolder;
            }
        }

        if (count($errors) > 0)
            Yii::trace("Move users errors: " . print_r($errors));

        return $this->redirect(['update', 'id' => $idFolder]);
    }

    /**
     * Delete media by primary key $id
     * and delete related media translation models via Media::beforeDelete()
     * @param $id value of database field 'id_media'
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model) {
            if ($model->type == 'userFolder') {
                $model->deleteWithChildren(); //delete root folder
            } else
                $model->delete(); //delete normal folder
        }

        return $this->redirect(['main/list']);
    }

    /**
     * Delete users from folder (click 'Remove button' on 'Users' section)
     * @param type $id folder id
     * @return type
     */
    public function actionDeleteUsers($id)
    {
        $model = $this->findModel($id);
        if ($model->type == 'userFolder') {
            foreach ($model->reportsTreeAccess as $userAccess) {
                $userAccess->delete();
            }
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    /** Find Folder model by primary key $id
     * @param $id value of database field 'id_media'
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ReportsTree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Generate data for form in Modal with medias for moving
     * @param type $id
     * @return type
     */
    public function actionMoveFiles($id, $type)
    {
        $model = ReportsTree::findOne(['id_report_tree' => $id]);

        $typesArray = SearchRepositoryFiles::getTypeMimeTypeArray($type); //contains mime-types and GET type

        /*
        $mediaAll = Media::find()
        ->joinWith('mediaLinks')
        //->where(['context' => 4])
        ->andWhere(['type' => $typesArray])
        ->asArray()->all(); // all available media
        */
        $mediaAllQuery = Media::find()
        ->joinWith('mediaLinks');
        
        $mediaInFolderQuery = Media::find()
        ->joinWith('mediaLinks')
        ->where(['id_container' => $id]);
        
        if ($type != 'file') { // choose files with current type (for type 'file' choose any type)
            $mediaAllQuery = $mediaAllQuery->andWhere(['type' => $typesArray]);
            $mediaInFolderQuery->andWhere(['type' => $typesArray]);
        }
        
        $mediaAll = $mediaAllQuery->asArray()->all();
        $mediaInFolder = $mediaInFolderQuery->asArray()->all(); //media in current folder

        $items = [];
        $selection = [];

        foreach ($mediaAll as $media) {
            $items[$media['id_media']] = $media['name'];
        }
        foreach ($mediaInFolder as $mediaFolder) {
            $selection[$mediaFolder['id_media']] = $mediaFolder['name'];
        }

        return $this->renderAjax('_moveFiles', [
            'model' => $model,
            'items' => $items,
            'selection' => $selection,
            'mediaType' => $type
        ]);
    }

    /**
     * Save moved files (from modal)
     * @return type json_encode array of error texts or true (success)
     */
    public function actionSaveMovedFiles()
    {
        $errors = [];
        $postData = Yii::$app->getRequest()->post();

        $idFolder = $postData['ReportsTree']['id_report_tree'];

        if (!isset($postData['mediaType']))
            $errors[] = 'Undefined media type';

        $typesArray = SearchRepositoryFiles::getTypeMimeTypeArray($postData['mediaType']); //contains mime-types and GET type

        $mediaInFolderBeforeQuery = Media::find()
        ->joinWith('mediaLinks')
        ->where(['id_container' => $idFolder]);
               
        if ($postData['mediaType'] != 'file') { // choose files with current type (for type 'file' choose any type)
            $mediaInFolderBeforeQuery = $mediaInFolderBeforeQuery->andWhere(['type' => $typesArray]);
        }
        $mediaInFolderBefore = $mediaInFolderBeforeQuery->all(); //folder's media with this type before moving
        
        $mediaInFolderAfter = (isset($postData['multiForm'])) ? $postData['multiForm'] : []; //folder's media with this type after moving

        foreach ($mediaInFolderBefore as $mediaBefore) {
            if (!in_array($mediaBefore->id_media, $mediaInFolderAfter)) { //media doesn't exist in folder after moving => delete media
                $modelDel = $this->findMediaLinksModel($mediaBefore->id_media, $idFolder);
                if ($modelDel)
                    $modelDel->delete();
                else
                    $errors[] = 'Don\'t remove media with id =' . $mediaBefore->id_media;
            } else
                $mediaInFolderAfter = $this->removeArrayElement($mediaInFolderAfter, $mediaBefore->id_media);
        }


        if (count($mediaInFolderAfter) > 0) {//need to save this media in folder
            foreach ($mediaInFolderAfter as $idMediaAdd) {
                $model = new MediaLinks();
                $model->id_media = $idMediaAdd;
                $model->id_container = $idFolder;
                //$model->context = 4;

                if (!$model->save())
                    $errors[] = 'Failed to save the media with id =' . $idMediaAdd;
            }
        }

        if (count($errors) > 0)
            return json_encode($errors);

        $success = true;
        return json_encode($success);
    }

    /**
     * Remove array's element by value
     * @param $array
     * @param $element
     * @return array
     */
    public function removeArrayElement($array, $element)
    {
        return array_diff($array, [$element]);
    }

    /**
     * Find ReportsTreeAccess model based on $idFolder and $idAccount
     * @param type $idFolder
     * @param type $idAccount
     * @return boolean false | ReportsTreeAccess object
     */
    protected function findReportsTreeAccess($idFolder, $idAccount)
    {
        if (($model = ReportsTreeAccess::findOne(['id_report_tree' => $idFolder, 'id_account' => $idAccount])) !== null) {
            return $model;
        } else {
            return false; //throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Finds the MediaLinks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return MediaLinks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMediaLinksModel($id_media, $id_container)
    {
        if (($model = MediaLinks::findOne(['id_media' => $id_media, 'id_container' => $id_container])) !== null) {
            return $model;
        } else {
            return false;//throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Detect active tab by sort params ('default' or 'users')
     * @param type $sortParams
     * @return string
     */
    public function detectActiveTab($sortParams)
    {
        if (!isset($sortParams['sort'])) return 'default';
        
        $sort = explode('-', $sortParams['sort']); 
        $sortFilter = array_filter($sort);
        
        if (array_shift($sortFilter) == 'user') return 'users';

        return 'default';
    }

}
