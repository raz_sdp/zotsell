<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/26/16
 * Time: 11:52 PM
 */
?>
<div class="hpanel">
    <div class="panel-body">
        <div class="m-b-md">
            <h4>
                Filters
            </h4>
            <small>
                Filter your data based on different options below.
            </small>
        </div>

    <?php $form_id="filter-contct-".yii::$app->controller->action->id."-form"?>
    <form id="<?php echo $form_id?>" action="javascript:void(0)">
        <div class="form-group">
            <label class="control-label">Classification:</label>

            <div class="input-group">
                <select class="form-control m-b" name="classification">
                    <option value="" selected>All</option>
                    <?php foreach($classification_list as $list){
                        echo "<option value=".$list['id_classification'].">".$list['label']."</option>";
                    }?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label">Status:</label>

            <div class="input-group">
                <select class="form-control m-b" name="status">
                    <option value="" selected>All</option>
                    <option value="1">Active</option>
                    <option value="0">Blocked</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label">Type:</label>

            <div class="input-group">
                <select class="form-control m-b" name="type">
                    <option value="" selected>All</option>
                    <option value="Company">Company</option>
                    <option value="Destination">Destination</option>
                    <option value="Prospect">Prospect</option>
                    <option value="Person">Person</option>
                </select>
            </div>
        </div>


        <!--<div class="form-group">
            <label class="control-label">Genre:</label>

            <div class="input-group">
                <select class="form-control m-b" name="genre">
                    <option value="" selected>All</option>
                    <option value="companies">Companies</option>
                    <option value="people">People</option>
                </select>
            </div>
        </div>-->


        <!--<div class="form-group">
            <label class="control-label">Favourites:</label>
            <div class="input-group">
                <div class="checkbox checkbox-primary">
                    <input checked id="checkbox1" type="checkbox" name="favourites">
                    <label for="checkbox1">
                        Favourites
                    </label>
                </div>
            </div>
        </div>-->


        <button type="submit" id="contact-records-filter-button" class="btn btn-success btn-block">Apply</button>
    </form>
    </div>
</div>