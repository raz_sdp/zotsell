<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Documents';

$this->params['headerTitle'] = "Documents Folder \"{$model->label}\" Create/update";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->title = 'Documents';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$parents = $model->parents()->all();
foreach ($parents as $parentFolder) {
    $this->params['breadcrumbs'][] = [
        'label' => $parentFolder->label,
        'url' => ['folder/view', 'id' => $parentFolder->id_folder]
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => ($model->label) ? $model->label : 'Folder name',
    'template' => "<li><b>{link}</b></li>\n"
];

?>

    <div class="col-md-3">
        <div class="hpanel panel-group">
            <div class="panel-body">
                <div class="text-center text-muted font-bold">Select translation or add new</div>

            </div>
            <div class="panel-section">

                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Select Area...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i
                            class="glyphicon glyphicon-plus small"></i></button>
                </span>
                </div>
                <button type="button" data-toggle="collapse" data-target="#notes"
                        class="btn-sm visible-xs visible-sm collapsed btn-default btn btn-block m-t-sm">
                    All notes <i class="fa fa-angle-down"></i>
                </button>
            </div>

            <div id="notes" class="collapse">
                <div class="panel-body note-link active_contact_side_bar">
                    <a href="#default" data-toggle="tab">
                        <small class="pull-right text-muted">WORLD</small>
                        <h5>Default</h5>

                        <div class="small">
                            <strong><?= $model->label ?></strong>
                        </div>
                        <div class="small">
                            <?= $model->description ?>
                        </div>
                    </a>
                </div>

                <?php foreach ($folderTranslations as $key => $languageRelation): ?>
                    <div class="panel-body note-link <?php if (!$model->id_folder) echo 'disabledTab'; ?>">
                        <a href="#<?= $key ?>" data-toggle="tab">
                            <small
                                class="pull-right text-muted"><?php if (isset($languageRelation['country'])) echo $languageRelation['country']; ?></small>
                            <h5><?php if (isset($languageRelation['language'])) echo $languageRelation['language'] ?></h5>

                            <div class="small">
                                <strong><?= $languageRelation['model']->label; ?></strong>
                            </div>
                            <div class="small">
                                <?= $languageRelation['model']->description; ?>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>

    <div class="col-md-9">
        <div class="hpanel">

            <div class="panel-body">

                <div class="text-center hidden">
                    We couldn't find any Document. Add one
                </div>

                <div class="tab-content">
                    <div id="default" class="tab-pane active">
                        <div class="pull-right text-muted m-l-lg">
                            WORLD
                        </div>
                        <h3>Default</h3>
                        <hr/>
                        <div class="note-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?=
                                    $this->render('_createForm', [
                                        'model' => $model,
                                        'idContainer' => $idContainer,
                                    ]);
                                    ?>
                                </div>
                            </div>

                        </div>

                        <?php $formDelete = ActiveForm::begin(['id' => 'delete-folder', 'method' => 'GET', 'action' => ['delete']]); ?>
                        <?= Html::hiddenInput('id', $model->id_folder); ?>
                        <?php ActiveForm::end(); ?>

                        <div class="btn-group">
                            <button class="btn btn-sm btn-default" form="create-folder"><i
                                    class="fa fa-thumbs-o-up"></i> Save
                            </button>
                            <button class="btn btn-sm btn-default" form="delete-folder"><i class="fa fa-trash"></i>
                                Remove
                            </button>
                        </div>
                    </div>


                    <?php foreach ($folderTranslations as $key => $languageRelation): ?>
                        <div id="<?= $key ?>" class="tab-pane">
                            <div class="pull-right text-muted m-l-lg">
                                <?= $languageRelation['country'] ?>
                            </div>
                            <h3><?= $languageRelation['language'] ?></h3>
                            <hr/>
                            <div class="note-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?=
                                        $this->render('_createTranslationForm', [
                                            'key' => $key,
                                            'folder' => $model,
                                            'model' => $languageRelation['model'],
                                        ]);
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <?php $formDelete = ActiveForm::begin(['id' => "delete-folderTr-{$key}", 'method' => 'GET', 'action' => ['delete-translation', 'id' => $model->id_folder, 'lang' => $key]]); ?>
                            <?= Html::hiddenInput('id', $model->id_folder); ?>
                            <?php ActiveForm::end(); ?>

                            <div class="btn-group">
                                <button class="btn btn-sm btn-default" form="create-folderTr-<?= $key ?>"><i
                                        class="fa fa-thumbs-o-up"></i> Save
                                </button>
                                <button class="btn btn-sm btn-default" form="delete-folderTr-<?= $key ?>"><i
                                        class="fa fa-trash"></i>
                                    Remove
                                </button>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

        </div>
    </div>

<?php
$js = <<<JS
/* bind clicks on the labels */
/*$('#icon_block').find('label').click(function(e) {
    //e.preventDefault();
    console.log(this);
    $(this).closest('.note-content').find('.create-update-media').yiiActiveForm('validateAttribute', 'icon_block');
});
*/
// event after upoading icon image
function eventAfterIconUpload(file){
$('#radio_custom').val(file.name);
//$('#create-media').yiiActiveForm('validateAttribute', 'icon_block'); // before
$('#icon_block').closest('.note-content').find('.create-update-folder').yiiActiveForm('validateAttribute', 'icon_block'); // validate Icon radio input after uploading
}

// active current language in tranlsation block
$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
});
JS;

$this->registerJs($js, \yii\web\View::POS_READY); //, 'projects-filter-form-script'
?>