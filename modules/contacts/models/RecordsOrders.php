<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_orders".
 *
 * @property integer $id_order
 * @property string $record_code
 * @property string $record_destination_code
 * @property string $ipad_udid
 * @property string $agent_code
 * @property string $date_access
 * @property string $date_order
 * @property string $status
 * @property string $management_status
 * @property string $inserted_by
 * @property string $order_code
 * @property string $management_code
 * @property string $management_external_ref
 * @property string $subject
 * @property string $description
 * @property string $data
 * @property string $sign_image
 * @property string $link_pdf
 * @property string $transfer_message
 * @property string $type
 * @property string $entity_id
 * @property string $user
 * @property string $date_modification
 * @property integer $record_status
 * @property string $parent_id
 * @property string $document_type
 * @property string $document_type_id
 *
 * @property ZseV1Records $recordCode
 */
class RecordsOrders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_access', 'date_order', 'date_modification'], 'safe'],
            [['data', 'sign_image'], 'string'],
            [['record_status'], 'integer'],
            [['record_code', 'status', 'management_status'], 'string', 'max' => 32],
            [['record_destination_code', 'inserted_by', 'order_code', 'type', 'entity_id', 'parent_id', 'document_type', 'document_type_id'], 'string', 'max' => 64],
            [['ipad_udid', 'agent_code', 'subject', 'description', 'link_pdf', 'transfer_message', 'user'], 'string', 'max' => 255],
            [['management_code', 'management_external_ref'], 'string', 'max' => 128],
            [['order_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_order' => 'Id Order',
            'record_code' => 'Record Code',
            'record_destination_code' => 'Record Destination Code',
            'ipad_udid' => 'Ipad Udid',
            'agent_code' => 'Agent Code',
            'date_access' => 'Date Access',
            'date_order' => 'Date Order',
            'status' => 'Status',
            'management_status' => 'Management Status',
            'inserted_by' => 'Inserted By',
            'order_code' => 'Order Code',
            'management_code' => 'Management Code',
            'management_external_ref' => 'Management External Ref',
            'subject' => 'Subject',
            'description' => 'Description',
            'data' => 'Data',
            'sign_image' => 'Sign Image',
            'link_pdf' => 'Link Pdf',
            'transfer_message' => 'Transfer Message',
            'type' => 'Type',
            'entity_id' => 'Entity ID',
            'user' => 'User',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
            'parent_id' => 'Parent ID',
            'document_type' => 'Document Type',
            'document_type_id' => 'Document Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecordCode()
    {
        return $this->hasOne(ZseV1Records::className(), ['code' => 'record_code', 'code_destination' => 'record_destination_code']);
    }

    public function searchOrdersByKeyword($keyword) {
        return RecordsOrders::find()
                ->Where(['LIKE', 'zse_v1_orders.subject', $keyword])
                ->orWhere(['LIKE', 'zse_v1_orders.description', $keyword])
                ->orWhere(['LIKE', 'zse_v1_orders.order_code', $keyword])
                ->all(); 
    }

    /**
     * function for getting order list of a contact
     * @param $field_list
     * @param $where
     * @param array $filter_where
     * @param null $limit
     * @param null $offset
     * @param string $sort
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getContactOrders($field_list, $where, $filter_where = [], $limit= null, $offset = null, $sort = ""){
        $orders = RecordsOrders::find()
            ->select($field_list)
            ->where($where)
            ->andFilterWhere($filter_where)
            ->limit($limit)
            ->offset($offset)
            ->orderBy($sort)
            ->all();
        return $orders;
    }

    /**
     * function for getting count of orders of a contact
     * @param $where
     * @param array $filter_where
     * @return int|string
     */
    public static function countContactOrders($where, $filter_where = []){
        $count = RecordsOrders::find()
            ->where($where)
            ->andFilterWhere($filter_where)
            ->count();

        return $count;
    }

}
