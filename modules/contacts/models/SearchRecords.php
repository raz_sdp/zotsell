<?php

namespace app\modules\contacts\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\contacts\models\Records;

/**
 * SearchRecords represents the model behind the search form about `app\modules\contacts\models\Records`.
 */
class SearchRecords extends Records
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_record', 'id_locality', 'id_classification', 'is_contact', 'is_customer', 'is_supplier', 'is_prospect', 'is_destination', 'is_reseller', 'is_intrested', 'authorize_send', 'record_status'], 'integer'],
            [['code', 'code_destination', 'business_name', 'legal_status', 'type', 'address', 'email', 'phone', 'fax', 'mobile', 'inserted_by', 'assigned_to', 'origin', 'coordinates', 'external_code', 'icon', 'country', 'province', 'city', 'postal_code', 'vat_code', 'fiscal_code', 'pricelist', 'options', 'person_in_charge', 'role', 'direct_mail', 'direct_phone', 'date_modification'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Records::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_record' => $this->id_record,
            'id_locality' => $this->id_locality,
            'id_classification' => $this->id_classification,
            'is_contact' => $this->is_contact,
            'is_customer' => $this->is_customer,
            'is_supplier' => $this->is_supplier,
            'is_prospect' => $this->is_prospect,
            'is_destination' => $this->is_destination,
            'is_reseller' => $this->is_reseller,
            'is_intrested' => $this->is_intrested,
            'authorize_send' => $this->authorize_send,
            'date_modification' => $this->date_modification,
            'record_status' => $this->record_status,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'code_destination', $this->code_destination])
            ->andFilterWhere(['like', 'business_name', $this->business_name])
            ->andFilterWhere(['like', 'legal_status', $this->legal_status])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'inserted_by', $this->inserted_by])
            ->andFilterWhere(['like', 'assigned_to', $this->assigned_to])
            ->andFilterWhere(['like', 'origin', $this->origin])
            ->andFilterWhere(['like', 'coordinates', $this->coordinates])
            ->andFilterWhere(['like', 'external_code', $this->external_code])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'province', $this->province])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code])
            ->andFilterWhere(['like', 'vat_code', $this->vat_code])
            ->andFilterWhere(['like', 'fiscal_code', $this->fiscal_code])
            ->andFilterWhere(['like', 'pricelist', $this->pricelist])
            ->andFilterWhere(['like', 'options', $this->options])
            ->andFilterWhere(['like', 'person_in_charge', $this->person_in_charge])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'direct_mail', $this->direct_mail])
            ->andFilterWhere(['like', 'direct_phone', $this->direct_phone]);

        return $dataProvider;
    }
}
