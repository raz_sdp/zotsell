<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use fedemotta\datatables\DataTables;


$this->title = 'List views';

// module root
$this->params['breadcrumbs'][] = [
    'label' => "Documents",
    'url' => ['']
];


if (isset($folder['id_folder']) && $folder['id_folder'] > 0) {
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

    $parents = $folder->parents()->all();
    foreach ($parents as $parentFolder) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentFolder->label,
            'url' => ['main/list', 'id_folder' => $parentFolder->id_folder]
        ];
    }

    $this->params['breadcrumbs'][] = [
        'label' => $folder['label'],
        'template' => "<li><b>{link}</b></li>\n"
    ];
} else {
    $this->params['breadcrumbs'][] = $this->title;
}

$this->params['headerTitle'] = "Documents List View";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list.";

$this->params['newEntities'] = $newEntities; // for dropdown list on click button NEW

//$searchModel = new app\modules\documents\models\MediaFolderSearch();//for testing
//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>

<div class="col-md-3">
    <div class="hpanel">
        <?= $this->render(
            'documentsMenu'
        ) ?>
    </div>
    <div class="hpanel">
        <div class="panel-body">
            <div class="m-b-md">
                <h4>
                    Filters
                </h4>
                <small>
                    Filter your data based on different options below.
                </small>
            </div>

            <?= Html::beginForm(['main/list'], 'GET', ['id' => 'filter-form']); ?>
            <div class="form-group">
                <label class="control-label">Type:</label>

                <div class="input-group">
                    <?= Html::dropDownList('type', (Yii::$app->getRequest()->getQueryParam('type')) ? Yii::$app->getRequest()->getQueryParam('type') : 0,
                        [
                            'all' => 'All',
                            'files' => 'Files',
                            'folder' => 'Folders',
                            '---' => '---',
                            'pdf' => 'pdf',
                            'movie' => 'movie',
                            'html5' => 'html5',
                            'gallery' => 'gallery',
                            'application/vnd.ms-powerpoint' => 'powerpoint',
                            'application/vnd.ms-excel' => 'excel',
                            'application/msword' => 'word',
                        ],
                        [
                            'class' => 'form-control m-b',
                        ]); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Status:</label>

                <div class="input-group">
                    <div class="checkbox checkbox-primary">
                        <?= Html::checkbox(
                            'sharable',
                            (Yii::$app->getRequest()->getQueryParam('sharable')) ? true : false,
                            ['id' => 'sharable']) ?>
                        <label for="sharable">
                            Sharable
                        </label>
                    </div>
                </div>
            </div>

            <?= Html::submitButton('Apply', ['class' => 'btn btn-success btn-block']); ?>

            <?= Html::endForm(); ?>

        </div>
    </div>

</div>

<div class="col-md-9">
    <div class="hpanel">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                <a class="closebox"><i class="fa fa-times"></i></a>
            </div>
            Standard table
        </div>

        <div class="panel-body">
            <?php
            /*
            $searchModel = new ModelSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            */
            ?>
            <?= DataTables::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable no-footer',
                ],
                'clientOptions' => [
                    'dom' => 'lftrip',
                    'searching' => true,
                    'paging' => true,
                    'aaSorting' => [],
                    'language' => [
                        "search" => "Search:"
                    ],
                    /*'columns' => [
                        null,
                        null,
                        null,
                        null,
                        [
                           'orderable'=>false //https://datatables.net/reference/option/columns.orderable
                        ]],
                    */
                    'columnDefs' => [
                        [
                            "type" => "num-html", "targets" => 0
                        ],
                        [
                            "type" => "num-html", "targets" => 3
                        ],
                        [
                            "type" => "file-size",
                            //"orderable"=> false,
                            "targets" => 4
                        ],
                        [
                            "orderable" => false,
                            "targets" => -1
                        ]
                    ]
                ],
                //'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'icon',
                        'format' => 'html',
                        'value' => function ($model) {
                            return $model['icon'];
                            //return Html::img(Yii::getAlias('@web').'/images/'. $data['image'], ['width' => '70px']); //for images instead icons
                        }
                    ],

                    [

                        //'header' => '<span class="glyphicon glyphicon-user"></span>',
                        'attribute' => 'name',
                        'format' => 'html',
                        'value' => function ($model) {
                            if ($model['type'] == 'folder') return Html::a($model['name'], ['list', 'id_folder' => $model['id']], ['class' => 'list-link']); //'nested'
                            else return $model['name'];
                        }
                    ],
                    'type',
                    [
                        'attribute' => 'sharable',
                        'format' => 'html',
                        'value' => function ($model) {
                            switch ($model['sharable']) {
                                case 0:
                                    return '<i class="fa fa-square-o"></i>';
                                    break;
                                case 1:
                                    return '<i class="fa fa-check-square-o"></i>';
                                    break;
                                case 2:
                                    return '<i class="fa fa-minus-square-o"></i>';
                                    break;
                            }
                        }
                    ],
                    [
                        'attribute' => 'size',
                        'value' => function ($model) {
                            return ($model['size']) ? $model['size'] . ' kb' : '0 b';
                        }
                    ],
                    [
                        'header' => 'Actions',
                        'headerOptions' => ['class' => ''],
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{download} {view} {update} {delete}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                if ($model['type'] == 'folder') $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-eye"></span>',
                                    [$uri . '/view', 'id' => $model['id']]
                                );
                            },
                            'update' => function ($url, $model) {
                                if ($model['type'] == 'folder') $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-pencil"></span>',
                                    [$uri . '/update', 'id' => $model['id']]
                                );
                            },
                            'download' => function ($url, $model) {
                                if ($model['type'] == 'folder') return Html::tag('span', '', ['class' => "fa fa-download"]);
                                else {
                                    if (isset($model['repFile'])) {
                                        return Html::a(
                                            '<span class="fa fa-download"></span>',
                                            ['/uploads/' . $model['repFile']]
                                        );
                                    }
                                    return Html::tag('span', '', ['class' => "fa fa-download"]);
                                }

                            },
                            'delete' => function ($url, $model) {
                                if ($model['type'] == 'folder') $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-trash"></span>',
                                    [$uri . '/delete', 'id' => $model['id']]
                                );
                            },
                        ],
                    ],
                ],

            ]); ?>
        </div>


    </div>


</div>
