<?php

$this->title = 'Folders views';

// module root
$this->params['breadcrumbs'][] = [
    'label' => "Documents",
    'url' => ['main/list']
];

$this->params['breadcrumbs'][] = $this->title;


$this->params['headerTitle'] = "Documents Folders View";
$this->params['headerDescription'] = "Use Folders view to show all documents in a tree .";

$this->params['newEntities'] = $newEntities; // for dropdown list on click button NEW

?>

    <div class="col-md-3">
        <div class="hpanel">
            <?=
            $this->render(
                'documentsMenu.php'
            )
            ?>
        </div>
    </div>

    <div class="col-md-9">
        <div class="row">
            <div class="col-lg-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <?= $this->render(
                            'searchForm.php'
                        ) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" id="body-result">
            <div class="hpanel">
                <div class="panel-heading hbuilt">
                    <div id="nestable-menu" class="pull-right">
                        <button type="button" data-action="expand-all" class="btn btn-default btn-xs">Expand All
                        </button>
                        <button type="button" data-action="collapse-all" class="btn btn-default btn-xs">Collapse All
                        </button>
                    </div>
                    Document Folders
                </div>


                <div class="panel-body">
                    <div class="dd" id="nestableFolders">
                        <?php
                        echo yii\widgets\Menu::widget([
                            'options' => ['class' => 'dd-list', 'tag' => 'ol'],
                            'encodeLabels' => false,
                            'submenuTemplate' => "\n<ol 'class'='dd-list'>\n{items}\n</ol>\n",
                            'labelTemplate' => "<div class='dd-nodrag'>{label}</div>",
                            'items' => $folders,
                            'itemOptions' => ['class' => 'dd-item'],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/scripts/folders_nestable.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/scripts/ajax-modal-popup.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/scripts/ajax-move-files.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->registerCssFile("/homer/js/lou-multi-select-05449b3/css/multi-select.css", [
    'depends' => [\yii\bootstrap\BootstrapAsset::className()],
]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/quicksearch/jquery.quicksearch.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/homer/js/lou-multi-select-05449b3/js/jquery.multi-select.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>