<?php

namespace app\modules\documents;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\documents\controllers';
    public $defaultRoute = 'main';
    //public $layout = 'doc';


    public function init()
    {
        parent::init();

        $this->layout = 'main';
    }
}
