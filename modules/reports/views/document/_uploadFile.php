<?php ?>
<div class="col-lg-4">
    <h3> Upload file</h3>

    <div class="upload-drop-zone" id="drop-zone-<?= $key ?>">
<?php
echo \kato\DropZone::widget([
    'id' => 'fileUploader_' . $key,
    'dropzoneContainer' => 'fileUploader_' . $key,
    'previewsContainer' => 'previewsFile_' . $key,
    'options' => $dropZoneConfig,
    'clientEvents' => [
        'complete' => "function(file){console.log(file)}",
        'removedfile' => "function(file){alert(file.name + ' is removed')}"
    ],
]);
?>
    </div>

</div>
