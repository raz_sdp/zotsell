// set checked inputs in gridview by values from textarea field Choosen files
function setCheckedColumns(gridObject, fileNames) {
    fileNames.forEach(function (entity) {
        gridObject.find("input[value=\'" + entity + "\']").prop("checked", true);
    });
}
//click on checkbox
function clickCheckbox(input, file_name, media_type) {

//var keys = $(\'#no-js-grid\').yiiGridView(\'getSelectedRows\'); //before
//console.log("ff"+$("#"+input.id).closest(".note-content").find(".grid-search-result").attr("id"));
    var keys = $("#" + input.id).closest(".note-content").find(".grid-search-result").yiiGridView("getSelectedRows");
    //console.log("keys" + keys);
    var mediaChecked = [];
    keys.forEach(function (entry) {
        var dataKey = "[data-key=" + entry + "]";
        var valueEntry = $(dataKey).find("input[type=checkbox]").val();
        mediaChecked.push(valueEntry);
        if ((input.checked) && (valueEntry != input.value)) {
            $(dataKey).find("input[type=checkbox]").prop("checked", false);
        }
        //console.log("need to uncheck " + $(dataKey).find("input[type=checkbox]").val());
    });


// actions with field Choosen files
    if (media_type != "gallery") {
        if (input.checked)
            $("#" + input.id).closest(".note-content").find(".choosen-list").val(file_name); //$("#choosen-file").val(file_name);
        else
            $("#" + input.id).closest(".note-content").find(".choosen-list").val(""); //$("#choosen-file").val("");

        //$("#create-media").yiiActiveForm("validateAttribute", "choosen-file"); //before 
        $("#" + input.id).closest(".note-content").find(".create-update-media").yiiActiveForm("validateAttribute", "choosen-file"); //validate File input field after checking on files checkbox
    }
    else { // gallery
        var beforeValue = $("#" + input.id).closest(".note-content").find(".choosen-list").val();//$("#choosen-gallery").val(); //value at field before changes
        var beforeArray = [];
        if (beforeValue.length > 0) {
            beforeArray = beforeValue.split("\n");
        }

        if (input.checked) {
            if (beforeArray.length > 0) {
                if ($.inArray(file_name, beforeArray) == -1) {
                    $("#" + input.id).closest(".note-content").find(".choosen-list").html(beforeValue + "\n" + file_name);//$("#choosen-gallery").html( beforeValue + "\n"+file_name);
                }
            }
            else
                $("#" + input.id).closest(".note-content").find(".choosen-list").html(file_name);//$("#choosen-gallery").html(file_name);
        }
        else {
            //console.log (\'Was \'+$("#choosen-gallery").val());
            beforeArray.splice(beforeArray.indexOf(file_name), 1);
            $("#" + input.id).closest(".note-content").find(".choosen-list").html(beforeArray.join("\n")); //$("#choosen-gallery").html(beforeArray.join("\n"));
        }

        if ($("#" + input.id).closest(".note-content").find(".choosen-list").val() != '') {
            //if($("#choosen-gallery").val()!=\'\'){
            //setCheckedColumns($(\'#no-js-grid\'), $("#choosen-gallery").val().split("\n"));
            setCheckedColumns($("#" + input.id).closest(".note-content").find(".grid-search-result"), $("#" + input.id).closest(".note-content").find(".choosen-list").val().split("\n"));
        }

        $("#" + input.id).closest(".note-content").find(".create-update-media").yiiActiveForm("validateAttribute", "choosen-gallery"); // validate Multimedia input field after checking on files checkbox

    }

};