<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Documents';

$this->params['headerTitle'] = "Documents Media Create/update";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$fileParentFolder = $media->folder;

if ($fileParentFolder) {
    $parentsFolder = $fileParentFolder->parents()->all();
    foreach ($parentsFolder as $parentFolder) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentFolder->label,
            'url' => ['folder/view', 'id' => $parentFolder->id_folder]
        ];
    }

    $this->params['breadcrumbs'][] = [
        'label' => $fileParentFolder->label,
        'url' => ['folder/view', 'id' => $fileParentFolder->id_folder]
    ];
}
else {
    $currentFolder = \app\modules\documents\models\DocsTree::find()->roots()->one();
    $this->params['breadcrumbs'][] = [
        'label' => $currentFolder->label,
        'url' => ['folder/view', 'id' => $currentFolder->id_folder]
    ];
}


$this->params['breadcrumbs'][] = [
    'label' => ($media->name) ? $media->name : 'File name',
    'template' => "<li><b>{link}</b></li>\n"
];

$counterIdDropzone = 0;
?>

<div class="col-md-3">
    <div class="hpanel panel-group">
        <div class="panel-body">
            <div class="text-center text-muted font-bold">Select translation or add new</div>

        </div>
        <div class="panel-section">

            <div class="input-group">
                <input type="text" class="form-control" placeholder="Select Area...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i
                            class="glyphicon glyphicon-plus small"></i></button>
                </span>
            </div>
            <button type="button" data-toggle="collapse" data-target="#notes"
                    class="btn-sm visible-xs visible-sm collapsed btn-default btn btn-block m-t-sm">
                All notes <i class="fa fa-angle-down"></i>
            </button>
        </div>

        <div id="notes" class="collapse">
            <div class="panel-body note-link active_contact_side_bar">
                <a href="#default" data-toggle="tab">
                    <small class="pull-right text-muted">WORLD</small>
                    <h5>Default</h5>

                    <div class="small">
                        <strong><?= $media->name ?></strong>
                    </div>
                    <div class="small">
                        <?= $media->description ?>
                    </div>
                </a>
            </div>

            <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                <div class="panel-body note-link <?php if (!$media->id_media) echo 'disabledTab'; ?>">
                    <a href="#<?= $key ?>" data-toggle="tab">
                        <small class="pull-right text-muted"><?= $languageRelation['country'] ?></small>
                        <h5><?= $languageRelation['language'] ?></h5>

                        <div class="small">
                            <strong><?php
                                /* if(isset($languageRelation['model'])) */
                                echo $languageRelation['model']->name;
                                ?></strong>
                        </div>
                        <div class="small">
                            <?php
                            /* if(isset($languageRelation['model'])) */
                            echo $languageRelation['model']->description;
                            ?>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="col-md-9">
    <div class="hpanel">

        <div class="panel-body">

            <div class="text-center hidden">
                We couldn't find any Document. Add one
            </div>

            <div class="tab-content">
                <div id="default" class="tab-pane active">
                    <div class="pull-right text-muted m-l-lg">
                        WORLD
                    </div>
                    <h3>Default</h3>
                    <hr/>
                    <div class="note-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <?=
                                $this->render('_createForm', [
                                    'media' => $media,
                                    'idContainer' => $idContainer,
                                ]);
                                ?>
                            </div>

                            <div class="col-lg-4">
                                <h3> Choose file</h3>

                                <?=
                                $this->render('_searchForm', [
                                    'key' => 'default',
                                    'searchModel' => $searchModel
                                ]);
                                ?>

                                <?php Pjax::begin(['id' => 'search-result', 'timeout' => 5000, 'options' => ['class' => 'pjax-search-result']]) ?>
                                <?=
                                GridView::widget([
                                    'id' => 'no-js-grid',
                                    'dataProvider' => $dataProvider,
                                    //'filterModel' => $searchModel,
                                    'tableOptions' => [
                                        'class' => 'table table-striped'
                                    ],
                                    'options' => ['class' => 'table-responsive grid-search-result'],
                                    'columns' => [
                                        [
                                            'class' => 'yii\grid\CheckboxColumn',
                                            'checkboxOptions' => function ($model, $key, $index, $column) use ($media) {
                                                return [
                                                    'onclick' => "console.log('clicked:'+this.checked+ '{$model->name}'); clickCheckbox(this,'{$model->name}', '{$media->type}');", //$.pjax.reload({container:'#search-files'});
                                                    'value' => $model->name,
                                                    'id' => 'repo-file-' . $model->id_file,
                                                ]; //['data-uid' => $model->user_id, 'data-jid'=>$model->job_id];
                                            }
                                        ],
                                        [
                                            'attribute' => 'name',
                                            'filter' => false,
                                        /* 'filterInputOptions' => [
                                          //'placeholder' => 'Type in some characters...',
                                          ] */
                                        ],
                                        [
                                            'attribute' => 'size',
                                            'value' => function ($model) {
                                                return ($model['size']) ? $model['size'] . ' b' : '-';
                                            },
                                            'filter' => false,
                                        ],
                                    ],
                                    'layout' => '{summary}{items}{pager}',
                                ]);
                                ?>
                                <?php Pjax::end() ?>
                            </div>


                            <div class="col-lg-4">
                                <h3> Upload file</h3>

                                <div class="upload-drop-zone" id="drop-zone">
                                    <?php
                                    echo \kato\DropZone::widget([
                                        'id' => 'fileUploader',
                                        'dropzoneContainer' => 'fileUploader',
                                        'previewsContainer' => 'previewsFile',
                                        'options' => $dropZoneConfig,
                                        'clientEvents' => [
                                            'complete' => "function(file){console.log(file)}",
                                            'removedfile' => "function(file){alert(file.name + ' is removed')}"
                                        ],
                                    ]);
                                    ?>
                                </div>

                            </div>


                        </div>

                    </div>

                    <?php $formDelete = ActiveForm::begin(['id' => 'delete-media', 'method' => 'GET', 'action' => ['delete']]); ?>
                    <?= Html::hiddenInput('id', $media->id_media); ?>
                    <?php ActiveForm::end(); ?>

                    <div class="btn-group">
                        <button class="btn btn-sm btn-default" form="create-media"><i class="fa fa-thumbs-o-up"></i> Save</button>
                        <button class="btn btn-sm btn-default" form="delete-media"><i class="fa fa-trash"></i> Remove</button>
                    </div>
                </div>


                <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                    <div id="<?= $key ?>" class="tab-pane">
                        <div class="pull-right text-muted m-l-lg">
                            <?= $languageRelation['country'] ?>
                        </div>
                        <h3><?= $languageRelation['language'] ?></h3>
                        <hr/>
                        <!-- build translation body -->
                        <div class="note-content">
                            <div class="row">
                                <div class="col-lg-4">
                                    <?=
                                    $this->render('_createTranslationForm', [
                                        'key' => $key,
                                        'media' => $media,
                                        'model' => $languageRelation['model'],
                                        'idContainer' => $idContainer,
                                    ]);
                                    ?>
                                </div>

                                <div class="col-lg-4">
                                    <h3> Choose file</h3>
                                    <?=
                                    $this->render('_searchForm', [
                                        'key' => $key,
                                        'searchModel' => $searchModel
                                    ]);
                                    ?>

                                    <?php Pjax::begin(['id' => 'search-result-'.$key, 'timeout' => 5000, 'options' => ['class' => 'pjax-search-result']]) ?>
                                    <?=
                                    GridView::widget([
                                        'id' => 'no-js-grid-'.$key,
                                        'dataProvider' => $dataProvider,
                                        //'filterModel' => $searchModel,
                                        'tableOptions' => [
                                            'class' => 'table table-striped'
                                        ],
                                        'options' => ['class' => 'table-responsive grid-search-result'],
                                        'columns' => [
                                            [
                                                'class' => 'yii\grid\CheckboxColumn',
                                                'checkboxOptions' => function ($model, $key, $index, $column) use ($media, $key) {
                                                    return [
                                                        'onclick' => "console.log('clicked:'+this.checked+ '{$model->name}'); clickCheckbox(this,'{$model->name}', '{$media->type}');", //$.pjax.reload({container:'#search-files'});
                                                        'value' => $model->name,
                                                        'id' => 'repo-file-' .$key.'-'.$model->id_file,
                                                    ]; //['data-uid' => $model->user_id, 'data-jid'=>$model->job_id];
                                                }
                                            ],
                                            [
                                                'attribute' => 'name',
                                                'filter' => false,
                                            /* 'filterInputOptions' => [
                                              //'placeholder' => 'Type in some characters...',
                                              ] */
                                            ],
                                            [
                                                'attribute' => 'size',
                                                'value' => function ($model) {
                                                    return ($model['size']) ? $model['size'] . ' b' : '-';
                                                },
                                                'filter' => false,
                                            ],
                                        ],
                                        'layout' => '{summary}{items}{pager}',
                                    ]);
                                    ?>
                                    <?php Pjax::end() ?>

                                </div>

                                <div class="col-lg-4">
                                    <h3> Upload file</h3>

                                    <div class="upload-drop-zone" id="drop-zone-<?= $key ?>">
                                        <?php
                                        echo \kato\DropZone::widget([
                                            'id' => 'fileUploader_'.$counterIdDropzone,
                                            'dropzoneContainer' => 'fileUploader_'.$counterIdDropzone,
                                            'previewsContainer' => 'previewsFile_'.$counterIdDropzone,
                                            'options' => $dropZoneConfig,
                                            'clientEvents' => [
                                                'complete' => "function(file){console.log(file)}",
                                                'removedfile' => "function(file){alert(file.name + ' is removed')}"
                                            ],
                                        ]);
                                        ?>
                                    </div>

                                </div>


                            </div>

                        </div>
                        <!-- build translation body -->
                        <?php $formDelete = ActiveForm::begin(['id' => "delete-media-{$key}", 'method' => 'GET', 'action' => ['delete-translation', 'id' => $media->id_media, 'lang' => $key]]); ?>
                        <?= Html::hiddenInput('id', $media->id_media); ?>
                        <?php ActiveForm::end(); ?>

                        <div class="btn-group">
                            <button class="btn btn-sm btn-default" form="create-mediaTr-<?= $key ?>"><i
                                    class="fa fa-thumbs-o-up"></i> Save
                            </button>
                            <button class="btn btn-sm btn-default" form="delete-media-<?= $key ?>"><i class="fa fa-trash"></i>
                                Remove
                            </button>
                        </div>
                    </div>
                <?php $counterIdDropzone++; ?>
                <?php endforeach; ?>
            </div>

        </div>

    </div>
</div>

<?php
$this->registerJs(<<<JS

                                $('.pjax-search-result').on('pjax:error', function (event) {
                                //window.alert('Failed to load the page Search-result. Server is busy. Try later');
                                event.preventDefault();
                                });

                                $('.pjax-search-result').on('pjax:timeout', function() {
                                console.log('pjax:timeout');
                                });
                    
                               //$('#search-result').on('pjax:complete', function() { //before
                                $('.pjax-search-result').on('pjax:complete', function() {
                                    console.log('pjax:complete'+this.id);
                                    //console.log("znachenie po class:"+$(this).closest(".note-content").find(".choosen-list").val());
                        
                                    //if($("#choosen-gallery").val()!=''){ //before
                                    if($(this).closest(".note-content").find(".choosen-list").val()!=''){ //before
                                        setCheckedColumns($(this).closest(".note-content").find(".grid-search-result"), $(this).closest(".note-content").find(".choosen-list").val().split("\\n"));
                                    }
});
JS
);
?>

<?php
$idMedia = ($media->id_media) ? $media->id_media : '';
$this->registerJs('
var urlAction = \'' . Yii::$app->controller->action->id . '\';
var mediaType = \'' . $media->type . '\';
var idFolder = \'' . $idContainer . '\';
var id = \'' . $idMedia . '\'; //media id (if isset)
//console.log ("id:"+id);
$(document).ready(function(){
$.pjax.defaults.timeout = 5000; //update timeout value (long response from server)
    //$(\'body\').on(\'click\', \'#searchButton\',  function(){ // worked before
    $(\'body\').on(\'click\', \'.m-t-n-xs\',  function(){ 
        console.log(\'clicked:\'+$(this).attr(\'id\'));
        console.log(\'found input without id: \'+$(this).closest(\'.search-form\').find(\'.form-control\').val());
        //$(this).closest(\'.search-form\').append( "<p><strong>Test</strong></p>" );
        //console.log(\'search:\'+$(\'#search-field\').val());
        if($(this).closest(\'.search-form\').find(\'.form-control\').val() !=""){
        //if($(\'#search-field\').val() !=""){ //old version
            //var url = "create?SearchRepositoryFiles%5Bname%5D="+$(\'#search-field\').val()+"&type=movie&id_folder=3"; //old version
            var url = urlAction+"?SearchRepositoryFiles%5Bname%5D="+$(this).closest(\'.search-form\').find(\'.form-control\').val()+"&type="+mediaType;

            if(idFolder>0) url = url+"&id_folder="+idFolder;

            if(id>0 ) url = url+"&id="+id;
            //window.alert(url);
            var pjaxResultContainer = $(this).closest(".note-content").find(".pjax-search-result");
            console.log("try "+pjaxResultContainer.attr("id"));
            $.pjax({url: url, container: "#"+pjaxResultContainer.attr("id")}); //before $.pjax({url: url, container: \'#search-result\'});
            //window.alert(\'after pjaX continer\');
            //$.pjax.reload(\'#search-result\', {url : url, timeout : 5000}); //update timeout value (long response from server)
        }
        else {
            console.log("empty search field");
        }
    });
});', \yii\web\View::POS_READY);
?>

<?php
$js = <<<JS
/* bind clicks on the labels */
/*$('#icon_block').find('label').click(function(e) {
    //e.preventDefault();
    console.log(this);
    $(this).closest('.note-content').find('.create-update-media').yiiActiveForm('validateAttribute', 'icon_block');
});
*/
// event after upoading icon image
function eventAfterIconUpload(file){
$('#radio_custom').val(file.name);
//$('#create-media').yiiActiveForm('validateAttribute', 'icon_block'); // before
$('#icon_block').closest('.note-content').find('.create-update-media').yiiActiveForm('validateAttribute', 'icon_block'); // validate Icon radio input after uploading
}

// active current language in tranlsation block
$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
});
JS;

$this->registerJs($js, \yii\web\View::POS_READY); //, 'projects-filter-form-script'
?>

<?php
$this->registerJs('
    // set checked inputs in gridview by values from textarea field Choosen files
    function setCheckedColumns(gridObject,fileNames) {
        fileNames.forEach(function(entity) {
            gridObject.find("input[value=\'"+entity+"\']").prop("checked", true );
        });
    }
    //click on checkbox
function clickCheckbox(input,file_name, media_type){

//var keys = $(\'#no-js-grid\').yiiGridView(\'getSelectedRows\'); //before
//console.log("ff"+$("#"+input.id).closest(".note-content").find(".grid-search-result").attr("id"));
var keys = $("#"+input.id).closest(".note-content").find(".grid-search-result").yiiGridView(\'getSelectedRows\');
console.log("keys"+keys);
        var mediaChecked = [];
        keys.forEach(function(entry) {
            var dataKey = \'[data-key=\'+entry+\']\';
            var valueEntry = $(dataKey).find("input[type=checkbox]").val();
            mediaChecked.push(valueEntry);
            if ((input.checked) && (valueEntry!=input.value)){
                $(dataKey).find("input[type=checkbox]").prop("checked", false );
            }
            console.log("need to uncheck "+$(dataKey).find("input[type=checkbox]").val());
        });
        console.log(mediaChecked);
        
// actions with field Choosen files
if(media_type != \'gallery\'){
    if(input.checked) $("#"+input.id).closest(".note-content").find(".choosen-list").val(file_name); //$("#choosen-file").val(file_name);
    else $("#"+input.id).closest(".note-content").find(".choosen-list").val(""); //$("#choosen-file").val("");

    //$("#create-media").yiiActiveForm("validateAttribute", "choosen-file"); //before 
    $("#"+input.id).closest(".note-content").find(".create-update-media").yiiActiveForm("validateAttribute", "choosen-file"); //validate File input field after checking on files checkbox
}
else { // gallery
    var beforeValue = $("#"+input.id).closest(".note-content").find(".choosen-list").val();//$("#choosen-gallery").val(); //value at field before changes
    var beforeArray = [];
    if (beforeValue.length>0) {
        beforeArray = beforeValue.split("\n");
    }

    if(input.checked) {
        if(beforeArray.length>0) {
            if($.inArray(file_name, beforeArray)==-1){
                $("#"+input.id).closest(".note-content").find(".choosen-list").html( beforeValue + "\n"+file_name);//$("#choosen-gallery").html( beforeValue + "\n"+file_name);
            }
        }
        else $("#"+input.id).closest(".note-content").find(".choosen-list").html(file_name);//$("#choosen-gallery").html(file_name);
    }
    else {
        //console.log (\'Was \'+$("#choosen-gallery").val());
        beforeArray.splice(beforeArray.indexOf(file_name),1);
        $("#"+input.id).closest(".note-content").find(".choosen-list").html(beforeArray.join("\n")); //$("#choosen-gallery").html(beforeArray.join("\n"));
    }
    
    if($("#"+input.id).closest(".note-content").find(".choosen-list").val()!=\'\'){
    //if($("#choosen-gallery").val()!=\'\'){
        //setCheckedColumns($(\'#no-js-grid\'), $("#choosen-gallery").val().split("\n"));
        setCheckedColumns($("#"+input.id).closest(".note-content").find(".grid-search-result"), $("#"+input.id).closest(".note-content").find(".choosen-list").val().split("\\n"));
    }
    
    $("#"+input.id).closest(".note-content").find(".create-update-media").yiiActiveForm("validateAttribute", "choosen-gallery"); // validate Multimedia input field after checking on files checkbox

}

//console.log(\'input\'+ input.value);
//jQuery(\'#no-js-grid\').yiiGridView(\'setSelectionColumn\', {"name":"selection","multiple":true,"checkAll":"selection_all"});
//$(\'#no-js-grid\').find("input[value=\'BSK.png\']").prop(\'checked\', true);
//console.log("findcheck:"+$(\'#no-js-grid\').find("input[value=\'BSKf.png\']").attr(\'id\'));
//$(\'#no-js-grid\').find("input[value=\'BSK.png\']").prop("checked", true );
//console.log(\'ff\'+file_name);
};', \yii\web\View::POS_BEGIN);
?>