<?php

namespace app\modules\contacts\models;

use app\components\BaseController;
use app\modules\contacts\components\AppHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "zse_v1_records".
 *
 * @property integer $id_record
 * @property integer $id_locality
 * @property integer $id_classification
 * @property string $code
 * @property string $code_destination
 * @property string $business_name
 * @property string $legal_status
 * @property string $type
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property string $mobile
 * @property string $inserted_by
 * @property string $assigned_to
 * @property string $origin
 * @property integer $is_contact
 * @property integer $is_customer
 * @property integer $is_supplier
 * @property integer $is_prospect
 * @property integer $is_destination
 * @property integer $is_reseller
 * @property integer $is_intrested
 * @property integer $authorize_send
 * @property string $coordinates
 * @property string $external_code
 * @property string $icon
 * @property string $country
 * @property string $province
 * @property string $city
 * @property string $postal_code
 * @property string $vat_code
 * @property string $fiscal_code
 * @property string $pricelist
 * @property string $options
 * @property string $person_in_charge
 * @property string $role
 * @property string $direct_mail
 * @property string $direct_phone
 * @property string $date_modification
 * @property integer $record_status
 *
 * @property ZseV1AccountsServicesLicenses[] $zseV1AccountsServicesLicenses
 * @property ZseV1Activities[] $zseV1Activities
 * @property ZseV1Activities[] $zseV1Activities0
 * @property ZseV1Orders[] $zseV1Orders
 * @property ZseV1RecordsClassifications $idClassification
 * @property ZseV1RecordsLinks[] $zseV1RecordsLinks
 * @property ZseV1RecordsLinks[] $zseV1RecordsLinks0
 */
class Records extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $upload_file;
    public static function tableName()
    {
        return 'zse_v1_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_locality', 'id_classification', 'is_contact', 'is_customer', 'is_supplier', 'is_prospect', 'is_destination', 'is_reseller', 'is_intrested', 'authorize_send', 'record_status'], 'integer'],
            [['options'], 'string'],
            [['date_modification'], 'safe'],
            [['code','code_destination','id_classification','business_name','phone','email','record_status'], 'required'],
            [['code', 'phone', 'fax', 'mobile'], 'string', 'max' => 32],
            [['code_destination', 'type', 'email', 'inserted_by', 'assigned_to', 'external_code', 'country', 'province', 'city'], 'string', 'max' => 64],
            [['business_name', 'address', 'icon', 'person_in_charge', 'role', 'direct_mail', 'direct_phone'], 'string', 'max' => 255],
            [['legal_status', 'origin', 'fiscal_code'], 'string', 'max' => 16],
            [['coordinates'], 'string', 'max' => 128],
            [['postal_code'], 'string', 'max' => 8],
            [['vat_code'], 'string', 'max' => 11],
            [['pricelist'], 'string', 'max' => 20],
            ['email','email'],
            [['icon'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_record' => 'Id Record',
            'id_locality' => 'Id Locality',
            'id_classification' => 'Id Classification',
            'code' => 'Code',
            'code_destination' => 'Code Destination',
            'business_name' => 'Business Name',
            'legal_status' => 'Legal Status',
            'type' => 'Type',
            'address' => 'Address',
            'email' => 'Email',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'mobile' => 'Mobile',
            'inserted_by' => 'Inserted By',
            'assigned_to' => 'Assigned To',
            'origin' => 'Origin',
            'is_contact' => 'Is Contact',
            'is_customer' => 'Is Customer',
            'is_supplier' => 'Is Supplier',
            'is_prospect' => 'Is Prospect',
            'is_destination' => 'Is Destination',
            'is_reseller' => 'Is Reseller',
            'is_intrested' => 'Is Intrested',
            'authorize_send' => 'Authorize Send',
            'coordinates' => 'Coordinates',
            'external_code' => 'External Code',
            'icon' => '',
            'country' => 'Country:',
            'province' => 'Province',
            'city' => 'City',
            'postal_code' => 'Zip',
            'vat_code' => 'Vat Code',
            'fiscal_code' => 'Fiscal Code',
            'pricelist' => 'Price list:',
            'options' => 'Options',
            'person_in_charge' => 'Person In Charge',
            'role' => 'Role',
            'direct_mail' => 'Direct Mail',
            'direct_phone' => 'Direct Phone',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1AccountsServicesLicenses()
    {
        return $this->hasMany(ZseV1AccountsServicesLicenses::className(), ['id_record' => 'id_record']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1Activities()
    {
        return $this->hasMany(ZseV1Activities::className(), ['record_code' => 'code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1Activities0()
    {
        return $this->hasMany(ZseV1Activities::className(), ['record_destination_code' => 'code_destination']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1Orders()
    {
        return $this->hasMany(ZseV1Orders::className(), ['record_code' => 'code', 'record_destination_code' => 'code_destination']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClassification()
    {
        return $this->hasOne(ZseV1RecordsClassifications::className(), ['id_classification' => 'id_classification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1RecordsLinks()
    {
        return $this->hasMany(ZseV1RecordsLinks::className(), ['id_record' => 'id_record']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1RecordsLinks0()
    {
        return $this->hasMany(ZseV1RecordsLinks::className(), ['id_link' => 'id_record']);
    }

    /**
     * @return array
     * function for contact data per agent per type
     */
    public function getAgentContactData($where=array()){
        set_time_limit(0);
        $command = (new \yii\db\Query())
            ->select(['`zse_v1_records`.*', '`zse_v1_records_agents`.*'])
            ->from('`zse_v1_records_agents`')
            ->filterWhere($where)
            ->leftJoin('`zse_v1_records`', '`zse_v1_records_agents`.`record_code` = `zse_v1_records`.`code` and `zse_v1_records_agents`.`destination_code` = `zse_v1_records`.`code_destination`')
            ->createCommand();

        $result = $command->queryAll();
        #BaseController::_setTrace($result);

        /*$command = (new \yii\db\Query())
            ->select(['`zse_v1_records`.*', '`zse_v1_records_agents`.*'])
            ->from('`zse_v1_records_agents`')
            ->Where(['`zse_v1_records`.`is_destination`' => 2])
            ->innerJoin('`zse_v1_records`', '`zse_v1_records_agents`.`record_code` = `zse_v1_records`.`code`')
            ->orWhere('`zse_v1_records_agents`.`destination_code` = `zse_v1_records`.`code_destination`')
            ->createCommand();

        $result_destination_is_2 = $command->queryAll();
        BaseController::_setTrace($result_destination_is_2);*/

        $agents_contacts = array(); // assigned contacts per agent per type
        $agents_contacts_codes = array(); // only assigned contacts codes per agent per type
        $associate_emails = [];
        foreach ($result as $key => $item) {
            $type = ucfirst(AppHelper::getIconFromArrayVal($item, true));

            // Anyway an array is needed for each type
            if(empty($agents_contacts[$item['agent_code']]['Company'])) {
                $agents_contacts[$item['agent_code']]['Company'] = array();
            }
            if(empty($agents_contacts[$item['agent_code']]['Destination'])) {
                $agents_contacts[$item['agent_code']]['Destination'] = array();
            }
            if(empty($agents_contacts[$item['agent_code']]['Prospect'])) {
                $agents_contacts[$item['agent_code']]['Prospect'] = array();
            }
            if(empty($agents_contacts[$item['agent_code']]['Person'])) {
                $agents_contacts[$item['agent_code']]['Person'] = array();
            }

            // Anyway an array is needed for each type
            if(empty($agents_contacts_codes[$item['agent_code']]['Company'])) {
                $agents_contacts_codes[$item['agent_code']]['Company'] = array();
            }
            if(empty($agents_contacts_codes[$item['agent_code']]['Destination'])) {
                $agents_contacts_codes[$item['agent_code']]['Destination'] = array();
            }
            if(empty($agents_contacts_codes[$item['agent_code']]['Prospect'])) {
                $agents_contacts_codes[$item['agent_code']]['Prospect'] = array();
            }
            if(empty($agents_contacts_codes[$item['agent_code']]['Person'])) {
                $agents_contacts_codes[$item['agent_code']]['Person'] = array();
            }


            $code_with_destination = $item['record_code'] . ",";
            $code_with_destination .= (empty($item['destination_code'])) ? "#" : $item['destination_code'];
            #if(empty($type)) break;
            if(!empty($type)) {
                array_push($agents_contacts[$item['agent_code']][$type], $item);
                array_push($agents_contacts_codes[$item['agent_code']][$type], $code_with_destination);

                ### Raaz COde
                $associate_emails[$item['agent_code']][] = $item['email'];
                array_unique($associate_emails[$item['agent_code']]);
                ##End raz Code

                // inserting in both array when company=1 & destination=2
                if($item['is_customer']==1 && $item['is_destination']==2) {
                    array_push($agents_contacts[$item['agent_code']]['Destination'], $item);
                    array_push($agents_contacts_codes[$item['agent_code']]['Destination'], $code_with_destination);
                }
            }
        }
        foreach($associate_emails as $key=> $value){
            $associate_emails[$key] = array_unique($value);
        }
        #BaseController::_setTrace($associate_emails);
        #BaseController::_setTrace($agents_contacts_codes);
        return array(
            'agents_contacts' => $agents_contacts,
            'agents_contacts_codes' => $agents_contacts_codes,
            'associate_emails' => $associate_emails,        #Raaz code
        );
    }

    /**
     * @return mixed
     * function for all contacts per type
     */
    public function getTypesAllContactData($where = array()){
        $all_contacts = Records::find()
            ->filterWhere($where)
            ->all();

        $type_contacts['Company'] = array();
        $type_contacts['Destination'] = array();
        $type_contacts['Prospect'] = array();
        $type_contacts['Person'] = array();

        // dynamically assign/arrange the contacts into types
        foreach ($all_contacts as $contact) {
            $type = ucfirst(AppHelper::getIconFromArrayVal($contact, 1));
            if(empty($type)) break; // return empty if type is not set

            array_push($type_contacts[$type], $contact);
            if($contact['is_customer']==1 && $contact['is_destination']==2) {
                array_push($type_contacts['Destination'], $contact);
            }
        }

        #BaseController::_setTrace($type_contacts);
        return $type_contacts;
    }

    public function searchContactByCode($code) {
        return Records::find()->Where(['zse_v1_records.code' => $code])->one();
    }

    public function searchContactByName($name) {
        return Records::find()->Where(['LIKE', 'zse_v1_records.business_name', $name])->all();
    }

    public static function getAllRecordCode() {
        $command = (new \yii\db\Query())
            ->select(['`zse_v1_records`.code'])
            ->from('`zse_v1_records`')
            ->createCommand();

        return $command->queryAll();
    }
}
