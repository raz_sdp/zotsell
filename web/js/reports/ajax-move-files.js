$(document).ready(function () {
    $("#modalSubmit").click(function (e) {
        e.preventDefault();
        var form = $("#formMoveFiles");
        $.ajax({
            url: "/reports/folder/save-moved-files",
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                var parseResponse = $.parseJSON(result);
                var modalContainer = $('#modal');
                var modalBody = modalContainer.find('#modalContent');
                var insidemodalBody = modalContainer.find('#formMoveFiles');

                if (parseResponse == true) {
                    insidemodalBody.html(result).hide(); // 
                    //$('#my-modal').modal('hide');
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').append("<strong>Files Moved successfully</strong>");
                    $('#success > .alert-success').append('</div>');


                    setTimeout(function () { // close modal after 2 seconds
                        $("#modal").modal('hide');
                        //$("#nestableFolders").load(location.href + " #nestableFolders");
                        //document.location.reload();
                    }, 1000);
                }
                else {
                    var parseErrors = '<ul>';
                    $.each(parseResponse, function (index, value) {
                        parseErrors += '<li>' + value + '</li>';
                    });
                    parseErrors += '</ul>';

                    //modalBody.html(result).hide().fadeIn();
                    insidemodalBody.html(result).hide();
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').append("<strong>" + parseErrors + "</strong>");
                    $('#success > .alert-danger').append('</div>');

                    setTimeout(function () { // close modal after 2 seconds
                        $("#modal").modal('hide');
                        //$("#nestableFolders").load(location.href + " #nestableFolders");
                        //document.location.reload();
                    }, 2000);

                }
            }
        });
        //$("#myForm").submit();
    });

});

