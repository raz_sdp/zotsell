


<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\boostrap\Modal;
    $this->title = 'Catalog';
    $this->params['breadcrumbs'][] = $this->title;
?>


  <h1>Catalog Flipbook</h1>
  <p class="lead">Manage all your  %number% Articles</p>
  
  




 <a  href="default/edit" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Add New Item </a> 
 <a  href="default/newfolder" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Add New folder </a> 
    <a  href="default/bulk" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Bulk Move </a>  
  <a  href="default/itemusers" class="btn btn-success" type="button"  ><i class="fa fa-link"></i> Assign items </a>  

  
  
  <hr>
<div class="row">

<div class="nav navbar-nav navbar-left col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Orientation</a></li>
            <li><a href="#">Classification</a></li>
            <li><a href="#">Status</a></li>
            <li><a href="#">Kind</a></li>
             <li><a href="#">Favorites</a></li>
              <li><a href="#">Type</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">remove filters</a></li>
          </ul>
        </li>
      </div>


<form class="col-md-10" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder=" Search">
  </div>
</form>



<div class="nav navbar-nav navbar-right col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
             <li><a href="/zot/web/catalog/">List</a></li>
            <li><a href="/zot/web/catalog/default/folders">  Folders</a></li>
            <li><a href="/zot/web/catalog/default/flow">Flow</a></li>
            <li><a href="/zot/web/catalog/default/squares">Squares</a></li>
             <li><a href="/zot/web/catalog/default/flipbook"> <i class="fa fa-check"></i> Flipbook</a></li>
              <li><a href="/zot/web/catalog/default/frequent">Frequents</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="pdf">Download pdf</a></li>
            <li><a href="csv">Download csv</a></li>
        </li>
      </div>

    
   </div>    




<hr>

http://codecanyon.net/item/wowbook-a-flipbook-jquery-plugin/1791563


<div id="all">
    <div id="page-flip">
        <div id="r1"><div id="p1"><div><div></div></div></div></div>
        <div id="p2"><div></div></div>
        <div id="r3"><div id="p3"><div><div></div></div></div></div>
        <div class="s"><div id="s3"><div id="sp3"></div></div></div>
        <div class="s" id="s4"><div id="s2"><div id="sp2"></div></div></div>
    </div>
</div>



  </div>   









