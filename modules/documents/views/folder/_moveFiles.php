<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="row">
    <div id="success"> </div> <!-- For success message -->
</div>

<div class="row">   
    <?php $form = ActiveForm::begin(['options' => ['class' => 'moveFiles', 'id' => 'formMoveFiles']]); ?>
    <div class="row">
        <div class="col-md-offset-3">
            <?= Html::listBox('multiForm', array_keys($selection), $items, ['multiple' => true, 'class' => 'searchable']); ?>
        </div>  
    </div>
    <?= $form->field($model, 'id_folder')->hiddenInput(['value' => $model->id_folder])->label(false); ?>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJs('
$(\'.searchable\').multiSelect({
  selectableHeader: "<input type=\'text\' class=\'search-input\' autocomplete=\'off\' placeholder=\'enter name\'>",
  selectionHeader: "<input type=\'text\' class=\'search-input\' autocomplete=\'off\' placeholder=\'enter name\'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = \'#\'+that.$container.attr(\'id\')+\' .ms-elem-selectable:not(.ms-selected)\',
        selectionSearchString = \'#\'+that.$container.attr(\'id\')+\' .ms-elem-selection.ms-selected\';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
        .on(\'keydown\', function(e){
            if (e.which === 40){
                that.$selectableUl.focus();
                return false;
            }
        });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
        .on(\'keydown\', function(e){
            if (e.which == 40){
                that.$selectionUl.focus();
                return false;
            }
        });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
},
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
}
//});
});

$(\'.ms-container\').css(\'width\', \'500px\');
    //});', \yii\web\View::POS_READY);
