<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\themes\Common;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetHomer extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/common';
    public $sourcePath = '@webroot';
    public $css = [
        'fontawesome/css/font-awesome.css',
        'metisMenu/dist/metisMenu.css',
        'animate.css/animate.css',
        'bootstrap/dist/css/bootstrap.css',

        'fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'fonts/pe-icon-7-stroke/css/helper.css',
        'datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        'awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
        'styles/static_custom.css', //for main/folders
        'styles/style.css',
        'jquery-multi-select/css/multi-select.css',
    ];

    public $js = [
        'jquery/dist/jquery-1.10.2.min.js',        
        'jquery-ui/jquery-ui.min.js',
        "slimScroll/jquery.slimscroll.min.js",
        'bootstrap/dist/js/bootstrap.min.js',
        'jquery-flot/jquery.flot.js',
        'jquery-flot/jquery.flot.resize.js',
        'jquery-flot/jquery.flot.pie.js',
        'flot.curvedlines/curvedLines.js',
        'jquery.flot.spline/index.js',
        "metisMenu/dist/metisMenu.min.js",
        "iCheck/icheck.min.js",
        'peity/jquery.peity.min.js',
        "sparkline/index.js",
        'datatables/media/js/jquery.dataTables.min.js',
        'datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
        'jquery-multi-select/js/jquery.multi-select.js',
        'quicksearch-master/jquery.quicksearch.js',
        "scripts/homer.js",
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
