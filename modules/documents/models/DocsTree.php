<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "zse_v1_docs_tree".
 *
 * @property integer $id_folder
 * @property integer $root
 * @property integer $nleft
 * @property integer $nright
 * @property integer $level
 * @property string $icon
 * @property string $label
 * @property string $description
 * @property integer $icon_type
 *
 * @property DocsTreeTr[] $docsTreeTrs
 */
class DocsTree extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public $id_media;
    public $lang;

    public static function tableName()
    {
        return 'zse_v1_docs_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['root', 'level'], 'required'],
            [['root', 'nleft', 'nright', 'level', 'icon_type', 'id_media'], 'integer'],
            [['description'], 'string'],
            [['icon', 'label'], 'string', 'max' => 255],
            [['label', 'description'], 'required', 'on' => ['create']],
            ['icon', 'compare', 'compareValue' => 'custom', 'operator' => '!=',
                'when' => function ($model) {
                    if ($model->icon == 'custom')
                        return true;
                },
                'whenClient' => "function (attribute, value) {
                    if($('input[name=\"DocsTree[icon]\"]:checked').val()=='custom'){
                        return true;
                    }
                    }",
                'on' => ['create'],
                'message' => 'When set Custom, you have to upload icon'] //compare validator
        ];
    }


    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['label', 'description', 'icon'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_folder' => 'Id Folder',
            'root' => 'Root',
            'nleft' => 'Nleft',
            'nright' => 'Nright',
            'level' => 'Level',
            'icon' => 'Icon',
            'icon_type' => 'Icon Type',
            'label' => 'Name',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocsTreeTr()
    {
        return $this->hasMany(DocsTreeTr::className(), ['id_folder' => 'id_folder']);
    }

    public function getMediaLinks()
    {
        return $this->hasMany(MediaLinks::className(), ['id_container' => 'id_folder'])->andOnCondition(['zse_v1_media_links.context' => 1]);//->where(['zse_v1_media_links.context' => 1])
    }

    /**
     * @return $this
     */
    public function getMedia()
    {
        return $this->hasMany(Media::className(), ['id_media' => 'id_media'])
            ->via('mediaLinks');
    }

    /**
     * @param $folderId
     * @return $this
     */
    public static function getChildFolders($folderId)
    {
        $folderId = (isset($folderId)) ? $folderId : self::id_folder; //refactor!
        //return static::findAll(['root' => $folderId]);
        return static::find()->where(['root' => $folderId]); //добавить orderBy
    }


    public function getFiles()
    {
        return $this->hasOne(RepositoryFiles::className(), ['name' => 'file'])
            ->via('media');
    }

    /**
     * Get size of folder (include files inside subfolders)
     * @return type
     */
    public function getSize()
    {
        $size = 0;
        foreach ($this->getFiles()->all() as $file) { //current folder
            if ($file["size"] > 0) $size += $file["size"];
        }

        foreach ($this->children()->all() as $subfolder) { //sum size of subfolders
            foreach ($subfolder->getFiles()->all() as $file) {
                if ($file["size"] > 0) $size += $file["size"];
            }
        }

        return $size;
    }


    /**
     * Check files in folder for sharable
     * @return 0 (if all files aren't sharable) | 1 (if all files in folder are sharable) | 2 (some sharable, some not)
     */
    public function getSharable()
    {
        $medias = $this->getMedia()->all();
        $countCansend = 0;
        $countNotCansend = 0;
        foreach ($medias as $media) {
            if ($media->cansend == 1) $countCansend += 1;
            else $countNotCansend += 1;
        }

        switch (true) {
            case $countCansend > 0 && $countNotCansend == 0;
                return 1;
                break;

            case $countCansend > 0 && $countNotCansend > 0;
                return 2;
                break;

            default:
                return 0;
                break;
        }
    }

    /**
     * Get status about sharable folder
     * @param $sharableContent
     * key 0: count medias with 'cansend'=0 value
     * key 1: count medias with 'cansend'=1 value
     * @return int
     */
    public static function sharableStatus($sharableContent)
    {
        switch (true) {
            case $sharableContent[1] > 0 && $sharableContent[0] == 0;
                return 1;
                break;

            case $sharableContent[1] > 0 && $sharableContent[0] > 0;
                return 2;
                break;

            default:
                return 0;
                break;
        }
    }

    /**
     * get path to current folder (calling from Media)
     */
    public function getPathToFolder($idFolder)
    {
        $path = ''; //'/';
        $id = $this->id_folder;
        while ($r = $this->getParentFolder($id)) {
            $id = $r['id_folder'];
            $path .= $r['label'];
        }
        $path .= '/' . $this->label . '/';

        return $path;
    }

    /**
     * get parrent folder
     * @param $id current folder's id
     * isset=> return array
     * !isset =>return
     */
    public function getParentFolder($id)
    {
        $query = new \yii\db\Query;
        $query
            ->select([
                'parent.id_folder as id_folder',
                'parent.label as label'
            ])
            ->from(['current' => 'zse_v1_docs_tree'])
            ->leftJoin(['parent' => 'zse_v1_docs_tree'], 'current.root = parent.id_folder')
            ->where(['current.id_folder' => $id])
            ->andWhere(['<>', 'parent.id_folder', $id]);

        $result = $query->one();

        if (!isset($result['id_folder'])) return false;
        return $result;
    }

    /**
     * get parrent folder
     * @return type
     */
    public function getFather()
    {
        return $this->hasOne(DocsTree::className(), ['id_folder' => 'root'])->from(['father' => DocsTree::tableName()]);
    }

    public static function getNameById($folderId)
    {
        return static::find()->select('label')->where(['id_folder' => $folderId])->scalar();
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->docsTreeTr as $docTr) {
                $docTr->delete(); // delete related media translation models
            }
            foreach ($this->mediaLinks as $link) {
                $link->delete(); // delete related media translation models
            }
            return true;
        } else {
            return false;
        }
    }
}
