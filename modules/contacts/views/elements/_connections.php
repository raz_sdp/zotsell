<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:20 PM
 */
use app\modules\contacts\components\AppHelper;
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3> Connections</h3>
<hr/>
<div class="note-content">
    <div class="row">
        <?php if(!isset($type)): ?>
        <div class="col-lg-4">
            <p>
            <h3> Choose Contact</h3>
            </p>
            <form role="form" id="form_connection">
                <div class="form-group">
                    <label for="name">Search</label>
                    <input type="text" id="name" name="name" placeholder="Enter contact name" class="form-control" required>
                </div>
                <div>
                    <button class="btn btn-block btn-success m-t-n-xs" type="submit">
                        <strong>Search</strong>
                    </button>
                </div>
            </form>

            <div class="table-responsive slim_scroll">
                <table class="table table-striped js-connection-search-result-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2">No Result Found</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-lg-8">
            <?php if(!isset($type)): ?>
                <p>
                <h3> Added Contacts</h3>
                </p>
            <?php endif; ?>

            <script type="text/javascript">
                var added_connection_records = [];
            </script>

            <table id="connections-table-update" class="table table-striped table-bordered table-hover js-connections-table">
                <thead>
                <tr>
                    <th>Type</th>
<!--                    <th>ID</th>-->
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($connections)) { ?>
                    <!--<tr class="connecction_no_res_msg">
                        <td colspan="6">No Connections.</td>
                    </tr>-->
                <?php } else { ?>
                    <?php foreach($connections as $connection) : ?>
                        <tr id="connections_<?=$connection['id_record']?>" rel="<?= $connection['id_record'];?>">
                            <td>
                            <?php
                            echo AppHelper::getIconFromArrayVal($connection);
                            ?>
                            </td>
                            <td><?= $connection['code'];?></td>
                            <td><?= $connection['business_name'];?></td>
                            <td><?= $connection['city'];?></td>
                            <td><?= $connection['country'];?></td>
                            <td><a id="<?=$connection['id_record'];?>"><i class="fa fa-minus"  onclick="delete_from_connection_table(this)"></i></a></td>
                        </tr>
                        
                        <script type="text/javascript">
                            added_connection_records.push("<?php echo $connection['id_record'];?>");
                        </script>

                    <?php endforeach; ?>
                <?php } ?>
                <!-- <img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64"
                             src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+"
                             data-holder-rendered="true" style="width: 64px; height: 64px;"> -->
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if(!isset($type)): ?>
<div class="btn-group save_btn_group" id="connection_save_btn_group">
    <button class="btn btn-sm btn-default" id="save_connections"><i class="fa fa-thumbs-o-up"></i> Save</button>
</div>
<?php endif; ?>
<?php
$active = 0;
if(isset($connections) && !empty($connections)){
    $active = 1;
}
?>
<input value="<?=$active?>" id="active_connections" type="hidden">