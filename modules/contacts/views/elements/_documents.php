<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:22 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3> Documents</h3>
<hr/>
<div class="note-content">


    <div class="row">
        <?php if(!isset($type)): ?>
            <div class="col-lg-4">

            <p>
            <h3> Choose Document</h3>
            </p>
            <div class="row">
                <form role="form" id="form_document">
                    <div class="form-group">
                        <label for="file_name">Search</label>
                        <input type="text" id="file_name" name="file_name" placeholder="Enter Document name" class="form-control" required>
                    </div>                
                    <div>
                        <button class="btn btn-block btn-success m-t-n-xs" type="submit"><strong>Search</strong>
                        </button>
                    </div>
                </form>
            
                <div class="table-responsive slim_scroll">
                    <table class="table table-striped js-document-search-result-table custom_table_brake">
                        <thead>
                        <tr>
                            <th class="col-lg-10">Name</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2">No Result Found</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-lg-8">
            <?php if(!isset($type)): ?>
                <p>
                <h3> Added Documents</h3>
                </p>
            <?php endif; ?>
            <script type="text/javascript">
                var added_documents = [];
            </script>
            <table id="documents-table-update" class="table table-striped table-bordered table-hover js-document-table custom_table_brake">
                <thead>
                <tr>
                    <th>Icon</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Sharable</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($documents)) { ?>
                    <!--<tr class="document_no_res_msg">
                        <td colspan="6">No Documents.</td>
                    </tr>-->
                <?php } else { ?>
                    <?php foreach($documents as $document) : ?>
                        <tr id="documents_<?= $document['id_media']?>" rel="<?= $document['id_media'];?>">
                            <td>
                            <?php
                            switch($document['type']) {
                                case "gallery":
                                    echo "<i class=\"fa fa-file-archive-o\"></i>"; break;
                                case "txt":
                                    echo "<i class=\"fa fa-file-text-o\"></i>"; break;
                                case "application/msword":
                                    echo "<i class=\"fa fa-file-word-o\"></i>"; break;
                                case "html5":
                                    echo "<i class=\"fa fa-file-code-o\"></i>"; break;
                                case "application/vnd.ms-excel":
                                    echo "<i class=\"fa fa-file-excel-o\"></i>"; break;
                                case "pdf":
                                    echo "<i class=\"fa fa-file-pdf-o\"></i>"; break;
                                case "application/vnd.ms-powerpoint":
                                    echo "<i class=\"fa fa-file-powerpoint-o\"></i>"; break;
                                case "movie":
                                    echo "<i class=\"fa fa-file-video-o\"></i>"; break;
                                case "image":
                                    echo "<i class=\"fa fa-file-image-o\"></i>"; break;
                                case "folder":
                                    echo "<i class=\"fa fa-folder-o\"></i>"; break;
                                default:
                                    echo "<i class=\"fa fa-file-o\"></i>"; break;
                            }
                            ?>
                            </td>
                            <td><?= $document['type'];?></td>
                            <td><?= $document['name'];?></td>
                            <td><?= $document['description'];?></td>
                            <td><i class="fa <?= $document['cansend'] ? 'fa-check-square-o' : 'fa-minus-square-o';?>"></i></td>
                            <td><a id="<?=$document['id_media']?>"><i class="fa fa-minus" onclick="delete_from_documents_table(this)"></i></a></td>
                        </tr>
                    
                        <script type="text/javascript">
                            added_documents.push("<?php echo $document['id_media'];?>");
                        </script>
                    <?php endforeach; ?>
                <?php } ?>
                </tbody>
            </table>            
        </div>       
    </div>
</div>
<?php if(!isset($type)): ?>
<div class="btn-group save_btn_group" id="document_save_btn_group">
    <button class="btn btn-sm btn-default" id="save_documents"><i class="fa fa-thumbs-o-up"></i> Save</button>
</div>
<?php endif; ?>
<?php
$active = 0;
if(isset($documents) && !empty($documents)){
    $active = 1;
}
?>
<input value="<?=$active?>" id="active_documents" type="hidden">

                    