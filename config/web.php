<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'documents' => [
            'class' => 'app\modules\documents\Module',
        ],
        'contacts' => [
            'class' => 'app\modules\contacts\Module',
        ],
        'catalog' => [
            'class' => 'app\modules\catalog\Module',
        ],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
            'treeStructure' => [
                'treeAttribute' => 'root',
                'leftAttribute' => 'nleft',
                'rightAttribute' => 'nright',
                'depthAttribute' => 'level',
            ],
            'dataStructure' => [
                'keyAttribute' => 'id_folder',
                'nameAttribute' => 'label',
                'iconAttribute' => 'icon',
                'iconTypeAttribute' => 'icon_type'
            ],
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'AtdkJ9nWj_pP2iImds_C4ybAS2alscf1',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'trace'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [               
                'contacts' => 'contacts/records/list',                
            ],
        ],
        'view' => [
            //'class' => '\yii\web\View',
            'class' => 'app\components\BaseView',
            'theme' => [
                'pathMap' => [
                    '@app/views',
                    '@app/modules/documents/views' => '@app/themes/Homer',
                    '@app/modules/contacts/views' => '@app/themes/Common',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    /*$config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];*/

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
