/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : zot

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-03-20 13:12:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `migration`
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1456293425');
INSERT INTO `migration` VALUES ('m160127_194202_addDocsTreeColumn', '1456293430');
INSERT INTO `migration` VALUES ('m160223_153409_drop_contrain_activities', '1456293430');
INSERT INTO `migration` VALUES ('m160283_173203_color_field_add', '1457080753');

-- ----------------------------
-- Table structure for `tbl_migration`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) COLLATE utf8_bin NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of tbl_migration
-- ----------------------------
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base', '1380885910');
INSERT INTO `tbl_migration` VALUES ('m130925_092153_create_catalog_cards_agents', '1380885929');
INSERT INTO `tbl_migration` VALUES ('m131017_110214_update_licenses', '1382348110');
INSERT INTO `tbl_migration` VALUES ('m131119_083307_alter_media_add_cansend', '1390299513');
INSERT INTO `tbl_migration` VALUES ('m140121_094437_add_field_visibility_accounts_services_licenses', '1390299513');
INSERT INTO `tbl_migration` VALUES ('m140127_085701_create_news_table', '1391076703');
INSERT INTO `tbl_migration` VALUES ('m140210_112354_create_logs_table', '1392035585');
INSERT INTO `tbl_migration` VALUES ('m140220_092905_update_logs', '1392978020');
INSERT INTO `tbl_migration` VALUES ('m140228_143414_update_logs', '1393934287');
INSERT INTO `tbl_migration` VALUES ('m140317_122638_update_orders', '1395306408');
INSERT INTO `tbl_migration` VALUES ('m140325_121005_create_custom_search_table', '1395751112');
INSERT INTO `tbl_migration` VALUES ('m140509_080233_update_recurrings', '1399868237');
INSERT INTO `tbl_migration` VALUES ('m140522_083412_alter_news_add_category', '1400767023');
INSERT INTO `tbl_migration` VALUES ('m140604_121530_update_orders', '1401888443');
INSERT INTO `tbl_migration` VALUES ('m140703_091430_update_activities', '1404395882');
INSERT INTO `tbl_migration` VALUES ('m140704_102242_update_orders', '1404482632');
INSERT INTO `tbl_migration` VALUES ('m140827_135512_zse_v1_catalog_pdf_pages_add_filename_pdf', '1409318021');
INSERT INTO `tbl_migration` VALUES ('m140916_134643_update_orders', '1410879140');
INSERT INTO `tbl_migration` VALUES ('m141021_134702_zse_v1_news_add_column_fullname', '1414050754');
INSERT INTO `tbl_migration` VALUES ('m141023_081043_zse_v1_news_modify_category', '1414051994');
INSERT INTO `tbl_migration` VALUES ('m141107_083142_update_geotargeting', '1415898220');
INSERT INTO `tbl_migration` VALUES ('m141119_143214_create_zse_v1_docs_tree_tr', '1416407700');
INSERT INTO `tbl_migration` VALUES ('m141201_082545_alter_news_add_lang', '1417620458');
INSERT INTO `tbl_migration` VALUES ('m141219_071328_create_zse_v1_media_tr_apns_admin', '1418973896');
INSERT INTO `tbl_migration` VALUES ('m141223_084657_create_zse_v1_catalog_tree_tr', '1419344998');
INSERT INTO `tbl_migration` VALUES ('m150107_100549_create_zse_v1_catalog_cards_tr', '1420703298');
INSERT INTO `tbl_migration` VALUES ('m150218_095026_create_zse_v1_orders_archive', '1425913637');
INSERT INTO `tbl_migration` VALUES ('m150505_082603_create_zse_v1_offers', '1430823896');
INSERT INTO `tbl_migration` VALUES ('m150512_144111_update_apns_admin', '1431441956');
INSERT INTO `tbl_migration` VALUES ('m150604_085612_zse_v1_reports_tree_add_column_icon', '1433515075');
INSERT INTO `tbl_migration` VALUES ('m150929_123848_alter_catalog_pdf_restrict_by_usercode', '1443714009');
INSERT INTO `tbl_migration` VALUES ('m150929_125516_create_zse_v1_catalog_pdf_access_restrictions', '1443714009');
INSERT INTO `tbl_migration` VALUES ('m151008_110145_update_zse_v1_catalog_pdf_icon', '1444316811');
INSERT INTO `tbl_migration` VALUES ('m151016_084755_alter_catalog_cards_agents', '1445263102');
INSERT INTO `tbl_migration` VALUES ('m151027_154005_create_table_zse_v1_gcm_devices', '1446039668');
INSERT INTO `tbl_migration` VALUES ('m151028_092046_create_table_zse_v1_gcm_messages', '1446039668');
INSERT INTO `tbl_migration` VALUES ('m151029_104801_alter_table_gcm_devices_add_username', '1446116255');
INSERT INTO `tbl_migration` VALUES ('m151030_085348_alter_table_gcm_messages_category_nullable', '1446195568');
INSERT INTO `tbl_migration` VALUES ('m151102_085843_alter_table_gcm_messages_category_nullable', '1446469062');

-- ----------------------------
-- Table structure for `zse_v1`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1`;
CREATE TABLE `zse_v1` (
  `key` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  UNIQUE KEY `key` (`key`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1
-- ----------------------------
INSERT INTO `zse_v1` VALUES ('credits', '100');

-- ----------------------------
-- Table structure for `zse_v1_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_accounts`;
CREATE TABLE `zse_v1_accounts` (
  `id_account` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL DEFAULT '',
  `working_code` varchar(40) NOT NULL DEFAULT '',
  `role` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_account`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_accounts_details`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_accounts_details`;
CREATE TABLE `zse_v1_accounts_details` (
  `id_account` int(11) unsigned NOT NULL,
  `country` varchar(255) NOT NULL,
  `language` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_account`),
  CONSTRAINT `zse_v1_accounts_details_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `zse_v1_accounts` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_accounts_details
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_accounts_devices`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_accounts_devices`;
CREATE TABLE `zse_v1_accounts_devices` (
  `did` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(10) unsigned NOT NULL,
  `udid` varchar(255) NOT NULL,
  `appid` varchar(255) NOT NULL,
  `appver` varchar(255) NOT NULL,
  `geo` varchar(255) NOT NULL,
  `dir` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  PRIMARY KEY (`did`),
  UNIQUE KEY `udid` (`udid`),
  KEY `id_account` (`id_account`),
  CONSTRAINT `zse_v1_accounts_devices_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `zse_v1_accounts` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_accounts_devices
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_accounts_services_licenses`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_accounts_services_licenses`;
CREATE TABLE `zse_v1_accounts_services_licenses` (
  `lid` int(11) NOT NULL AUTO_INCREMENT,
  `id_owner` int(10) unsigned NOT NULL,
  `id_account` int(10) unsigned DEFAULT NULL,
  `id_record` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `serial` varchar(255) NOT NULL,
  `usercode` varchar(255) DEFAULT NULL,
  `userzone` varchar(255) DEFAULT NULL,
  `key` varchar(255) NOT NULL DEFAULT '',
  `expiration` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `connection` tinyint(4) NOT NULL DEFAULT '0',
  `renewal` tinyint(4) NOT NULL DEFAULT '0',
  `visibility` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lid`),
  KEY `id_account` (`id_account`),
  KEY `id_owner` (`id_owner`),
  KEY `id_record` (`id_record`),
  CONSTRAINT `zse_v1_accounts_services_licenses_ibfk_1` FOREIGN KEY (`id_owner`) REFERENCES `zse_v1_accounts` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zse_v1_accounts_services_licenses_ibfk_2` FOREIGN KEY (`id_record`) REFERENCES `zse_v1_records` (`id_record`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_accounts_services_licenses
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_activities`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_activities`;
CREATE TABLE `zse_v1_activities` (
  `id_activity` int(11) NOT NULL AUTO_INCREMENT,
  `agent_code` varchar(255) NOT NULL DEFAULT '',
  `record_code` varchar(32) DEFAULT '',
  `record_destination_code` varchar(64) DEFAULT '',
  `associated_with` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(64) NOT NULL DEFAULT '',
  `status` varchar(64) NOT NULL DEFAULT '',
  `date_insertion` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_creation` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_activity` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `inserted_by` varchar(64) NOT NULL DEFAULT '',
  `ipad_udid` varchar(255) NOT NULL DEFAULT '',
  `management` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext,
  `data` mediumtext,
  `kind` varchar(32) NOT NULL DEFAULT 'standard',
  `delegated_to` varchar(255) NOT NULL DEFAULT '',
  `transfer_status` varchar(32) NOT NULL DEFAULT '',
  `transfer_message` varchar(255) NOT NULL DEFAULT '',
  `entity_id` varchar(64) NOT NULL DEFAULT '',
  `user` varchar(255) DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `parent_id` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_activity`),
  KEY `record_code` (`record_code`),
  KEY `record_destination_code` (`record_destination_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_activities
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_apns_admin`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_apns_admin`;
CREATE TABLE `zse_v1_apns_admin` (
  `pid` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `id_account` int(10) unsigned NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `badge` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `category` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pid`),
  KEY `id_account` (`id_account`),
  CONSTRAINT `zse_v1_apns_admin_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `zse_v1_accounts` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_apns_admin
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_apns_devices`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_apns_devices`;
CREATE TABLE `zse_v1_apns_devices` (
  `pid` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `clientid` varchar(64) NOT NULL DEFAULT '0',
  `appname` varchar(255) NOT NULL,
  `appversion` varchar(25) DEFAULT NULL,
  `deviceuid` char(40) NOT NULL,
  `devicetoken` char(64) NOT NULL,
  `devicename` varchar(255) NOT NULL,
  `devicemodel` varchar(100) NOT NULL,
  `deviceversion` varchar(25) NOT NULL,
  `pushbadge` enum('disabled','enabled') DEFAULT 'disabled',
  `pushalert` enum('disabled','enabled') DEFAULT 'disabled',
  `pushsound` enum('disabled','enabled') DEFAULT 'disabled',
  `development` enum('production','sandbox') CHARACTER SET latin1 NOT NULL DEFAULT 'production',
  `status` enum('active','uninstalled') NOT NULL DEFAULT 'active',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `os` varchar(32) DEFAULT '''iphone''',
  PRIMARY KEY (`pid`),
  UNIQUE KEY `appname` (`appname`,`appversion`,`deviceuid`),
  KEY `devicetoken` (`devicetoken`),
  KEY `devicename` (`devicename`),
  KEY `devicemodel` (`devicemodel`),
  KEY `deviceversion` (`deviceversion`),
  KEY `pushbadge` (`pushbadge`),
  KEY `pushalert` (`pushalert`),
  KEY `pushsound` (`pushsound`),
  KEY `development` (`development`),
  KEY `status` (`status`),
  KEY `created` (`created`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store unique devices';

-- ----------------------------
-- Records of zse_v1_apns_devices
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_apns_messages`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_apns_messages`;
CREATE TABLE `zse_v1_apns_messages` (
  `pid` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `fk_device` int(9) unsigned NOT NULL,
  `message` text NOT NULL,
  `delivery` datetime NOT NULL,
  `status` enum('queued','delivered','failed') CHARACTER SET latin1 NOT NULL DEFAULT 'queued',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pid`),
  KEY `fk_device` (`fk_device`),
  KEY `status` (`status`),
  KEY `created` (`created`),
  KEY `modified` (`modified`),
  KEY `delivery` (`delivery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Messages to push to APNS';

-- ----------------------------
-- Records of zse_v1_apns_messages
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards`;
CREATE TABLE `zse_v1_catalog_cards` (
  `id_card` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) NOT NULL DEFAULT '',
  `type` varchar(32) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `text` mediumtext,
  `characteristics` mediumtext,
  `headframe` mediumtext,
  `data` mediumtext,
  `rate` varchar(30) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_card`),
  KEY `codice_breve` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards` VALUES ('1', 'A001', '', '', 'Articolo Test Sconto Caso 1', 'Anagrafica variante', '', 'CODICE_ANAGRAFICA AND CODICE_VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 1\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V001\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('2', 'A004', '', '', 'Articolo Test Sconto Caso 4 (In sviluppo)', 'Anagrafica dimensione', '', 'CODICE_ANAGRAFICA AND CODICE_DIMENSIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 4\",\"c\":\"004001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V004\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('3', 'A003', '', '', 'Articolo Test Sconto Caso 3', 'Anagrafica dimensione', '', 'CODICE_ANAGRAFICA AND CODICE_DIMENSIONE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 3\",\"c\":\"003001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V003\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('4', 'A002', '', '', 'Articolo Test Sconto Caso 2 (In sviluppo)', 'Anagrafica variante', '', 'CODICE_ANAGRAFICA AND CODICE_VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 2\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V002\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('5', 'A005', '', '', 'Articolo Test Sconto Caso 5', 'Anagrafica articolo', '', 'CODICE_ANAGRAFICA AND CODICE_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('6', 'A006', '', '', 'Articolo Test Sconto Caso 6', 'Anagrafica articolo', '', 'CODICE_ANAGRAFICA AND CODICE_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('7', 'A007', '', '', 'Articolo Test Sconto Caso 7', 'Anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA07\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('8', 'A008', '', '', 'Articolo Test Sconto Caso 8', 'Anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA08\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('9', 'A009', '', '', 'Articolo Test Sconto Caso 9 (In sviluppo)', 'Gruppo anagrafica variante', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 9\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V009\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('10', 'A010', '', '', 'Articolo Test Sconto Caso 10 (In sviluppo)', 'Gruppo anagrafica variante', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 10\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V010\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('11', 'A011', '', '', 'Articolo Test Sconto Caso 11 (In sviluppo)', 'Gruppo anagrafica dimensione', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_DIMENSIONE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 11\",\"c\":\"011001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V011\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('12', 'A012', '', '', 'Articolo Test Sconto Caso 12 (In sviluppo)', 'Gruppo anagrafica dimensione', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_DIMENSIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 12\",\"c\":\"012001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V012\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('13', 'A013', '', '', 'Articolo Test Sconto Caso 13', 'Gruppo anagrafica articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('14', 'A014', '', '', 'Articolo Test Sconto Caso 14', 'Gruppo anagrafica articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('15', 'A015', '', '', 'Articolo Test Sconto Caso 15', 'Gruppo anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA15\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('16', 'A016', '', '', 'Articolo Test Sconto Caso 16', 'Gruppo anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA16\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('17', 'A017', '', '', 'Articolo Test Sconto Caso 21', 'Gruppo articolo dimensione', '', 'CODICE_GRUPPO_ARTICOLO AND CODICE_DIMENSIONE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 21\",\"c\":\"017001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V017\"}]}]}}', '{\"gruppi\":[\"GA17\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('18', 'A018', '', '', 'Articolo Test Sconto Caso 22 (In sviluppo)', 'Gruppo articolo dimensione', '', 'CODICE_GRUPPO_ARTICOLO AND CODICE_DIMENSIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Dimensione Test Sconto Caso 22\",\"c\":\"018001001\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V018\"}]}]}}', '{\"gruppi\":[\"GA18\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('19', 'A019', '', '', 'Articolo Test Sconto Caso 23', 'Gruppo articolo', '', 'CODICE_GRUPPO_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA19\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('20', 'A020', '', '', 'Articolo Test Sconto Caso 24', 'Gruppo articolo', '', 'CODICE_GRUPPO_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA20\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('21', 'A021', '', '', 'Articolo Test Sconto Caso 25', 'Gruppo anagrafica', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('22', 'A022', '', '', 'Articolo Test Sconto Caso 26', 'Gruppo anagrafica', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('23', 'A023', '', '', 'Articolo Test Sconto Caso 27', 'Anagrafica', '', 'CODICE_ANAGRAFICA AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('24', 'A024', '', '', 'Articolo Test Sconto Caso 28', 'Anagrafica', '', 'CODICE_ANAGRAFICA', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('25', 'A058', '', '', 'Articolo Test Sconto Caso 17', 'Variante', '', 'CODICE_VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 17\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V058\"}]}]}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('26', 'A059', '', '', 'Articolo Test Sconto Caso 18 (In sviluppo)', 'Variante', '', 'CODICE_VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Sconto Caso 18\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V059\"}]}]}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('27', 'A060', '', '', 'Articolo Test Sconto Caso 19', 'Articolo', '', 'CODICE_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('28', 'A061', '', '', 'Articolo Test Sconto Caso 20', 'Articolo', '', 'CODICE_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('29', 'A056', '', '', 'Articolo Test Maggiorazione Caso 10', 'Mag. gruppo anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"G56\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('30', 'A055', '', '', 'Articolo Test Maggiorazione Caso 9', 'Mag. gruppo anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"G55\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('31', 'A054', '', '', 'Articolo Test Maggiorazione Caso 8', 'Mag. gruppo anagrafica articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('32', 'A053', '', '', 'Articolo Test Maggiorazione Caso 7', 'Mag. gruppo anagrafica articolo', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('33', 'A052', '', '', 'Articolo Test Maggiorazione Caso 6', 'Mag. anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA52\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('34', 'A051', '', '', 'Articolo Test Maggiorazione Caso 5', 'Mag. anagrafica gruppo articolo', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"GA51\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('35', 'A050', '', '', 'Articolo Test Maggiorazione Caso 4', 'Mag. anagrafica articolo', '', 'CODICE_ANAGRAFICA AND CODICE_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('36', 'A049', '', '', 'Articolo Test Maggiorazione Caso 3', 'Mag. anagrafica articolo', '', 'CODICE_ANAGRAFICA AND CODICE_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('37', 'A048', '', '', 'Articolo Test Maggiorazione Caso 2', 'Mag. anagrafica variante', '', 'CODICE_ANAGRAFICA AND CODICE_VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Maggiorazione Caso 2\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V048\"}]}]}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('38', 'A047', '', '', 'Articolo Test Maggiorazione Caso 1', 'Mag. anagrafica variante', '', 'CODICE_ANAGRAFICA AND CODICE_VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Maggiorazione Caso 1\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V047\"}]}]}}', null, '21.00', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('39', 'A025', '', '', 'Articolo Test Prezzi Caso 1', 'Listino variante (prezzo netto)', '', 'LISTINO AND VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 1\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V025\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('40', 'A027', '', '', 'Articolo Test Prezzi Caso 3', 'Listino articolo (prezzo netto)', '', 'LISTINO AND ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('41', 'A028', '', '', 'Articolo Test Prezzi Caso 4', 'Listino articolo (prezzo netto)', '', 'LISTINO AND ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('42', 'A029', '', '', 'Articolo Test Prezzi Caso 5', 'Anagrafica variante (prezzo personalizzato)', '', 'CODICE_CLIENTE AND VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 5\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V029\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('43', 'A030', '', '', 'Articolo Test Prezzi Caso 6', 'Anagrafica variante (prezzo personalizzato)', '', 'CODICE_CLIENTE AND VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 6\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V030\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('44', 'A031', '', '', 'Articolo Test Prezzi Caso 7', 'Anagrafica articolo (prezzo personalizzato)', '', 'CODICE_CLIENTE AND ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('45', 'A032', '', '', 'Articolo Test Prezzi Caso 8', 'Anagrafica articolo (prezzo personalizzato - in sviluppo)', '', 'CODICE_CLIENTE AND ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('46', 'A033', '', '', 'Articolo Test Prezzi Caso 9', 'Gruppo anagrafica variante (prezzo personalizzato)', '', 'GRUPPO_ANAGRAFICA AND VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 9\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V033\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('47', 'A034', '', '', 'Articolo Test Prezzi Caso 10', 'Gruppo anagrafica variante (prezzo personalizzato)', '', 'GRUPPO_ANAGRAFICA AND VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 10\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V034\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('48', 'A035', '', '', 'Articolo Test Prezzi Caso 11', 'Gruppo anagrafica articolo (prezzo personalizzato)', '', 'GRUPPO_ANAGRAFICA AND ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('49', 'A036', '', '', 'Articolo Test Prezzi Caso 12', 'Gruppo anagrafica articolo (prezzo personalizzato)', '', 'GRUPPO_ANAGRAFICA AND ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('50', 'A037', '', '', 'Articolo Test Prezzi Caso 13', 'Listino variante (prezzo base)', '', 'LISTINO_ANAGRAFICA AND VARIANTE AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 13\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V037\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('51', 'A038', '', '', 'Articolo Test Prezzi Caso 14', 'Listino variante (prezzo base)', '', 'LISTINO_ANAGRAFICA AND VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 14\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V038\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('52', 'A039', '', '', 'Articolo Test Prezzi Caso 15', 'Listino articolo (prezzo base)', '', 'LISTINO_ANAGRAFICA AND ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('53', 'A040', '', '', 'Articolo Test Prezzi Caso 16', 'Listino articolo (prezzo base)', '', 'LISTINO_ANAGRAFICA AND ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('54', 'A026', '', '', 'Articolo Test Prezzi Caso 2', 'Listino variante (prezzo netto)', '', 'LISTINO AND VARIANTE', 'NULL', '{\"versione\":\"2.2.1\",\"opzioni_vincolate\":{\"contesti\":[{\"nome\":\"pronto\",\"dimensioneX\":{\"l\":\"Quantit\\u00e0\",\"lista\":[{\"i\":1,\"l\":\"Quantit\\u00e0\",\"c\":\"MONOCOLONNA\",\"p\":\"1\"}]},\"dimensioneY\":{\"l\":\"Righe(Dimensione Y)\",\"lista\":[{\"i\":1,\"l\":\"Variante Test Prezzi Caso 2\",\"c\":\"\",\"p\":\"\"}]},\"vincoli\":[{\"x\":1,\"y\":1,\"c\":\"V026\"}]}]}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('55', 'A057', '', '', 'Articolo Test Supplemento Caso 1', 'Listino articolo (con supplemento)', '', 'LISTINO_ANAGRAFICA AND ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('56', 'A041', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 1', 'Anagrafica gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN041,FAM041,SER041\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('57', 'A042', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 2', 'Anagrafica gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_ANAGRAFICA AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN042,FAM042\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('58', 'A043', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 3', 'Gruppo anagrafica gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN043\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('59', 'A044', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 4', 'Gruppo anagrafica gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_ANAGRAFICA_RAGGRUPPAMENTO AND CODICE_GRUPPO_ARTICOLI', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN044\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('60', 'A045', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 5', 'Gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_GRUPPO_ARTICOLO AND CONDIZIONE', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN045,FAM045\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('61', 'A046', '', '', 'Articolo Test Sconto Gruppi Articolo Cascata Caso 6', 'Gruppo articolo (sconto gruppo articolo a cascata)', '', 'CODICE_GRUPPO_ARTICOLO', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"LIN046,FAM046,SER046\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('62', 'A062', '', '', 'Articolo Test File Allegati Caso 1', '', '', 'NULL', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards` VALUES ('63', 'A063', '', 'articolo_thumb.png', 'Listino Test Articolo Immagine-Icona Caso 1', '', 'articolo.png', 'NULL', 'NULL', '{\"versione\":\"2.2.1\",\"opzione_quantita\":{\"vista\":\"tastierino\"}}', '{\"gruppi\":[\"NULL\"]}', '21.00', '2015-12-21 10:22:44', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_agents`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_agents`;
CREATE TABLE `zse_v1_catalog_cards_agents` (
  `card_code` varchar(32) NOT NULL DEFAULT '0',
  `agent_code` varchar(32) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`card_code`,`agent_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_agents
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A001', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A001', '001', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A002', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A002', '001', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A003', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A003', '001', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A004', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A004', '001', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A005', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A005', '001', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A006', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A007', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A008', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A009', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A010', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A011', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A012', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A013', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A014', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A015', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A016', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A017', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A018', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A019', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A020', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A021', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A022', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A023', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A024', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A025', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A026', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A027', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A028', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A029', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A030', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A031', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A0325', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A033', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A034', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A035', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A036', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A037', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A038', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A039', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A040', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A041', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A042', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A043', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A044', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A045', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A046', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A050', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A051', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A052', '000', '2015-11-16 17:26:44', '1');
INSERT INTO `zse_v1_catalog_cards_agents` VALUES ('A053', '000', '2015-11-16 17:26:44', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_availability`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_availability`;
CREATE TABLE `zse_v1_catalog_cards_availability` (
  `id_availability` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_code` varchar(128) NOT NULL DEFAULT '0',
  `context` varchar(64) NOT NULL DEFAULT '',
  `type` varchar(32) DEFAULT NULL,
  `availability` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `availability_scheduled` decimal(12,4) DEFAULT NULL,
  `availability_scheduled_date` date DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_availability`),
  UNIQUE KEY `codice` (`card_code`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_availability
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_links`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_links`;
CREATE TABLE `zse_v1_catalog_cards_links` (
  `id_card` int(11) NOT NULL DEFAULT '0',
  `id_link` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '',
  `order` int(11) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_card`,`id_link`),
  KEY `id_link` (`id_link`),
  CONSTRAINT `zse_v1_catalog_cards_links_ibfk_1` FOREIGN KEY (`id_card`) REFERENCES `zse_v1_catalog_cards` (`id_card`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zse_v1_catalog_cards_links_ibfk_2` FOREIGN KEY (`id_link`) REFERENCES `zse_v1_catalog_cards` (`id_card`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_links
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_pricelists`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_pricelists`;
CREATE TABLE `zse_v1_catalog_cards_pricelists` (
  `pricelist_code` varchar(32) NOT NULL DEFAULT '',
  `card_code` varchar(128) NOT NULL DEFAULT '',
  `context` varchar(64) NOT NULL DEFAULT '',
  `price` float(10,4) NOT NULL DEFAULT '0.0000',
  `extra` float(10,4) NOT NULL DEFAULT '0.0000',
  `type` varchar(128) NOT NULL DEFAULT 'LISTINO_ARTICOLO',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pricelist_code`,`card_code`,`context`),
  KEY `tipo` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_pricelists
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('C030', 'V030', '', '82.0000', '0.0000', 'ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('C032', 'A032', '', '39.0000', '0.0000', 'ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('C048', '', '', '4.0000', '0.0000', 'MAG_ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('C050', 'A050', '', '18.0000', '0.0000', 'MAG_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('C052', 'GA52', '', '14.0000', '0.0000', 'MAG_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('G34', 'V034', '', '146.0000', '0.0000', 'GRUPPO_ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('G36', 'A036', '', '59.0000', '0.0000', 'GRUPPO_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('G54', 'A054', '', '27.0000', '0.0000', 'MAG_GRUPPO_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('G56', 'GA56', '', '16.0000', '0.0000', 'MAG_GRUPPO_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A001', '', '170.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A002', '', '130.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A003', '', '144.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A004', '', '156.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A005', '', '37.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A006', '', '28.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A007', '', '45.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A008', '', '87.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A009', '', '215.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A010', '', '15.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A011', '', '68.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A012', '', '72.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A013', '', '122.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A014', '', '66.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A015', '', '44.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A016', '', '139.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A017', '', '178.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A018', '', '208.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A019', '', '110.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A020', '', '80.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A021', '', '22.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A022', '', '43.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A023', '', '12.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A024', '', '126.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A027', '', '62.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A028', '', '124.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A031', '', '87.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A032', '', '55.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A035', '', '34.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A036', '', '71.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A040', '', '24.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A041', '', '33.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A042', '', '54.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A043', '', '67.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A044', '', '93.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A045', '', '122.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A046', '', '77.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A047', '', '10.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A048', '', '13.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A049', '', '16.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A050', '', '21.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A051', '', '24.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A052', '', '29.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A053', '', '31.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A054', '', '36.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A055', '', '41.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A056', '', '45.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A057', '', '40.0000', '13.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A058', '', '12.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A059', '', '25.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A060', '', '34.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A061', '', '33.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A062', '', '100.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'A063', '', '100.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V025', '', '94.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V026', '', '113.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V029', '', '139.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V030', '', '107.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V033', '', '61.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V034', '', '178.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('L001', 'V038', '', '91.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN025', 'C025', '', '0.0000', '0.0000', 'LISTINO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN026', 'C026', '', '0.0000', '0.0000', 'LISTINO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN026', 'V026', '', '93.0000', '0.0000', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN027', 'C027', '', '0.0000', '0.0000', 'LISTINO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN028', 'A028', '', '105.0000', '0.0000', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_pricelists` VALUES ('LN028', 'C028', '', '0.0000', '0.0000', 'LISTINO_ANAGRAFICA', '2015-12-21 10:22:49', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_rebates`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_rebates`;
CREATE TABLE `zse_v1_catalog_cards_rebates` (
  `id_rebate` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record_code` varchar(32) DEFAULT '',
  `card_code` varchar(128) DEFAULT '',
  `context` varchar(32) NOT NULL DEFAULT '',
  `data` mediumtext,
  `type` varchar(128) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_rebate`),
  UNIQUE KEY `record_code` (`record_code`,`card_code`,`context`,`type`),
  KEY `tipo` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_rebates
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('1', 'LN027', 'A027', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":41,\"l\":\"Prezzo corrente\",\"c\":\"Q>14\"}]}', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('2', 'L001', 'A039', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":60,\"l\":\"Prezzo corrente\",\"c\":\"Q>50\"}]}', 'LISTINO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('3', 'LN025', 'V025', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":56,\"l\":\"Prezzo corrente\",\"c\":\"Q>10\"}]}', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('4', 'L001', 'V037', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":78,\"l\":\"Prezzo corrente\",\"c\":\"Q>16\"}]}', 'LISTINO_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('5', 'C023', '', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":23,\"l\":\"23%\",\"c\":\"Q>18\"}]}', 'ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('6', 'C024', '', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":47,\"l\":\"47%\",\"c\":\"\"}]}', 'ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('7', 'G04', '', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":65,\"l\":\"65%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('8', 'G21', '', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":17,\"l\":\"17%\",\"c\":\"Q>7\"}]}', 'GRUPPO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('9', 'G22', '', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":13,\"l\":\"13%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('10', 'C007', 'GA07', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":40,\"l\":\"40%\",\"c\":\"Q<70\"}]}', 'ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('11', 'C008', 'GA08', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":45,\"l\":\"45%\",\"c\":\"\"}]}', 'ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('12', 'C041', 'SER041', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":28,\"l\":\"28%\",\"c\":\"Q>21\"}]}', 'ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('13', 'C042', 'FAM042', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":14,\"l\":\"14%\",\"c\":\"\"}]}', 'ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('14', 'C031', 'A031', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":66,\"l\":\"Prezzo corrente\",\"c\":\"Q>20\"}]}', 'ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('15', 'C006', 'A006', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":35,\"l\":\"35%\",\"c\":\"\"}]}', 'ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('16', 'C005', 'A005', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":30,\"l\":\"30%\",\"c\":\"Q>20 AND Q<60\"}]}', 'ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('17', 'C029', 'V029', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":73,\"l\":\"Prezzo corrente\",\"c\":\"Q>3\"}]}', 'ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('18', 'C001', 'V001', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":10,\"l\":\"10%\",\"c\":\"Q>9\"}]}', 'ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('19', 'C002', 'V002', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":15,\"l\":\"15%\",\"c\":\"\"}]}', 'ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('20', 'G15', 'GA15', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":80,\"l\":\"80%\",\"c\":\"Q>5\"}]}', 'GRUPPO_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('21', 'G16', 'GA16', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":85,\"l\":\"85%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('22', 'G44', 'LIN044', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":5,\"l\":\"5%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('23', 'G43', 'LIN043', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":35,\"l\":\"35%\",\"c\":\"Q>26\"}]}', 'GRUPPO_ANAGRAFICA_GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('24', 'G35', 'A035', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":25,\"l\":\"Prezzo corrente\",\"c\":\"Q>6\"}]}', 'GRUPPO_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('25', 'G13', 'A013', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":70,\"l\":\"70%\",\"c\":\"Q>60\"}]}', 'GRUPPO_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('26', 'G14', 'A014', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":75,\"l\":\"75%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('27', 'G33', 'V033', '', '{\"versione\":\"1.0\",\"prezzi\":[{\"p\":47,\"l\":\"Prezzo corrente\",\"c\":\"Q>30\"}]}', 'GRUPPO_ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('28', 'G09', 'V009', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":50,\"l\":\"50%\",\"c\":\"D > \'20150101\' AND D < \'20500101\'\"}]}', 'GRUPPO_ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('29', 'G10', 'V010', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":55,\"l\":\"55%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA_VARIANTE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('30', '', 'GA03', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":33,\"l\":\"33%\",\"c\":\"Q>12\"},{\"s\":57,\"l\":\"57%\",\"c\":\"\"}]}', 'GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('31', '', 'GA19', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":12,\"l\":\"12%\",\"c\":\"Q>3\"}]}', 'GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('32', '', 'GA20', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":27,\"l\":\"27%\",\"c\":\"\"}]}', 'GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('33', '', 'SER046', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":47,\"l\":\"47%\",\"c\":\"\"}]}', 'GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('34', '', 'FAM045', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":22,\"l\":\"22%\",\"c\":\"Q>70\"}]}', 'GRUPPO_ARTICOLO', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('35', 'C003', '003001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":20,\"l\":\"20%\",\"c\":\"Q<40\"}]}', 'ANAGRAFICA_DIMENSIONE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('36', 'C004', '004001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":25,\"l\":\"25%\",\"c\":\"\"}]}', 'ANAGRAFICA_DIMENSIONE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('37', 'GA17', '017001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":41,\"l\":\"41%\",\"c\":\"Q>26\"}]}', 'GRUPPO_ARTICOLO_DIMENSIONE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('38', 'GA18', '018001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":16,\"l\":\"16%\",\"c\":\"\"}]}', 'GRUPPO_ARTICOLO_DIMENSIONE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('39', 'G11', '011001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":60,\"l\":\"60%\",\"c\":\"D < \'20500101\'\"}]}', 'GRUPPO_ANAGRAFICA_DIMENSIONE', '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_cards_rebates` VALUES ('40', 'G12', '012001001][', '', '{\"versione\":\"1.0\",\"sconti\":[{\"s\":65,\"l\":\"65%\",\"c\":\"\"}]}', 'GRUPPO_ANAGRAFICA_DIMENSIONE', '2015-12-21 10:22:49', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_sections`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_sections`;
CREATE TABLE `zse_v1_catalog_cards_sections` (
  `id_card` int(11) NOT NULL DEFAULT '0',
  `id_catalog_tree` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_card`,`id_catalog_tree`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_sections
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('1', '19', '1', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('2', '19', '2', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('3', '19', '3', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('4', '19', '4', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('5', '19', '5', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('6', '19', '6', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('7', '19', '7', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('8', '19', '8', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('9', '19', '9', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('10', '19', '10', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('11', '19', '11', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('12', '19', '12', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('13', '19', '13', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('14', '19', '14', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('15', '19', '15', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('16', '19', '16', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('17', '19', '17', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('18', '19', '18', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('19', '19', '19', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('20', '19', '20', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('21', '19', '21', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('22', '19', '22', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('23', '19', '23', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('24', '19', '24', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('25', '19', '25', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('26', '19', '26', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('27', '19', '27', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('28', '19', '28', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('29', '19', '29', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('30', '19', '30', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('31', '19', '31', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('32', '19', '32', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('33', '19', '33', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('34', '19', '34', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('35', '19', '35', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('36', '19', '36', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('37', '19', '37', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('38', '19', '38', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('39', '19', '39', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('40', '19', '40', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('41', '19', '41', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('42', '19', '42', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('43', '19', '43', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('44', '19', '44', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('45', '19', '45', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('46', '19', '46', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('47', '19', '47', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('48', '19', '48', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('49', '19', '49', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('50', '19', '50', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('51', '19', '51', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('52', '19', '52', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('53', '19', '53', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('54', '19', '54', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('55', '19', '55', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('56', '19', '56', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('57', '19', '57', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('58', '19', '58', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('59', '19', '59', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('60', '19', '60', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('61', '19', '61', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('62', '19', '62', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_catalog_cards_sections` VALUES ('63', '19', '63', '2015-12-21 10:22:44', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_tr`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_tr`;
CREATE TABLE `zse_v1_catalog_cards_tr` (
  `id_card` int(11) NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `text` mediumtext,
  `characteristics` mediumtext,
  PRIMARY KEY (`id_card`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_tr
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_catalog_cards_variants`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_cards_variants`;
CREATE TABLE `zse_v1_catalog_cards_variants` (
  `code_card` varchar(128) NOT NULL DEFAULT '',
  `code_variant` varchar(128) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`code_card`,`code_variant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_cards_variants
-- ----------------------------
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A001', 'V001', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A002', 'V002', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A003', 'V003', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A004', 'V004', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A009', 'V009', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A010', 'V010', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A011', 'V011', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A012', 'V012', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A017', 'V017', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A018', 'V018', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A025', 'V025', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A026', 'V026', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A029', 'V029', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A030', 'V030', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A033', 'V033', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A034', 'V034', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A037', 'V037', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A038', 'V038', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A047', 'V047', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A048', 'V048', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A058', 'V058', '2015-12-21 10:22:43', '1');
INSERT INTO `zse_v1_catalog_cards_variants` VALUES ('A059', 'V059', '2015-12-21 10:22:43', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_labels`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_labels`;
CREATE TABLE `zse_v1_catalog_labels` (
  `id_label` int(11) NOT NULL AUTO_INCREMENT,
  `code_external` varchar(64) NOT NULL DEFAULT '',
  `code_internal` varchar(64) NOT NULL DEFAULT '',
  `label` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `context` varchar(64) NOT NULL DEFAULT '',
  `type_control` varchar(255) DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_label`),
  UNIQUE KEY `label` (`code_external`,`context`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_labels
-- ----------------------------
INSERT INTO `zse_v1_catalog_labels` VALUES ('1', 'A00', '', 'RIMESSA DIRETTA R.F.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('2', 'A02', '', 'RIMESSA DIRETTA R.F. SC. 2%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('3', 'A03', '', 'RIMESSA DIRETTA R.F. SC. 3%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('4', 'A04', '', 'RIMESSA DIRETTA R.F. SC. 4%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('5', 'A05', '', 'RIMESSA DIRETTA R.F. SC. 5%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('6', 'A08', '', 'RIMESSA DIRETTA R.F. SC. 8%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('7', 'ANT', '', 'ANTICIPATO/ADVANCED', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('8', 'ANT3', '', 'ANTICIPATO SCONTO 3%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('9', 'B00', '', 'RIMESSA DIRETTA 30 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('10', 'B03', '', 'RIMESSA DIRETTA 30 GG DF FM 3%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('11', 'B05', '', 'RIMESSA DIRETTA 30 GG DF FM 5%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('12', 'B100', '', '10 RIMESSA DIRETTA. da 90 a 360 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('13', 'B32', '', 'RIMESSA DIRETTA 30/60 GG. D.F.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('14', 'B33', '', 'RIMESSA DIRETTA 30/60/90 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('15', 'B34', '', 'RIMESSA DIRETTA 30/60/90/120 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('16', 'B35', '', 'RIMESSA DIRETTA 30/60/90/120/150 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('17', 'B36', '', 'RIMESSA DIRETTA 30/60/90/120/150/180 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('18', 'B40', '', 'RIMESSA DIRETTA 120 GG. D.F. FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('19', 'B42', '', 'RIMESSA DIRETTA 120/150 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('20', 'B43', '', 'RIMESSA DIRETTA 120/150/180 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('21', 'B50', '', 'RIMESSA DIRETTA 150 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('22', 'B52', '', 'RIMESSA DIRETTA 150/180 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('23', 'B53', '', 'RIMESSA  DIRETTA 150/180/210 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('24', 'B60', '', 'RIMESSA DIRETTA 60 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('25', 'B62', '', 'RIMESSA DIRETTA 60/90 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('26', 'B63', '', 'RIMESSA DIRETTA 60/90/120 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('27', 'B64', '', 'RIMESSA DIRETTA 60/90/120/150 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('28', 'B65', '', 'RIMESSA DIRETTA 60/90/120/150/180 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('29', 'B67', '', 'RIMESSA DIRETTA 60/90/120/150/180/210/240 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('30', 'B70', '', 'RIMESSA DIRETTA 180 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('31', 'B81', '', 'RIMESSA DIRETTA 240 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('32', 'B82', '', 'RIMESSA DIRETTA 270 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('33', 'B83', '', 'RIMESSA DIRETTA 360 GG D.F.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('34', 'B90', '', 'RIMESSA DIRETTA 90 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('35', 'B92', '', 'RIMESSA DIRETTA 90/120 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('36', 'B93', '', 'RIMESSA DIRETTA 90/120/150 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('37', 'B94', '', 'RIMESSA DIRETTA 90/120/150/180 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('38', 'BL1', '', 'AGAINST B/L', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('39', 'BL3', '', '60 DAYS FROM B/L DATE', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('40', 'CAM', '', 'CAMPIONATURA GRATUITA', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('41', 'CD0', '', 'CONTRO DOCUMENTI A VISTA', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('42', 'CD1', '', 'CONTRO DOCUMENTI A 30 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('43', 'CD2', '', 'CONTRO DOCUMENTI A 60 GG. F.M..', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('44', 'CD3', '', 'CONTRO DOCUMENTI A 90 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('45', 'CD4', '', 'CONTRO DOCUMENTI A 120 GG. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('46', 'CON', '', 'CONTANTI', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('47', 'LC0', '', 'LETTERA DI CREDITO', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('48', 'LC2', '', 'LETTERA DI CREDITO A 60 GG.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('49', 'LC3', '', 'LETTERA DI CREDITO A 90 GIORNI', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('50', 'M00', '', 'CONTRASSEGNO', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('51', 'M02', '', 'CONTRASSEGNO SC. 2%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('52', 'M03', '', 'CONTRASSEGNO SC. 3%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('53', 'M04', '', 'CONTRASSEGNO SC. 4%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('54', 'M05', '', 'CONTRASSEGNO SC. 5%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('55', 'M08', '', 'CONTRASSEGNO SC. 8%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('56', 'R00', '', 'RI.BA. 30 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('57', 'R02', '', 'RI.BA. 30 GG D.F.F.M. SC.  2 %', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('58', 'R03', '', 'RI.BA. 30 GG. D.F. F.M. SC. 3%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('59', 'R04', '', 'RI.BA. 30 GG. D.F. F.M. SC. 4%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('60', 'R05', '', 'RI.BA. 30 GG. D.F. F.M. SC. 5%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('61', 'R06', '', 'RI.BA. 30 GG. D.F. F.M. SC. 6%', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('62', 'R100', '', '10 RI.BA. da 90 a 360 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('63', 'R30', '', 'RI.BA. 30 GG DF', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('64', 'R32', '', 'RI.BA. 30/60 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('65', 'R33', '', 'RI.BA. 30/60/90 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('66', 'R34', '', 'RI.BA. 30/60/90/120 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('67', 'R35', '', 'RI.BA. 30/60/90/120/150 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('68', 'R36', '', 'RI.BA. 30/60/90/120/150/180 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('69', 'R37', '', 'RI.BA. 30/60/90/120/150/180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('70', 'R38', '', 'RI.BA. 30/60/90/120/150/180/210/240 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('71', 'R39', '', 'RI.BA.30/60/90/120/150/180/210/240/270/300 d.f.F.M', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('72', 'R40', '', 'RI.BA. 120 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('73', 'R42', '', 'RI.BA. 120/150 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('74', 'R43', '', 'RI.BA. 120/150/180 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('75', 'R44', '', 'RI.BA. 120/150/180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('76', 'R45', '', 'RI.BA. 120-150-180-210-240 GG.DF.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('77', 'R47', '', 'RI.BA. 120/150/180/210/240/270/300 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('78', 'R49', '', 'RI.BA.120/150/180/210/240/270/300/330/360 GG.FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('79', 'R50', '', 'RI.BA. 150 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('80', 'R52', '', 'RI.BA. 150/180 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('81', 'R53', '', 'RI.BA. 150/180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('82', 'R54', '', 'RI.BA. 150-180-210-240 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('83', 'R55', '', 'RI.BA. 150-180-210-240-270 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('84', 'R56', '', 'RI.BA. 150/180/210/240/270/300 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('85', 'R57', '', 'RI.BA. 150/180/210/240/270/300/330 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('86', 'R58', '', 'RI.BA. 150/180/210/240/270/300/330/360 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('87', 'R60', '', 'RI.BA. 60 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('88', 'R62', '', 'RI.BA. 60/90 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('89', 'R63', '', 'RI.BA. 60/90/120 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('90', 'R64', '', 'RI.BA. 60/90/120/150 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('91', 'R65', '', 'RI.BA. 60/90/120/150/180 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('92', 'R66', '', 'RI.BA. 60/90/120/150/180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('93', 'R67', '', 'RI.BA. 60/90/120/150/180/210/240 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('94', 'R68', '', 'RI.BA. 60/90/120/150/180/210/240/270 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('95', 'R69', '', 'RI.BA. 60/90/120/150/180/210/240/270/300 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('96', 'R70', '', 'RI.BA. 180 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('97', 'R71', '', 'RI.BA 11 DA 60 A 360 GG. FINE MESE', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('98', 'R72', '', 'RI.BA. 180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('99', 'R73', '', 'RI.BA. 180/210/240 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('100', 'R74', '', 'RI.BA. 180/210/240/270 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('101', 'R75', '', 'RI.BA. 180/210/240/270/300 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('102', 'R76', '', 'RI.BA. 180/210/240/270/300/330 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('103', 'R77', '', 'RI.BA. 180/210/240/270/300/330/360 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('104', 'R80', '', 'RI.BA. 210 GG. D.F. F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('105', 'R81', '', 'RI.BA. 240 GG.D.F.FINE MESE', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('106', 'R82', '', 'RI.BA. A 270 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('107', 'R83', '', 'RI.BA. A 360 GG FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('108', 'R90', '', 'RI.BA. 90 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('109', 'R92', '', 'RI.BA. 90/120 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('110', 'R93', '', 'RI.BA. 90/120/150 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('111', 'R94', '', 'RI.BA. 90/120/150/180 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('112', 'R95', '', 'RI.BA. 90/120/150/180/210 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('113', 'R96', '', 'RI.BA. 90-120-150-180-210-240 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('114', 'R97', '', 'RI.BA. 90/120/150/180/210/240/270 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('115', 'R98', '', 'RI.BA. 90/120/150/180/210/240/270/300 GG.D.F.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('116', 'R99', '', 'RI.BA.90/120/150/180/210/240/270/300/330GG.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('117', 'RS1', '', 'RI.BA.MENSILE DA 30 A 360 d.f.F.M', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('118', 'RS2', '', 'RI.BA.MENSILE DA 120 A 390 d.f.F.M.', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('119', 'W01', '', 'RI.BA 60-120-180-240-300-360 FM', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('120', 'Z00', '', 'COME CONVENUTO *', '', '', 'CONDIZIONE_DI_PAGAMENTO', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('128', '00', '', 'DEFAULT', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('129', '02', '', 'EX WORKS', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('130', '03', '', 'F.O.B. PORT EUROPEEN', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('131', '04', '', 'CIF BUDAPEST', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('132', '05', '', 'C.I.F. ITALIAN HARBOUR', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('133', '06', '', 'CIF LEINI', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('134', '07', '', 'C.I.P. TERMINAL DOGANA', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('135', '08', '', 'CFR SKIKDA', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('136', '09', '', 'F.O.B. PORT ITALIEN', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('137', '10', '', 'PORTO FRANCO', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('138', '11', '', 'C.I.F.', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('139', '13', '', 'FERMO DEPOSITO NAPOLI', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('140', '20', '', 'PORTO ASSEGNATO', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('141', '29', '', 'FR.CO C/ADD.FAT.FINO CONFINE', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('142', '30', '', 'PORTO F.CO ADD.FATT.', '', '', 'CONDIZIONE_DI_RESA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('143', '001', '', 'Ordine', '', '', 'TIPO_DOCUMENTO_VENDITA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('144', '002', '', 'Offerta', '', '', 'TIPO_DOCUMENTO_VENDITA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('145', '003', '', 'Preventivo', '', '', 'TIPO_DOCUMENTO_VENDITA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('146', '004', '', 'Proposta Acquisto', '', '', 'TIPO_DOCUMENTO_VENDITA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('147', '005', '', 'Copia Commissione', '', '', 'TIPO_DOCUMENTO_VENDITA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('148', 'P', '', 'Picking Magazzino', '', '', 'ORDINE_STATO_GESTIONALE', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('149', 'R', '', 'Reso', '', '', 'ORDINE_STATO_GESTIONALE', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('150', 'L', '', 'Sosta Logistica', '', '', 'ORDINE_STATO_GESTIONALE', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('151', '004', '', 'Proposta Acquisto', '', '', 'ORDINE_STATO_GESTIONALE', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('152', 'C', '', 'Consegnato', '', '', 'ORDINE_STATO_GESTIONALE', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('153', '099', '', '0.0', 'ESCLUSO ART.3,4^C. L.e)', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('154', '019', '', '19.0', '19%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('155', '020', '', '20.0', '20%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('156', '080', '', '0.0', 'ESCL. ART.4', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('157', '079', '', '0.0', 'ESENTE ART.10 p.27 quinquies', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('158', '050', '', '20.0', '20% AUTOFATT.UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('159', '049', '', '19.0', '19% AUTOFATTURE UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('160', '010', '', '10.0', '10%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('161', '004', '', '4.0', '4%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('162', '081', '', '0.0', 'NON IMPONIBILE ART.8/A', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('163', '082', '', '0.0', 'NON IMPONIBILE ART.8/B', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('164', '083', '', '0.0', 'NON IMPONIBILE ART.8 2C', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('165', '084', '', '0.0', 'ESP.PP.TT.8/A', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('166', '085', '', '0.0', 'INVERSIONE CONTABILE ART.41 UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('167', '086', '', '0.0', 'NON IMPONIBILE ART.9', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('168', '088', '', '0.0', 'ESENTE ART.10', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('169', '089', '', '0.0', 'ESCLUSO ART.2 C3 La', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('170', '090', '', '0.0', 'ESCLUSO ART. 2 C3 Lh', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('171', '091', '', '0.0', 'ESCLUSO ART. 2 C3', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('172', '092', '', '0.0', 'NON IMPONIBILE ART. 40 c4 bis', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('173', '093', '', '0.0', 'ESCLUSO ART.7', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('174', '094', '', '0.0', 'ESCLUSO ART.7-AUTOF', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('175', '095', '', '0.0', 'ESCLUSO ART.15', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('176', '096', '', '0.0', 'ESCLUSO ART.26', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('177', '097', '', '0.0', 'ESCLUSO ART.74 1C Lc', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('178', '098', '', '0.0', 'NON IMPONIBILE ART.58 c1 UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('179', '219', '', '19.0', '19% IND. 50%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('180', '220', '', '20.0', '20% IND. 50%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('181', '310', '', '10.0', '10% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('182', '319', '', '19.0', '19% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('183', '320', '', '20.0', '20% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('184', '078', '', '0.0', 'ESCL.ART.74 C.8 e C. 9', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('185', '304', '', '4.0', '04% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('186', '20C', '', '20.0', '20% OMAGGI UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('187', '060', '', '20.0', '20% AUTOFATTURA ART.17', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('188', '000', '', '0.0', '', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('189', '077', '', '0.0', 'ESCL.ART.74 ter', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('190', '055', '', '20.0', '20% AUTOFATT.UE SERV.', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('191', '420', '', '20.0', '20% IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('192', '076', '', '0.0', 'ESENTE ART.10 c.2', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('193', '100', '', '0.0', 'NON IMPONIBILE A. 8/A S.Marino', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('194', '101', '', '0.0', 'ESCLUSO ART.6 c12 L.133/99', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('195', '20P', '', '20.0', '20% PROFESS.(SU 2%)', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('196', '102', '', '0.0', 'ESCLUSO ART.2', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('197', '048', '', '0.0', 'AUTOFATT. UE ART. 7 ter ESENTI', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('198', '103', '', '0.0', 'ESCLUSO ART.13 C.3', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('199', '450', '', '20.0', '20% AUTOFATT.UE IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('200', '104', '', '0.0', 'ESCLUSO ART.74 1C Ld', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('201', '065', '', '20.0', '20% INTEGR.FT.ART.74 c.7,8', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('202', '016', '', '16.0', '16%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('203', '087', '', '0.0', 'NON IMPONIBILE ART. 41 pt UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('204', '105', '', '0.0', 'ESENTE ART.10 c.1 n.8', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('205', '106', '', '0.0', 'ESCLUSO ART.1', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('206', '520', '', '20.0', '20% IND. 60%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('207', '107', '', '0.0', 'ESCLUSO ART.26BIS L.196/97', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('208', '316', '', '16.0', '16% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('209', '108', '', '0.0', 'NON IMPONIBILE A.1 c100 247/07', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('210', '109', '', '0.0', 'ESCLUSO A.17 c.5 DPR 633/1972', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('211', '110', '', '0.0', 'ESCLUSO ART.7-ter', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('212', '021', '', '21.0', '21%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('213', '051', '', '21.0', '21% AUTOFATT.UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('214', '056', '', '21.0', '21% AUTOFATT.UE SERV.', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('215', '061', '', '21.0', '21% AUTOFATTURA ART.17', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('216', '066', '', '21.0', '21% INTEGR. FT.74 c.7, 8', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('217', '21C', '', '21.0', '21% OMAGGI UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('218', '21P', '', '21.0', '21% PROFESS. (SU 2%)', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('219', '221', '', '21.0', '21% IND 50%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('220', '321', '', '21.0', '21% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('221', '421', '', '21.0', '21% IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('222', '451', '', '21.0', '21% AUTOFATT.UE IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('223', '521', '', '21.0', '21% IND 60%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('224', '422', '', '21.0', '21% IND. 80%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('225', '111', '', '0.0', 'ESCLUSO A.17 c.6 DPR 633/1972', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('226', '112', '', '0.0', 'ESCLUSO A.36 D.L. 41/95', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('227', '022', '', '22.0', '22%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('228', '222', '', '22.0', '22% IND 50%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('229', '522', '', '22.0', '22% IND 60%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('230', '452', '', '22.0', '22% AUTOFATT.UE IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('231', '052', '', '22.0', '22% AUTOFATT.UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('232', '057', '', '22.0', '22% AUTOFATT.UE SERV.', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('233', '067', '', '22.0', '22% INTEGR. FT.74 c.7, 8', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('234', '423', '', '22.0', '22% IND. 90%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('235', '424', '', '22.0', '22% IND. 80%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('236', '062', '', '22.0', '22% AUTOFATTURA ART. 17', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('237', '22P', '', '22.0', '22% PROFESS.(SU 2%)', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('238', '22C', '', '22.0', '22% OMAGGI UE', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_catalog_labels` VALUES ('239', '322', '', '22.0', '22% IND. 100%', '', 'CONDIZIONE_IVA', '', '2015-12-21 10:22:38', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_pdf`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_pdf`;
CREATE TABLE `zse_v1_catalog_pdf` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(11) DEFAULT NULL,
  `key` varchar(255) NOT NULL,
  `restrict_by_usercode` tinyint(1) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `icon` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_pdf
-- ----------------------------
INSERT INTO `zse_v1_catalog_pdf` VALUES ('1', 'asd_vicenza_20131102.pdf', 'Brochure monopagina', 'test', '3', '2fb16f5afc3685489d241955ed139c55c81b4262', '0', '2015-10-26 08:45:18', '1', 'transport_site_new.png');
INSERT INTO `zse_v1_catalog_pdf` VALUES ('2', 'Catalogo_test_2014.pdf', 'Catalogo multipagina', 'test', '3', '8e5273847ac0d8149d8d9e4aa3e8747dc7f465de', '0', '2015-10-26 08:45:16', '1', 'cat_ico_003.png');
INSERT INTO `zse_v1_catalog_pdf` VALUES ('6', 'Stella Bianca_cat2015.pdf', 'Catalogo Stellabianca', 'A4 Verticale - 11MB - 352 pag', '3', 'fafdda8f6452efe87fe119f5454e348b9295b0e5', '0', '2015-10-26 08:45:09', '1', '');
INSERT INTO `zse_v1_catalog_pdf` VALUES ('7', 'pdf_example_corrotto.pdf', 'Catalogo sfogliabile Corrotto', 'File corretto', '1', 'e6335353dffc2392303d825437c4a5cf59a69fc2', '0', '2015-09-21 10:16:26', '1', '');
INSERT INTO `zse_v1_catalog_pdf` VALUES ('8', 'asd_vicenza_20131102.pdf', 'Nuovo ctalogo ', 'test', '1', 'dc6b8af48288a9ab17eb6ce265fac25488353031', '0', '2015-09-24 11:34:18', '1', '');

-- ----------------------------
-- Table structure for `zse_v1_catalog_pdf_access_restrictions`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_pdf_access_restrictions`;
CREATE TABLE `zse_v1_catalog_pdf_access_restrictions` (
  `id_catalog` int(11) NOT NULL,
  `usercode` varchar(255) COLLATE utf8_bin NOT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `fk_zse_v1_catalog_pdf_access_restrictions_1` (`id_catalog`),
  CONSTRAINT `zse_v1_catalog_pdf_access_restrictions_ibfk_1` FOREIGN KEY (`id_catalog`) REFERENCES `zse_v1_catalog_pdf` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_catalog_pdf_access_restrictions
-- ----------------------------
INSERT INTO `zse_v1_catalog_pdf_access_restrictions` VALUES ('1', '002', '2015-10-02 12:28:42');
INSERT INTO `zse_v1_catalog_pdf_access_restrictions` VALUES ('1', '012', '2015-10-02 12:28:43');
INSERT INTO `zse_v1_catalog_pdf_access_restrictions` VALUES ('1', '001', '2015-10-02 12:28:51');
INSERT INTO `zse_v1_catalog_pdf_access_restrictions` VALUES ('2', '000', '2015-10-07 11:28:19');
INSERT INTO `zse_v1_catalog_pdf_access_restrictions` VALUES ('1', '000', '2015-10-15 11:30:06');

-- ----------------------------
-- Table structure for `zse_v1_catalog_pdf_pages`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_pdf_pages`;
CREATE TABLE `zse_v1_catalog_pdf_pages` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filename_pdf` varchar(255) NOT NULL DEFAULT '',
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `pagenumber` int(11) NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `cid` (`cid`),
  CONSTRAINT `zse_v1_catalog_pdf_pages_ibfk_1` FOREIGN KEY (`cid`) REFERENCES `zse_v1_catalog_pdf` (`cid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=536 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_pdf_pages
-- ----------------------------
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('1', '1', 'asd_vicenza_20131102.jpg', '', '595', '842', '0');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('2', '2', 'Catalogo_test_2014-48.jpg', '', '595', '842', '48');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('3', '2', 'Catalogo_test_2014-47.jpg', '', '595', '842', '47');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('4', '2', 'Catalogo_test_2014-46.jpg', '', '595', '842', '46');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('5', '2', 'Catalogo_test_2014-45.jpg', '', '595', '842', '45');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('6', '2', 'Catalogo_test_2014-44.jpg', '', '595', '842', '44');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('7', '2', 'Catalogo_test_2014-43.jpg', '', '595', '842', '43');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('8', '2', 'Catalogo_test_2014-42.jpg', '', '595', '842', '42');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('9', '2', 'Catalogo_test_2014-41.jpg', '', '595', '842', '41');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('10', '2', 'Catalogo_test_2014-40.jpg', '', '595', '842', '40');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('11', '2', 'Catalogo_test_2014-39.jpg', '', '595', '842', '39');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('12', '2', 'Catalogo_test_2014-38.jpg', '', '595', '842', '38');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('13', '2', 'Catalogo_test_2014-37.jpg', '', '595', '842', '37');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('14', '2', 'Catalogo_test_2014-36.jpg', '', '595', '842', '36');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('15', '2', 'Catalogo_test_2014-35.jpg', '', '595', '842', '35');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('16', '2', 'Catalogo_test_2014-34.jpg', '', '595', '842', '34');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('17', '2', 'Catalogo_test_2014-33.jpg', '', '595', '842', '33');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('18', '2', 'Catalogo_test_2014-32.jpg', '', '595', '842', '32');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('19', '2', 'Catalogo_test_2014-31.jpg', '', '595', '842', '31');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('20', '2', 'Catalogo_test_2014-30.jpg', '', '595', '842', '30');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('21', '2', 'Catalogo_test_2014-29.jpg', '', '595', '842', '29');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('22', '2', 'Catalogo_test_2014-28.jpg', '', '595', '842', '28');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('23', '2', 'Catalogo_test_2014-27.jpg', '', '595', '842', '27');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('24', '2', 'Catalogo_test_2014-26.jpg', '', '595', '842', '26');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('25', '2', 'Catalogo_test_2014-25.jpg', '', '595', '842', '25');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('26', '2', 'Catalogo_test_2014-24.jpg', '', '595', '842', '24');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('27', '2', 'Catalogo_test_2014-23.jpg', '', '595', '842', '23');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('28', '2', 'Catalogo_test_2014-22.jpg', '', '595', '842', '22');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('29', '2', 'Catalogo_test_2014-21.jpg', '', '595', '842', '21');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('30', '2', 'Catalogo_test_2014-20.jpg', '', '595', '842', '20');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('31', '2', 'Catalogo_test_2014-19.jpg', '', '595', '842', '19');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('32', '2', 'Catalogo_test_2014-18.jpg', '', '595', '842', '18');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('33', '2', 'Catalogo_test_2014-17.jpg', '', '595', '842', '17');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('34', '2', 'Catalogo_test_2014-16.jpg', '', '595', '842', '16');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('35', '2', 'Catalogo_test_2014-15.jpg', '', '595', '842', '15');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('36', '2', 'Catalogo_test_2014-14.jpg', '', '595', '842', '14');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('37', '2', 'Catalogo_test_2014-13.jpg', '', '595', '842', '13');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('38', '2', 'Catalogo_test_2014-12.jpg', '', '595', '842', '12');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('39', '2', 'Catalogo_test_2014-11.jpg', '', '595', '842', '11');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('40', '2', 'Catalogo_test_2014-10.jpg', '', '595', '842', '10');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('41', '2', 'Catalogo_test_2014-9.jpg', '', '595', '842', '9');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('42', '2', 'Catalogo_test_2014-8.jpg', '', '595', '842', '8');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('43', '2', 'Catalogo_test_2014-7.jpg', '', '595', '842', '7');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('44', '2', 'Catalogo_test_2014-6.jpg', '', '595', '842', '6');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('45', '2', 'Catalogo_test_2014-5.jpg', '', '595', '842', '5');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('46', '2', 'Catalogo_test_2014-4.jpg', '', '595', '842', '4');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('47', '2', 'Catalogo_test_2014-48.jpg', '', '595', '842', '48');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('48', '2', 'Catalogo_test_2014-47.jpg', '', '595', '842', '47');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('49', '2', 'Catalogo_test_2014-46.jpg', '', '595', '842', '46');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('50', '2', 'Catalogo_test_2014-45.jpg', '', '595', '842', '45');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('51', '2', 'Catalogo_test_2014-44.jpg', '', '595', '842', '44');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('52', '2', 'Catalogo_test_2014-43.jpg', '', '595', '842', '43');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('53', '2', 'Catalogo_test_2014-42.jpg', '', '595', '842', '42');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('54', '2', 'Catalogo_test_2014-41.jpg', '', '595', '842', '41');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('55', '2', 'Catalogo_test_2014-40.jpg', '', '595', '842', '40');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('56', '2', 'Catalogo_test_2014-39.jpg', '', '595', '842', '39');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('57', '2', 'Catalogo_test_2014-38.jpg', '', '595', '842', '38');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('58', '2', 'Catalogo_test_2014-37.jpg', '', '595', '842', '37');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('59', '2', 'Catalogo_test_2014-36.jpg', '', '595', '842', '36');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('60', '2', 'Catalogo_test_2014-35.jpg', '', '595', '842', '35');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('61', '2', 'Catalogo_test_2014-34.jpg', '', '595', '842', '34');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('62', '2', 'Catalogo_test_2014-33.jpg', '', '595', '842', '33');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('63', '2', 'Catalogo_test_2014-32.jpg', '', '595', '842', '32');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('64', '2', 'Catalogo_test_2014-31.jpg', '', '595', '842', '31');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('65', '2', 'Catalogo_test_2014-30.jpg', '', '595', '842', '30');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('66', '2', 'Catalogo_test_2014-29.jpg', '', '595', '842', '29');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('67', '2', 'Catalogo_test_2014-28.jpg', '', '595', '842', '28');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('68', '2', 'Catalogo_test_2014-27.jpg', '', '595', '842', '27');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('69', '2', 'Catalogo_test_2014-26.jpg', '', '595', '842', '26');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('70', '2', 'Catalogo_test_2014-25.jpg', '', '595', '842', '25');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('71', '2', 'Catalogo_test_2014-24.jpg', '', '595', '842', '24');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('72', '2', 'Catalogo_test_2014-23.jpg', '', '595', '842', '23');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('73', '2', 'Catalogo_test_2014-22.jpg', '', '595', '842', '22');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('74', '2', 'Catalogo_test_2014-21.jpg', '', '595', '842', '21');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('75', '2', 'Catalogo_test_2014-20.jpg', '', '595', '842', '20');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('76', '2', 'Catalogo_test_2014-19.jpg', '', '595', '842', '19');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('77', '2', 'Catalogo_test_2014-18.jpg', '', '595', '842', '18');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('78', '2', 'Catalogo_test_2014-17.jpg', '', '595', '842', '17');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('79', '2', 'Catalogo_test_2014-16.jpg', '', '595', '842', '16');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('80', '2', 'Catalogo_test_2014-15.jpg', '', '595', '842', '15');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('81', '2', 'Catalogo_test_2014-14.jpg', '', '595', '842', '14');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('82', '2', 'Catalogo_test_2014-13.jpg', '', '595', '842', '13');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('83', '2', 'Catalogo_test_2014-12.jpg', '', '595', '842', '12');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('84', '2', 'Catalogo_test_2014-11.jpg', '', '595', '842', '11');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('85', '2', 'Catalogo_test_2014-10.jpg', '', '595', '842', '10');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('86', '2', 'Catalogo_test_2014-9.jpg', '', '595', '842', '9');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('87', '2', 'Catalogo_test_2014-8.jpg', '', '595', '842', '8');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('88', '2', 'Catalogo_test_2014-7.jpg', '', '595', '842', '7');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('89', '2', 'Catalogo_test_2014-6.jpg', '', '595', '842', '6');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('90', '2', 'Catalogo_test_2014-5.jpg', '', '595', '842', '5');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('91', '2', 'Catalogo_test_2014-4.jpg', '', '595', '842', '4');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('92', '2', 'Catalogo_test_2014-3.jpg', '', '595', '842', '3');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('93', '2', 'Catalogo_test_2014-2.jpg', '', '595', '842', '2');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('94', '2', 'Catalogo_test_2014-1.jpg', '', '595', '842', '1');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('184', '6', 'Stella Bianca_cat2015-352.jpg', '', '595', '842', '352');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('185', '6', 'Stella Bianca_cat2015-351.jpg', '', '595', '842', '351');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('186', '6', 'Stella Bianca_cat2015-350.jpg', '', '595', '842', '350');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('187', '6', 'Stella Bianca_cat2015-349.jpg', '', '595', '842', '349');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('188', '6', 'Stella Bianca_cat2015-348.jpg', '', '595', '842', '348');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('189', '6', 'Stella Bianca_cat2015-347.jpg', '', '595', '842', '347');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('190', '6', 'Stella Bianca_cat2015-346.jpg', '', '595', '842', '346');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('191', '6', 'Stella Bianca_cat2015-345.jpg', '', '595', '842', '345');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('192', '6', 'Stella Bianca_cat2015-344.jpg', '', '595', '842', '344');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('193', '6', 'Stella Bianca_cat2015-343.jpg', '', '595', '842', '343');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('194', '6', 'Stella Bianca_cat2015-342.jpg', '', '595', '842', '342');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('195', '6', 'Stella Bianca_cat2015-341.jpg', '', '595', '842', '341');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('196', '6', 'Stella Bianca_cat2015-340.jpg', '', '595', '842', '340');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('197', '6', 'Stella Bianca_cat2015-339.jpg', '', '595', '842', '339');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('198', '6', 'Stella Bianca_cat2015-338.jpg', '', '595', '842', '338');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('199', '6', 'Stella Bianca_cat2015-337.jpg', '', '595', '842', '337');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('200', '6', 'Stella Bianca_cat2015-336.jpg', '', '595', '842', '336');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('201', '6', 'Stella Bianca_cat2015-335.jpg', '', '595', '842', '335');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('202', '6', 'Stella Bianca_cat2015-334.jpg', '', '595', '842', '334');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('203', '6', 'Stella Bianca_cat2015-333.jpg', '', '595', '842', '333');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('204', '6', 'Stella Bianca_cat2015-332.jpg', '', '595', '842', '332');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('205', '6', 'Stella Bianca_cat2015-331.jpg', '', '595', '842', '331');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('206', '6', 'Stella Bianca_cat2015-330.jpg', '', '595', '842', '330');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('207', '6', 'Stella Bianca_cat2015-329.jpg', '', '595', '842', '329');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('208', '6', 'Stella Bianca_cat2015-328.jpg', '', '595', '842', '328');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('209', '6', 'Stella Bianca_cat2015-327.jpg', '', '595', '842', '327');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('210', '6', 'Stella Bianca_cat2015-326.jpg', '', '595', '842', '326');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('211', '6', 'Stella Bianca_cat2015-325.jpg', '', '595', '842', '325');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('212', '6', 'Stella Bianca_cat2015-324.jpg', '', '595', '842', '324');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('213', '6', 'Stella Bianca_cat2015-323.jpg', '', '595', '842', '323');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('214', '6', 'Stella Bianca_cat2015-322.jpg', '', '595', '842', '322');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('215', '6', 'Stella Bianca_cat2015-321.jpg', '', '595', '842', '321');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('216', '6', 'Stella Bianca_cat2015-320.jpg', '', '595', '842', '320');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('217', '6', 'Stella Bianca_cat2015-319.jpg', '', '595', '842', '319');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('218', '6', 'Stella Bianca_cat2015-318.jpg', '', '595', '842', '318');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('219', '6', 'Stella Bianca_cat2015-317.jpg', '', '595', '842', '317');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('220', '6', 'Stella Bianca_cat2015-316.jpg', '', '595', '842', '316');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('221', '6', 'Stella Bianca_cat2015-315.jpg', '', '595', '842', '315');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('222', '6', 'Stella Bianca_cat2015-314.jpg', '', '595', '842', '314');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('223', '6', 'Stella Bianca_cat2015-313.jpg', '', '595', '842', '313');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('224', '6', 'Stella Bianca_cat2015-312.jpg', '', '595', '842', '312');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('225', '6', 'Stella Bianca_cat2015-311.jpg', '', '595', '842', '311');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('226', '6', 'Stella Bianca_cat2015-310.jpg', '', '595', '842', '310');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('227', '6', 'Stella Bianca_cat2015-309.jpg', '', '595', '842', '309');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('228', '6', 'Stella Bianca_cat2015-308.jpg', '', '595', '842', '308');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('229', '6', 'Stella Bianca_cat2015-307.jpg', '', '595', '842', '307');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('230', '6', 'Stella Bianca_cat2015-306.jpg', '', '595', '842', '306');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('231', '6', 'Stella Bianca_cat2015-305.jpg', '', '595', '842', '305');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('232', '6', 'Stella Bianca_cat2015-304.jpg', '', '595', '842', '304');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('233', '6', 'Stella Bianca_cat2015-303.jpg', '', '595', '842', '303');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('234', '6', 'Stella Bianca_cat2015-302.jpg', '', '595', '842', '302');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('235', '6', 'Stella Bianca_cat2015-301.jpg', '', '595', '842', '301');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('236', '6', 'Stella Bianca_cat2015-300.jpg', '', '595', '842', '300');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('237', '6', 'Stella Bianca_cat2015-299.jpg', '', '595', '842', '299');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('238', '6', 'Stella Bianca_cat2015-298.jpg', '', '595', '842', '298');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('239', '6', 'Stella Bianca_cat2015-297.jpg', '', '595', '842', '297');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('240', '6', 'Stella Bianca_cat2015-296.jpg', '', '595', '842', '296');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('241', '6', 'Stella Bianca_cat2015-295.jpg', '', '595', '842', '295');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('242', '6', 'Stella Bianca_cat2015-294.jpg', '', '595', '842', '294');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('243', '6', 'Stella Bianca_cat2015-293.jpg', '', '595', '842', '293');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('244', '6', 'Stella Bianca_cat2015-292.jpg', '', '595', '842', '292');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('245', '6', 'Stella Bianca_cat2015-291.jpg', '', '595', '842', '291');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('246', '6', 'Stella Bianca_cat2015-290.jpg', '', '595', '842', '290');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('247', '6', 'Stella Bianca_cat2015-289.jpg', '', '595', '842', '289');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('248', '6', 'Stella Bianca_cat2015-288.jpg', '', '595', '842', '288');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('249', '6', 'Stella Bianca_cat2015-287.jpg', '', '595', '842', '287');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('250', '6', 'Stella Bianca_cat2015-286.jpg', '', '595', '842', '286');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('251', '6', 'Stella Bianca_cat2015-285.jpg', '', '595', '842', '285');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('252', '6', 'Stella Bianca_cat2015-284.jpg', '', '595', '842', '284');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('253', '6', 'Stella Bianca_cat2015-283.jpg', '', '595', '842', '283');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('254', '6', 'Stella Bianca_cat2015-282.jpg', '', '595', '842', '282');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('255', '6', 'Stella Bianca_cat2015-281.jpg', '', '595', '842', '281');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('256', '6', 'Stella Bianca_cat2015-280.jpg', '', '595', '842', '280');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('257', '6', 'Stella Bianca_cat2015-279.jpg', '', '595', '842', '279');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('258', '6', 'Stella Bianca_cat2015-278.jpg', '', '595', '842', '278');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('259', '6', 'Stella Bianca_cat2015-277.jpg', '', '595', '842', '277');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('260', '6', 'Stella Bianca_cat2015-276.jpg', '', '595', '842', '276');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('261', '6', 'Stella Bianca_cat2015-275.jpg', '', '595', '842', '275');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('262', '6', 'Stella Bianca_cat2015-274.jpg', '', '595', '842', '274');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('263', '6', 'Stella Bianca_cat2015-273.jpg', '', '595', '842', '273');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('264', '6', 'Stella Bianca_cat2015-272.jpg', '', '595', '842', '272');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('265', '6', 'Stella Bianca_cat2015-271.jpg', '', '595', '842', '271');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('266', '6', 'Stella Bianca_cat2015-270.jpg', '', '595', '842', '270');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('267', '6', 'Stella Bianca_cat2015-269.jpg', '', '595', '842', '269');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('268', '6', 'Stella Bianca_cat2015-268.jpg', '', '595', '842', '268');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('269', '6', 'Stella Bianca_cat2015-267.jpg', '', '595', '842', '267');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('270', '6', 'Stella Bianca_cat2015-266.jpg', '', '595', '842', '266');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('271', '6', 'Stella Bianca_cat2015-265.jpg', '', '595', '842', '265');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('272', '6', 'Stella Bianca_cat2015-264.jpg', '', '595', '842', '264');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('273', '6', 'Stella Bianca_cat2015-263.jpg', '', '595', '842', '263');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('274', '6', 'Stella Bianca_cat2015-262.jpg', '', '595', '842', '262');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('275', '6', 'Stella Bianca_cat2015-261.jpg', '', '595', '842', '261');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('276', '6', 'Stella Bianca_cat2015-260.jpg', '', '595', '842', '260');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('277', '6', 'Stella Bianca_cat2015-259.jpg', '', '595', '842', '259');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('278', '6', 'Stella Bianca_cat2015-258.jpg', '', '595', '842', '258');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('279', '6', 'Stella Bianca_cat2015-257.jpg', '', '595', '842', '257');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('280', '6', 'Stella Bianca_cat2015-256.jpg', '', '595', '842', '256');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('281', '6', 'Stella Bianca_cat2015-255.jpg', '', '595', '842', '255');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('282', '6', 'Stella Bianca_cat2015-254.jpg', '', '595', '842', '254');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('283', '6', 'Stella Bianca_cat2015-253.jpg', '', '595', '842', '253');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('284', '6', 'Stella Bianca_cat2015-252.jpg', '', '595', '842', '252');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('285', '6', 'Stella Bianca_cat2015-251.jpg', '', '595', '842', '251');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('286', '6', 'Stella Bianca_cat2015-250.jpg', '', '595', '842', '250');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('287', '6', 'Stella Bianca_cat2015-249.jpg', '', '595', '842', '249');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('288', '6', 'Stella Bianca_cat2015-248.jpg', '', '595', '842', '248');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('289', '6', 'Stella Bianca_cat2015-247.jpg', '', '595', '842', '247');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('290', '6', 'Stella Bianca_cat2015-246.jpg', '', '595', '842', '246');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('291', '6', 'Stella Bianca_cat2015-245.jpg', '', '595', '842', '245');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('292', '6', 'Stella Bianca_cat2015-244.jpg', '', '595', '842', '244');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('293', '6', 'Stella Bianca_cat2015-243.jpg', '', '595', '842', '243');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('294', '6', 'Stella Bianca_cat2015-242.jpg', '', '595', '842', '242');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('295', '6', 'Stella Bianca_cat2015-241.jpg', '', '595', '842', '241');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('296', '6', 'Stella Bianca_cat2015-240.jpg', '', '595', '842', '240');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('297', '6', 'Stella Bianca_cat2015-239.jpg', '', '595', '842', '239');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('298', '6', 'Stella Bianca_cat2015-238.jpg', '', '595', '842', '238');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('299', '6', 'Stella Bianca_cat2015-237.jpg', '', '595', '842', '237');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('300', '6', 'Stella Bianca_cat2015-236.jpg', '', '595', '842', '236');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('301', '6', 'Stella Bianca_cat2015-235.jpg', '', '595', '842', '235');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('302', '6', 'Stella Bianca_cat2015-234.jpg', '', '595', '842', '234');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('303', '6', 'Stella Bianca_cat2015-233.jpg', '', '595', '842', '233');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('304', '6', 'Stella Bianca_cat2015-232.jpg', '', '595', '842', '232');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('305', '6', 'Stella Bianca_cat2015-231.jpg', '', '595', '842', '231');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('306', '6', 'Stella Bianca_cat2015-230.jpg', '', '595', '842', '230');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('307', '6', 'Stella Bianca_cat2015-229.jpg', '', '595', '842', '229');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('308', '6', 'Stella Bianca_cat2015-228.jpg', '', '595', '842', '228');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('309', '6', 'Stella Bianca_cat2015-227.jpg', '', '595', '842', '227');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('310', '6', 'Stella Bianca_cat2015-226.jpg', '', '595', '842', '226');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('311', '6', 'Stella Bianca_cat2015-225.jpg', '', '595', '842', '225');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('312', '6', 'Stella Bianca_cat2015-224.jpg', '', '595', '842', '224');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('313', '6', 'Stella Bianca_cat2015-223.jpg', '', '595', '842', '223');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('314', '6', 'Stella Bianca_cat2015-222.jpg', '', '595', '842', '222');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('315', '6', 'Stella Bianca_cat2015-221.jpg', '', '595', '842', '221');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('316', '6', 'Stella Bianca_cat2015-220.jpg', '', '595', '842', '220');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('317', '6', 'Stella Bianca_cat2015-219.jpg', '', '595', '842', '219');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('318', '6', 'Stella Bianca_cat2015-218.jpg', '', '595', '842', '218');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('319', '6', 'Stella Bianca_cat2015-217.jpg', '', '595', '842', '217');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('320', '6', 'Stella Bianca_cat2015-216.jpg', '', '595', '842', '216');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('321', '6', 'Stella Bianca_cat2015-215.jpg', '', '595', '842', '215');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('322', '6', 'Stella Bianca_cat2015-214.jpg', '', '595', '842', '214');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('323', '6', 'Stella Bianca_cat2015-213.jpg', '', '595', '842', '213');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('324', '6', 'Stella Bianca_cat2015-212.jpg', '', '595', '842', '212');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('325', '6', 'Stella Bianca_cat2015-211.jpg', '', '595', '842', '211');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('326', '6', 'Stella Bianca_cat2015-210.jpg', '', '595', '842', '210');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('327', '6', 'Stella Bianca_cat2015-209.jpg', '', '595', '842', '209');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('328', '6', 'Stella Bianca_cat2015-208.jpg', '', '595', '842', '208');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('329', '6', 'Stella Bianca_cat2015-207.jpg', '', '595', '842', '207');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('330', '6', 'Stella Bianca_cat2015-206.jpg', '', '595', '842', '206');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('331', '6', 'Stella Bianca_cat2015-205.jpg', '', '595', '842', '205');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('332', '6', 'Stella Bianca_cat2015-204.jpg', '', '595', '842', '204');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('333', '6', 'Stella Bianca_cat2015-203.jpg', '', '595', '842', '203');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('334', '6', 'Stella Bianca_cat2015-202.jpg', '', '595', '842', '202');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('335', '6', 'Stella Bianca_cat2015-201.jpg', '', '595', '842', '201');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('336', '6', 'Stella Bianca_cat2015-200.jpg', '', '595', '842', '200');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('337', '6', 'Stella Bianca_cat2015-199.jpg', '', '595', '842', '199');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('338', '6', 'Stella Bianca_cat2015-198.jpg', '', '595', '842', '198');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('339', '6', 'Stella Bianca_cat2015-197.jpg', '', '595', '842', '197');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('340', '6', 'Stella Bianca_cat2015-196.jpg', '', '595', '842', '196');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('341', '6', 'Stella Bianca_cat2015-195.jpg', '', '595', '842', '195');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('342', '6', 'Stella Bianca_cat2015-194.jpg', '', '595', '842', '194');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('343', '6', 'Stella Bianca_cat2015-193.jpg', '', '595', '842', '193');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('344', '6', 'Stella Bianca_cat2015-192.jpg', '', '595', '842', '192');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('345', '6', 'Stella Bianca_cat2015-191.jpg', '', '595', '842', '191');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('346', '6', 'Stella Bianca_cat2015-190.jpg', '', '595', '842', '190');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('347', '6', 'Stella Bianca_cat2015-189.jpg', '', '595', '842', '189');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('348', '6', 'Stella Bianca_cat2015-188.jpg', '', '595', '842', '188');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('349', '6', 'Stella Bianca_cat2015-187.jpg', '', '595', '842', '187');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('350', '6', 'Stella Bianca_cat2015-186.jpg', '', '595', '842', '186');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('351', '6', 'Stella Bianca_cat2015-185.jpg', '', '595', '842', '185');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('352', '6', 'Stella Bianca_cat2015-184.jpg', '', '595', '842', '184');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('353', '6', 'Stella Bianca_cat2015-183.jpg', '', '595', '842', '183');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('354', '6', 'Stella Bianca_cat2015-182.jpg', '', '595', '842', '182');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('355', '6', 'Stella Bianca_cat2015-181.jpg', '', '595', '842', '181');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('356', '6', 'Stella Bianca_cat2015-180.jpg', '', '595', '842', '180');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('357', '6', 'Stella Bianca_cat2015-179.jpg', '', '595', '842', '179');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('358', '6', 'Stella Bianca_cat2015-178.jpg', '', '595', '842', '178');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('359', '6', 'Stella Bianca_cat2015-177.jpg', '', '595', '842', '177');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('360', '6', 'Stella Bianca_cat2015-176.jpg', '', '595', '842', '176');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('361', '6', 'Stella Bianca_cat2015-175.jpg', '', '595', '842', '175');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('362', '6', 'Stella Bianca_cat2015-174.jpg', '', '595', '842', '174');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('363', '6', 'Stella Bianca_cat2015-173.jpg', '', '595', '842', '173');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('364', '6', 'Stella Bianca_cat2015-172.jpg', '', '595', '842', '172');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('365', '6', 'Stella Bianca_cat2015-171.jpg', '', '595', '842', '171');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('366', '6', 'Stella Bianca_cat2015-170.jpg', '', '595', '842', '170');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('367', '6', 'Stella Bianca_cat2015-169.jpg', '', '595', '842', '169');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('368', '6', 'Stella Bianca_cat2015-168.jpg', '', '595', '842', '168');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('369', '6', 'Stella Bianca_cat2015-167.jpg', '', '595', '842', '167');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('370', '6', 'Stella Bianca_cat2015-166.jpg', '', '595', '842', '166');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('371', '6', 'Stella Bianca_cat2015-165.jpg', '', '595', '842', '165');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('372', '6', 'Stella Bianca_cat2015-164.jpg', '', '595', '842', '164');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('373', '6', 'Stella Bianca_cat2015-163.jpg', '', '595', '842', '163');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('374', '6', 'Stella Bianca_cat2015-162.jpg', '', '595', '842', '162');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('375', '6', 'Stella Bianca_cat2015-161.jpg', '', '595', '842', '161');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('376', '6', 'Stella Bianca_cat2015-160.jpg', '', '595', '842', '160');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('377', '6', 'Stella Bianca_cat2015-159.jpg', '', '595', '842', '159');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('378', '6', 'Stella Bianca_cat2015-158.jpg', '', '595', '842', '158');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('379', '6', 'Stella Bianca_cat2015-157.jpg', '', '595', '842', '157');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('380', '6', 'Stella Bianca_cat2015-156.jpg', '', '595', '842', '156');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('381', '6', 'Stella Bianca_cat2015-155.jpg', '', '595', '842', '155');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('382', '6', 'Stella Bianca_cat2015-154.jpg', '', '595', '842', '154');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('383', '6', 'Stella Bianca_cat2015-153.jpg', '', '595', '842', '153');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('384', '6', 'Stella Bianca_cat2015-152.jpg', '', '595', '842', '152');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('385', '6', 'Stella Bianca_cat2015-151.jpg', '', '595', '842', '151');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('386', '6', 'Stella Bianca_cat2015-150.jpg', '', '595', '842', '150');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('387', '6', 'Stella Bianca_cat2015-149.jpg', '', '595', '842', '149');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('388', '6', 'Stella Bianca_cat2015-148.jpg', '', '595', '842', '148');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('389', '6', 'Stella Bianca_cat2015-147.jpg', '', '595', '842', '147');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('390', '6', 'Stella Bianca_cat2015-146.jpg', '', '595', '842', '146');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('391', '6', 'Stella Bianca_cat2015-145.jpg', '', '595', '842', '145');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('392', '6', 'Stella Bianca_cat2015-144.jpg', '', '595', '842', '144');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('393', '6', 'Stella Bianca_cat2015-143.jpg', '', '595', '842', '143');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('394', '6', 'Stella Bianca_cat2015-142.jpg', '', '595', '842', '142');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('395', '6', 'Stella Bianca_cat2015-141.jpg', '', '595', '842', '141');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('396', '6', 'Stella Bianca_cat2015-140.jpg', '', '595', '842', '140');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('397', '6', 'Stella Bianca_cat2015-139.jpg', '', '595', '842', '139');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('398', '6', 'Stella Bianca_cat2015-138.jpg', '', '595', '842', '138');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('399', '6', 'Stella Bianca_cat2015-137.jpg', '', '595', '842', '137');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('400', '6', 'Stella Bianca_cat2015-136.jpg', '', '595', '842', '136');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('401', '6', 'Stella Bianca_cat2015-135.jpg', '', '595', '842', '135');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('402', '6', 'Stella Bianca_cat2015-134.jpg', '', '595', '842', '134');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('403', '6', 'Stella Bianca_cat2015-133.jpg', '', '595', '842', '133');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('404', '6', 'Stella Bianca_cat2015-132.jpg', '', '595', '842', '132');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('405', '6', 'Stella Bianca_cat2015-131.jpg', '', '595', '842', '131');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('406', '6', 'Stella Bianca_cat2015-130.jpg', '', '595', '842', '130');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('407', '6', 'Stella Bianca_cat2015-129.jpg', '', '595', '842', '129');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('408', '6', 'Stella Bianca_cat2015-128.jpg', '', '595', '842', '128');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('409', '6', 'Stella Bianca_cat2015-127.jpg', '', '595', '842', '127');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('410', '6', 'Stella Bianca_cat2015-126.jpg', '', '595', '842', '126');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('411', '6', 'Stella Bianca_cat2015-125.jpg', '', '595', '842', '125');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('412', '6', 'Stella Bianca_cat2015-124.jpg', '', '595', '842', '124');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('413', '6', 'Stella Bianca_cat2015-123.jpg', '', '595', '842', '123');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('414', '6', 'Stella Bianca_cat2015-122.jpg', '', '595', '842', '122');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('415', '6', 'Stella Bianca_cat2015-121.jpg', '', '595', '842', '121');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('416', '6', 'Stella Bianca_cat2015-120.jpg', '', '595', '842', '120');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('417', '6', 'Stella Bianca_cat2015-119.jpg', '', '595', '842', '119');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('418', '6', 'Stella Bianca_cat2015-118.jpg', '', '595', '842', '118');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('419', '6', 'Stella Bianca_cat2015-117.jpg', '', '595', '842', '117');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('420', '6', 'Stella Bianca_cat2015-116.jpg', '', '595', '842', '116');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('421', '6', 'Stella Bianca_cat2015-115.jpg', '', '595', '842', '115');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('422', '6', 'Stella Bianca_cat2015-114.jpg', '', '595', '842', '114');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('423', '6', 'Stella Bianca_cat2015-113.jpg', '', '595', '842', '113');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('424', '6', 'Stella Bianca_cat2015-112.jpg', '', '595', '842', '112');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('425', '6', 'Stella Bianca_cat2015-111.jpg', '', '595', '842', '111');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('426', '6', 'Stella Bianca_cat2015-110.jpg', '', '595', '842', '110');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('427', '6', 'Stella Bianca_cat2015-109.jpg', '', '595', '842', '109');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('428', '6', 'Stella Bianca_cat2015-108.jpg', '', '595', '842', '108');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('429', '6', 'Stella Bianca_cat2015-107.jpg', '', '595', '842', '107');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('430', '6', 'Stella Bianca_cat2015-106.jpg', '', '595', '842', '106');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('431', '6', 'Stella Bianca_cat2015-105.jpg', '', '595', '842', '105');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('432', '6', 'Stella Bianca_cat2015-104.jpg', '', '595', '842', '104');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('433', '6', 'Stella Bianca_cat2015-103.jpg', '', '595', '842', '103');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('434', '6', 'Stella Bianca_cat2015-102.jpg', '', '595', '842', '102');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('435', '6', 'Stella Bianca_cat2015-101.jpg', '', '595', '842', '101');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('436', '6', 'Stella Bianca_cat2015-100.jpg', '', '595', '842', '100');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('437', '6', 'Stella Bianca_cat2015-99.jpg', '', '595', '842', '99');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('438', '6', 'Stella Bianca_cat2015-98.jpg', '', '595', '842', '98');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('439', '6', 'Stella Bianca_cat2015-97.jpg', '', '595', '842', '97');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('440', '6', 'Stella Bianca_cat2015-96.jpg', '', '595', '842', '96');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('441', '6', 'Stella Bianca_cat2015-95.jpg', '', '595', '842', '95');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('442', '6', 'Stella Bianca_cat2015-94.jpg', '', '595', '842', '94');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('443', '6', 'Stella Bianca_cat2015-93.jpg', '', '595', '842', '93');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('444', '6', 'Stella Bianca_cat2015-92.jpg', '', '595', '842', '92');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('445', '6', 'Stella Bianca_cat2015-91.jpg', '', '595', '842', '91');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('446', '6', 'Stella Bianca_cat2015-90.jpg', '', '595', '842', '90');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('447', '6', 'Stella Bianca_cat2015-89.jpg', '', '595', '842', '89');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('448', '6', 'Stella Bianca_cat2015-88.jpg', '', '595', '842', '88');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('449', '6', 'Stella Bianca_cat2015-87.jpg', '', '595', '842', '87');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('450', '6', 'Stella Bianca_cat2015-86.jpg', '', '595', '842', '86');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('451', '6', 'Stella Bianca_cat2015-85.jpg', '', '595', '842', '85');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('452', '6', 'Stella Bianca_cat2015-84.jpg', '', '595', '842', '84');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('453', '6', 'Stella Bianca_cat2015-83.jpg', '', '595', '842', '83');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('454', '6', 'Stella Bianca_cat2015-82.jpg', '', '595', '842', '82');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('455', '6', 'Stella Bianca_cat2015-81.jpg', '', '595', '842', '81');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('456', '6', 'Stella Bianca_cat2015-80.jpg', '', '595', '842', '80');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('457', '6', 'Stella Bianca_cat2015-79.jpg', '', '595', '842', '79');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('458', '6', 'Stella Bianca_cat2015-78.jpg', '', '595', '842', '78');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('459', '6', 'Stella Bianca_cat2015-77.jpg', '', '595', '842', '77');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('460', '6', 'Stella Bianca_cat2015-76.jpg', '', '595', '842', '76');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('461', '6', 'Stella Bianca_cat2015-75.jpg', '', '595', '842', '75');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('462', '6', 'Stella Bianca_cat2015-74.jpg', '', '595', '842', '74');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('463', '6', 'Stella Bianca_cat2015-73.jpg', '', '595', '842', '73');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('464', '6', 'Stella Bianca_cat2015-72.jpg', '', '595', '842', '72');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('465', '6', 'Stella Bianca_cat2015-71.jpg', '', '595', '842', '71');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('466', '6', 'Stella Bianca_cat2015-70.jpg', '', '595', '842', '70');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('467', '6', 'Stella Bianca_cat2015-69.jpg', '', '595', '842', '69');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('468', '6', 'Stella Bianca_cat2015-68.jpg', '', '595', '842', '68');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('469', '6', 'Stella Bianca_cat2015-67.jpg', '', '595', '842', '67');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('470', '6', 'Stella Bianca_cat2015-66.jpg', '', '595', '842', '66');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('471', '6', 'Stella Bianca_cat2015-65.jpg', '', '595', '842', '65');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('472', '6', 'Stella Bianca_cat2015-64.jpg', '', '595', '842', '64');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('473', '6', 'Stella Bianca_cat2015-63.jpg', '', '595', '842', '63');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('474', '6', 'Stella Bianca_cat2015-62.jpg', '', '595', '842', '62');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('475', '6', 'Stella Bianca_cat2015-61.jpg', '', '595', '842', '61');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('476', '6', 'Stella Bianca_cat2015-60.jpg', '', '595', '842', '60');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('477', '6', 'Stella Bianca_cat2015-59.jpg', '', '595', '842', '59');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('478', '6', 'Stella Bianca_cat2015-58.jpg', '', '595', '842', '58');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('479', '6', 'Stella Bianca_cat2015-57.jpg', '', '595', '842', '57');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('480', '6', 'Stella Bianca_cat2015-56.jpg', '', '595', '842', '56');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('481', '6', 'Stella Bianca_cat2015-55.jpg', '', '595', '842', '55');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('482', '6', 'Stella Bianca_cat2015-54.jpg', '', '595', '842', '54');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('483', '6', 'Stella Bianca_cat2015-53.jpg', '', '595', '842', '53');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('484', '6', 'Stella Bianca_cat2015-52.jpg', '', '595', '842', '52');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('485', '6', 'Stella Bianca_cat2015-51.jpg', '', '595', '842', '51');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('486', '6', 'Stella Bianca_cat2015-50.jpg', '', '595', '842', '50');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('487', '6', 'Stella Bianca_cat2015-49.jpg', '', '595', '842', '49');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('488', '6', 'Stella Bianca_cat2015-48.jpg', '', '595', '842', '48');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('489', '6', 'Stella Bianca_cat2015-47.jpg', '', '595', '842', '47');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('490', '6', 'Stella Bianca_cat2015-46.jpg', '', '595', '842', '46');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('491', '6', 'Stella Bianca_cat2015-45.jpg', '', '595', '842', '45');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('492', '6', 'Stella Bianca_cat2015-44.jpg', '', '595', '842', '44');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('493', '6', 'Stella Bianca_cat2015-43.jpg', '', '595', '842', '43');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('494', '6', 'Stella Bianca_cat2015-42.jpg', '', '595', '842', '42');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('495', '6', 'Stella Bianca_cat2015-41.jpg', '', '595', '842', '41');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('496', '6', 'Stella Bianca_cat2015-40.jpg', '', '595', '842', '40');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('497', '6', 'Stella Bianca_cat2015-39.jpg', '', '595', '842', '39');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('498', '6', 'Stella Bianca_cat2015-38.jpg', '', '595', '842', '38');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('499', '6', 'Stella Bianca_cat2015-37.jpg', '', '595', '842', '37');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('500', '6', 'Stella Bianca_cat2015-36.jpg', '', '595', '842', '36');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('501', '6', 'Stella Bianca_cat2015-35.jpg', '', '595', '842', '35');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('502', '6', 'Stella Bianca_cat2015-34.jpg', '', '595', '842', '34');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('503', '6', 'Stella Bianca_cat2015-33.jpg', '', '595', '842', '33');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('504', '6', 'Stella Bianca_cat2015-32.jpg', '', '595', '842', '32');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('505', '6', 'Stella Bianca_cat2015-31.jpg', '', '595', '842', '31');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('506', '6', 'Stella Bianca_cat2015-30.jpg', '', '595', '842', '30');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('507', '6', 'Stella Bianca_cat2015-29.jpg', '', '595', '842', '29');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('508', '6', 'Stella Bianca_cat2015-28.jpg', '', '595', '842', '28');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('509', '6', 'Stella Bianca_cat2015-27.jpg', '', '595', '842', '27');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('510', '6', 'Stella Bianca_cat2015-26.jpg', '', '595', '842', '26');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('511', '6', 'Stella Bianca_cat2015-25.jpg', '', '595', '842', '25');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('512', '6', 'Stella Bianca_cat2015-24.jpg', '', '595', '842', '24');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('513', '6', 'Stella Bianca_cat2015-23.jpg', '', '595', '842', '23');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('514', '6', 'Stella Bianca_cat2015-22.jpg', '', '595', '842', '22');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('515', '6', 'Stella Bianca_cat2015-21.jpg', '', '595', '842', '21');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('516', '6', 'Stella Bianca_cat2015-20.jpg', '', '595', '842', '20');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('517', '6', 'Stella Bianca_cat2015-19.jpg', '', '595', '842', '19');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('518', '6', 'Stella Bianca_cat2015-18.jpg', '', '595', '842', '18');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('519', '6', 'Stella Bianca_cat2015-17.jpg', '', '595', '842', '17');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('520', '6', 'Stella Bianca_cat2015-16.jpg', '', '595', '842', '16');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('521', '6', 'Stella Bianca_cat2015-15.jpg', '', '595', '842', '15');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('522', '6', 'Stella Bianca_cat2015-14.jpg', '', '595', '842', '14');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('523', '6', 'Stella Bianca_cat2015-13.jpg', '', '595', '842', '13');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('524', '6', 'Stella Bianca_cat2015-12.jpg', '', '595', '842', '12');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('525', '6', 'Stella Bianca_cat2015-11.jpg', '', '595', '842', '11');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('526', '6', 'Stella Bianca_cat2015-10.jpg', '', '595', '842', '10');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('527', '6', 'Stella Bianca_cat2015-9.jpg', '', '595', '842', '9');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('528', '6', 'Stella Bianca_cat2015-8.jpg', '', '595', '842', '8');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('529', '6', 'Stella Bianca_cat2015-7.jpg', '', '595', '842', '7');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('530', '6', 'Stella Bianca_cat2015-6.jpg', '', '595', '842', '6');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('531', '6', 'Stella Bianca_cat2015-5.jpg', '', '595', '842', '5');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('532', '6', 'Stella Bianca_cat2015-4.jpg', '', '595', '842', '4');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('533', '6', 'Stella Bianca_cat2015-3.jpg', '', '595', '842', '3');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('534', '6', 'Stella Bianca_cat2015-2.jpg', '', '595', '842', '2');
INSERT INTO `zse_v1_catalog_pdf_pages` VALUES ('535', '6', 'Stella Bianca_cat2015-1.jpg', '', '595', '842', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_pdf_pages_hotspots`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_pdf_pages_hotspots`;
CREATE TABLE `zse_v1_catalog_pdf_pages_hotspots` (
  `hid` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `card_code` varchar(128) NOT NULL DEFAULT '',
  `position_x` float NOT NULL,
  `position_y` float NOT NULL,
  PRIMARY KEY (`hid`),
  KEY `pid` (`pid`),
  CONSTRAINT `zse_v1_catalog_pdf_pages_hotspots_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `zse_v1_catalog_pdf_pages` (`pid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_pdf_pages_hotspots
-- ----------------------------
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('2', '1', 'A031', '63', '153');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('3', '1', 'A028', '74', '378');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('4', '94', 'A031', '290', '286');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('5', '93', 'A032', '557', '813');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('6', '46', 'A007', '69', '799');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('7', '46', 'A008', '143', '802');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('8', '46', 'A009', '225', '804');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('9', '526', 'A001', '225', '204');
INSERT INTO `zse_v1_catalog_pdf_pages_hotspots` VALUES ('12', '526', 'A005', '315', '369');

-- ----------------------------
-- Table structure for `zse_v1_catalog_pricelists`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_pricelists`;
CREATE TABLE `zse_v1_catalog_pricelists` (
  `id_pricelist` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `data` mediumtext,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_pricelist`),
  UNIQUE KEY `codice_listino` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_pricelists
-- ----------------------------
INSERT INTO `zse_v1_catalog_pricelists` VALUES ('1', 'L001', 'Listino Test Sconto Caso 1', null, '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_pricelists` VALUES ('2', 'LN026', 'Listino Netto Test Prezzi Caso 2', null, '2015-12-21 10:22:49', '1');
INSERT INTO `zse_v1_catalog_pricelists` VALUES ('3', 'LN028', 'Listino Netto Test Prezzi Caso 4', null, '2015-12-21 10:22:49', '1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_tree`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_tree`;
CREATE TABLE `zse_v1_catalog_tree` (
  `id_catalog_tree` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) NOT NULL,
  `nleft` int(11) NOT NULL DEFAULT '0',
  `nright` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `external_code` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_catalog_tree`),
  KEY `nleft` (`nleft`),
  KEY `nright` (`nright`),
  KEY `root` (`root`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_tree
-- ----------------------------
INSERT INTO `zse_v1_catalog_tree` VALUES ('1', '1', '1', '4', '1', '', 'Catalogo', 'Catalogo', '');
INSERT INTO `zse_v1_catalog_tree` VALUES ('19', '1', '2', '3', '2', '', 'Articoli Test Immagine - Icona', 'Articoli Test Immagine - Icona', 'CAT1');

-- ----------------------------
-- Table structure for `zse_v1_catalog_tree_temp`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_tree_temp`;
CREATE TABLE `zse_v1_catalog_tree_temp` (
  `id_catalog_tree` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) NOT NULL,
  `nleft` int(11) NOT NULL DEFAULT '0',
  `nright` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `external_code` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_catalog_tree`),
  KEY `nleft` (`nleft`),
  KEY `nright` (`nright`),
  KEY `root` (`root`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_tree_temp
-- ----------------------------
INSERT INTO `zse_v1_catalog_tree_temp` VALUES ('1', '1', '1', '2', '1', '', 'Catalogo', 'Catalogo', '');

-- ----------------------------
-- Table structure for `zse_v1_catalog_tree_tr`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_catalog_tree_tr`;
CREATE TABLE `zse_v1_catalog_tree_tr` (
  `id_catalog_tree` int(11) NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `description` text,
  PRIMARY KEY (`id_catalog_tree`,`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_catalog_tree_tr
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_custom_search`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_custom_search`;
CREATE TABLE `zse_v1_custom_search` (
  `id_custom` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code_reference1` varchar(64) NOT NULL DEFAULT '',
  `code_reference2` varchar(64) NOT NULL DEFAULT '',
  `context` varchar(64) NOT NULL DEFAULT 'catalog',
  `section` varchar(128) NOT NULL DEFAULT '',
  `tag_1` varchar(255) DEFAULT NULL,
  `tag_2` varchar(255) DEFAULT NULL,
  `tag_3` varchar(255) DEFAULT NULL,
  `tag_4` varchar(255) DEFAULT NULL,
  `tag_5` varchar(255) DEFAULT NULL,
  `tag_6` varchar(255) DEFAULT NULL,
  `tag_7` varchar(255) DEFAULT NULL,
  `tag_8` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `tag_9` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `tag_10` decimal(10,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id_custom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_custom_search
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_docs_tree`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_docs_tree`;
CREATE TABLE `zse_v1_docs_tree` (
  `id_folder` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) NOT NULL,
  `nleft` int(11) NOT NULL DEFAULT '0',
  `nright` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `icon_type` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_folder`),
  KEY `nleft` (`nleft`),
  KEY `nright` (`nright`),
  KEY `root` (`root`),
  KEY `level` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_docs_tree
-- ----------------------------
INSERT INTO `zse_v1_docs_tree` VALUES ('1', '1', '1', '38', '1', 'ramo.png', 'Documentazione', 'Documentazione', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('2', '1', '2', '3', '2', 'cat001sub003.png', 'Folder 2 ', 'Cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('3', '1', '4', '7', '2', '', 'Redirect', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('4', '1', '5', '6', '3', '', 'Subfolder 1.1', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('5', '1', '8', '9', '2', '', 'Promozioni', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('6', '1', '12', '13', '2', '', 'Video', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('7', '1', '14', '15', '2', '', 'Corsi', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('8', '1', '16', '17', '2', '', 'Html5', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('9', '1', '18', '33', '2', '', 'Manuel', 'Cartella di Manuel', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('11', '1', '19', '28', '3', '', 'cartella dentro la cartella', 'cartella dentro la cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('12', '1', '20', '27', '4', '', 'cartella dentro la cartella', 'cartella dentro la cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('13', '1', '21', '26', '5', '', 'cartella dentro la cartella', 'cartella dentro la cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('14', '1', '22', '25', '6', '', 'cartella dentro la cartella', 'cartella dentro la cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('15', '1', '23', '24', '7', '', 'cartella dentro la cartella', 'cartella dentro la cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('16', '1', '29', '32', '3', '', 'altra cartella', 'altra cartella', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('17', '1', '30', '31', '4', '', 'terzo livello', 'terzo livello', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('18', '1', '34', '35', '2', '', 'Documenti office', '', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('19', '1', '10', '11', '2', '', 'Cataloghi pdf', 'Descrizione Main', '1');
INSERT INTO `zse_v1_docs_tree` VALUES ('20', '1', '36', '37', '2', '', 'Prova Try', '', '1');

-- ----------------------------
-- Table structure for `zse_v1_docs_tree_tr`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_docs_tree_tr`;
CREATE TABLE `zse_v1_docs_tree_tr` (
  `id_folder` int(11) NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `description` text,
  UNIQUE KEY `lang` (`lang`,`id_folder`),
  KEY `id_folder` (`id_folder`),
  CONSTRAINT `zse_v1_docs_tree_tr_ibfk_1` FOREIGN KEY (`id_folder`) REFERENCES `zse_v1_docs_tree` (`id_folder`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_docs_tree_tr
-- ----------------------------
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('1', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('2', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('3', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('4', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('5', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('6', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('7', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('8', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('9', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('11', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('12', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('13', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('14', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('15', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('16', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('17', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('18', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('19', 'de', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('20', 'de', '', null);
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('1', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('2', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('3', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('4', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('5', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('6', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('7', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('8', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('9', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('11', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('12', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('13', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('14', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('15', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('16', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('17', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('18', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('19', 'en', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('20', 'en', '', null);
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('1', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('2', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('3', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('4', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('5', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('6', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('7', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('8', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('9', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('11', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('12', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('13', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('14', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('15', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('16', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('17', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('18', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('19', 'it', '', '');
INSERT INTO `zse_v1_docs_tree_tr` VALUES ('20', 'it', '', null);

-- ----------------------------
-- Table structure for `zse_v1_gcm_devices`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_gcm_devices`;
CREATE TABLE `zse_v1_gcm_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_token` text COLLATE utf8_bin NOT NULL,
  `device_id` text COLLATE utf8_bin NOT NULL,
  `creation_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `status` varchar(45) COLLATE utf8_bin NOT NULL,
  `api_level` int(11) DEFAULT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_gcm_devices
-- ----------------------------
INSERT INTO `zse_v1_gcm_devices` VALUES ('6', 0x63594E6C51586B717775773A415041393162475F56597079465A483255315F4F54434F444F67594865356B426C4371544238454849446E46474A7A6B67506D7556417850475F6D6373686266755F6D73516E6279716D42426A3448584A476264626A4B4B364255376A4231664B5F6137556F776F4B664A746E333967316C51624F4D727467726662423661482D50434C522D314D4F4D6F72, 0x38386137353230643263643333303164, '2015-10-30 09:26:19', '2015-10-30 09:26:19', 'active', null, 'enterpriseuser001@zotsell.com');
INSERT INTO `zse_v1_gcm_devices` VALUES ('5', 0x63797371617148626237413A415041393162486C5F3931705256476666705F74327A64466F5669507062677970565469787963332D7242335A6E3641336D337974704F6A784542423836644B59645F4F6850477232536359593758654267654E78636550366B775F357363656B334651456A474443484C5354346E7A7475676642574B4C5A6B7370354F67314736786255376E7661743447, 0x37316337343235356636323033343939, '2015-10-29 17:29:39', '2015-10-29 17:29:39', 'active', null, 'enterpriseuser002@zotsell.com');
INSERT INTO `zse_v1_gcm_devices` VALUES ('4', 0x664C476D4F69615546326F3A415041393162483564734654656366377041527151453746656A5A5150484D52684B665646416945544E6D3354333241424635793650545866454D74397A536742523277455567564A3849634A306453573159417679305A394548616A3241795236506B5258696247303877626647436238444F657063614A443278625A39526D6A68485A5170504B65484D, 0x39653032393663663434366637656665, '2015-10-29 11:59:31', '2015-10-29 11:59:31', 'active', null, 'enterpriseuser002@zotsell.com');

-- ----------------------------
-- Table structure for `zse_v1_gcm_messages`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_gcm_messages`;
CREATE TABLE `zse_v1_gcm_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text COLLATE utf8_bin NOT NULL,
  `category` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `badge` varchar(32) COLLATE utf8_bin NOT NULL,
  `creation_time` datetime NOT NULL,
  `device_id` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_gcm_messages
-- ----------------------------
INSERT INTO `zse_v1_gcm_messages` VALUES ('1', 0x74657374, 'contacts', '1', '2015-10-28 15:06:01', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('2', 0x74657374, 'contacts', '1', '2015-10-28 15:06:06', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('3', 0x7465737432, 'contacts', '1', '2015-10-28 15:10:22', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('4', 0x7465737433, 'contacts', '1', '2015-10-28 15:14:31', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('5', 0x7465737434, 'contacts', '1', '2015-10-28 15:15:56', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('6', 0x7465737435, 'contacts', '1', '2015-10-28 15:19:20', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('7', 0x7465737436, 'contacts', '1', '2015-10-28 15:21:24', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('8', 0x7465737437, 'contacts', '1', '2015-10-28 15:26:55', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('9', 0x7465737438, 'contacts', '1', '2015-10-28 15:30:09', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('10', 0x7465737439, 'contacts', '1', '2015-10-28 15:30:35', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('11', 0x746573743130, 'contacts', '1', '2015-10-28 15:34:35', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('12', 0x74657374, 'contacts', '1', '2015-10-28 15:41:34', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('13', 0x7465737438, 'contacts', '1', '2015-10-28 15:49:28', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('14', 0x7465737438, 'contacts', '1', '2015-10-28 15:53:44', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('15', 0x7465737438, 'contacts', '1', '2015-10-28 16:08:03', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('16', 0x7465737438, 'contacts', '1', '2015-10-28 16:14:32', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('17', 0x7465737438, 'contacts', '1', '2015-10-28 16:16:43', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('18', 0x7465737438, 'contacts', '1', '2015-10-28 16:17:09', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('19', 0x74657374, 'geotargeting', '1', '2015-10-28 17:06:29', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('20', 0x74657374, 'news', '1', '2015-10-28 17:14:36', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('21', 0x74657374, 'orders', '1', '2015-10-28 17:15:30', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('22', 0x507573682F50756C6C20436F6E7461747469, 'contacts', '1', '2015-10-29 08:37:52', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('23', 0x507573682050756C6C2043617274, 'cart', '1', '2015-10-29 08:38:44', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('24', 0x507573682F50756C6C20636174616C6F67, 'catalog', '1', '2015-10-29 08:39:44', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('25', 0x507573682F50756C6C, 'documents', '1', '2015-10-29 08:40:05', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('26', 0x507573682F50756C6C20436174616C6F67, 'catalog', '1', '2015-10-29 08:40:19', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('27', 0x507573682F50756C6C20446F63756D656E7473, 'documents', '1', '2015-10-29 08:40:42', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('28', 0x74657374, 'cart', '1', '2015-10-29 11:08:53', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('29', 0x746573742074657374, 'cart', '1', '2015-10-29 11:17:57', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('30', 0x746573742034, 'cart', '1', '2015-10-29 11:18:36', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('31', 0x74657374, 'cart', '1', '2015-10-29 11:27:42', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('32', 0x34205465737420636F6C6C657469766520636F6E74616374, 'contacts', '1', '2015-10-30 08:52:42', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('33', 0x36205465737420536F6C6F20616E64726F696420436F6E7461637420, 'contacts', '1', '2015-10-30 08:54:07', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('34', 0x7465737420393236, 'contacts', '1', '2015-10-30 09:27:06', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('35', 0x746573742039323720436F6E7461637473, 'contacts', '1', '2015-10-30 09:27:52', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('36', 0x746573742073656E7A612063617465676F726961, '', '1', '2015-10-30 10:00:08', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('37', 0x74657374203845, 'sync', '1', '2015-11-03 08:56:23', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('38', 0x6465627567207370616C6C61, 'catalog', '1', '2015-11-05 09:30:55', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('39', 0x6465627567207370616C6C612076656E64697461, 'cart', '1', '2015-11-05 09:32:10', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('40', 0x31, 'sync', '1', '2015-11-23 17:20:13', null);
INSERT INTO `zse_v1_gcm_messages` VALUES ('41', 0x617474656E7A696F6E65, 'sync', '1', '2015-11-23 17:22:27', null);

-- ----------------------------
-- Table structure for `zse_v1_geotargeting`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_geotargeting`;
CREATE TABLE `zse_v1_geotargeting` (
  `id_geotargeting` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `predicate` varchar(255) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `options` text,
  PRIMARY KEY (`id_geotargeting`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_geotargeting
-- ----------------------------
INSERT INTO `zse_v1_geotargeting` VALUES ('9', 'Clienti in zona', '', '', 'isCliente=1', '2014-11-14 10:13:49', '1', '');
INSERT INTO `zse_v1_geotargeting` VALUES ('10', 'Contatti in Zona', '', '', 'isContatto=1', '2014-11-14 10:13:45', '1', '');
INSERT INTO `zse_v1_geotargeting` VALUES ('11', 'Prova', '', '', 'isCliente=1 or isProspect=1', '2014-11-14 10:13:25', '1', '');
INSERT INTO `zse_v1_geotargeting` VALUES ('12', 'Clienti Classificazione', 'Ferramenta: Rossi - Arredamento Verdi ', '', '(isCliente=1 AND classificazione = 3) OR (isCliente=1 AND classificazione = 4)', '2014-11-27 10:09:26', '1', '{\n\"colors\" : {\n\"classificazione\" : {\n\"3\" : \"red\",\n\"4\" : \"green\"\n}\n}\n}');
INSERT INTO `zse_v1_geotargeting` VALUES ('13', 'destinazioni', '', '', 'isDestinazione=1', '2015-02-17 11:42:37', '1', null);

-- ----------------------------
-- Table structure for `zse_v1_logs`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_logs`;
CREATE TABLE `zse_v1_logs` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `appId` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `appver` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `type_log` tinyint(4) NOT NULL DEFAULT '0',
  `context` tinyint(4) NOT NULL DEFAULT '0',
  `devid` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `working_code` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_account` int(10) unsigned NOT NULL,
  `usercode` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `language` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `geo` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `dir` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_log` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action_log` tinyint(4) NOT NULL DEFAULT '0',
  `entity_id` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `name_entity` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `content_entity` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `log_complete` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `long_log_complete` mediumtext COLLATE utf8_bin,
  PRIMARY KEY (`id_log`),
  KEY `type_log` (`type_log`),
  KEY `context` (`context`),
  KEY `usercode` (`usercode`),
  KEY `date_log` (`date_log`),
  KEY `action_log` (`action_log`),
  KEY `name_entity` (`name_entity`)
) ENGINE=MRG_MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin INSERT_METHOD=FIRST UNION=(`zse_v1_logs_current`);

-- ----------------------------
-- Records of zse_v1_logs
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_logs_current`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_logs_current`;
CREATE TABLE `zse_v1_logs_current` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `appId` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `appver` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `type_log` tinyint(4) NOT NULL DEFAULT '0',
  `context` tinyint(4) NOT NULL DEFAULT '0',
  `devid` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `working_code` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_account` int(10) unsigned NOT NULL,
  `usercode` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `language` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `geo` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `dir` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_log` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `action_log` tinyint(4) NOT NULL DEFAULT '0',
  `entity_id` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `name_entity` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `content_entity` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `log_complete` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `long_log_complete` mediumtext COLLATE utf8_bin,
  PRIMARY KEY (`id_log`),
  KEY `type_log` (`type_log`),
  KEY `context` (`context`),
  KEY `usercode` (`usercode`),
  KEY `date_log` (`date_log`),
  KEY `action_log` (`action_log`),
  KEY `name_entity` (`name_entity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_logs_current
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_media`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_media`;
CREATE TABLE `zse_v1_media` (
  `id_media` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `multimedia` text,
  `cansend` tinyint(4) NOT NULL DEFAULT '1',
  `entity_id` varchar(64) NOT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_media`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_media
-- ----------------------------
INSERT INTO `zse_v1_media` VALUES ('1', 'gallery', '', 'Galleria', 'Descrizione Galleria', '', 'galleria_a001_00.png\ngalleria_a001_01.png\ngalleria_a001_02.png', '1', '98c1303ab421b91b48c4c861b39f0fc3781482a9', '2014-02-13 09:28:33', '1');
INSERT INTO `zse_v1_media` VALUES ('3', 'txt', 'pdf.jpg', 'File Txt', 'Descrizione file TXT', 'esempio_testo.txt', '', '1', '8c6e2c621f24c85342a22ea8b0a2ba60d3e86bda', '2014-02-13 09:28:33', '1');
INSERT INTO `zse_v1_media` VALUES ('6', 'application/msword', '', 'File Word', 'Descrizione Word', 'esempio_word.doc', '', '1', '11a5071a30a0d39caaf16f108f35114692f34638', '2014-02-13 09:28:33', '1');
INSERT INTO `zse_v1_media` VALUES ('7', 'html5', '', 'File Html5', 'Descrizione file html', 'esempio_html5.zip', '', '1', '53fb60b8cd89a7f4ad2b1ce13a7d2a9e25649dbf', '2014-02-13 09:28:33', '1');
INSERT INTO `zse_v1_media` VALUES ('9', 'pdf', '', 'File Pdf', 'Pdf prodotto A003', 'pdf_a003_00.pdf', '', '0', '98056aba44a434dc2182b24e78b142a3c5b7b686', '2015-06-11 10:46:00', '1');
INSERT INTO `zse_v1_media` VALUES ('32', 'pdf', '', '111_PROVVMENS_201405.PDF', '', 'asd_vicenza_20131102.pdf', null, '1', '3dd6f1ca221944ce7cba952c36e921eab4c40f60', '2014-07-07 17:49:29', '1');
INSERT INTO `zse_v1_media` VALUES ('51', 'gallery', '', 'dfadsf', '', '', 'blabla.png', '1', '86e66b34db23e6d274e52505174cc3e1d7a209ca', '2014-10-22 17:23:29', '1');
INSERT INTO `zse_v1_media` VALUES ('70', 'gallery', '01-catalogue.png', 'test icone', '', '', '02-promotions.png\n03-news.png\n01-catalogue.png', '1', 'f1383247ac797596f9d0c603f03108d54b3569a9', '2014-10-30 08:54:26', '1');
INSERT INTO `zse_v1_media` VALUES ('89', 'html5', '', 'MuseExport.zip', 'muse_prova', 'MuseExport.zip', null, '1', '4a4e0cce8aaf78a3e7739e0641a50f774cb1b287', '2014-11-06 19:44:05', '1');
INSERT INTO `zse_v1_media` VALUES ('90', 'application/vnd.ms-excel', '', 'dega_tab_C20141109.xls', '', 'dega_tab_C20141109.xls', null, '1', '0d8688047e90cac0adb1f7c6004b78c0103f799c', '2014-11-20 17:34:52', '1');
INSERT INTO `zse_v1_media` VALUES ('91', 'application/vnd.ms-excel', '', 'dega_tab_C20141109.xls', 'dsfasdfasf', 'dega_tab_C20141109_2.xls', null, '1', 'a104bfef4b410fb2970ccc1a318c826113f649d9', '2014-11-20 17:34:54', '1');
INSERT INTO `zse_v1_media` VALUES ('92', 'application/vnd.ms-excel', '', 'clienti_C20141106_b.xls', 'ver 3 ', 'clienti_C20141106_b.xls', null, '1', '44839943415569b9794fce1a79660e644191922f', '2014-11-25 10:22:28', '1');
INSERT INTO `zse_v1_media` VALUES ('96', 'application/vnd.ms-excel', '', 'provaprovaprova2.xlsx', '', 'provaprovaprova2.xlsx', null, '1', '2461c76d083204a4df3a5faa6df874e4da40fae3', '2014-11-25 11:04:53', '1');
INSERT INTO `zse_v1_media` VALUES ('115', 'html5', '', 'Redirect a wistia', '', 'wistia.zip', null, '1', '6060f940ff512dcfed37a89772edcc8e8a1033c5', '2015-03-31 11:23:54', '1');
INSERT INTO `zse_v1_media` VALUES ('134', 'pdf', '', '01003738_ultime_fatture.pdf', '', '01003738_ultime_fatture.pdf', null, '0', '24e79acf2dc9d71d34f3f19c94d378e5f0664f40', '2015-06-19 11:36:27', '1');
INSERT INTO `zse_v1_media` VALUES ('155', 'html5', '', 'pdf_sfogliabile.zip', 'Pdf Sfogliabile Roche', 'pdf_sfogliabile.zip', null, '1', 'f8ea09724b15e7d24116d5535da2d588bf997717', '2015-04-15 18:13:24', '1');
INSERT INTO `zse_v1_media` VALUES ('176', 'html5', '', 'Esempio HTML5 Euronda', '', 'sterilisation_process.zip', null, '1', '9faa19ca3bfbde72e6d3d080f7c0146d4bfb6055', '2015-05-05 11:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('197', 'txt', '', 'test.txt', 'file di test teso', 'esempio_testo.txt', null, '1', 'b78e1f3488975bdcdd8286f136f3fc1cfd4f555b', '2015-05-15 12:21:49', '1');
INSERT INTO `zse_v1_media` VALUES ('198', 'pdf', '', 'TEST pdf strano', '', 'asd_vicenza_20131102.pdf', null, '1', 'bc8d5e8ddfe30fdaa8b062066b1358f043732f3f', '2015-06-12 17:27:37', '1');
INSERT INTO `zse_v1_media` VALUES ('199', 'pdf', '', 'test2', '', 'AR001_FATTCRED_20150605.PDF', null, '1', 'b5db2d41fb244227d1ff74f955c574166a0f982b', '2015-06-12 17:44:08', '1');
INSERT INTO `zse_v1_media` VALUES ('200', 'pdf', '', 'test3', '', 'AR001_FATTCRED_20150609.PDF', null, '1', '428df959c228f5291106b6c6beb9ddeb35d86979', '2015-06-12 17:45:27', '1');
INSERT INTO `zse_v1_media` VALUES ('201', 'pdf', '', 'AR001_FATTCRED_20150609', '', 'AR001_FATTCRED_20150609.PDF', null, '1', 'f5eab8b9d3bac11d5ca70960387613cbe8304739', '2015-06-12 18:11:00', '1');
INSERT INTO `zse_v1_media` VALUES ('202', 'pdf', '', 'AR001_FATTCRED_20150605.PDF', 'AR001_FATTCRED_20150605.PDF', 'AR001_FATTCRED_20150605.PDF', null, '1', '230f88184f6d4f80197a84cf6ac2a7c377ac1b69', '2015-06-15 09:27:15', '1');
INSERT INTO `zse_v1_media` VALUES ('203', 'pdf', '', 'AR001_FATTCRED_20150609.PDF', 'AR001_FATTCRED_20150609.PDF', 'AR001_FATTCRED_20150609.PDF', null, '1', '5f20e18d02a3f09dfe3ef5ec387d704446ff11f8', '2015-06-15 09:27:39', '1');
INSERT INTO `zse_v1_media` VALUES ('223', 'pdf', '', 'Doc Pdf corrotto', 'Esempio corrotto', 'pdf_example_corrotto.pdf', null, '1', '50a0c9be8223983af63928882f74d1b69b14d2e2', '2015-09-21 10:18:11', '1');
INSERT INTO `zse_v1_media` VALUES ('240', 'pdf', '', '00001R_PROVVIGIONI_20151109.pdf', 'Da f6', '00001R_PROVVIGIONI_20151109.pdf', null, '1', '190d63511c6c0454639ddbd6e98f73a5d0a10ce7', '2015-11-11 12:30:09', '1');
INSERT INTO `zse_v1_media` VALUES ('241', 'pdf', '', '000_FATTMENS_201501.pdf', '000_FATTMENS_201501.pdf', '000_FATTMENS_201501.pdf', null, '1', 'f4b4ffa333169e961fe3a9c4630a861034928e11', '2015-12-16 10:22:32', '1');
INSERT INTO `zse_v1_media` VALUES ('242', 'pdf', '', '000_FATTMENS_201502.pdf', '000_FATTMENS_201502.pdf', '000_FATTMENS_201502.pdf', null, '1', '9990b0d4e8add79b82bc6af27540f6b3c5bbcdbf', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('243', 'pdf', '', '000_FATTMENS_201503.pdf', '000_FATTMENS_201503.pdf', '000_FATTMENS_201503.pdf', null, '1', '0c6ed90e35bb1755d7cd186f992cf53e43474e22', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('244', 'pdf', '', '000_FATTMENS_201504.pdf', '000_FATTMENS_201504.pdf', '000_FATTMENS_201504.pdf', null, '1', '1d495a5783e45dff328b7071f69763c5e5e991d2', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('245', 'pdf', '', '000_FATTMENS_201505.pdf', '000_FATTMENS_201505.pdf', '000_FATTMENS_201505.pdf', null, '1', 'd23ba1741324392ab301ce00e236a054f0a8ad6d', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('246', 'pdf', '', '000_FATTMENS_201506.pdf', '000_FATTMENS_201506.pdf', '000_FATTMENS_201506.pdf', null, '1', 'abf6b655a17a93108959a407cfabfadbb1116b89', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('247', 'pdf', '', '000_FATTMENS_201507.pdf', '000_FATTMENS_201507.pdf', '000_FATTMENS_201507.pdf', null, '1', '630c90501b32dad80f3e169394d5191f22f8d5ee', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('248', 'pdf', '', '000_FATTMENS_201508.pdf', '000_FATTMENS_201508.pdf', '000_FATTMENS_201508.pdf', null, '1', 'c1297b7934cfe9580c8fa14403a2f6542ed06aab', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('249', 'pdf', '', '000_FATTMENS_201509.pdf', '000_FATTMENS_201509.pdf', '000_FATTMENS_201509.pdf', null, '1', '47f5c58e42fc2dd522bc09c7bb6c61632898911d', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('250', 'pdf', '', '000_FATTMENS_201510.pdf', '000_FATTMENS_201510.pdf', '000_FATTMENS_201510.pdf', null, '1', '1ec00599b957ba42b1f4d0fa93aa570db453ff62', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('251', 'pdf', '', '000_FATTMENS_201511.pdf', '000_FATTMENS_201511.pdf', '000_FATTMENS_201511.pdf', null, '1', '84f0844834d5d404ed1a2bcbb035b5f4618f1a43', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('252', 'pdf', '', '000_FATTMENS_201512.pdf', '000_FATTMENS_201512.pdf', '000_FATTMENS_201512.pdf', null, '1', '33b8b8514ab89ea2eb4b12087f98c8e0408e1372', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('253', 'pdf', '', '000_BUDGET_2012.pdf', '000_BUDGET_2012.pdf', '000_BUDGET_2012.pdf', null, '1', '93ad4be9cf172fd48b761e4f1407d7163fa3e5de', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('254', 'pdf', '', '000_BUDGET_2013.pdf', '000_BUDGET_2013.pdf', '000_BUDGET_2013.pdf', null, '1', 'deb503e009d9de511d2303fe912059687cb8eebb', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('255', 'pdf', '', '000_BUDGET_2014.pdf', '000_BUDGET_2014.pdf', '000_BUDGET_2014.pdf', null, '1', '5fe89e1ac23debca58d3e73534ea950ea670e290', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('256', 'pdf', '', '000_BUDGET_2015.pdf', '000_BUDGET_2015.pdf', '000_BUDGET_2015.pdf', null, '1', 'a12357e5f2e119e693c4494e0b57b3e94c6e23eb', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media` VALUES ('257', 'pdf', '', '000_GIROVISITA_20151211.pdf', '000_GIROVISITA_20151211.pdf', '000_GIROVISITA_20151211.pdf', null, '1', '9d2d311e0a0b7f365234de3c297086f749ddad54', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('258', 'pdf', '', '000_GIROVISITA_20151212.pdf', '000_GIROVISITA_20151212.pdf', '000_GIROVISITA_20151212.pdf', null, '1', '0e976474b0c6bd22f0f9614bc9aa5f63429dabec', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('259', 'pdf', '', '000_GIROVISITA_20151213.pdf', '000_GIROVISITA_20151213.pdf', '000_GIROVISITA_20151213.pdf', null, '1', '8b04761818603753c109f0851e8a2707c51c554e', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('260', 'pdf', '', '000_GIROVISITA_20151214.pdf', '000_GIROVISITA_20151214.pdf', '000_GIROVISITA_20151214.pdf', null, '1', '1e6a5e66c35b661e7b8733f6c025d11ecfca8aaa', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('261', 'pdf', '', '000_GIROVISITA_20151215.pdf', '000_GIROVISITA_20151215.pdf', '000_GIROVISITA_20151215.pdf', null, '1', '2d398d660fee1a36d516fd629392d8366b77b702', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('262', 'pdf', '', '000_GIROVISITA_20151216.pdf', '000_GIROVISITA_20151216.pdf', '000_GIROVISITA_20151216.pdf', null, '1', '4a68d5edb4f30a61f2ce2b8e191fd9d129389400', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('263', 'pdf', '', '000_GIROVISITA_20151217.pdf', '000_GIROVISITA_20151217.pdf', '000_GIROVISITA_20151217.pdf', null, '1', '810d7cb97b2106df6708b3483de4c4bcc6bf9aec', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media` VALUES ('264', 'gallery', '', 'Test File Tipo GALLERY', 'Test File Allegati Caso 1', '', 'galleria1_test_file.jpg\ngalleria2_test_file.jpg\ngalleria3_test_file.jpg', '1', '133cd7fe39d4f6de3ddced27d166ba3f3bcda1c4', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('265', 'pdf', '', 'Test File Tipo PDF', 'Test File Allegati Caso 1 - pdf_test_file.pdf', 'pdf_test_file.pdf', '', '1', '37cf561fc0743686e511341fc7aefeff9253a3aa', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('266', 'txt', '', 'Test File Tipo TXT', 'Test File Allegati Caso 1 - txt_test_file.txt', 'txt_test_file.txt', '', '1', '9d509343d76b567e415d53c9d9e12baafff496f4', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('267', 'movie', '', 'Test File Tipo MOVIE', 'Test File Allegati Caso 1 - movie_test_file.mp4', 'movie_test_file.mp4', '', '1', 'bb76eb732ba677e84a3973ddc7fcd5595c9d9c3a', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('268', 'application/vnd.ms-powerpoint', '', 'Test File Tipo PPT', 'Test File Allegati Caso 1 - ppt_test_file.ppt', 'ppt_test_file.ppt', '', '1', 'd69f0f6096c63b57b067155de228771b9b0c610c', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('269', 'application/msword', '', 'Test File Tipo DOC', 'Test File Allegati Caso 1 - word_test_file.doc', 'word_test_file.doc', '', '1', 'e2d6eb1332c6a456424746b5c4cb618636a8174a', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('270', 'html5', '', 'Test File Tipo HTML5', 'Test File Allegati Caso 1 - html5_test_file.zip', 'html5_test_file.zip', '', '1', 'b24e874f10810ee38ba6412c546ace6439f45061', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('271', 'application/vnd.ms-excel', '', 'Test File Tipo XLS', 'Test File Allegati Caso 1 - xls_test_file.xls', 'xls_test_file.xls', '', '1', 'd7892b5de51d9b7b9bf1d29bfab14c0c9a705b1f', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media` VALUES ('272', 'gallery', 'icona_galleria_test_file.jpg', 'Test File Tipo GALLERY', 'Test File Allegati Caso 1', '', 'galleria1_test_file.jpg\ngalleria2_test_file.jpg\ngalleria3_test_file.jpg', '1', '1a2547e59be7642565ccba1ce04dcbf7fb68cab8', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('273', 'pdf', '', 'Test File Tipo PDF', 'Test File Allegati Caso 1 - pdf_test_file.pdf', 'pdf_test_file.pdf', '', '1', 'd7a217f6a018848ec7c2ff367d8777ca4c0fe20e', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('274', 'txt', '', 'Test File Tipo TXT', 'Test File Allegati Caso 1 - txt_test_file.txt', 'txt_test_file.txt', '', '1', '176bea02a0a48fd51535bec038ed336d12ce188f', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('275', 'movie', '', 'Test File Tipo MOVIE', 'Test File Allegati Caso 1 - movie_test_file.mp4', 'movie_test_file.mp4', '', '1', '9a2925a3bdaa12a1187665addbbe48c181660f4c', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('276', 'application/vnd.ms-powerpoint', '', 'Test File Tipo PPT', 'Test File Allegati Caso 1 - ppt_test_file.ppt', 'ppt_test_file.ppt', '', '1', 'aa3ca21db8a147cb5df8e066947e5fed69429a1c', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('277', 'application/msword', '', 'Test File Tipo DOC', 'Test File Allegati Caso 1 - word_test_file.doc', 'word_test_file.doc', '', '1', 'b1688cfbb0aca6dba6e7dc07df58e48f9d142341', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('278', 'html5', '', 'Test File Tipo HTML5', 'Test File Allegati Caso 1 - html5_test_file.zip', 'html5_test_file.zip', '', '1', '202ad63cc4753f0e3328909e60862a6840c98b4b', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media` VALUES ('279', 'application/vnd.ms-excel', '', 'Test File Tipo XLS', 'Test File Allegati Caso 1 - xls_test_file.xls', 'xls_test_file.xls', '', '1', '2a3d04b82f416a9fa49ac71c3ebb1b0e71a28603', '2015-12-21 10:22:44', '1');

-- ----------------------------
-- Table structure for `zse_v1_media_links`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_media_links`;
CREATE TABLE `zse_v1_media_links` (
  `id_media` int(11) NOT NULL,
  `id_container` int(11) NOT NULL DEFAULT '0',
  `context` int(11) NOT NULL,
  `weight` int(11) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_media`,`id_container`,`context`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_media_links
-- ----------------------------
INSERT INTO `zse_v1_media_links` VALUES ('1', '2', '1', '0', '2014-02-14 12:18:32', '1');
INSERT INTO `zse_v1_media_links` VALUES ('3', '2', '1', '0', '2014-02-14 12:18:34', '1');
INSERT INTO `zse_v1_media_links` VALUES ('3', '3', '1', '0', '2014-03-12 12:38:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('6', '2', '1', '0', '2014-02-14 12:18:41', '1');
INSERT INTO `zse_v1_media_links` VALUES ('6', '5', '4', '0', '2014-06-17 10:38:52', '1');
INSERT INTO `zse_v1_media_links` VALUES ('6', '8', '4', '0', '2014-06-17 10:39:06', '1');
INSERT INTO `zse_v1_media_links` VALUES ('7', '2', '1', '0', '2014-02-14 12:18:36', '1');
INSERT INTO `zse_v1_media_links` VALUES ('7', '3', '1', '0', '2014-09-18 11:18:32', '1');
INSERT INTO `zse_v1_media_links` VALUES ('7', '5', '4', '0', '2014-06-17 10:38:53', '1');
INSERT INTO `zse_v1_media_links` VALUES ('9', '1', '1', '0', '2014-03-12 12:37:04', '1');
INSERT INTO `zse_v1_media_links` VALUES ('9', '2', '1', '0', '2014-02-14 12:18:38', '1');
INSERT INTO `zse_v1_media_links` VALUES ('9', '8', '4', '0', '2014-06-17 10:39:07', '1');
INSERT INTO `zse_v1_media_links` VALUES ('9', '10', '4', '0', '2015-06-08 10:40:22', '1');
INSERT INTO `zse_v1_media_links` VALUES ('32', '8', '4', '1', '2014-07-07 17:49:29', '1');
INSERT INTO `zse_v1_media_links` VALUES ('51', '3', '1', '1', '2014-10-22 17:23:29', '1');
INSERT INTO `zse_v1_media_links` VALUES ('51', '10', '4', '0', '2015-06-08 10:38:59', '1');
INSERT INTO `zse_v1_media_links` VALUES ('70', '2', '1', '1', '2014-10-30 08:54:26', '1');
INSERT INTO `zse_v1_media_links` VALUES ('89', '8', '1', '1', '2014-11-06 19:44:05', '1');
INSERT INTO `zse_v1_media_links` VALUES ('90', '10', '4', '0', '2015-06-08 10:40:14', '1');
INSERT INTO `zse_v1_media_links` VALUES ('90', '18', '1', '1', '2014-11-14 09:29:46', '1');
INSERT INTO `zse_v1_media_links` VALUES ('91', '1', '1', '1', '2014-11-17 15:37:54', '1');
INSERT INTO `zse_v1_media_links` VALUES ('92', '18', '1', '2', '2014-11-25 10:22:28', '1');
INSERT INTO `zse_v1_media_links` VALUES ('96', '18', '1', '3', '2014-11-25 11:04:53', '1');
INSERT INTO `zse_v1_media_links` VALUES ('115', '3', '1', '2', '2015-03-31 11:23:54', '1');
INSERT INTO `zse_v1_media_links` VALUES ('134', '3', '1', '3', '2015-04-01 09:50:56', '1');
INSERT INTO `zse_v1_media_links` VALUES ('155', '8', '1', '2', '2015-04-15 18:13:24', '1');
INSERT INTO `zse_v1_media_links` VALUES ('176', '8', '1', '3', '2015-05-05 11:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('197', '1', '1', '2', '2015-05-15 12:21:49', '1');
INSERT INTO `zse_v1_media_links` VALUES ('198', '1', '1', '3', '2015-06-12 17:27:37', '1');
INSERT INTO `zse_v1_media_links` VALUES ('199', '1', '1', '4', '2015-06-12 17:44:08', '1');
INSERT INTO `zse_v1_media_links` VALUES ('200', '1', '1', '5', '2015-06-12 17:45:27', '1');
INSERT INTO `zse_v1_media_links` VALUES ('201', '5', '4', '1', '2015-06-12 18:11:00', '1');
INSERT INTO `zse_v1_media_links` VALUES ('202', '12', '4', '1', '2015-06-15 09:27:15', '1');
INSERT INTO `zse_v1_media_links` VALUES ('203', '12', '4', '2', '2015-06-15 09:27:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('223', '1', '1', '6', '2015-09-21 10:18:11', '1');
INSERT INTO `zse_v1_media_links` VALUES ('240', '5', '4', '2', '2015-11-11 12:30:09', '1');
INSERT INTO `zse_v1_media_links` VALUES ('241', '14', '4', '1', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('242', '14', '4', '2', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('243', '14', '4', '3', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('244', '14', '4', '4', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('245', '14', '4', '5', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('246', '14', '4', '6', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('247', '14', '4', '7', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('248', '14', '4', '8', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('249', '14', '4', '9', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('250', '14', '4', '10', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('251', '14', '4', '11', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('252', '14', '4', '12', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('253', '15', '4', '1', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('254', '15', '4', '2', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('255', '15', '4', '3', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('256', '15', '4', '4', '2015-12-16 10:22:33', '1');
INSERT INTO `zse_v1_media_links` VALUES ('257', '16', '4', '1', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('258', '16', '4', '2', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('259', '16', '4', '3', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('260', '16', '4', '4', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('261', '16', '4', '5', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('262', '16', '4', '6', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('263', '16', '4', '7', '2015-12-16 10:28:13', '1');
INSERT INTO `zse_v1_media_links` VALUES ('264', '65', '3', '1', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('265', '65', '3', '2', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('266', '65', '3', '3', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('267', '65', '3', '4', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('268', '65', '3', '5', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('269', '65', '3', '6', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('270', '65', '3', '7', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('271', '65', '3', '8', '2015-12-21 10:22:39', '1');
INSERT INTO `zse_v1_media_links` VALUES ('272', '62', '2', '1', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('273', '62', '2', '2', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('274', '62', '2', '3', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('275', '62', '2', '4', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('276', '62', '2', '5', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('277', '62', '2', '6', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('278', '62', '2', '7', '2015-12-21 10:22:44', '1');
INSERT INTO `zse_v1_media_links` VALUES ('279', '62', '2', '8', '2015-12-21 10:22:44', '1');

-- ----------------------------
-- Table structure for `zse_v1_media_tr`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_media_tr`;
CREATE TABLE `zse_v1_media_tr` (
  `id_media` int(11) NOT NULL,
  `lang` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `file` varchar(255) NOT NULL DEFAULT '',
  `multimedia` text,
  `entity_id` varchar(64) NOT NULL,
  UNIQUE KEY `lang` (`lang`,`id_media`),
  KEY `id_media` (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_media_tr
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_news`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_news`;
CREATE TABLE `zse_v1_news` (
  `id_news` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) unsigned NOT NULL,
  `text` varchar(140) COLLATE utf8_bin NOT NULL,
  `creation_time` datetime DEFAULT NULL,
  `love_number` int(11) DEFAULT '0',
  `category` enum('buy','sell','public') COLLATE utf8_bin DEFAULT 'sell',
  `fullname` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `lang` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_news`),
  KEY `idaccount` (`id_account`),
  CONSTRAINT `zse_v1_news_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `zse_v1_accounts` (`id_account`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_news
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_offers`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_offers`;
CREATE TABLE `zse_v1_offers` (
  `oid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `category` tinyint(4) DEFAULT NULL,
  `abstract_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `abstract_subtitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `abstract_items` int(11) DEFAULT NULL,
  `abstract_cover` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price_full` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price_rebate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` text COLLATE utf8_bin,
  `content_sidebar` text COLLATE utf8_bin,
  `images` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `valid_until` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_offers
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_offers_categories`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_offers_categories`;
CREATE TABLE `zse_v1_offers_categories` (
  `cid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of zse_v1_offers_categories
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_orders`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_orders`;
CREATE TABLE `zse_v1_orders` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `record_code` varchar(32) DEFAULT '',
  `record_destination_code` varchar(64) DEFAULT '',
  `ipad_udid` varchar(255) NOT NULL DEFAULT '',
  `agent_code` varchar(255) NOT NULL DEFAULT '',
  `date_access` datetime DEFAULT '0000-00-00 00:00:00',
  `date_order` datetime DEFAULT '0000-00-00 00:00:00',
  `status` varchar(32) NOT NULL DEFAULT 'nuovo',
  `management_status` varchar(32) NOT NULL DEFAULT 'nuovo',
  `inserted_by` varchar(64) NOT NULL DEFAULT '',
  `order_code` varchar(64) NOT NULL DEFAULT '',
  `management_code` varchar(128) DEFAULT '',
  `management_external_ref` varchar(128) DEFAULT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `data` mediumtext,
  `sign_image` mediumtext,
  `link_pdf` varchar(255) DEFAULT NULL,
  `transfer_message` varchar(255) DEFAULT '',
  `type` varchar(64) NOT NULL DEFAULT 'standard',
  `entity_id` varchar(64) NOT NULL DEFAULT '',
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `parent_id` varchar(64) NOT NULL DEFAULT '',
  `document_type` varchar(64) NOT NULL DEFAULT '',
  `document_type_id` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `codice_ordine` (`order_code`),
  KEY `codice_agente` (`agent_code`),
  KEY `codice_gestionale` (`management_code`),
  KEY `record_code` (`record_code`),
  KEY `record_destination_code` (`record_destination_code`),
  KEY `record_code_and_destination` (`record_code`,`record_destination_code`),
  CONSTRAINT `zse_v1_orders_ibfk_1` FOREIGN KEY (`record_code`, `record_destination_code`) REFERENCES `zse_v1_records` (`code`, `code_destination`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_orders
-- ----------------------------
INSERT INTO `zse_v1_orders` VALUES ('303', 'C034', '', '1038140F-D959-45E0-9D07-FB77D14B86C4', '001', '2015-12-15 16:03:13', '2015-12-15 16:03:09', 'trasferito_in_gestionale', 'P', '', 'IPAD_20151215160309_001', 'Ordine 1', null, 'Anagrafica Test Prezzi Caso 10', null, '{\n  \"signature_location\" : {\n    \"longitude\" : 11.35901008707585,\n    \"latitude\" : 45.71260786836118,\n    \"timestamp\" : \"2015-12-15 16:00:31\"\n  },\n  \"versione\" : \"1.0\",\n  \"ordine_testata\" : {\n    \"tipo_documento\" : \"Ordine\",\n    \"numero_ordine\" : \"IPAD_20151215160309_001\",\n    \"confirm_location\" : {\n      \"longitude\" : 11.35907141792554,\n      \"latitude\" : 45.71258614360971,\n      \"timestamp\" : \"2015-12-15 16:03:06\"\n    },\n    \"supplierLabel\" : \"Invalid supplier\",\n    \"codice_cliente\" : \"C034\",\n    \"supplierCode\" : \"invalid\",\n    \"data_ordine\" : \"20151215160309\",\n    \"codice_agente\" : \"001\",\n    \"nome_agente\" : \"enterpriseuser001\"\n  },\n  \"totali\" : {\n    \"totale_quantita\" : 3,\n    \"totale_ordine\" : 410.19,\n    \"totale_imposta\" : 71.19,\n    \"totale_imponibile\" : 339\n  },\n  \"righe_carrello\" : [\n    {\n      \"descrizione_breve\" : \"Listino variante (prezzo netto)\",\n      \"label_colonna\" : \"Quantità\",\n      \"prezzo_originale\" : 113,\n      \"tipo_acquisto\" : 0,\n      \"label_sconto\" : \"\",\n      \"groupCode\" : \"NULL\",\n      \"decimali_in_quantita\" : 0,\n      \"prezzo\" : 113,\n      \"codice_articolo\" : \"A026\",\n      \"totale_riga\" : 339,\n      \"codice_variante\" : \"V026\",\n      \"supplemento\" : 0,\n      \"label_riga\" : \"Variante Test Prezzi Caso 2\",\n      \"stringa_opzioni\" : \"Quantità Variante Test Prezzi Caso 2\",\n      \"fattore_conversione\" : 1,\n      \"label_unita_misura\" : \"PZ\",\n      \"aliquota\" : 21,\n      \"riga_ordine\" : 1,\n      \"etichetta\" : \"Articolo Test Prezzi Caso 2\",\n      \"descrizione_variante\" : [\n        {\n          \"etichetta_variante\" : \"Quantità\",\n          \"dimensione\" : \"x\",\n          \"codice\" : \"MONOCOLONNA\",\n          \"posizione\" : \"1\",\n          \"nome_dimensione\" : \"Quantità\"\n        },\n        {\n          \"etichetta_variante\" : \"Variante Test Prezzi Caso 2\",\n          \"dimensione\" : \"y\",\n          \"codice\" : \"\",\n          \"posizione\" : \"\",\n          \"nome_dimensione\" : \"Variante Test Prezzi Caso 2\"\n        }\n      ],\n      \"sconto\" : 0,\n      \"sigla_unita_misura\" : \"PZ\",\n      \"quantita\" : 3\n    }\n  ],\n  \"service\" : {\n    \"userzone\" : \"000\",\n    \"usercode\" : \"001\",\n    \"dir\" : \"\",\n    \"language\" : \"it\",\n    \"devid\" : \"1038140F-D959-45E0-9D07-FB77D14B86C4\",\n    \"appver\" : \"2.2.15\",\n    \"geo\" : \"\",\n    \"token\" : \"7a627114a27f7f9008f0b6778dfd7b809a5a0201\",\n    \"servicetype\" : 1,\n    \"country\" : \"IT\",\n    \"appId\" : \"com.tradeservices.app.zotsell.push-8E\",\n    \"servicekey\" : \"0bf41a571c973dc1de2513ae20e3829e78a14805\",\n    \"servicestatus\" : 3\n  },\n  \"fatturazione\" : {\n    \"note\" : \"\",\n    \"classificazione_code\" : \"1\",\n    \"codice_cliente\" : \"C034\",\n    \"ragione_sociale\" : \"Anagrafica Test Prezzi Caso 10\",\n    \"indirizzo\" : \"1St Street\",\n    \"codice_fiscale\" : \"01234556755\",\n    \"provincia\" : \"WA\",\n    \"classificazione_label\" : \"\",\n    \"partita_iva\" : \"01234556755\",\n    \"email\" : \"test@examplemail.com\",\n    \"codice_destinazione\" : \"\",\n    \"citta\" : \"Seattle\",\n    \"cap\" : \"10024\",\n    \"nazione\" : \"USA\",\n    \"fax\" : \"\",\n    \"telefono\" : \"011\\/4634234\"\n  },\n  \"listino\" : \"L001\",\n  \"destinazione\" : {\n    \"note\" : \"\",\n    \"classificazione_code\" : \"1\",\n    \"codice_cliente\" : \"C034\",\n    \"ragione_sociale\" : \"Anagrafica Test Prezzi Caso 10\",\n    \"indirizzo\" : \"1St Street\",\n    \"codice_fiscale\" : \"01234556755\",\n    \"provincia\" : \"WA\",\n    \"classificazione_label\" : \"\",\n    \"partita_iva\" : \"01234556755\",\n    \"email\" : \"test@examplemail.com\",\n    \"codice_destinazione\" : \"\",\n    \"citta\" : \"Seattle\",\n    \"cap\" : \"10024\",\n    \"nazione\" : \"USA\",\n    \"fax\" : \"\",\n    \"telefono\" : \"011\\/4634234\"\n  },\n  \"ordine_fine\" : {\n    \"note\" : \"\",\n    \"listino_descrizione\" : \"Listino Test Prezzi Caso 1\",\n    \"condizione_di_resa_codice\" : \"\",\n    \"valuta_etichetta\" : \"\",\n    \"valuta_isocode\" : \"\",\n    \"data_consegna\" : \"20151215160254\",\n    \"condizione_di_pagamento_codice\" : \"\",\n    \"riferimento_ordine\" : \"\",\n    \"condizione_di_pagamento_label\" : \"\",\n    \"giorni_di_consegna\" : \"\",\n    \"notifica_a\" : \"test@examplemail.com\",\n    \"condizione_di_resa_label\" : \"\",\n    \"condizione_iva_label\" : \"\",\n    \"condizione_iva_code\" : \"\",\n    \"dati_aggiuntivi_personalizzati\" : {\n      \"assegnazione_materiale_totale\" : \"NO\",\n      \"formID\" : \"sales_additional_data\",\n      \"form_display\" : \"\",\n      \"assegnazione_materiale_totaleID\" : \"no\",\n      \"form_model\" : \"\",\n      \"form_version\" : \"\"\n    },\n    \"listino\" : \"L001\"\n  }\n}', '/9j/4AAQSkZJRgABAQAASABIAAD/4QBYRXhpZgAATU0AKgAAAAgAAgESAAMAAAABAAEAAIdpAAQAAAABAAAAJgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAACqKADAAQAAAABAAABDwAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgBDwKoAwERAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/dAAQAVf/aAAwDAQACEQMRAD8A/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//Q/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//R/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//S/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//T/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD4L/AOCj3/BQn4O/8Ey/2bb39pb42aT4o1/wyvi/w/4D0jQvCMFnLq2reJvEVtq+oWVqZr+5trWytY9M0HV7ue7kMpU20cCws06ugB3f7EH7bnwE/wCCgv7PnhT9pH9nbxDca14I8SSXWnX2matDb2Xijwf4k07y/wC1PCvizS7a7vYtO1qxWe3uNkV1cWt3YXdlqFlcT2l3E7AH1zQAUAFABQAUAFABQAUAFABQAUAeK/Gz9o/4C/s36ToGvfHv4vfD/wCEWj+Kdcg8N+HdR8feJtN8OWur61PtIsrKTUJohL5KOkt7c4Fpp0LxzX89tE6O4B7BY31jqljZ6npl5a6jpuo2tvfafqFjcQ3djfWN3ClxaXlnd27yQXVrdQSJPb3EMkkM0LpJG7IyswBaoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//U/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD+Qf8A4O9PjF+1d8Cv2cf2R/iD+zn8TviB8MfDNl8ZPGNr8RNS8A+IdY8N3Nx4h/sLw5qnwzmvr3SLi3kb+zTpXjaeySZyqTSySoN6AqAfoN/wQD/4K2aZ/wAFL/2VtG0H4j6xZx/tX/Bbw/pGj/F3TpJoYLzx1pMIj03SvixptgSsrrq7i3s/GS2yva6b4pmS422Nl4j0fT4AD98qACgAoAKAPx+/4Lyfst2P7Wn/AASz/ak8CPpCat4g8EeEV+NPg4mRY5NO1r4WSnxFql9bbiBJdP4JTxdp0MIy8x1Axwq0zIrAH+fb/wAEZf8AgoD+0R/wSm8Z2/xd0nT9a8ZfsreNvFlp4J+NvgpJbj+wdSSAyTWt9a7hLbaL458P29xeah4a1UxpMUl1DTbg3Ojanq1jdAH+pv8AAj46fC/9pX4SeB/jf8GvFNl4x+HPxC0W31zw7rVk6kmKXMd1p+oQBmew1jSbyOfTdX02bE9hqFtcW0mTHvYA9coAKACgAoA/mv8A+Dn79oz9t79lT9h7wB8aP2OfH/iD4cWWh/GLTNM+MXiXwc7W3iiy0PVdJvD4TlF/FE89p4bk1y0udP10CSK2urvUNDtrrzVdEoA9v/4IC/8ABWm3/wCCo37I9pefEbU9Ii/al+DYtPDHxq0mzjt9NPimBwyeHvihpWjQxwwW1j4jhiaz16301TY6b4ns7uSO30rTtZ0PT1AP3hoAKACgAoA/ib/4POvCOqeKPhf+wR/Z11cWn2fx/wDHWyLpJJHaNdanpfwjNol1tZVDlbK6MMhyyItxtVgzigCt/wAEHf8Agrd45/Zl8QeEv+CZX/BQy41LwgkVh4fsv2dPip4yeW1t9Htdbs4bvQPAfinVL/as3g7Xre7tp/A3iozi20OWeLS79m8OX1tf+GAD+22gAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD/9X+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgDx/8AaG1/x74U+APxx8UfCu2jvPif4b+D/wATNf8AhxZzWsd9DdePdH8F61qPg+2lspVeK8jn8Q2+nRPayI8dwrGF1ZWIoA/gy/4Ixf8ABzt8cfD3xx8Q/BD/AIKZeNLzxp8MPGPiFodC+Ll9pFtH4k+D3iC5v5oZE1lNIs4LjXfh/NKwTUbI295q3huKKG80ATW8FxoeoAH+g1outaN4k0fSvEXh3VtM1/w/r2m2Os6Hrui39rquj6zpGp20V7puq6VqdjLPZajpuoWc0N3Y31nPNa3dtLFPBLJE6OwBp0AFABQAUAFABQAUAFAH5T/8FrP2Qov21P8AgnF+0J8LbSwW/wDGPhbw6/xe+HMTecznxj8N7W91cWdvDBFNJc3mu+GZPEvhmxgCENea3bsxAQsoB/m5/sOeIv2h/wBja50H9vr9mU37zfs9fECw8OfGTQbdbuews9F8QtPDFpni6wtHjkfwj4ws4tT0K8eTyRFcgKs0Fy9rNEAf6lH7DP7aPwk/b6/Zv8C/tIfB28/4knii2Nl4j8NXF3Dd6v4D8a2EFs3iHwXrckMcIe90qW5gmtLs29qNV0e80zWI7S1j1BLdAD68oAKACgDL1zRNJ8S6LrHhzXrC31XQ/EGl6homtaZdp5lrqWk6raS2Oo2FygIL295Z3E1vMgI3RyMMjOaAP4IP+Can7PXwu+Dv/BRb9vz/AIIrftceGbbX/hD+0MviBPAE+owWul6pF4l8Hfb/ABh8MvGvg7V9QiabQNW1b4d6xqWqeHp7aOdtS1JtG0+7s9U0+4uLG4AN/wDZ1+N37RH/AAbaftw6t+yp+0Tea58Qv2APjX4hOteEvGKWtw9lp9jeTx2Fh8TfCUDGdNJ8UaPCLTSPiR4OiuJIdSgskjMt49j4W12AA/uy8I+LfDPj7wt4e8beC9d0zxP4R8WaNp3iHw34i0a6jvdK1rRdWtYr3TtSsLqEtHNbXdrNHLE4OdrYYKwZVAOioAKACgD53/a1/Z38KftZ/s0fG79m/wAaW1nc6D8YPh34h8Hs9+k0lrpusXdo1x4Y19kgIleXw14nttH8QWyruzc6ZEGjlQtG4B/k/wD/AAT8/aX+NP8AwR//AOCgN54y0mz1WGy+H3jnV/AHxi8BSzTWkXibwbb64+j+L/CuqwTRvDDdKLR59Mu7m1lfS9atNM1WGM3Vnb0Af62vwR+M3w9/aH+Enw++N3wp12HxJ8PPiZ4Z07xV4X1aExbpbG/jPmWl7FFLMlpq2lXkdzpOs6eZXk07VrG9sJmM1s9AHqdABQAUAfzmf8HQ/wABpPi1/wAExde+IGm6ZDc65+z38T/BHxI/tDdi8sfDGqz3PgXX4LVc4lS71DxN4cubmNUeQJpscwKRQzbgDz39nj9hD9l7/gtL/wAE0/2AvjR8XVu9A+MXwq+GuheAbf4l+CDYR+JfI+FWqXHgefwv4tjniP8Aa1nfWfhy316ziuJoLzSr3WDqNnMLa/vbO/AP6WfDGgWnhTw14e8LWFxf3dj4a0PSdAsrvVLn7bqdzaaNYW+nW9xqN4UjN3fzQ2ySXdyY4/PuGkl2Lu2qAblABQAUAFABQAUAFABQAUAFABQAUAFABQAUAf/W/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP8y3/AIKR/wDBNb4S/ss/8Fe774NfEUf8K9/Zj/a015fE3wy+INrA7Wnw6sPiPqUttv8Amubi4uNN+HPjRr7SNQtru4N9eeHrGC+Ox7+3loA/Rv8AZB/bV/a8/wCCAP7Stl+wd+35Bq/jf9kDxHrDH4Z/Ey2a91bTPCui6pqUkdv41+G2q3MSSXnhK4uJXuvFfga4EF7pV8L+SGx07Xl1G21AA/uh0DXtE8VaFovijw1qthrvhzxJpOm69oGuaVdQ32l6zomsWcOoaVqum3tu8kF3YajY3EF3Z3ULvDcW80UsbsjK1AGtQAUAFAH8x/hf/g5G+GMX/BXzx9/wTL+LnwjHww8IaX8VdZ+Bfgr426j4mkZrn4l6PdTaVpreKdHn06G0svDHj7W4oNP8NajbXUa6R/aWl3esTz6dNeX9kAf04UAFABQAUAfwe/BD4c+Bv+CeH/Bff4/fsQ/FPwvpWr/skf8ABQHTNY8D2PhPV7S5ufDWo+HfjGYvFfw2022NxJaTLd6D40EXgCfW7aZLrS7kale6Zdi8tre5QAwtKuPjJ/wbM/8ABR86Zqv/AAlvjH/gnF+07rP7m7jBvIG8Ox3yqLyJP9G0lfip8JDqwS7tC2ltrujXWPM0zSvFVpeRAH92fgTx14P+J3gzwv8AET4f+IdM8WeCfGuh6d4k8LeJNHn+06brWiatbR3dhf2suFcJNBKpeKZIri3k329zDDcRSRKAdZQAUAFAH8ZH/By58HvFX7Mf7Sn7GP8AwVZ+DtvJpniDwR448M+C/iBqlnZB7dfEvgzUF8TfD3U9YeQyQ3kmu6Bb654dktp4ltzpvhS1t5PPW5dEAP6C/wBo39mf9mX/AILG/sN+FLLxdbwy+Evi74B0D4mfCX4g6Wthqfij4XeJPEeg29/p2q6ZcRzJDcXOlzz/ANi+MfDb3Vvb6qlpfaXcyabqdrY6jpYB/NN/wS4/bm+O/wDwR1/ax1D/AIJPf8FB714/gvqfiqS2+CXxQ1Oeb+w/Blx4ivm/sPXdB1y/EK3Xwj8dSyLdX9rceQPCetXF1q5h0m6Hiyx1AA/uFoAKACgAoA/zu/8Agtj+yB4C+EX/AAWg0hvGkFvoPwT/AG6rTRNd1DXmBlg8PeJ/GV3J4U8aeIJZPLCpeaf45sbjxfPb794stTtw26KUbgD7c/4Ik/tS/E//AIJiftofEr/gjz+2PevovhnXvGk8nwK8R6qZhpWneNtWSBtEt9J1C4ERbwd8W9GOmXGjyBZraDxE2kyQrYRatrlywB/bRQAUAFAHzP8AtnfBW2/aM/ZM/aN+B0+nJqs/xM+Dnj7wxo9lISFbxPdeHr6XwlOCMfvLLxPDpF7Fn5TLboGypYUAfzjf8GlHxmu9W/Za/aT/AGadamhXVvgR8bofEFlbTTAaiNL+I2ky2F3Atu7h2stN1fwHcSNJFEUgudY2zS5uIFoA/rRoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA/9f+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD/LR/ZC/wCCrn/BWf8AZ3/aW/aH/aN8PeLfiP8AtEfCnQPGt5dfHbwT4wuNe8Y+A/7J1HxHNBa3uqWbS3Engm9VoRZaR4k0k6XdWG86WLiXT7q8067AP7+P+Cb/APwVe/ZZ/wCCl/gL+2/hB4hTwz8T9Fsba48e/A/xVf2cfjrwrK6Ktzeaci+Qvi3wvFdbrZPEukW0SwlrVdc03Qry/tbKUA/TagAoAKACgAoA/n9/4OMv2DJv2w/2FNY+I3gnTXufjN+ypNqXxb8HSWaZ1HUvB0FrA3xJ0C3Zdrbxo2nWnii3JLuJPDElpbRtNqNAHyh+xxp/wq/4OAP+CNNl8D/jDc6Uv7R3wAs4fhtD46vYvN8Q+BviF4Y0pI/hz4+aS3hN6NC8b+GrK20HxhbyLdnXbnR/EGqG2bWNP0W7tADxf/ggf+3v8S/2YPjP4u/4I2/twy3Phv4gfD/xVrOifAHXPEd8giGoQT3M178MI9Qnnksr7SvEOP7e+G97YXLW1/e39xpGntqJ8QaHHaAH9jNABQAUAf5+/wDwd/f8E17nwp42+HH/AAU/+DGkS2E2vXmifDr4/wAmjqYZrXxnoFiW+GnxDdkuZJo5tX0DSpPC2p3FvZ21lZ3HhXQ7i6nfUtfZ5QD9/v8Ag3j/AOCs1j/wUq/ZE0rw18RtfS5/an+AWk6T4X+LEeoalDPrHj7Q4wbHw78VIYJSl/dS6jFDBpXjS4IvfJ8TpFql7eQHxVp1kgB/QVQAUAFAH8ln/B1L+yv4h1T4RfAX9vr4Y2tzB48/Zg8Z6d4c8X6tpcd4dTtvA2v61Fq3hPWJLiBGisdO8L+OI7i1ad5Ine+8a2caebnEQB+oUPgv9n7/AILu/wDBLD4fXPj37I4+Lfw+07Vk8R6ba2M2tfCL4+eHbGbRdd1TTLJLl/ITSfFkOpxvo1xPZP4g8FajEhbTv7Vsr61APwj/AOCOP7cXxc/4Jc/tV+Jf+CQH7fuqt4e8EL4rvbH4E+P/ABHNOmgeFtd1mY3mi2tjrl2oUfDL4npcW9/o15KYtL0XX9Ri1S4OlWereI71AD+22gAoAKAPgr/gpz+yXp/7bf7Dn7QP7P0ljHe+JPEPgm/134dObeKe4tfiV4SjbxB4MFm0skS2smrarYp4cursPuh0zWr8hJQzROAfi7/waxfta3fxH/ZN+If7H3jm/lHxF/ZQ8b38WjadqFzGt8fht4y1C8u47S2sGRboReGfGcOvJqNxI8iRHxNpNoqQKiLKAfen/BbT/glt4f8A+Ckf7M19/wAIlpumaf8AtOfCC1v/ABP8FPFhUWl/q7W0Ul5qfwz1DUEKg6b4pMStosl7uh0bxOlpcRXWl6fqXiCecA+K/wDg3c/4Ki61+0D8PNS/YQ/aYvp9H/an/ZrsbnQdBPie4e1174ifD3w1L/Zz2E0N+Unu/F/w7WNdO1G1iC3t34WhtNRaxY6Fr2pygH9O9ABQAUAfzL/8HSn7LV18Xf2EfDv7RPhiykl8bfsnfEDT/FMl3btFHNB4A8cXOmeHfEsn3Rc3Eln4it/BV3DHE5W2tf7TuXQIJJUAPhr9sH9m2X/gsH/wSF/Zh/4KJ/BRPN/bK/Zs+GMFp4wudLvppPEvjSw+Fbz2HjnRb2dENxL4tsLzSv8AhZ3hyIvFcvYa/q2modRvL/RokAP2Q/4IXf8ABT2x/wCCiP7LVppXj3VYIv2nPgXb6d4O+MekXVxt1bxLbQQ/ZdA+JkdtMzTSr4hit2tPErxPOLPxTa3c9wLG21vSLVgD9vaACgAoA/iG/YPurb/gmr/wceftKfsv63Fa+H/hp+1hL4lsfAJuJp3sbVfHEll8WfhRFZPbForjUbq4hTwGnmxMlvfardwTi2kjeWIA/t5oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA/9D+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAOI1n4mfDfw54r0DwH4h+IHgjQfHHiyMy+FvBus+K9B0vxX4liWSSFpNA8O31/Bq+sxiaKWIvp1ncKJI5EJ3oyqAdvQAUAFABQAUAFAH8K3wZ0+1/wCCQ/8AwcJfEX4E+NLKztf2Vv270vvD+lQa7ZQTeE7vwl8ZdRk1DwbDdtrwh06603wr4/S48BeJr+7aS1/sq18RxzedDK6TgHtv/BW3/gi34z/ZC8Xp/wAFMf8AglKuv/DbxX8LdQPjr4n/AAc8EXF0YtGstPVrrWPHHgHTl82e48MC3S4l8beCJPtlpp2ny3uo6dbnwsl7pehgH7kf8Ehv+CrXwx/4Kc/Ae31iOXT/AAn+0X8PrCx0/wCN/wAKmmWK5stQAjtV8ceFreQie+8E+ILnkbQ9z4Y1eSTw/q26NtD1fXwD9dKACgAoAKAK93aWmoWl1YX9rb3tje281peWV3DHc2l3aXMbQ3FrdW8yvDPb3ELvFNDKjxyxs0cisjEUAfwvfAW6vf8Aghl/wXk8TfAnV7ifQv2Qf2y7u3h8MXmpXL22gad4e8favLc+ANaN1eG2sVk+HHjf7Z4M13WJI8x6ZB4j8kiG6R2AP1N/4OEP+CWOq/tL/DnT/wBtj9mrTv7H/ax/ZusIvEF+/h2Oay8QfErwD4bkGpBLeexXzLzxj4EWKXU9Dlk8u+1DQYr3R47q5uNM8NaU4B9T/wDBED/gqdpf/BR39m+PSvHl3YaV+1F8E7PTfDPxk8NtKttf+JoLeNdP034oafpkrCY22vyQeT4pjtEa00jxSzgxaXYa5oVhQB+2tABQB89ftY/s3+Bv2vP2cPjH+zX8R4El8JfF/wAEar4UvLh4WuW0fUpBHe+HPElvbrcWv2i88L+JLPSfEVlbtcRRT3emQwzkwu60Af5Nv7OnxX/aJ/4Igf8ABS/VLi3t5tO8TfBb4la74F+IXhKaXfofjHwzbalLpXibw1eywNNa3uj69pIefTNUtnk8lpdN13R7hLu2s7yIA/1pf2a/2h/hj+1d8Dvhx+0F8HtbTXfAPxM8OWmv6ROdi3unTyAw6roGsQI8i2mueH9Uiu9H1e2WSWKO+s5jbT3Nq0FxKAe5UAFAHkPx++Cngr9o/wCCfxR+BHxEso7/AMGfFfwTr3grXI3giuJLWDWbGW3ttWs45gYhqeiXxtdZ0mZv+PfU7C0uFIeJCoB/It/wbqfGjxp+xj+2P+1Z/wAEhfjney2d/pnjDxX4p+E8F1NO1nJ4t8Fo0Xiy10LcBDLY+MvBFpaeKba9T9zPbeFrf7OX+2I7gH69/wDBcX/gk5pH/BR74Cp4u+HcFro37V3wQ0rUNX+EXiBB9ln8X6bambVbz4X6nqMO2aJdTuvPu/CF3MzWuk+JbmWOY2Vhrmq6laAHy9/wb8/8FZNb/aU8J3n7C37U91JoX7XP7PunXWg6RP4kkax174peDPCebK6hvra9aOe68eeBYLf7PrSQxi81fw7arrdzbPeaR4j1O4AP6baACgAoA/h38ZSn/gkB/wAHH2m+Ksp4X/Zt/bldJ9UaNrTTNAttJ+MuqvZ6v5tvZRtb2OmeAfjFpv8AbEOnpDHMuh6XZtHBFDdwyUAf3EUAfxv/APBfz9gn4mfsz/Gjwb/wWS/YjjuPDPj74eeJ9C1r496R4Z06JUi1O0uLW0034ozabBGbPUtM8RKV8P8AxGtLy3EOoXl1Bqup/wBqN4i1p7UA/on/AOCbP7fPw0/4KM/steCPj/4DuLCx8QT28WhfFTwLbzu958P/AIh2MCf2xos0M5e4/sq+BTWfDV60tyl3ot7bRTXR1Wz1W2tQD75oAKAPKfjr8IvDXx++C/xV+CPjCKKTwz8V/h/4r8A6u8tnDf8A2O28T6LeaSNTt7WdkjkvtJmuYtT09jJE8N/Z208UsUsaSIAfycf8GvPxf8T/AAZ+Jn7bP/BND4pyXWleKvhN491bx/4V8P6nIlpLp99oGrxfD/4l6bDaTJHNLeTTQ+Eb4W8HMVtpmpXLRFFmlQA8A/4KNfAz4p/8EK/+CjXg/wD4KV/sr+G7yb9lj40+KLiw+LHgTS0Nl4Y0nU/Ekkl743+GWpx2dvJp+n6P4lgt7nxT8PJ7iyiXSL6waLTIprnwibuUA/sz/Z3/AGgfhd+1N8GPAHx7+DPiBPEnw7+I+hW2uaFfYjivbUvuiv8ARtZtIprhbDXdDv47nStYsVuJ47e/tZ1guLm2MNxOAe00AFAH8cv/AAdJ/BfxF8JvGP7GX/BS34XR3el+MPg74+0j4feKdf0wpbT2M+k6vL8QPhjqJktvLujdC9tfGFlLfSmRYYYNLtQyDykYA/qy/Zz+Nfhr9pD4DfCD48+EZIH8P/Fr4e+FvHNlDb3Auk0+XXdKtrvUdGkuFVRJdaFqb3mjXvyrtvLCdCMqdoB7RQAUAFABQAUAFABQAUAFABQAUAFABQAUAFAH/9H+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP8AO0/4O6v2R/i34Q/bH+Cf7YvhvxZr8Hw/+K/hDRfBcd2l/cxWvgf4gfDl4LO70+z+x3Jm0601bw/daFr1nO0dub3Vn1/7L5r2NxIgB9N/8EeP+C4nxp/Zb8Z+CP2RP+ClHifXPEHwr8aaVocnwc+P/iq7utYv/Cmn6lawpoMereIZ/PuPEfw6uoPJtIbyWe5vPC5jjW3YabBLYRAH90thf2OqWNlqemXtpqOm6jaW9/p+oWFxDd2N/Y3kKXFpe2V3bvJb3VpdQSRz29xBJJDNC6SRuyMrMAW6ACgAoAKAP5cP+DpX9jm8+K37Jfgj9sDwDYMvxN/ZJ8VWd9q+oWEMaajP8MfFWpWVrczSXIkW6mXwt4vXRL2xtYo5BbQa9r2oM0McMxcA/X7/AIJU/tiaf+3X+wh8Bvj01/Ff+Lr7wrb+DfipCZvtFxa/E7wbBBo3ipr8+RCkc+vtHaeLY7dFkSCw8R2cLSyOjtQB/Mz/AMFXP+CeXxt/4JSftLaf/wAFYf8Agm9Z3Wi/Dy28RDVvjX8MPD1rK+i+CrnWrtYtZF7oVniK4+EvjQ3D6fd2aRJaeG9QvV09fsVvcaE0QB/T7/wTi/4KE/Bn/gpD+zn4f+OHwrv7ax162jtNH+Knw3nu45fEHw18bfZy11pOowfLPPoupGK4vfCuvrELPW9NSVA0Gr6brml6aAffVABQAUAFAH85v/Byj+wbP+1L+xS/x6+H+lyy/Gr9ka6u/iNpVxpluzavrHw0k8hvH2lI9pZy3s7eHUtLDxtZvPeW9lpOnaN4omjV7nUSrgH13/wRE/bttv29f2Bvhb451rV11D4u/DO0g+EnxmgnuY5dTl8V+FLK2h0zxPdI17dX8yeMPDjaZq1xql4lsl94jXxLbWsbJpztQB/Pd/wVY/Zg+K3/AARi/bo8Ef8ABVv9i/TEt/gj8QPGksXxl+Gul2Mtp4W8P67r0jz+KvCGrabpscFlb+AviRZteXfh5rU2reHdZt72z0+LSX0vw3e3AB/X5+yd+1H8KP2y/gD8O/2iPgzrttrXgz4gaJbX5t0nSXUvDGupFGNe8H+IYVWN7PX/AA3qDS6ffwvEkdyqQanYNc6Vf2F3cAH0ZQAUAfw5f8HYf/BOa2kv/Af/AAUQ+H+grIl4+k/DD9oCO0hdmS9s7UwfD3xzcn7VJIVvNItH8H6pLFa2tjY/2J4bMkk1/rDbwD4i/wCDfb/gqHqP7A3xo0j9mX46+Ibw/snftHahZXnhPxDf3m3Rfhj8RNSFrp9j4pj+0sba00PVWitvD/jhIJLYwRx6ZrUr3R8PLY3YB/o1UAFABQB/GV/wcjfAvxn+yl+0x+yj/wAFePgLaHTvFHg3xt4R8I/FS8trfz4JfEng+RNS+H2uazFcS3MV7b6/4W0/UPBmoWf2OHTf7O8Mafb3Ynm1WXeAf1lfs3/HfwX+098B/hP+0D8PbmO48I/FnwRofjHS0S4W7fTpNStR/amhXVysUCS6h4c1iO/0HU2SGJP7Q026CIFCigD+Xn/gv1/wTL+IngLxvpX/AAVv/YVgu/CXxt+D2o6Z4w+N+jeDE+x6lq0OhXMMlt8XdL0+2CreajYqkVt8Qra2iZ9Q00f8JTf2t1s8VamwB+1H/BJj/gpl8PP+Cmn7M+kfEjSvsfh/4w+C4NM8N/HT4eRzqZfD3jA2rAa9pUDbbgeEfF7Wt3qWg+crS6fIl/oE9xezaO2pX4B+pNABQB/Mp/wdGfsjTfGb9h/Qf2l/CViD8Q/2SPGNp4olv7YQQ3rfDrxhe6ZomvqJAFvbyTSfEkXhPUrKCOVksLSTXr4IiNcyUAfqp/wSd/ax/wCG0/2Av2dPjlqGpJqXjK98GW/g/wCJLm5lu7xPiF4FY+GvENzqk0vznUdf+w2niqYEt+71+EgjJVQD788TeGtB8Z+G/EHg/wAVaTZa94X8V6Jq3hrxJoepQi407WdB12wuNL1jSb+BvlmstR0+6uLO6hPEkE0iHrQB/CNqlr8Sf+Dar/gqJbatYDxXr3/BOj9qvVjHcQ+X9tgh8LvqUb3dmm+WHTpviV8GrvVlms5BNp13rnh66hSZtL03xdcIgB/dh4O8YeF/iD4T8NeOvBOuaf4m8H+MND0zxL4Y8Q6TOLnTda0LWbOK/wBM1KymH37e8tLiKaPcFcB9siI6uigHSUAFAH8Rv7fMVv8A8E1f+DjX9mz9q6wa18P/AAt/amHhy58dzwQXB021Txil18I/itLerPiCfVWkQePZvLeRILvWLK5jaCULFAAf2EftE/s+/C39qf4MePvgL8ZvD0fiX4efEbQrnRNcssxxX1o0gEljrWi3csNwthruiXyW+p6RfGCeOC+tojcW11atPbTgH8YX7BHx7+LP/BAb/goF4p/4J7ftaa1qtx+x98avESa18MPiFeRzr4c0SfXZzpnhP4raJFPNLBbaLq/2GHwv8SrHT7lp9NuNNea5S/1HwnFp1wAf3RQTw3MMNzbTRXFvcRRz288EiSwzwyoJIpoZULJJFJGyvHIjMjowZSQQaAJaAPgz/gp1+y1a/tl/sI/tJ/AH+zxqPiHxP8OtW1jwFGmnw6hfL8Q/Byr4r8GQaasuJLa61jW9ItvD013astymm6zfxRiZJpLeUA/Gj/g1c/anufid+xh4+/Zb8WX8h8b/ALKPxEvdM03T764hS6i+Hfju51DVdNtrSxbZePDo3i6x8Wrf3GJYbc6zpduzQmWGNwD+oqgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP//S/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD+Pz/g6F/bp/4Kaf8E/fF37JPxr/AGTPG2qeCP2d5H1XTfGl9peh6XrWjal8XLHVJtUg8N/EeG/029hufD+v+DYrc+HdK1FxZ382l+K5rOIXVlNKoB+0X/BIz/gq/wDBn/gqp+zno3xI8JzaX4U+M/hvTNPs/jZ8IFvd954T8RmKOG51nw/HczSX2peBNauw8uh6jI09xp/mLo+rzPfQx3d+AfrDQAUAFABQB8Mf8FH/ANinwj/wUA/ZC+Kv7N/idbS31XxBph134ceILsPt8J/E7QILqfwjru9FkaK2aee50TWJEguJf7A1nVkgha4eJlAP4wv+Cb/wI+HP/BQX4MfG7/gjh+2RYH4Y/tgfsua7401z9l34l6tYfZ/GXhs6XdzWvjv4YayJpYL/AFfQND1i0i18eG0tZrmLTL3xPrNrc276NHBdgH1l/wAEzP8Agpn8ff8AglJ8fJP+CYP/AAVBTVdJ+Hel6qujfCL4u609zf2ngm0vrjbobw623mLr/wAJ9dRlubO+t2kbwy0jzwpHZrfWEQB/bPYX9jqtjZappd7aalpmpWltf6dqNhcw3ljf2N5ClxaXtld27yW91aXVvJHPbXMDvDPC6SxuyMrUAW6ACgAoA4L4p/DXwl8ZPhr4++E3j3Txqvgv4k+EPEPgjxRp+Id9xofibSrrSNRFu88NxFBeR2908tldNDI1peRwXMamSFKAP42/+DfP4meLP2Cf+Chn7Xn/AASS+MOoOltqnirxJ4i+Fk07Xa2Nx4v8CLczyz6FHcy21vBpfxB+G5GvrdGya6v5PDvh+3i2ec4oA/tO1vRNG8S6Nq/hzxFpOm694f8AEGmX+ia7oes2Vtqekazo2q2ktjqek6rp15FNZ6hpuo2VxPZ31ldwy213azSwTxPE7owB/CX+2N+zN+0J/wAG8P7aOmfty/sdWmr+I/2Kvij4h/srx78PWuL260XQbPV7p7/VPhb4vIEhjsH8uTUPh74nuvMvIJbKHFxc6tpd28oB/Zf+yH+1l8HP22vgH4H/AGh/gb4hg13wZ4yssXNmZoG1rwj4ltY4TrngzxTZxSO2meItCnnjS5tpQq3VlcWGsWDXOkappt5cAH0xQAUAFAFHVNL03W9M1HRdZ0+y1bR9XsbvS9V0rUrWG+07U9Nv7eS0vtPv7K5jlt7uyvLWWW2urW4ikhuIJJIpY3RmVgD+Hb9knVdT/wCCGn/Bcfx5+yX4r1C+0z9kX9su+09PAOpatdyx6LZ2PirU765+Efiia7vZdD0qW+8F+JbrUvhp4t1ySE2Gn2c/i+6tIXEVtvAP7R/jV8G/h3+0L8J/H3wT+LHh618U/Dv4leG7/wAL+KdEu0jZbiwvVDR3Nq8scwtdU0u8ittV0bUEQzabq1lZahb7bi2iZQD+JX9kL4t/E3/g3g/4KSeK/wBi/wDaC1nVNU/Yc/aH8RWWqeC/iBqGn3P9laTYaldz6d4Q+Kmmxx/aWsLjSd//AAjXxP0XSZLxTHZ3TiHV7zQNAlUA/uxsr2z1KztNR067tr/T7+2gvbG+sp4rqzvbO6iSe1u7S6geSC5trmCRJoJ4XeKaJ0kjdkYNQBZoA8Z/aI+BXgT9pv4HfFH4A/EuxXUPBHxW8Hav4Q1uPy0kmsxfw7tP1mxWT92NU0DVYrHXNJkkBSLU9OtJXVlQqwB/mgeB/wBgzxDqPx3/AGiP+CUPxVmtPDX7RPgTX/E2u/sreJr+Q6fZ+KPF+iQPqth4QtL+9FuTofxZ8Kw2moeE7lisUmsvowHlx385QA/qe/4N3v8AgqFr/wAaPB+p/wDBPb9qC5vdE/ak/Zusr7QvCsnimd7TXfHnw/8ACr/YJ9AurXUDHdzeL/h1FA1rNBEGmvfCNtFdGygHhvVb27AP6h6ACgD5Y/ba/Zc8J/tofsr/ABq/Zq8YR2q2XxP8FalpOj6ndoWj8P8Ai+1Ual4O8Rh0guLiNNF8S2emXt2LWM3Fzp8d5Yr8l06uAfzV/wDBsN+054u+Hmp/tKf8EsvjtJd6H8S/2f8Axj4l8VeANB15xbahbWFjrX/CO/EzwfBFfXEVyZtH1xNN8Q2OkWdo7rb33ivU5tkdtM6gH9eN9Y2WqWV5pupWdrqGnaja3FjqGn31vDd2V9ZXcL291Z3lrcJJBc2tzBJJDcW8yPDNC7xyIyMy0Afwoft3/s7fGv8A4N+/289C/wCCgn7Hejape/sdfF/XzpHxG+HUEt5N4b0FdauI9R8S/CjxI6GQQ6LqT2k2u/DbVr+OW806WxgjhuNQ1XwvPe3AB/Zn+yt+1D8Hv2yPgX4E/aE+Bvia38S+A/HelxXcQDImreHNZjji/trwj4msVd30vxJ4evHay1KzdpIZdsOo6Zc3+j3+m6jdgH0NQB5Z8cvhP4f+PHwZ+K3wV8VLCfD3xW+Hvi/4f6rLPZw34s7bxXoV9o39pQ2s7JHJd6XJdpqNi3mQyRXtrbzQzQzRpKgB/Jv/AMGsvxZ8R/Cvxn+27/wTq+Ist7Y+J/hH49vPiJ4e0C+IiGm3Gl6x/wAK8+JUCQTEXC3Ml7F4JfyVBVI7S5kZVO5qAP7G6APhn/gon+wp8MP+Ch/7L3jv9nr4jW1rbX+pWk2sfDfxk8O++8A/ESwtpx4f8R2rojytYtLK2neILFVb+0NCvL6GHyb9bG9tAD+bL/ggr+3P8T/2N/j941/4Iy/txy3/AIe8W+E/GOp6R+z3rOvaikthYa1JczXNx8OLG+mZ4L7w546+0Q+I/h1f6ddtY3WrXs9tYR6l/wAJdZXFkAf2Z0AFAH8wn/B1L+zXd/E/9hbwT+0P4ds7qbxT+yz8UNP1y4urSMFrHwR8Qn0/w/rl7NKv71fs3ijTvA6Qf8s0+1XDHDEBgD9iv+CYn7SkX7XH7BP7MHx1e8a91zxP8LtD0nxlLNepfXreOfBiP4O8X3OoOuJIbrVNd0K81tLe5UXC2ep2sjmRZUmnAPDf+Cv/APwTJ8F/8FLf2Y9X8Fx29npPx4+H0N94n+BHjhzDbyaf4lSNJLrwnq91JtDeGvF8Vulhcl57f+ydVXS9bWc29lfWOpAH5X/8G+n/AAU28aapPrv/AAS5/bOn1nw1+0z+z9NqfhP4YTeM/Ot9Y8S+GPBpuLHVPhrqrag0d+vifwBBYN/YMVws4vfC8E2mJJZHw5p9tqQB/VnQAUAfxA/BLy/+CVX/AAcreOfhSVTw78C/222uLHw9BDZrDpKW3xjuIvFHgqx0w3rqtvbeG/ixZW/hGe/hkV4rO11KLzJkkljlAP7fqACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//9P+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAPjj9v79jzwN+3n+yP8Z/2YPHdnp0kHxD8K3aeFdX1CCGUeFPiBpanUfBPieCd7K/uLJdO12C1i1WXToVv7vw9dazpUMqpqMoYA/yov2WNZ/a//wCCav7VvjXxN8FZ/EPhH4vfs8eKddsfHXgmeCZpp9M8P6vLpXi7w/4n8PvganoDpE9rrFndQfLC/nEQvGk0QB/px/8ABLT/AIKk/BP/AIKd/A+38deCZ7Twr8WvC1taWfxf+EN1epJq/hLWWVYn1bSVlb7TqngzVrkO2k6qVMtq7f2Xqmy8jiluwD9QaACgAoAKAP4v/wDg4U/Zh+IH7F37UHwK/wCCyX7LdqdF1jw9428M6X8bbbSreJbRfFmmKT4f8S6tp8TKt5o3j3QLK78K+LhLaPZTXlnHJqs0974rVWAP15/aK/Za/ZO/4L+/sDfDL4r6QbHw74r8UeDl8TfCL4m2qRah4k+FnjQK8XiDwD4luYbezu9X0DTvEltd6RrtgYrfzHtovEmiQx+fCl6Afip/wTM/4KZ/Hr/glF8en/4Jf/8ABT+PVdI+Hek6pFovwh+LmsvNe2fgq01C6kGizRa0xZdc+E2uq6zWN5btM/htnaaGJLRb+zUA/tnsL+x1SxstT0y9tNR03UbS3v8AT9QsLiG7sb+xvIUuLS9sru3eS3urS6gkjnt7iCSSGaF0kjdkZWYAt0AFABQB/E5/wcXeFbz9jv8A4KQf8E/v+CkngSyh069v9e0PR/Gd1bGYnWPEXwf8Q6LdxTa0rO0TQ6r4E17TPDgh2pFdafoM0TRswndwD+03QNd0jxRoWi+JvD9/Bqug+ItJ07XdE1O2LG21HSNXs4dQ02/ty6o5gvLO4huIiyIxjkXcqnigDlvip8LPh78bvhz4y+EvxX8J6R44+HXj/Qrzw54t8K67bC507VtKvkw8bAFJra7tplhvdM1Kzlt9S0jU7az1TTLq01GztrmIA/ha1uw/aN/4Njf26P7Z0H/hJ/ih/wAE5v2hdeCz2VxKZobjR4p3c6feSAR6ZpHxY+HsV87adqMcNnFr2mvny4tJ1m/sLQA/uV+C/wAZfhv+0J8K/A/xp+EPijT/ABn8OPiLoNr4i8LeIdMlWSC8sbjfHLBOisz2mpabeRXOmavptxsu9L1WzvNPvIorq2ljUA9PoAKACgD+cb/g5P8A2Cbn9p/9jeP9on4caXI/xx/ZEmvPH2n3Wl25/tjWvhc5gn8caWJLOxkvrp/Cz2tn43043F9Bp+kabpvi+eOJ7vUxuAPs3/gil+3vb/8ABQP9hL4a/EXXNVS9+MHw8hi+FXxrt5rpJtSn8Y+GLO2isvFl2sl/e38y+N9BbT9cudSu0tYbvxK3ie0sYjDpjNQB03/BW3/gm54L/wCCln7LGufC+8FrpHxe8FJqXi74EeNpY4t+geN1tI/M0S9uHKSReG/GcdlaaRrhjmi+yTw6TrhW6bREsroA/H3/AIN7/wDgpP47tdV8R/8ABKj9tG51Hwx+0P8AAS71fwx8IpPGAa31jXfD3hSa4t9X+GGpXF0UuJdd8HR20lz4VFwshvPDUV1pENxFHoeiWd2Af1kUAFAH8k//AAcx/sZ+K9FsPg7/AMFS/wBnmK80L4wfs1+IfDVh8S9X8Oxvbam3hiz1iC88AePJZrC2jmN14R8RFfD2o6leXc88mnav4YsreKK00d2UA/PX9vLwFqv7RXwO/Z6/4OFf2AXi8J/GPwjJ4an/AGsvBngy28i58P8AxQ8ItZ2GveOhp1qtxBLaSXwtbXxXaXbG41Hw9rvhnxJfWuoS6r4mu7UA/rR/4Jh/8FCvh1/wUl/Za8J/HXwi2maR40tFi8N/GD4f2d2ZbjwL4/tLdGvrZLa4llv18O64gOr+Fr24e4WfT5ZdOkvrrVNH1bygD9EaACgD+I3/AILf/D/xb/wTJ/4Klfsx/wDBWD4M6bLa+D/iN4j07T/i3p2mA2On3vi3RbeDQvG2kaodOS38iy+Jvw9vFe7lnka51bVh4qujI0251AP7P/hz8QPCvxX+H/gn4neBtTj1nwb8QvCugeM/C2qxbdt/oPiXS7XV9LuSqs3lSyWd3EZoCxeCbfDJh0YUAYXxq+Dfw5/aE+FPjz4KfFvw3Z+Lfhz8SPD154a8VaDfIrR3dhdbZIri3kZXNpqemXsNrquj6jEBc6Zq1lZahaslzawuoB/Dp8MvF3x3/wCDZr/goFP8K/idL4g+Iv8AwTw/aP1ZrnRtftmeWCLRGvfs1l4t0mF5Taad8Svhy9zHZ+LvDE5g/t3R5HjhuPsGseGPE1uAf3Y+BvHHhD4meDvDPxB8A+IdL8WeCvGWi6f4i8L+JdFuVu9L1rRdUt47qxv7OdeTHNDIpaORUmhk3wTxRTRyRqAdVQB/Eb+1gI/+CaX/AAcs/BT4/oLPw/8ACL9sA6JP4rvJ5br+yreP4pQ3Pwy+Imqaq1uQRcab4ytbnx2LbbN5bPYXDxOjJE4B/blQAUAfzR/8HC//AASt1P8Aal+GGn/tmfs46a+mftX/ALN2nrrlx/wjyTWmvfEr4f6BMupta272YU3fi/wMYpdW0CV2gvL/AEVdS0Zbu5uLPw7pygH0R/wQ6/4K0+H/APgov8CYfAvxGv7XRf2tfgvo9npHxW8NXLi1vPGumaaYdKt/ifpNjNIZ2/tCY29t4xtYd8WmeJJ/tUUdnput6XZW4B+6lAHz7+1f8CtI/ab/AGafjp8ANbgsp7T4s/DDxf4NtX1DzPsdjrmqaRcr4a1ibyg0g/sPxGml6zGVR8S2EZMcihkYA/mS/wCDUP466lY/DX9qv9h/xpdG18V/An4nyeOfD+iXiONSTRfEEreEfGaZORHaaL4g8O+Hwbc7Cl14gldULSSlQD+u+gD+Uv8A4OCv+CZPjTUrjQv+Cov7GFvrHhj9pf8AZ/m07xb8T4/BjTWuseI/DPg5re/0z4laUun+Vep4k8Bw2JbxFNbtP9u8MwQapJFZ/wDCP6nd6gAfqn/wSA/4Ka+C/wDgpd+zFpPjZp7TSPjt8PodP8L/AB58DoIYH0/xQsMkdt4s0i1iIH/CM+MY7WbULNBDAdJ1NNU0NomhsLO/1AA/WGgD+Rn/AIOtP2dNTj+Fv7Nf7eXgK3a08cfs8/EKz8E+I9YsjcjUo/DPiC8bxN4Nvi8amK2svDnivStXjE5eJ/t3i21TE4KqgB/SP+xX+0XpH7W37KHwC/aO0aSF4fix8NfD3iPU0to/KtrTxSludL8Z6bbpvkxBpXi7Ttb02ElyWjtFY7SStAH0/QAUAFABQAUAFABQAUAFABQAUAFABQAUAf/U/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA/iK/4OBv2ede/YS/bn/Z+/wCCsXwf8GabrXgbxj4p0fw/8f8Awtc6Sl14T1Lxho8MUN5aeK7CGwt9NOifFzwLb3NjqEFzPd3us61o3ivUbyRJr+B3APBf2rv2a/FH7A3iX4O/8Fxv+CRd5f3v7KnxNtNH8ZfEP4bad9pvtN+H1t4mvUs/EXhPxXpVnPKbr4danrAu/COofv5f+ER8V2r6HNfQ3R8N6legH9h3/BPz9vj4Jf8ABRT9nnw38d/g3qSQTzQ22mfEPwBe3cM3iT4aeNVt1fUvDespGsRurJpVmuPDviCO3gtPEGlBLlYLHUYNU0nTQD7hoAKACgDxv9oX4FeAP2mvgj8TvgF8UNNGq+BPip4S1Pwnr1vtRp7dL1BJYavYGQNHHqug6rDY65pE0iukGqadaTOjqhRgD+Pv/ghv8c/iD/wTI/4KA/HX/gjx+0xqclt4b8XeOL2/+B+t38l5Bo58dfZkuPD9x4fOovaBdC+M/g06VNpzmze+vdasfCdpFFam81DcAf0X/wDBUT/gl98FP+CmnwQuvA3je2tPDPxY8L2l7d/CD4u2toraz4P1uRDKmm6lJHE1xqXhDVbhUTVtMJkktWb+09MVL6J0ugD+fv8A4I6/8FG/jF+wB+0Fq3/BI7/gpXfX3hJ/Dmsp4d+A/wAR/Fk5OlaDd3kofQ/C954guZPIuPhv4ztZ7e+8EeJ0uJdL0u6u7RJJIvD+rS6hoIB/Z7QAUAFAH8/H/BzD+z1/wu//AIJd/ELxbp9ray+I/wBnzxp4N+L2nXDQltR/slL2TwZ4isbCdY3eGOW08VW+rXyEpC8OhJK5MkEAoA+of+CHX7RI/aX/AOCYH7K/jK71GPUPEXg/wQvwh8VbFCtZ6p8LriTwrpdtOVCiS4l8IWfhnUJpvmeV77fKzTF2YA/WagD59/aj/Zf+DX7Y3wR8bfs/fHjwpb+K/h/43097e4jxFFq+gatFHL/ZPivwtqTxTNo/ifQbiT7VpeoJHLE2Z7DUrXUdHvtS0y7AP4s/2Yfj3+0P/wAG4n7bmo/se/tOahrnjj9g34x+IjrHgzxqttdTaVpNjqVxHp+n/E/whAzzLpmr2EUVrpfxH8IwTyG9Sy8stqF1p3h7VFAP7tvD3iDQ/FugaH4q8Matp+v+GvE2j6b4g8Pa7pN1DfaVrWh6zZQ6jpOraZe27yW95p+o2Fxb3lndQO8NxbTRyxuyOrUAbFABQBR1TS9N1vTNR0XWdPstW0fV7G70vVdK1K1hvtO1PTb+3ktL7T7+yuY5be7sry1lltrq1uIpIbiCSSKWN0ZlYA/h2/ZJ1XU/+CGn/Bcfx5+yX4r1C+0z9kX9su+09PAOpatdyx6LZ2PirU765+Efiia7vZdD0qW+8F+JbrUvhp4t1ySE2Gn2c/i+6tIXEVtvAP7l6AP5T/8Ag4Q/4JleNNcfQf8AgqF+xrbaz4d/ac/Z7l0zxT8SU8GtNbaz4k8LeDPIvtK+I2l/YAl+viX4fw2Cf21LbvL9s8LwQak8dl/wjd7cagAfqP8A8EfP+Cnngn/gpf8Asy6V4vlubPR/j78O7bT/AA18ePAqrDbSWXiNYpIrbxhotqmzd4X8XpbS3tuqQQHRtXTU9CkhNtZ6dfakAfrTQBw/xN+HHg/4wfDvxv8ACv4g6PBr/gj4h+Ftb8HeKtHuVUx32h+INPn02/iRmVzBcCC4aS0u4wJrO6SG7t2SeFHUA/in/wCCUHijVv8Agld/wU//AGiP+CR37R8ttrHwG/aQ1i70b4av4msornwzr+q69Yz/APCvrySwnh1K1n034reDtVHgrWtGwLeXXL/SLXWZRDo1wqAHnvxP8OfEr/g2w/4Kd6Z8VfAltrniX/gn5+1HqEtpq2imOaa3s/Dl1qUN1rvhZ5JXNqnj34XXtymq+FtSE9vJq2hy2QvZLaz13XNKiAP7ofh98QPBfxW8D+E/iV8OvEem+LvAnjnQdN8TeFPE2jytNp2s6Jq1tHd2N7bl1jmj8yKRRNbXMUF5ZzrLaXlvBdQzQoAdjQB+fP8AwVH/AGL9J/b3/Yi+NX7Pc9rBL4u1HQZPFvwpvZkjZ9L+KnhGG41Lwk8Lz3dna2w1uQ3fhG+vbmUxWGleI9QvBFJLDGKAPxr/AODXf9tDUviR+zh8Qv2G/idd3EHxa/ZF8QagmgadqbMmpXPws1zWZ47ixFu1nHLnwR41mvLK7uL27lm+y+KtC0+2hS3007QD+pqgD4v/AG9f2F/gx/wUJ/Z38Wfs+/Gaw8q11WGTUPBnjSytIbnxB8O/GUEEiaT4q0QSvD9oEEjCHV9He5tYNb0qS5sGurG5az1KyAP5Sv8AglP+3P8AGf8A4I//ALV2vf8ABJz/AIKCajJYfCC98XSW/wAFPibrM7r4f8F33iC8J0fV9K17UPJL/Cfx+ZIL2dLpooPC2r3UutPDpbXHiqO7AP7haAP5av8Ag6u/ZsuvH37GXwx/af8ADFjcSeLv2XPilZXGoXljY7prbwN8Q2s9Mu9QvtQhBuIbfSfGGkeEILBJA1vHca9dSI8EsrC4AP29/wCCcn7ScP7XX7D37NP7QH2uS91bxz8MNCTxbPPPFPcyeOfDSyeFPG01wYydjXnijRNUv4Y5P3v2S7tnfO8MwB9sUAFAH8YH/BZX/gmr8WP2C/jnpf8AwV4/4Jox3/gjU/COut4l+O3w38JQH+ztEub6RodZ8Y6ToVvE1pefDrxbb3M+m+P/AAbc2s+k6c9/dTwwv4Y1afSdAAP6Df8Aglr/AMFMfhL/AMFNf2edO+KHg57Lw78TfDMdho3xn+FwujJfeCvFUkDf6VYxzuby48J689vc3Xh6/mMrosV1pV3PNfabPPOAfplQB/Ekvmf8E0v+Dn4j/StB+D/7c7nnEei6VqbfHSHcBnIsX0jQPjrpoAz+7WPRuBbTLiAA/ttoAhuLeC7gntbqCG5tbmGS3uba4jSaC4gmQxzQTwyBo5YZY2aOSORWR0YqwKkigD+GH/goF+zR8fv+CDP7cVj/AMFHP2ItDv8AUv2TfidrD2PxW+Glv9ruPDfh/wD4SC6hvfEvw28UwWgLx+EtZvLY6z8P9YmhE+h31vZWqXVxrOgW+o34B/W5+wz+3P8AAj/goF8CPD3x0+BXiCG9sb2G2tPF/hC7uYG8U/DvxS1uJbzwz4ms4iGjljYSPpmppGtjrdiovbIqRcW9uAcl/wAFSfhl4e+Lv/BOj9tPwV4niM2lt+zp8T/FaKACV1j4d+G734h+HZhnGPJ8QeFtMlYjkKhxnpQB+U//AAaqeJdZ8Qf8EtWsdWvZ7uDwj+0Z8UfDGhxzSNItjpC+G/h3r/2O3DcRQ/2rr+qXflphBLdSuOXYsAf0m0AFABQAUAFABQAUAFABQAUAFABQAUAFAH//1f7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD5z+A37Wv7Ov7TmpfErR/gT8U/D3xG1H4Q+IYPC/xCtdEF8r+H9XuvtwtI5Te2tsl3a3b6ZqUdrf2LXNnO9jcCOdgqM4B9GUAfPH7V/7M/wAN/wBsL9nv4o/s5/FbTob7wh8TfDF9okl01v8AaLvw7rOzz/D/AIr0pRLbsNW8M6zFZ6xZILiGK6e1NjeM9hdXUEoB/Ib/AMESPjd4o/Yc/as+P/8AwQ8/bksbS78HeONe8QaX8KrTxXZpdeGdY1/X9P8AJk8Ow/2qbdL3wX8avBEllfeHwYbqLUNX/sW30+zibxJf3CgHmX7SHwP/AGgf+Db79uHSv2tP2ctP17x7+wD8afEa6P418Ffa7h9P0yx1CeW/1D4Z+KbkQ3EekeIdJhW61f4a+LriCRdRisi5TUJtP8T6QoB/ad+zP+0r8H/2ufgt4J+PnwM8U2/iz4e+OdNjvbC5HlQ6po9+qJ/aXhvxJp0c9w2keJNDuHNpqunPNKiyKl1Y3N7pl1ZX92Ae8UAFABQB/K3/AMHL37Amv+Pvhd4J/wCCiPwEtbrTPjt+ypdaVc+M7zw+gtdZ1f4a2urx32meJY57aWK8n1X4ea/LHKHghurw6Bq93dzTW9h4ZRKAP15/4JPft+eG/wDgot+xx8Pfjbb3VnF8SdKtYPBPxr8PQSWqT6N8StDsrZNV1BbG2itls9I8WRNF4l0ZI7WO0t4r+50e3luZtGunUA+c/wDgtR/wSQ8M/wDBSv4MW/iHwOun+Gf2rPhHpt3d/CLxi7w6ePEtpC82ov8ADbxFqreUINO1G+ea58OajdTra6Br11NLNJa6dqurXMQB8Nf8EHv+CtniXxtc3P8AwTY/bkm1PwX+1t8F7q98FeBtV8b+bpuqfELTPC7SafN4D8QLqIiuIfiN4UWylhsHnLN4o0eJbOSO117SVbxIAf1OUAFAHknx9+FGl/Hf4G/GH4K60LUaZ8WPhn44+Hl1Ne26XVvZjxd4b1HQ4tRMLpJmXTZ72LULd0XzYbi2imhKTRo9AH8pX/Bp78XNS8L6N+2n+xH4uubm0174R/E+H4h6B4fuBh7WC+uLnwN4+lZWIeF7fVNF8FwtEE2brls7HVt4B/YhQAUAfB3/AAUV/wCCf/wb/wCCjX7OPif4F/FXTrW11kQXerfC74hxWqS698NfHKW+2w1zTZhiWbSrx44bHxRobP8AZdb0kkYh1Wy0fUtLAP5hv+CQP7f/AMY/+CX/AO0vq3/BIz/gotqE/hvwha+JpdI+BfxF8TXkg0LwdqWtXn2jR7GHX78pb/8ACq/H5u01PSNVaZdK0DVb/wDtOZ7PTtT1y9tQD+2qgAoAKAP5xv8Ag5P/AGCbn9p/9jeP9on4caXI/wAcf2RJrzx9p91pduf7Y1r4XOYJ/HGliSzsZL66fws9rZ+N9ONxfQafpGm6b4vnjie71MbgD7N/4Ipft72//BQP9hL4a/EXXNVS9+MHw8hi+FXxrt5rpJtSn8Y+GLO2isvFl2sl/e38y+N9BbT9cudSu0tYbvxK3ie0sYjDpjNQB+s80MNzDLb3EUVxb3EUkM8E0aSwzQyoUlilicMkkUiMySRurI6MVYEEigD+GD/goR+zv8Xv+CCn7e3hb/goz+x3pOoyfsnfF/xJ/ZHxV+HVq8//AAjeiXGuznUPFnww15Yg8MfhvxAlpL4h+Hl9cwtdaNf6ekVrJNqHhePUboA/st/Zn/aQ+E/7W3wR8A/H/wCCniKLxH4A+IWi2+q6dN+7j1LSL0oq6p4b8QWSSzf2d4h0G983TtVsjLLEtxD59ncXmnz2d7cAHu9AH8v3/BzP+wjq3xg/Z98H/tw/CCyubf44/si3UGoa1f6Osi6xqnwll1Qag9zFJHceabj4feJZhrtotpbCVNM13xHf3lx9m0yFFAPo39nzxH8FP+DgT/gkhZ+FvizLajxrqGkw+EPiDfW0Vrcax8N/2hPBGl+TpXxE06x2wBYtXjvofEH2SJbKC90XxD4g8K219C0dxdwAH5O/8ESf21vin/wTk/ak8b/8Ecv27dQu/D1haeMLuz/Z+8Va/Kx0jR/EmszreadoNjql59nb/hAfilZ3NvrnhG+h87ToNev7eaK3trbxLq2pW4B/atQAUAfw0/8ABQjSdW/4Iz/8Fyfg/wDt0eD7C8sf2df2sdXuLz4j2WnwSrpsk/iC6t9D+N3h7DeVZz6nBPqVj8RdHtsm30661jRSm1rM0Af3EaZqena1pun6zpF9a6npOrWNpqel6lYzx3VlqGnX9vHdWV9Z3MLPFcWt3bSxT288TvHLC6SIzKwNAF6gD8eP+CyP/BKnwL/wUz/Z7udP0uz0vw9+0r8NLS71f4I/EZglndNdRiS6ufh94hv1aIT+FvEc3Nq92/8AxTmvNDq1lPaWl14gt9VAPy2/4II/8FWPG0PiS+/4JZft43eoeC/2j/g5dXHgb4Q6v478+w1TxVaeGZG02T4T63dagyy/8JZoMNsF8GNelTrWkwN4dgnGpWXh/T9ZAP6QP2wfgFpX7Uv7Lnx5/Z71iC2mh+K/wx8U+FdPa7YpbWfiO406S58J6pMwBIXR/FFto+q9CM2YBBBxQB/NV/waifH3UY/hL+1B+xF40uhbeLfgB8UJPGXh7R7tbhdSHh/xO58N+LoFEg8uOx0DxL4f0gtFiNxeeJ5WCuGdlAP65aACgClqOnafrGn3+kavYWWq6TqtldadqemajawX2n6lp99BJbXthf2V1HLbXlleW0stvdWtxFJBcQSSRTRvG7KwB/DH/wAFB/2Qfjx/wQY/bB0X/gpD+wJa6jdfst+N9dbT/ix8Kma8v/D3hSPXr2K41z4ceKrZC0t58O/EkkYvPBOuzSjVfDWqWtram8j13RNF1/VQD+u/9iD9tT4Mft8fs+eEP2hPgnq6XWia/Atn4i8OXFxDLr3gTxdbQwvrPhHxDFEIyl7p8kySWt35MMOq6bPZ6nbxpFdCJAD+cv8A4OvPgRf2Xwy/ZW/bg8GW/wBk8WfAn4oweBtf1m1eUagui+IJf+Et8GyYQ4jtNF8QeH/EA89ShW58RQozMWi2gH9Nf7J/xy0v9pj9mf4E/H3SLizuIPix8LfBvjO8GniQWllruq6Lav4m0mIShXH9ieI11TR5AdwEti4R5EAdgD6DoA5Hx94B8F/FLwX4m+HXxE8M6R4y8D+MtHvNA8UeGNes477SdZ0m/jMVzZ3dtJwQQRJDNG0dxa3CRXVrNDcwxTIAfwoftT/spftW/wDBuj+1hF+2f+xtca78QP2I/Huux2HjbwLfTXl/pukabqN01xc/Dv4jW9rhUSMebN4J8cLHG4nhhlWSDVYLq0lAP6XLv9vX9nv/AIKC/wDBKz9rT41fAjxXZX0Un7JXx6tvG3gi8vbX/hMPhv4mu/g54x3+HfFOmo6zQmSeO4XRtWWFdO1+3hefT5DNDe2toAfCv/Bp5/yjD8U/9nW/FP8A9QL4Q0Af010AFABQAUAFABQAUAFABQAUAFABQAUAFAH/1v7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD+H7/AIIbzw/suf8ABeH/AIKPfsl3moX39l+KG+L1j4Qtpy6R3914H+Jdn4n0O9MMjHhvBD67PC6BiEl+XajPQB/cDQAUAfy1f8HJ/wDwTs1v4q/Cvwx/wUG/Z8sLzTv2if2VmsNU8WXXhq3MGueI/hfpN9/akPiBbmzliu5dY+Gupqt+k0cM1w3hi91Ka5vLey8NWcVAH3Z/wTx/ag+Cf/BbH/gnBNpHxs0XQ/Fuu6hoP/Cp/wBpnwJNHp8FxY+NrO0ja18Y6ZZW+X0VPEQht/F3hbVLaxsoNL8QW2qafpSyDw9I9AH86vw+8WftBf8ABsx+3g/w1+JB8S/E3/gnR+0TrTXOka9ZxtNDHpK3K28XifR4ZJRY6T8T/h6LyC28V+GZZraHxDpLxoJ4dP1bwz4itAD+6H4efELwT8WfA/hX4lfDfxNpPjLwJ420Sx8ReFfE+h3AutM1nR9RhWa1u7aTCSI2CYri1uI4byyuo5rO9t7e7gmgQA7KgAoAyte0LRvFGh6z4Z8RaZZa14f8RaVqOha7o2pQJdadq2javZzafqemX9rKGiubK/srie1uoJFZJoJZI3BViKAP4YPgb4g1v/g31/4LLeJ/gN4u1LU7P9hr9r3ULCTw7qup3N3PpOk+G/EGr3g8DeLrl4YLW2n134Wa5dah4U8U3aae002jS+JH0uzVNQsHoA/uzVgwDKQysAyspBDAjIII4II5BHBHIoA/mQ/4Lvf8EfNb/aHsk/bv/Y5hufCv7YnwZtrbxNrWneFnk0vV/ivovhRFvLXUdFuLF4Zv+FleGLe0STTQp+1eJtLtV0q0d9cs9JstVAPov/giL/wWA8O/8FD/AIUj4VfFm5tPC/7Yfwg0mDTviL4cudmnn4i6ZpgWxPxD8P2EnltFqDska+NdDt4zFpuqS/2ppyw6RqQ03RQD95aACgD+JP4ez/8ADAP/AAdOeNPC73l1pXw8/bNfVEVbaAWdnqt38dNDt/GGj6ZHbb44ZbPT/jNZ2Wnb4yVRrAyxReaqW6gH9tlABQAUAfi9/wAFoP8Agk74Q/4KYfAhbnwzb6Z4d/ai+FFhfal8GPHTC3sJ9WVC9/P8N/EWqsI/+JDrN4rTaLc30xt/DWv3El/C9lZaprzXYB+dH/BBX/grF4t1fVLz/gmL+3bc6r4K/al+DN7d+CPhlqvjtJtN1Pxtp/hl5NOuPhp4hk1EQ3EHjzwwtr5PhwXIZfEejxHR0lh1nS9NttfAP6uKACgCjqml6bremajous6fZato+r2N3peq6VqVrDfadqem39vJaX2n39lcxy293ZXlrLLbXVrcRSQ3EEkkUsbozKwB/Dt+yTqup/8ABDT/AILj+PP2S/FeoX2mfsi/tl32np4B1LVruWPRbOx8VanfXPwj8UTXd7LoelS33gvxLdal8NPFuuSQmw0+zn8X3VpC4itt4B/cvQB5D8e/gX8NP2l/g/4++Bfxg8PQ+J/h18SNAuvD3iPSpdizCGbZLaajp1w8costY0e/itdV0e/Ebmy1Oztbny5AjIwB/FL+x18aPix/wbx/8FEPEn7Ef7Seu6pq37Dnx58Swap4G+IN9Y3f9jaNa6vcvp3hX4r6RCDePYtaJGvh/wCKGi6XNfECyucR6xfeHNDLAH91un6hYatYWWq6Ve2mp6Zqdpbahp2o6fcw3lhqFheQpcWd7ZXls8lvdWl3byRz21zBJJDPC6SxOyMrMAUvEXh7RPFvh/XfCnibS7PW/DfibR9T8PeINF1CIT2Gr6JrVlPpuq6XfQN8s1nf2FzcWlzE3EkMroeGoA/iD/Ya1fXv+CIX/BbPx/8AsQeM9R1CH9lv9r3VtPg+G+qanNImlxL4mv7y4+Dnirzpm07TjfaPrN1e/DbxVqDZs7EXHiKdFmeytRQB+yv/AAXg/wCCS9t+318FofjN8GNOfTv2v/gRpsureAL/AEpRFffEXw1p0kup3fw/unjCzvrVtMZtT8EXcbPINUa50OSGRNZtrvSwDj/+CB3/AAVsuf20vhle/sxftG602nftj/AWy/sfVovEJe01z4r+D9Exp58UH7RtkvvGXhxoUsfGsMoTUtQR7XxMRqEsniK4sAD+i+gD+Ur/AIO5rTRpP2DfgZfXFnbyeIbP9p3QItF1BlBu7KyuvBHjB9Yt7dvvLDez2mjSXKrkM9lakg7FFAH9Cf7Dvnn9iv8AZAN1I810f2Xf2f8A7TK7Fnln/wCFT+E/Okdjks7ybmZickkk5zQB9RUAFAH8/f8AwWK/4Id+F/8AgoXfaf8AtCfBDxda/Bb9sPwXp9hFpXi/Zc2eg/EaDQjCND0/xdqOlD+09I13RrWEWugeLbWDUJo7W2sdD1C1WwhsdS0YA/Jn4F/8Fu/+CgX/AAS58f6f+zP/AMFh/gj498a+ELUi10D46WltFdeOm0aG++yN4htPE6yP4V+NGgRLa3ym8/tW116a9dzceK7hbE6ZQB4P/wAERfjlpn7TH/Bwh+1p8c/2WbHxBYfs4/EbRPjV4w8VxXenro5/4RLxBqNhfaNLrel28s1rp73vxJm8OXdppqT3C2snlRxSypBvUA/vioAKACgDjfiH8PPBHxZ8DeKvhp8SfDGk+M/AfjbRb3w94q8L65bC60vWdI1CIxXNrcR5R0YfLLbXVvJDeWN1FDe2Vxb3lvBOgB/Cv8UvBP7R3/Bsp+3K3xe+EVt4n+LX/BOz4/anHb654auy8kK6X9qkuZ/B+vXUMT2OjfE7wGs9xdeCvF0VvBFr2lPLJJamw1PxV4VtwD9O/wDgrL/wUp/YI/bq/wCCMP7T2qfBn45+B/EHiTVdG+GdxoXw48S3tt4c+KWjeKI/ib4L1Gezk8GajOmozXun6Hb64l5qehvqugiNLuK31i53xlwD9F/+CAc0k/8AwSC/YrklkaV/+EO+IEYdjuPlw/Gj4kwxLk9kijRF9FUDtQB+xFABQByPj3wF4L+KXgzxL8O/iJ4Z0fxl4H8Y6Rd6D4n8L6/ZxX+kazpN9H5dxaXltKCpBGJIZkKXFrcRxXVrLDcwxSoAfw4/8FGv+DZH9oz4Nv8AEj4sf8EqfiZ4k1Pwv4w03VoPFH7O1/4p/wCEc8Y2+h38Mlzqui+GtfkurDRPHugkQyx2+g6w1h4iZZrHTbS38T3qy3bgH7m/8G4P7In7Rf7GH/BOlfhp+0/4PufAfxJ8UfHDx78R4PC+ozWz6xp3hvWfDfgLw/p39tW1tLMdO1G4vvCuq3Bsbny7pLSS0mkiVJ0LAH730AFABQAUAFABQAUAFABQAUAFABQAUAFAH//X/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP4gP8Agtf4F+Iv/BL7/grN+zz/AMFcvhbpU+o/Df4j6xo2k/E+ws41tbOPxHpWhReEPHnhTUJ4vtH2eL4h/DstqFpqdzFDJc6rceITbRSPpBuGAP7O/hH8VfA/xy+GHgH4w/DXWrbxD4D+JPhXRfGPhbV7aWCVbnSdcsor2CO4FvNcR22o2Zkax1WwaVp9N1O2u9PudtzazIoB6JQBS1LTdO1nTr/R9YsLLVdJ1WyutN1TS9StYL7TtS06+gktb2wv7G6jltryyvLaWW3urW4ikguIJJIZo3jdlYA/g/uv+Ei/4N0P+CxSXSDWZP2CP2u5nDoFNzZaZ4N1zWA0kKbjco3ir4L+JpVePMltquseFQAX0+x8WFKAP7CP2yf2QvgP/wAFD/2avEHwU+KUGneIfBnjfSrbX/BHjjRGsdWu/C+uTWLz+F/H/g7U4pHtrkxRXazI1tdCy1zRru5sWnNrfecgB/JV/wAE9/2vPjn/AMEHv2vdX/4Js/t8T39z+y/4510aj8Ivixvvb3QPCaa9fyW2ifELwzdzRFr/AOHPiRojZeNdEjWLVfDGr211eC0h1rQ9e0DWgD+4+wv7DVbCy1TS7201LTNStLa/07UbC5hvLC/sLyFLi0vbK7t3kt7q0ureSOe2uYJJIZ4XSWJ2RlZgC3QAUAfit/wXW/4JzJ/wUH/Yy1218F6Sbr9oL4HHUfiT8Frm0ink1XV7i2tEfxV8P7ZYHLSnxjpljbNp0C2txcTeJNG0G1t3toru8dwDwT/g3R/4KMT/ALX37KL/AAB+KWrMf2i/2UYbHwP4htNSkjj1fxN8NbXGmeDvExikn+13d3oJt28H+IZVtEitfsvhu5u7ie/1yQqAf0SUAfxyf8Fqf+CZnxS/ZI+M2mf8Fe/+CcovfCPjfwH4ig8X/HP4feFbENFaz+YTqXxF0jR4YpLPU/CuvRPNY/EvwtdWs1mi3kupm0vtC1TV7fQAD9wv+CVv/BV/4Gf8FNfg9Y654b1HSfB3x68MaXap8Xfgpc3yxazoWpRiO2vPEfha1vJ3v9a8C396f9Fvo2u7nQZ7iDR9elFxLp19q4B+rNAH8Zf/AAdOeBtb+D/xl/4J8/t7eCpruz8R/D3xo/gPUNRt4/JTTbjwj4m074i+B5Vvo3Ewuru51TxhtTClU05GiZt0gQA/sA+G/jnSfif8O/AXxK0ASDQviH4L8LeOdFEpBlGk+LdDsdf00SFQFMgs9Qh3kAAtkgDpQB2lABQAUAfzKf8ABer/AIJAa7+0vplt+3R+yJb3Hh39sX4JWdp4g1Ww8NO2max8WPDvhGNbuxutKubTyppfiR4RtrON9EXd9s8RaRZxaHZyy6rp+h6bqAB79/wQ7/4K/wCif8FDvhO3wn+L89t4Y/bE+DulRaf8Q9AutlgfiPpGmsLAfETQrCRYpINSEixW/jbRIkMdjqrjWNOEOl6odL0MA/e2gAoA/nC/4OVf2EJP2mP2M1/aO+H2nMPjb+yFPc+PbG906Hbq2s/C6aS3fxtpfnWtk97cyeF5bex8a6abm/g0/SNO07xdNHE93qa7gD7g/wCCLX7cc/7fP7Anwm+LHiK7luvif4Pjm+EvxdluJGkub/xz4Js9PiPiKaWa8vLq5m8V+H73Q/EOpXk/kLLr9/rUFtAttaxO4B+rtAH5if8ABWD/AIJt+Af+CmH7MGtfCnWntdD+KfhBdT8VfA3x5LCrv4Z8bmyVTpd/KMTr4X8XC0stL8QrAzPatBpmuLbX82iQafdAH4q/8EAP+ClPj7wP4v1v/gkn+3BPd+Efjf8ABfUdW8KfBDUfGIez1PWbPQbqdb74Talfz/LeappiLPf+A7i6lP2/SEm8N2V5N5HhTSpQD+uWgD+Tn/g60/Ziv/EX7OvwQ/bV8C2Bi8c/sz/EOx0PxLrFhbzHUU8B+MLyK70W/vLyMlbfTvC/jbTraG1LKpF540l2u28ooB/QD/wT9/adsP2yP2M/2eP2jrS5judQ+I/w30S88WCJLaAW3j3SI38P+PLYWtoxhtIU8W6VrD2MAWL/AIl0lnKIYklRFAP5qf8Agu5/wTz+KP7Lnxo0L/gsf+wbJe+GPiF4F8Vaf4o+P3hrw7Z78aj9oRZPiemnxxyWmq6J4kMj6T8S9Ivrdobu5vxq10mpWut6yumgH9BP/BMX/goh8Mv+Ck/7MXhr43+CpdO0jxppwt/Dnxh+HcF2ZL7wF48htle6t/stw7agPDeuqsup+FdSufOju7L7Tp5vLnVNF1dLcA/C/wD4O9NS8n9kj9l3RyxC6n+0Ld3JUYOTp3hSWNW28E7RqLYweCc4PFAH9LX7Idp9g/ZO/Zfscbfsf7O/wUtNuMbfs/w28Mw4xxjGzGMfligD6IoAKACgDxn47/s8fBD9p74e6p8Kvj/8MfCXxW8A6wrC68P+LNNW8jgmaNovt2k38T2+raDqscTyRw6xod/p2qQRySJBdorurAHzJ+wr/wAEw/2OP+Ccun+O7T9lv4aP4Uv/AIj6hb3fivxJrer3XiTxLe2Ni0smm+HrfVb4I1j4f06e4uLmKwtYYnu7mWOfVbnUJLLTjZAH6A0AFABQAUAcF8Tfhb8OPjP4I174b/FjwR4Z+IngPxNZyWOueFPFukWetaNfwOrKrvaXkUiw3duzedY39uYb/T7pY7uxuba6iinUA/lS/bq/4NK/2ZvjdJqPiL9kL4o67+zdrOo6gt7deAPEi3/jX4aOr3O9rbRr/wC0L4u8O21tC0jww303jDzpVSISWcbGRQD+iT/gn7+yZa/sL/scfAj9lG08VS+N1+D3ha/0i68VS2Z08axqmu+Jdc8X6zPBZtNO8NlFq3iG+trASyec9jBbyTJFKzxIAfY1ABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFAH/9D+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA+Pv29P2PPAX7d/7KvxY/Zp8fw20dv440CeTwnr80MUk/g7x/pcct14O8V2kj21zNB/ZurCKHVRZIl5feHrzWdJimiXUXdQD+aP/AINuP2xPHfwK+KHxo/4I9/tRXN9onxE+EnijxjqHwWs9fuJpJYrjQr25l+IXw706RvtEbW0kcdx478PpFNBp5ji8UTwy3VzqtmjgH9jtABQB+W//AAV7/wCCeOgf8FIP2OvGnwjhs7CL4u+FFn8dfAvxHcLYQT6b4+0y0lC6BLqd4qGy0PxtZb9B1VWvLOwivG0bW78yjQbdVAPyp/4Nrv8AgohrvxL+GXir/gnf+0PqF3p37RH7LL6lp/g238S3Ig1rxF8MNJvl0268MfZruKO9m1n4aaoZLTyZbi4un8L31jBbWVtp/hS6mYA/Wn/gqb/wTN+FP/BTT9nnUPhl4s+xeG/ij4Xjv9a+DHxPa2Ml14O8VSQLiy1J4I5Ly58Ja/JBbW2v2cAkmg8q11eyguL3TorW4APwO/4Iz/8ABTH4p/sU/GjUv+CRH/BSk33gnxP4I18eEvgR8SPFl6n2HTZJnJ0rwJq+uTym01LwR4kgktdQ+G3jG3updPtUvYbJp7rw1qmm33h4A/smoAKACgD+Gj/gpd8OfF//AARG/wCCsHwn/wCCkPwN0W5j/Zs/aI8TX0fxW8G6ItrY6PDqWqzwD4teA3t4kntNOtfEFhdp4y8G3VxpiWml6tK0ei2s8vhNmQA/tf8Ahn8R/Bvxg+Hvgr4p/DvW7TxJ4G+IPhrR/FvhXW7J1eDUNF1yyivrKUgMxguEimEV5aS7bixvI57O6jiuYZI1AOwvLO01C0urC/tbe+sb63ns72yvIIrm0vLS5iaG5tbq2mV4bi3uIXeKeCVHilido5FZGYUAfyI/8FLP+CB/xD+FvxHm/bt/4JE6trHwo+MnhO5n8Y698DfCOqz6SNQvrRXutR1D4RSecqpc6jCs7Xfw3u3ltdRkkn0/wqrJd6b4UQA+nv8Agk1/wcAeBv2p9Usv2Yv207TSv2fP2u9C2+HvtevIvhTwT8U9a04fY7mGGPU2t4PBnj28mglkn8M3Jt9H1XUhPb+Gzp9zc6b4YUAxv+Dse3gl/wCCZPgyaSJGmt/2svhUYZSqmSLzPAvxbEgRypZVkCrvUFQ+xCclFKgH7h/sGkn9hv8AYyJJJP7KP7OxJJyST8IfB5JJPJJPUn+tAH1dQAUAFABQB/Gp/wAFs/8Agmr8Uv2O/jTov/BX3/gnMl54P8X+BvEkfi747eBPCtgjW+n3ZmBvfiFpujQxSWmpeEfEqS3OnfErwxeWc9hEbyW+e3vdD1rVLXQwD9+/+CWX/BSz4W/8FNP2ctN+K3hH7D4e+JfhgWGgfGr4ZxXTSXPgvxhJbM/2mxiuJHvpPCXiI293eeGr268yQJBfaTcXV1faTd3E4B+mFAHM+NPCOh+P/B3izwH4ntft3hrxt4Z17wj4hsgQv2zQ/Eml3Wjata7mV1X7RYXtxFko4G/JVulAH8df/BsB4k1z4C/tQf8ABRn9gXxhDLY634G8VHxdZ6fdSmFtPv8A4b+MdT+HXiq2gs2O0y3h8RaBLO8fzeXpcZwUDFQD+zqgAoA/mB/4ODv+CWHiD43+GrD/AIKDfsrRX2gftX/s72mneIPEkXhgTWet/ELwT4QZLy01rT7iw23Enjb4fQW0dzaSv++1LwtZPYxXUNxoWkWV+Afbf/BFL/gql4f/AOCk/wCzpHF4vvdM0n9p/wCD9tYeHvjX4SQR6fda00UaWdj8S9I0g+Xt0nxFNG0WtwWEX2TQvEnm2xt9N0/VNAt7gA/Rn9rn9n3Qv2rP2ZPjn+zr4ijszYfFz4b+JfCNtcX/AJ32PTNfu7F5/CuuT/Z1kmK+H/FFto+thUilLtp6qYZlZo2AP5i/+DUz9oHWtD8L/tW/8E+fiD9osfGXwD8f3vxC8N6ReROt9baPqOoQeCviBY3Blffbw6L4l0/ws9vaCLAn1+/kLhywYA/ru1fSNK8QaVqeg69pmn63oet6fe6RrOjavZW2paVq+lalbS2eo6ZqenXkU1pf6ff2c01re2V1DLbXVtLLBPE8TujAH8I/7Snwq+L/APwbhf8ABRHQP2rPgTpeq+K/2C/2hddn0rxZ4Iiu7ttP0vS9QvVu/EPw21e4eWX7B4j8NB0174beIrxLmK8gt4Gma/az8UaPQB7J/wAHRnxz+HX7Rn7HP/BOX4sfCLxJb+J/h18W/iN4y8WeF9Ztiv7+zTRfCFpNa3kKSSGz1TSb66utI1mykZ5dM1WzvbCf97C60Af2C/s+aedJ+AfwQ0pgA2mfCD4a6eQOgNl4M0W2IHsDHxQB6/QAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQB//0f7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA/jm/4ORf2O/iB8EPij8Fv+CwX7Ldre6N8RfhD4m8IWHxqvNBtppZYZtAvLWL4e/EXU41W5iezMcNt4E8QPNBBYCGHwrBItxcaldOwB/SL/AME/P22vhn/wUB/Zc+HP7Rnw2vbZX8Q6Xbad498LxyKbvwJ8RtPs7X/hK/Cl5F59xKsFpezi80O5nk83U/Dt7pOpyJBLdyW0AB9qUAFAH8UP/Bez9m3x7/wTs/bL+Cn/AAWT/ZThfQv7R8e6Rpfxv0vTf9H09vHb210k17qVnYpatP4e+LPhS31XSfFcEpuVvtXg1qbUrjf4ks4FAP67/wBmv49eDP2ovgH8JP2hPh9MsvhL4t+B9D8ZaZCJ/tMmmS6lar/augXVx5Fss1/4c1mLUNA1GWOCOJ7/AE25aEGIozAH5Wf8Fov+CP8A4Q/4KT/CmHxh4CGm+Dv2tPhbps9x8LfG5aPTovFdras96vw/8VaivlNHaXNx5j+HNYuJfK0LVLiQXJTTL68ntQD8/wD/AIIdf8FfvHFx4uf/AIJk/wDBQuTU/An7UHwz1GTwH8M/FPj0TaZqHjiXRpDp8Xw18VXGp+VIPHVl5K2/hS+uWY+LLYRaKZG19dLj18A/rCoAKAPjH/goD+xn4E/b3/ZS+Kf7NnjqK1hbxbo8t94J8Q3CZbwd8RtIguJ/B/iiKRba7nhhs9QkNlrH2SBry68O6jrOn2zxS3iyoAfzof8ABt5+2Z49+FHjf4uf8EhP2n5L/Rvij8EvEfjO++DVpr0t018LTQr27n+IXw5tPNlmi+zac9vfeO9Bjtkgtzay+LbiWeZptPiUA/r9oAKAPw4/4Km/8ELv2cP+CjUcnxI0G5j+Av7T2nW076d8X/CulI1p4vuIYvM02y+JGjWc1k2qvFdRQwweLbKVPEen2krrdp4htbLTNLtQD+PT/gph+zP/AMF6Pg98JvAP7GHx6tfiP+0x+zjofxG0fU/hx408J2+o/FXw1d6hp1rfeGfClrF4sh0+TxXoiwaV/aKaD4O8ZrpN7penXFzJp+j2dtcu84B/ohfsb+FPEngP9kP9lbwP4y0m50Hxf4M/Zv8Agd4U8VaHeIY7vRvEnh34Y+GNI1zSbqNsNHc6dqdndWc6MMrLCynBFAH0hQAUAFABQBS1HTtP1jT7/SNXsLLVdJ1WyutO1PTNRtYL7T9S0++gktr2wv7K6jltryyvLaWW3urW4ikguIJJIpo3jdlYA/hw/bp/Y8/aQ/4IQftjL/wUR/4J+6DrOufsneNNQMfxY+FVub/VNA8J2Gs38M/iD4f+Mba3LXEngDU73yr3wR4iut194fv49PtJdUbxBo+n61qgB/UN/wAE8P8Agp/+y/8A8FJvhxH4t+CXiUaZ460fTLW7+Ifwb8SXNpD498C3bmG2vJJbSJwuv+GU1GZLWx8VaXGLO4S408anaaJqd8ukQAH6LUAfxPeOU/4Ya/4Op/Buuw2V7p3gb9sNNJiAmk8i31jUfjZ4Tm8J6jeCfYkU9tb/ABgtbi+ELK+JbJITIZEZ1AP7YaACgAoA/h//AOCqf7I3xm/4I3ftp+Fv+CsP7Cuhbvgt4n8WTTfGz4YaTYXC+GfC+pa7cBvE3hvWtI00W8MHw1+IkE922jTWBtT4Z1sTafaf2LLaeGL27AP6jv8Agn//AMFF/wBm7/go38HNN+KfwJ8VWp1m2tLVPiF8LdVvLdPHfw112RFFxpuuaaPKlvNKkn3jRfFNhbnR9agG1Xs9Vt9T0jTQD+Wj9rDy/wDglX/wci/Cf9opCui/BL9se5tNV8aXU8tzJptvH8WJJ/AvxPv9RNssBa40bxxFcePYLMmYQLLpMkyTKy7wD+4mgD59/ak/Zk+Ev7YPwL8f/s+fGvw7b+IfA3j7RrjT5iyKNT8P6usUh0bxX4duyC+n+IfD180eoabdJmKRo5LDUIbzSr2/sroA/wApH/go1+z1+1F/wT2/aY8L/sMfF261/wARfDLwz46Tx38IrmP7ZN4Z8T+F/FeqxWlp4u8IxSNNBH/a8GlRWOsWluy3NjrGl3mk3yw3+n3ESgH+uX4M046P4P8ACmkmFrc6X4a0LTjA6GN4DY6Xa2xhdGwyNGYtjIRlSuDjFAHS0AFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAf//S/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgDj/iD8P/AAX8VvBHiv4bfEXw5pni/wAC+ONC1Hw14r8M6xCZ9O1nRNVt3tb2yuFVo5Y98Tlobm2lhvLO4WK7s54LqGCVQD+E3xFb/HL/AINlf+Ch39teH08WePP+Ccn7S+uKLiwnf7ZE/h2G8Z5LG5kWO00q3+K/wsGqStp2oKmnjXNIuvMkjsdL8SXtlAAf3TfC/wCJ/gH40/Dzwd8V/hb4o0vxp8PfH+gWHibwl4n0afz9P1bR9ShEsEyblSe2uYW32uoafeQ2+oaXqEF1puo2trf2txbwAHeUAfH37fn7Luk/tm/sdftAfs36nbQT3fxI+Hur2nhSWdraJbDx5pKLrvgS/wDtNzHIlpFB4r0zSRfTr5TnTXvYRNEszuoB/Pt/war/ALUuq6z8EPjt+wj8RLua28f/ALMHjrUNf8LaNqUl2dSi8C+JtTl0/wATaZBBKphtLDwr45txPJEDFI1542YiN1VzAAf1lUAfz8/8Fuf+CNHh/wDb78Dt8e/gNa2vgn9tj4XafBf+FfEWmyRaQnxb0rQo/Ns/Bnie9QwrD4msI4kj8D+LZn8228uLwxrUr6JLpF94UAPDP+CIH/BaHXPj3d/8MIftzTXngj9sz4YyT+EtD1/xkr6TqHxeXw+xsZtE8RLqPkTx/FfSxbtFcSTosvjSKFrmcf8ACUJcS+IQD+nmgAoA/jo/4OPP2R/HH7PfxW+Cv/BYb9l+Kfw94/8AhR4q8H6Z8abjRbd2Y3mj30C+AviBqcTNPBNa3cMMPgTxGssEGnyRxeGYp4rq51a9kYA/pv8A2JP2pfCf7af7K/wW/aY8HpFa6f8AFHwbZatqmkxyLJ/wj/iuykm0jxf4fz500vlaR4lsNTtLKW4Zbi701LK+kii+1KigH1RQAUAFABQAUAFABQAUAFAGfq2k6Vr+lanoWu6Zp+taJrWn3mk6xo+rWdtqOlatpWo20tnqGmanp95FNaX+n39pNNa3lndQy211bSywTxPE7owB/If/AMFDv+CBXxP+BvxHb9uL/gj54j1/4X/FbwrcXfizXPgV4f1mSxkuriATXGpTfCi7kkjF1DqFgZbe5+G+ry3TagxnstAuNQGo2Xhm3APp7/glb/wcF+Bf2j9Zs/2Yv25NPsP2c/2s9DkTw1/aevW0nhfwL8S9f090sbi2lj1AJF4E8bXjq1xcaPfNa+G9RvlvYdFuNHuJdK8NTgHx5/wdZfDvU/h74r/YK/bd8KRXltrnw2+IN34C1jWoEIisP7L1jTPiD4Ej89Ruine9HjaVAzDcIcxAsrigD+uj4V/EDSviz8MPhv8AFTQY2i0P4l+AvB/xA0aJ5FmePSvGXh7TvEenRvMqRrKyWmpQq0ioiuQWCKCFoA72gAoAwfFPhXwz448O614Q8Z+H9F8V+FPEenXOkeIPDfiLTLPWdD1rS7yMxXWn6ppeoRT2V9Z3EZKS29zDJE46rwKAP4t/2+v+CLX7Sv8AwTd+Leof8FDv+CPfiPxPpWleFL2bxR42+AukXN1q+ueGNGaVrrW4dB0m8+2xfEn4aeSHh1jwtrSaprOnaO++8t9e0y11XV7IA/O7/gqX/wAFVPgB/wAFUf2A/ht4t8cafD8Df26/2WPiZaXOpeCJBcR+HPFnhXxVYCy8Xal4J1K8WXUYZIde0Hwzer4Z1i6a60iNLz7NqGsLdPcWgB/an/wSM/aytP20v+Cev7NnxtOqR6p4nl8DWPgf4hSefNc3cfj3wAi+GNcn1Oafc7ahrsdjY+KZTvkUx6/CQ5JKoAfpJQB4d8WP2Zv2evjvrngfxL8Zvgv8Nvid4h+GuqprXgTWvGvhLR9f1PwvqCSLNv0u8v7WWeK2knSK4n053k064uIILi4tZJoIXQA9xoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP/9P+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD5b/bM/ZD+EX7cn7PPjz9nP40aRFqHhbxjZebpmrLbQz6v4M8WWUVwPD/jTw7JLtNtrWh3E8hUxywC/0641HRruX+z9TvY5QD+QD/gnH+1p8av+CE/7Z3iP/gmb+3bd3Un7M/jvxEl98I/il5s9x4a8KTeILtIPD3xD8P3k6sLj4feLI1Wy8aaSBBqPhnWIJLyeC11bQdd0PVAD+5y1ura9tre8s7iC7s7uCK6tLu1ljntrm2njWWC4t54maKaCaJ1kiljZo5I2V0YqwLAE9AH8On7Ssw/4JF/8HGfw/wDjvbA+H/2ev20Zre/8YOZnXSIrP4rXb+GPiRLfx2EdtGD4Y+JFu/j3T9LKOLa0j0Iy/achpQD+4ugAoA/mj/4Lj/8ABGC7/aptU/bL/Y+tX8HftmfC9LfxBeWXhljpF78X7Lw8BdWclnPZeRIPiXo628f9i3IkWbxDBDFpbStqcOmicA6v/gh5/wAFnLb9tbQj+yv+01Mvgz9tX4XWE+m31nrSLpc3xf0vw9FLHqGtWNpP5TR+ONGt7SWbxdoixiW7ghuPEVhE1smrw6WAf0Y0AfNH7ZfwE0/9qP8AZS/aD/Z91Cytr8/FX4U+MPC2kQ3kogtofFM+kz3Pg3UJZmKpENK8WWui6osjkRq9mpk+QGgD+bf/AINNPjpe3PwB/aZ/ZC8U3MUHiX4C/FweLtI0y5eb+1P7E8bwS6Hr8CQyMUjsND8QeELZpERUaK98RuXB85doB/W/QAUAFABQAUAFABQAUAFABQAUAfiZ/wAFUf8AgiJ+zp/wUf0a78c6Ulj8Ff2otJsJG8NfGTw5piQw+JL20R5tL0z4nadp/wBnm121M4SzTxRAX8T6PZtFg65YabY6DQB/MT+0l/wSR/4ORPjT8DvDv7IHjvxDpfxp+Anw78Wadq/gx9W+NfwlljUaDYXGgaDcQ6p4o8QaV45m0nSNDuJ7TQ9I1iNU0ayuZ7awsbPzpkYA/tc/4J4fAr4j/sy/sSfs1fAT4u65ZeIfiP8AC34Y6P4X8U6jp13JfWKXlvNd3FtpNreyKn2uHw/p9zZ6AtzGDbTf2b5loz2zROwB9m0AFABQAUAfzpf8FJv+Da39i7/goJ8QP+FtaHresfsy/EfVrgT+PL/4b+GdI1Xwt46uPMMkmsaj4QuL3RLXTPE10WA1HWNJvra21N0F9qGlXOrz3+pXYB+sX7A/7Dvwe/4J3/szeCf2Yfgm2uXvhbwrLqGr6r4h8S3aXWveLfFmtvFJrniPUVt44NPsTdG3tbSy0zTbaG1sNMsbG3ke+vlvNTvQD7LoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//1P7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD8pf8Agrh/wS/+HP8AwUz/AGctU8F3sNj4e+Ongaz1HXPgZ8SDFGlxo3iUQ+d/wi+t3AUTS+EPFMkENlqO1/O0a8Nrr1olx9jvNM1QA/Gf/ggl/wAFQPiX4C8e3/8AwSV/b4nvvCXxu+E+o3vgn4H+IPHE32LUtVj0SWWAfCHVtQv3UXt9bQxqfhpcSyySalZeX4RsLi4x4T01QD+vKgD+bv8A4Oef2Rn+Pf7AI+Ofhmwef4ifsl+LLX4g2E9uR9pfwH4juNP0HxtawxpBJNPJa3kfhbX93mxx2lho2qTFWLkqAffn/BGf9ryD9tT/AIJ2/s/fFa6vEu/Gfhzw5F8JfiVhp5Jk8c/De1stEurq9muJJZJtQ8QaCfD/AIrvpNwT7Tr8qRpEqCNAD9SKACgD+Wz/AILd/wDBG/xV8QvEEX/BRn/gn+NR+H/7Ynwrvrfx14x8OeBpZ9G1D4pN4ekTUk8beEjphgltPivpDW4ub+1tiv8AwndvH9rh2eMrZ18YAH1X/wAEV/8Agsx4T/4KKeBG+EnxefTvA37Znwz002vjrwnLFDo9p8S7LSkEF5468IaaRDHa6khjMvjHwnaxImlXLSavokK6BNPY+HgD96KAP4iP2cZB/wAE6/8Ag5/+MPwgaa30T4Yfthf8JBBpNvZ2zR6Wf+FvadZfFHwRpGnwzxxiNtO+IFvY+FWmtiBC8d5BE80DMJQD+3egAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP//V/v4oA+UvjBZ/DTUPjT8PbX4rz+GYvDA+GfxGntR4t1S10rSm1uPxP8N47QxTXt3ZwPfixm1LyYxI0ptzdMqFFdlAOO8VeHv2XILbSj4NX9nW+u5Nf0qLWU17x7pVpDB4aeVhrN3YNaa07S6tBCEawt5QtvLISJmAAoAv+IvDn7JcWhavL4Xk/Z/u/Ecen3T6Ha6x450a30q41NYXNnDqE9trbXENpJOEWeSFWkWMkoMgbgC1pfhr9j59M059Zn+A8GrvYWbarBYeN9ElsYdSa3jN9FZyy64sslrHdGVLeSVVkeEIzgMWFAHo37Na6Snw71OLw/JaS+H4Pib8WoNAfT7hLrTzokHxH8SxaSNPuI5Jo5rAaeluLOSKWSJ7bymjdkIagD3+gDzb4m/F34ffCDRo9b8feILfR7e5kMNhaKkl3quqTLs3x6bpdqst5diHehuZkiW2tVdGuriBHRmAPnrw/wDt5/s/a7q0WlT6j4l8OpPOLeHVvEOhLb6Q7Nwkslzp99qU1pbs/wC7M97bW0UJ+e4eCFXmoA+x7W6tr22t7yzuILuzu4Irq1uraVJ7a5tp41lguLeeJmimgmidZIpY2ZJI2V0YqQWAPk3xz+258BvA2tXegyavrPim/sJ/s183hDTIdTsbadW2zRjU7u/06wuzbYPntYT3aIwaIFpkeNQD1X4TfHv4YfGq3u38Ca+Lu/09BLqGh6hBJpuuWUDMiLdPp85Jnsy8kcRvbKS6tEmdIJJ0nZY6APZKAPHvGHxv8F+CviP4E+Fmprq114q+IBc6XHptvZT2emw+c0EFzrctxqFrcW0F7LDdJaG0tb6RzZ3JeOIKjSgHsNAHzT8S/wBrL4PfCfxZeeCvF2oa3HrthbWN1dRadolxfwRR6hbJd2ytPHKi+a1vJHKyYyqyJ6mgDhYv29/2e5pY4k1LxTvlkSNM+GLsDc7BVyfP4GSM0AfaFABQAUAePeMPjf4L8FfEfwJ8LNTXVrrxV8QC50uPTbeyns9Nh85oILnW5bjULW4toL2WG6S0Npa30jmzuS8cQVGlAPw3/wCC5n/BG26/bn0HR/2mf2ZBZ+D/ANs/4R2lvcaVfafLFoU/xY0XQ2W60vRb/Vomt1i8Z6AYfL8Ja5eyK8tn5fh+9vFs7TRf7PAIv+CTv/BUb9ojxT8C7nwP/wAFE/g58QfAPxU+GN/c+DrD4m32lWlvf/E2Hw60mm3jeLPCF5fWXifSvFthd2wtJPEY0c6F4t3S3c81jqNjeXmsgH7FaL8V/gH+1v4E8d/Cw6oLzS/HnhTxL4G8U+Cdda30XxHqnhnxRoF7pWvLYWLXM731s2k3l5FPeaa12li3zXJizHvAP5R/+DeLxx4j/Yh/4KJ/tt/8EqfiPfM0c/iLxL4j+HryC5MN54m+GV1efaJ9IjkdEhsPFvw5u5vEcty0DSXMPh7SVVkUkOAf2uUAFAHjw+N/gt/jH/wo6BdWufGMehHXru5gt7JtDsYhbm8+wXl22oLfJqRs2t7sQx6bJAIbu2LXKu8iIAfzJf8ABYj/AIIu/FKw+Kkf/BST/gmP/aPgn9pDwdqo8b+Pvht4GLabqXi3VLBzeXfjP4f2lmEjuPE16qSHxF4UjhZvEbvLc6bDdajd3GnXYB+lP/BOX/gqVqfxz/Zt8BeKP2xPBsvwX+LlxoVrNqWpabY3Os+F/G9vtiitdfg0Xw/b6jrvg3xBfgTTa14Y1DSU0mzmi+2aTqwg1GPQtGAPxf8A+C+/hiL4kfHv9lr/AIKHfsm+MtO1HxZ+yjol/wCIPifczWut+D7/AEnw78LfEsHxH8K63pU2taTpk2uaoHvfFFu1ta/vreGw0+PzmSdEiAP7D/hp480X4qfDj4f/ABP8NM7eHfiP4J8KePNAaXaZG0XxfoVh4h0ppCuVLmx1GAvtO3cTjIxQB21AHHeP/HOg/DXwdr/jnxNLNHovh2xa9u1tUikvLli6QW1lYxTz20Mt9fXU0NpaRS3EET3E0YkmiQvIgAvgDxtpPxH8G+H/ABxocGoW2keJLBdQsINUit4NQigaWWILdQ2t1eW8cu6JiViup12lTvJJFADvHHjvwn8N/Dl74s8a61baFoNgY0mvLhZZXknmO2C0tLS2jmu728nYHyrW0gmndVdxH5cbugB8paf/AMFAP2f77U49Pmk8ZaXbSTJENa1Dw5GdLUM+0yuLDU77U1iQEPITpm9UOdpIZaAPszStV03XNNsNZ0e+tdT0rVLSC/07ULKZLi0vbO6jWa3ubeaNmSSKWJ1dHViCG7dKAL9AHj/xY+O/wy+C1paT+O9fWyvNRBfTtEsYJNR1y/iVij3EOn253xWiOrIb67a3szKpgWczYjYA8i8E/tw/ATxrrNroY1fWvC15fT/ZrObxfpcOmadPMxxEr6naX+o2Nks/Bik1CeziJKo8iSOkbAH17QB8jeOf23PgN4G1q70GTV9Z8U39hP8AZr5vCGmQ6nY206ttmjGp3d/p1hdm2wfPawnu0Rg0QLTI8agHqvwm+Pfww+NVvdv4E18Xd/p6CXUND1CCTTdcsoGZEW6fT5yTPZl5I4je2Ul1aJM6QSTpOyx0AeyUAVL/AFCx0qxu9T1O8tdO06wt5ry+v76eK1s7O0t0Ms9zdXMzxwwQQxq0kssrqiIpZmABNAHxrrP7fX7P2lapJp1vd+LNdiimkhk1bRvD6vpYMblDKj6lf6beTQOQfLlgs5VlXDplGU0AfSnw3+KXgX4taD/wkfgPXrfW9OSUW14ipNbX2m3hjWU2epWF1HDdWk4Rgy+ZF5U6gy20s0OHoA9BoA8e8HfG/wAF+OviN46+Gfh9dWn1r4eiMa5qMtvZLoc8xmS1uLbTbqLUJ7yeexvGks7sXFhaRpcW86xSTIqO4B7DQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAf/W/v4oA8G8Sadp+qftCeBLfUrCy1GBPg/8TZlgvrWC7hWVfGPwrRZViuI5UEipI6BwoYK7AHDMKALHxT8V/Cf4RaENa8TeG9JlkuBIul6Xp/hm0ur3VJop7K3lihdLH7LarC1/bvNcX9xbW8cbFt7EbaAI/GWveGPDHiXTPCOj/B+48c67qGh3viKW08NaZ4FtF07SbPULPTBcXtx4n1fw9b7rq9vBFaw20lxNJ5Fw5jCRO1AHoml+G/DV/pun3134F0jRrq8s7a5uNJ1DSNAlvtMnnhSSWwvJNOF9YSXNo7GCd7K8urVpEYwXE0Wx6AOA+AMMVv4R8UQQRRwQQfGD41QwwwoscUMUfxS8VJHFFGgVI440UIiIqqqqFUAACgD3CgD4D1L4ReJvil+2TqfiT4i+FtRvPhV4G8N2p8FyarZh/DeqajaWejbbLYzCK8zr2sa5rLJIhS5/suG2ufPtYRFQB77+0h4F8EeJvgp4/j8TaVpgg8PeDte1nRdQa0t0udB1LR9Lur3TbjS5lVJbZhdW8ML21tJGt9byS6fIjw3MkbAHxp8P/iV4m8L/APBPrxFrZvLqPUrGfVvBnhe+NzL9rj03WfEFlpCy2lwcywNpEWrarHp/lt/oqabBHAY1ijVAD6U/Y3+FfhzwR8E/COtxaVZN4k8b6TF4l1zWJLaF765t9X8y40uwFwyNNHYWmkzWsS2iyeQ073dyYxJdSigD57+O2h6d8DP2rfgf8SPCFrBoNh8RNUOi+KdP05Us7C8b+1dM0jxBdvawhYA13pniOwu5Y/L8qTU7BNQZReO8zAH2b8avDPxq8Tadott8GviBo3gG6inv/wDhILnVtLttRbULSaGBbKKze40XWHtJLeVbh3lt1tZT5iYlbaAoB+Z1n8KvH/wv/a9+DS/Ejxjb+OPEnirWLPXptbiudSu5XiSS/wBNjguZtUgt5t0QswsEcS+RDbCKGIIiBFAP2eoA/Mz4YeLfh5/w0V+1B8VfiVrPhvS9E0zW7D4f6Pd+JHtGgvZbG4n06WPS7e4Ekl5PDaeDbGUpZwyzLb3CSYMbu1AH2R4G8d/AT4kXD2vgbVvAHiG/hhNy+nWdppsWqpbqwVrg6ZdWltqAhjYqHmFvsjZkDld6bgD2ugAoA8S+NXhn41eJtO0W2+DXxA0bwDdRT3//AAkFzq2l22otqFpNDAtlFZvcaLrD2klvKtw7y262sp8xMSttAUA/M6z+FXj/AOF/7XvwaX4keMbfxx4k8VaxZ69NrcVzqV3K8SSX+mxwXM2qQW826IWYWCOJfIhthFDEERAigH60/Em78R2Hw98b3fg+2ubzxZB4U19/DNtZwi4uZdf/ALMul0cQwNlZnXUDbv5bfK4XacA5oA/n4/bT/wCCMupft6/shaFdL4z8d/Cf9rbw9rfjPxvpeoa/4p11/Dvi7XLvWrpbPSfH2jG5eCC5vrbTrDUNK8VpDdatp1/qmoXV/JqFnqRg00A/Oj/gmr+0p+114X+Kdz8Ef+CgXhi+8CfEL9ijxt8M9P8AEXxj8Vw3Gn+JNf8AhhqtzcsLPxhr25rPxlo/hfwl4XvLrQPGEH2u6v8Awbrkdpe6tqmjQ+HEsADjP+ChV7bfs3f8HOP7Dv7Qmka9ptp4V/aKsvgPeXniHTL+CXTbrwz4oF58C9YvHu7B3iubC+0HSZLtpVZ47uwukuVaaCYPQB/cNQB83/HDwR+0J4vv9Pj+EPxT0XwBoH9kvbazZ3unRS6jd6k11MwvLTVE0LUr+yRbRoIQLS9tGEiM4UsxegD4V/Z3+HfiX4XftoT+FPF+vQeJvESeFdc1bU9cgnvrkahPrekW+pNPNcalHFez3LG4/wBIlnUtJNubc2d1AH67ySRwxyTSuscUSNJJI5wqRopZ3YnACqoLEk8AZ4oA/kk/4KJ/sE3n7cXwp8WftO/BD9oS8+Bnx++Hnxl+L/iz4TeGPEvjiw8EaT8SPC2rPoHipdFi8jUoLqy8bC+ge20y8W71DSJ76EaVMbOMyamoB+OH7dn/AAVm+Mvjv/gmL4r/AGLf+CkfwW+KWgftK6LrHhe8+CXx40uyWHw78UP+EWv/AOztUHjTVoJDpeq60nhTUdYsNT1Tw/f3SeIo7/TL7XbCLVIbzUteAP6oP+Dcn45678df+CTf7PV94jsdQttR+G9z4u+E0V9qDStJrWk+FdYN9oV/AZQD9ltNF1yw0GIKWRTorhduGjUA/brW4dUuNG1e30O7hsNan0y/h0e/uIlmt7LVJbWVNPu54XimSWG2u2hmkiaGVZERlaJwdjAH5O/tJfAr9pWfwBrfjf4q/GXRPGOh+EYrfU20Cwhu9Ltnmlu7fTY57fSdP0LSdGN5F9uIFzPF5yQtMiSjzGVwD7z/AGVP+TePhP8A9ivH/wClt5QB8w/Em1s/j1+2Hp/w18RzySfDb4M+Fh4p8S6VPdfZ9Kv9QFvYajcXF55hEP2eSXXfD+l35cB/7PsdSiilhSeWSgD6S8T+JP2bvih4c1P4Tz+OfhZewX1lLo+n6Vaa74ad9NvZI3tbCfw/bx3cUa6jptx5ctmmmP50UsSICinDAHU/Ab4UXnwV+Htp4AuvFknjCDTtS1K703UJdJOjG0sdQlS6OmraHVdYDJDeveXKzLcxqRd+WLdPKLygHAfs/wD7T2lfHzW/Fui6f4S1Dw4/hS2tLiae81S2v0vBd3d1aBY44LS3aEobYuS7vkOAMbSWAPmn4OaHp/x0/a9+NXjvxhaw69pfwyv5ND8MadqCreadbT2erXmh6Bdpazq8BSG00PVdUigMRRNWv/7QBF1EHcA+jv2vvhX4c8d/BPxnqk+k2S+IfBWg3nijQtYjtoY7+yj0CH7fqNklwqiVrG90q1uraWzZmgL/AGecIJrWB0APE7T4066v7Ac3jP8AtC4/4Sq20F/AC6j9ok+3C4bxInhCG/W7/wBeNQi8Pzxaj9oLNP8Aa0Mvm+YQ9AHsX7G/wr8OeCPgn4R1uLSrJvEnjfSYvEuuaxJbQvfXNvq/mXGl2AuGRpo7C00ma1iW0WTyGne7uTGJLqUUAfPfx20PTvgZ+1b8D/iR4QtYNBsPiJqh0XxTp+nKlnYXjf2rpmkeILt7WELAGu9M8R2F3LH5flSanYJqDKLx3mYA+pf2i/2kNM/Z5t/CVxqPha+8TDxXNrUMK2WpW+nGzOjJpbu0hntbkSif+01CBQuzymJJ3AKAY37Ufhf4l/Fr4K6d4d+F2nie98V6loN7r9tLq2n6Wq+GRY3OqSWr3V9PaJMX1VdHWSKE7poo5g6GFnRgD2zwH8NPB/w68E6b4K0XRdLg0iy0uCy1EPZ2x/tiVLcJfX+sO0WL+5v5PNnu5bkOGMjKFSEJGgB8FfsswaZaftZ/Hq3+GsUZ+FiaXexs2msf7Ag1Zda0drGKwMLNaNGl0fFEWjJETAulLcmzK26orAHv/wAa/hj+01458QapH8PPjLo3grwJeWdlBb6ILR7LWYZ1tEj1GQ67pugzawi3Vz5ssaxauoSNgqomCqgHzL+wt4Sv/Afxw+OPgzVLy31DUfDWmRaPe31oZjbXdzZ6/wCVLPCbhI5zHIwLKZUSQ5+YA5oA/VKgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA/9f+/igDxPVf+TivA3/ZG/if/wCpn8KKALvx80XVfEPwg8c6Lodhc6pqt/pcEVlp9nGZrm5kXUrGUpDGMFmEcbuQP4VJ5wRQB5v8WvC/he/+IE+r+OfgrrPxK0278C2OjeG9a8PWF34hubHU7XVdcur7RrrS/t8Ft4bvJBqFld6N4vjhtTGZLyC51izFnAjgHs/wp0rxPoXw28EaP4zuXu/FOm+G9Ls9bnlu/t8326G2RXiuL8ZF/c2yhLa5vgzi8nikuRJKJd7AHJ/Af/kVvFv/AGWT42/+rU8V0Ae2UAcR8QfiN4N+Fvhy58VeONbt9E0i3YRI8oeW6vrt0d4bDTbKEPc399MsbtHb28bssaSTymK3hmmQA+AtZ1L40ftszRaL4f0m9+Fn7Pf22KbUde1dP+Jz4xgtLlJYjDAuE1HZJCJbbT7CZtDsryNpNT1jULq2srdQD0X9rrwRovgL9ka58H+FbNrTQfC954PtbeIlXlMI162jlvLyVVUTXl9f3Ju7242J597cyS7F37VAPpH9n+eK4+BfwdkiZWVfhn4IgJU5AltfDmn20yk8fMk0MiOP4XVgc4JoA+Of28j9o8dfsxafBh7ybxhrZSNcGQebrHgCCH5c5All3KmRhmiYA/KRQB+klAH52fH/AP5PT/Zp/wCvC1/9PWvUAfonQB8aeAv2IvhP4Z1vXPEnjAT/ABO1bWNRn1CIeJraKLStPe6ma6umXSbWVrW/ubm4kYyz6j9oiESxRQWsLCeS4APEv2u/2f8Awj8L/DGn/HH4RWS+APEvgzxBost5FoJkttOmhvb+O0s763si7W9le2epT2cZW1SO0vLOe4hu7aUhGoA998V/tE/EDRfAPwh8X+DPgr4h+K8nxD8Iwa/ri+Fv7ZWDw1fHTtDuWtJxpvhzxGQt1dalfxWwuHtyBpswja5+dkAOb8HftP8Axp8R+LPDXh/V/wBlL4geGNK1rXdL0vUfEd8fFH2LQrG+vYba61a7+0+AbG3+z2EMj3U3n3lrFsibzLiJcuoB9v0AfnZ8f/8Ak9P9mn/rwtf/AE9a9QB+idAHy/8AG/8Aaj8I/Ca8TwhotheeP/ijf+VDpngbw+stxcQ3N2gazOs3FtFcvaGZWSWHTrWC71a6R4mSzhtZxeqAfD/7Qn/BPzxx/wAFCPhH8V9K/aM8f678E/EfxM8GWXgjwanwzh0qe58D+FY72+1G7sPE1jeQvca7Dq0l9cxX2lJ4osr+Sx1PWLG91O2hurbS9IAP5WNX/wCDYD9s/wCF/wAcPA3i/wAVftNat8RfCvw31bw9q/wmvvhn4I8ZfES/Fh4d1241K60TxLbavq3h9/hzd25TSbjTlsB4o0jUP7SvmtLwTaPdQOAf2o+Fv2mfjPr3izwv4f1b9lDx94X0rXNd03S9S8R3t7r13ZaHY3t7b21zq13/AMW/0+3jt9Phle6l+1XlrEyRNm4iVXdQD7WoA/Oyz/5SL6r/ANk+j/8AUS02gD778Q6U2vaBrmhpeS6c+s6PqelLqECLJPYtqNlNaLeQxuQkktsZvOjRyFZ0CsQCTQB8r/C/9iX4LfD+xmXXNGh+JGsTzSO2q+LbSKe3t7ZsrFaWWiK76bAkakvJcSx3N5LcM8i3EUQgt4AD4h/4KMfsmfs7Xdl4D8QeMPg34Q8YfDDXvGOj2HxF+FV5ZQ2fhLxC2gvZ63pEmmWNnDHF4Z1C5stH1O1u7/Rkt/8ASPsupR266g+qXGpAH6zfDTwD8Pfhb4C8K+APhT4S8N+BPh14Y0mCw8JeE/COkWeheHdH0py90kWnaZYRQW0C3E1xNe3MgTzry8ubi9upJrq5nlcA7igD5u/a9/5Nw+Kf/YG0/wD9SDR6AL/7Kn/JvHwn/wCxXj/9LbygD87dS+H+qfFP9tf4ofDq61vUdH8O+INQluvGraXIILzUfCmj6fo+q22krKySgR3l5Fo0TCTMCuIrt4ZXtYYGAPs/xt+w/wDAnX/CN9o3hnwonhLX47GZdC8Q2Wra3PcW2oqjNavqSX99fxanZyzhEvkuYZLk2rSizntZxFMoBi/sIfEnxD42+F2r+G/FN3PqGr/DzXv7ChvLyaWe+k0a6txc6db3s0zyyTS2FxHqNhBIZDtsbazg2jyN7gH2Lpfhvw7ock82i6DoujzXQVbmXS9LsdPkuFVmZRO9pBC0wVmZlEhcKzMRgsTQB+e37DrfZvit+1Tp85C3g8Y6czRscSf6F4k+IMFydvXCS3ESyHPysyg53CgD7Q+Oc8dt8FPi9NMyqi/DLx2PmIAZn8L6okcfJGWkkZY0GcszBRkkUAfl5bWlz/w7pv5tj+V/wsJLvOPl+zf8JZZWO/8A3Ptf7vP/AD04oA/Tj9n+eK4+BfwdkiZWVfhn4IgJU5AltfDmn20yk8fMk0MiOP4XVgc4JoA+Of28j9o8dfsxafBh7ybxhrZSNcGQebrHgCCH5c5All3KmRhmiYA/KRQB+hureHtA14QLruh6RrS2pkNsurabZ6iLczbBKYBeQTiIyiOMSGMLv8tN2dq0AWprzTdNFtBcXdlYCUiCzhmngtRKU2osNtG7pvKBo1EcQO3coAGVFAHzv8dP2b7P443lpd3vxB8aeFYrTRm0g6ToV1F/Yd8Gurm5N1qemzbFvJiLgwMPOizDGibuKAPmn9nnxD4t/Z7+NP8Awyv4vTQ9U0DWoLrV/B3ibStItNIvLqaayvNVgvNSa2UT3wv4dPvtLmGqTX2oWGoWdtZ22oz6XHBuAP0roA/Oz9mv/k7f9qb/AK/73/1JnoA/ROgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//9D+/igD5+8fXureGfjB4K8ZQ+DvGHirRbf4dePPDd2/hHSY9XuLLU9X8SeANSsFuopLuzWOKa10PUSJPNY741UIdxKgGr/wuiX/AKJD8bP/AAirT/5e0AH/AAuiX/okPxs/8Iq0/wDl7QAf8Lol/wCiQ/Gz/wAIq0/+XtAE3wKstVtPBurT6xo2q6Bcaz8Rvih4kt9L1u2FnqkGmeI/H3iDWtLa8tlklWGabT723laMSuFL4DtjLAHs1AH41/F/4y/D3xB+1ZrFx8ZbbXNf+G3wrlvfD3hvwnolrbajaX3iHSri3hvpdYs77U9NtZbabVRqM1/td2vItN0jSrqGewSdaAPp2H/god8BLeKKCDQfiLBBBGkMMMPh3w/HFDFGoSOKKNPFSpHHGihERFVVVQqgAAUAev3Wq+Fv2uvgB4ut/Cyalp2m+KbbUtH01/EVpBaz2uvaHc219pl1cwWN9qKfZIdXtbGVmjnaVoUkxEGwGAPlH4FftQ2vwC8Lf8KZ+Pvh/wAVeG9d8Fz3dvol3Fo32qO70WaaS6gtZQlwjTGC4lnTTtSsxdabfaa9qVuUMLTXAAeF5vEP7Xn7SXhX4nW3h7VdG+DPwqktrnRL7WbT7OdV1DSrpdTgiVkaW3udU1DXPslxeW1tcXFtp+i6fDHdOt5MiXoB+o1AH52fH/8A5PT/AGaf+vC1/wDT1r1AH6EX9omoWN7YSSTQpe2lzaPLbyPDcRJcwvC0kEyFXimQOWikRldHCspBANAH5jfDX45+MP2VdW8RfC/9oqx8Z63pE2uXWo+EviIn23XY7+3eGCCRLe41S7DXmkTRW8F7DDaXU1/o97c3lpe2RaX/AEUAr/GT4w6t+19Dpfwa+BXhbxBd6Fdazp+o+MfGetWI07S7G0sZiYFk/eypbWFvK6ajM91NDqV7PaQWGnWE0jv54B+lPg/w1ZeDPCfhnwhpzvLYeF9A0jw/aSyqqyzW+j2FvYRTzBPl8+dbcSzFcgyu5yc5oA6OgAoA/Oz4/wD/ACen+zT/ANeFr/6eteoA+vfjl4+m+GHwl8d+OrTyft+haHK2lfaBug/tjUJ4NK0czJ/y1jXU760Z4uPNVTHkbiaAPy4/Zp/aJ+A3wh0u+8R+NNI8c+I/i74lvtSu/EfixdH0nVXhhubuZorLS9Q1DxFbXaJdxFb3Vrj7PBcX19cSRXEk9tZ2W0A+udK/4KBfBDWNU03SbXR/iKt1ql/Z6dbNNoWhrCs97cR20LSsvil2WMSSqZGVHYJkhWIAoA+5KACgAoA/Oyz/AOUi+q/9k+j/APUS02gD7f8AiN4a1Dxj4E8W+F9I1a40LV9b0HUbDSdZtbm6tJtL1WW3c6bfC4snju447e9WCSf7O6ytAsiLndigD8/vhH+1ZefA/SP+FTftJaF420zxL4bub5NN8Tz2txrTa3pc17PPG93cXV0t1frbySywadq2ntqFle6cltGXhlgaS4AMHx3r+u/tzeOPBfhPwL4Y8QaR8G/COr/2p4p8Z6zax2X2qSZFiuTbr5stuLqOxS5sdHsLe5ur2WfUJL7UIbSzhZ4AD9UURY1VEVURFCIigBVVRhVUDACqAAABgDgYxQA6gD5u/a9/5Nw+Kf8A2BtP/wDUg0egC/8Asqf8m8fCf/sV4/8A0tvKAPmj9ofQfGHwU+PegftQeEfDNz4o8MXOlJo3xF07TkY3VuIrJtKlvboxxSvbWtzpEemtZ6gYntrXU9GWPUpIory3juADU8Uft/eAL/w7NZfC/wAPeM/EPxB1m2ey8O6LcaHHGlpq10jRW0t6La7v5L42sjC4Sw0uK9a/eNbUz2qSvcxAHqP7HPwY1z4PfDC4XxbE1t4v8Y6vJ4h1qweWKeTS4RBHa6Zp080TSJJdpAkl7eYkfybm/ktSS9u7sAVv2a/2oL34+6/400W68HWvhhfCdrZXCXFvrc2qtfG7vbq0KPFJplgLfYLYOCry7i+0gYDMAfPXjVvEn7Jn7SviP4ut4e1TWvg78UTcS+JLvR7QXD6XqGrTrf30csjyRwW2q22vwy6nZJdS21tqWl6hcWlvObtJ2tAA+Of7VVh8c/CUnwd+Anh/xX4l8R+OJLex1a4k0U2q2eiCRJ723hRppnWW5aOO3v766SDTLDTTezNdh2jliAPqK2/Z4hj/AGXD8A5Lq0/tCXwnJBJqOJDYjxfLfnxML4nHnvYw+KCjq20TGyiQCNWAjUA+WfgV+1Da/ALwt/wpn4++H/FXhvXfBc93b6JdxaN9qju9FmmkuoLWUJcI0xguJZ007UrMXWm32mvalblDC01wAHhebxD+15+0l4V+J1t4e1XRvgz8KpLa50S+1m0+znVdQ0q6XU4IlZGlt7nVNQ1z7JcXltbXFxbafounwx3TreTIl6AfTP7UP7SV5+zvbeC7i08JW3io+LJ9ehdbnWJdI+xf2NHpDqyGLTtQ8/7R/ajBgwi8vygQX3sFAOY/a3+GPir4v/Cnwh4s8D2kk/jHwZe2HjHTtHtVFxe3trqFnbPf2emFvL86/tJUsr+CIruvUsJbeGJ7ua2iYA5jw9/wUC+GMWhxx/EDQfGPhnxtp0Itte8P2+hi5jOqwDy7pNPlnvrSSBJJVZ1t9WSwmtSxtpWm8kXEoByfwZ0rxv8AtCftHQ/tI6/4WvvCPgDwro76b4HtdYhlW51qN7DU7Cwa1MixpdRwS6tqet3mo2qtYwXr2un2kl4yT3EQB+k9AH52fs1/8nb/ALU3/X/e/wDqTPQB+idABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQB/9H+/igAoAKACgAoAKACgAoAKACgCrd2NjfosV9Z2t7GjiRI7u3huEWRSCrqkyOodSAQwAIOCM0AWVUKAqgKqgKqqAAoAwAAOAAOABwBwKAFoAKACgCG4tre7he3u7eG6t5BtkguIkmhkHo8Uiujj2YY/KgBYYILaJILaGK3giULHDDGkUUajoqRxhURR2CgAfhQBLQAUAFABQAUAFABQAUAFABQAUAFAFW7srK/i8i+tLW9h3BxFd28VzEHXlW8uZHTcDyGwCOozQBYVVRVRFVEUBVVQFVVAwFVRgAAcAAYA6YwKAHUAFABQAUAFAGfbaRpVnPJdWemafaXMuRLcW1lbQTyg9fMmiiSR84Gd7NnHXgCgDQoAKAEIDAqwBBBBBGQQeCCDkEEcEEfnmgCraWFjYI6WNnaWSSOZJEtLeG2R5GOWkdYUQM7EklmG4nkk5IUAt0AVbuxsb9FivrO1vY0cSJHd28NwiyKQVdUmR1DqQCGABBwRmgCyqhQFUBVUBVVQAFAGAABwABwAOAOBQAtABQBSn03TrqeG6ubCyubm35t7ie1gmng/wCuMsiNJF3+4R+OaALtABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQB/9L+/igAoA8DXxP45+Kt9qFt8OtSt/BfgHS9SvtJufiO9pY61rvii/0yZ7PUYPAekX0VzottpFlfxXFjN4q1uLUory5t54tI0aeFBqdAF5vhF4htV+0aR8cPi5baqnzR3OrXvhLX9Nkk9L3Qr3wnHYSwMMh4rEabJ3huIHVWoAv+EPHPiO28Sp8OfiXYabY+LZtNudU8OeIdEeVfDPj3TNOkii1K40u0vJHvtF17SxcWk2s+Grme/MEFyl9p2o6hYCeS1APX6ACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD/0/7+KAPI/jzq2oaN8H/H13pdzJY6hNob6Ta30TMk1hJr1zbaF9vhkUq0U1iuotdwyg/upIVk52YYAveLtRsvg78I9e1Lw3o8D2Pw98F3cui6OuYbXytD00rYwXDwqGjtE8mN76dF3pbrPPy4JYA8x1yL4ifDHTfD3jzUPipqfjo3niTwfovirwtqGk+GLXw3qdv4x8Q6V4c3+BI9I0ay1rSrzSZ9Yiv9LivNZ1ldTs7SW31Dzp5lvYADqv2g4Y7XwHb+LoQI9a8A+LfBvivQrtRiWG4g8TaZp2pWisMOYNZ0LUdU0W8gV1FzbX7xNnIoA9yoAKAPhT4vfFz4ieGv2uvg98NtE8SS2PgnxNo3hy61zQl0/SZ47+e+1/xZZ3UjXtzp82pQebbabZRFba9gRRAGRVd5GYA+66ACgAoAKAPmP4x/HfWPhn8Vfgt8PrDQtM1Ky+KGu22k6hqF5PdRXWlxz67o+ktNZxwkRSusWpPKFnBUvGgOFLUAfTlABQAUAFABQB8s/tU/HuX4MeCjaeGNt58SPE1tqA8M2cdul8+k6fptrLd634svbN0ljey0SyimlhW5ie3mu1DzxTWFnqIQA6z9mLxr4l+IvwN8C+MvGGojVvEesxeIG1LUFs7GwFwbHxVrumWp+yabbWllD5dlZ20OILeMP5fmSbpXd3APGPjr8XfjXbfHPwn8GPglL4ZXVNV8GS+JNSfxDZpNbRTLPrUm2a7bzGtVSw0mMoi27+bNfW6krv3KAZv2b/goN/z/APwe/wC+I/8A5BoA+2fDQ1weHdAHic2jeJRoulDxC1h/x4trgsYP7WNlwv8Aohv/ALQbf5V/c7OB0oA15fM8uTysebsfy93TzNp2Z6cbsZ56elAH5/8Amf8ABQ7/AJ9/hd+ej/8Ax+gDzf4m/Fz9tn4R6PZaz40ufhpax6pqUGkaTYWFpp2p6vq2oT9LfTtMtZmubkxp887qBHEDEjN509vFOAfffhvxhc+Hfhn4T8R/GfWPD/g7XbnR9K/4SafWdR0zQdMttfvLbzpdPE11dJZR3IYSILWK4kYvFKsW8ITQB8V/Hf8AbRth4t8B+C/gb4qgvHn8T6cvjDxLa6ZYajpc9hc3cNkmg6ZNrFldQ3LTCaW6vdSsIFWER2UdjqErS3scQB+klAHwx8Uf2iPibr3xWuvgX+zroOkap4o0SNpvFvi3XCLjSNE8qOL7XAke5ba3XTJrq1tb6+uzeO+pyNpFrpb3SeY4Bwfir4qftf8A7Ov9n+Kfi3B4K+J3w/nvoLPWLzwxDFZXGkteOPJj+1W+j6BNZTMQ0FpdXujajpk03l2k91Fd3Vq7gH3zpPjXw3rPguy+INpqKJ4UvvDyeKU1O5V4Fg0VrD+0pLm6jZTJA1tah2uomUvC8ckbKWUigD4X074v/tNftF6tr1/+z6vhbwD8NNE1CTS9O8V+M7JZL7X7mFY2lfZcaX4iUPsZLgWlpo6R2EVxHBfajNdkIgB0Xwz+PHxi8H/GHTfgb+0VpuiS6n4ntzP4Q8aeHoUgstTmZZvs0UiwJbWtxZ3slpdWcMiadpmoWWoiGC9sXgu0ntwDrf2j/wBovxJ8PvEXhj4T/Cnw/beKfiv4zSOW0guw0tlotnczyW9nNLbLLax3N3eNb3sqfab21s9MtLN9Q1EvauiOAeTeKvFP7dPwg0Wb4heLLr4dePfDGm+Xf+IfD+kWcIu9H0tWxdSSSWmjaFdiCCMb5r21vdbFkG+1XEMtlBOygH2l8KfifoPxZ+Huh/ELRc2ljqtpI99Z3EqtLo2o2TvBqunXU2yJX+w3MUqpcmKFLq28m8SNIp0FAHxXpnxy/aR/aN8R+I4/2eIPC3gn4f8Ahu9bTl8YeLbVZ7nUp2+eEyC607WlS5uLZVvE0yx0KY6dbzwjU78Pc2oYA3fDvx8+OHwm+KnhT4X/ALSGneG9T07x3cQWPhrx54WjMFsb6eeOwiMiR2tpBdWw1GeytNQgl0zR73TEvYtSfz7J4UnAP0BoAKACgD4U/Z0+LnxE8bftB/H/AMF+KPEkuq+GfBus+JrXw1pb6fpNsmlwWHja80q0jS5stPtby58nT4ktw15cXLuF8yRmlLOwB910AFABQAUAFABQAUAFABQAUAFABQAUAcJ4y+KHw5+HoUeN/G/hjwxNJbNewWWsazZWmpXdqjtG09jpby/2lfxiRHizZ2s+ZFZAC4K0AfGnww/ao1f4wftSP4S8J6pKnwkh8OastlY3OkafBc61qOl2plfX3uZ7Rtas4ri5lKWdkby3Q2MFrLdWUF3NcwKAfoNQB8vfFz9rL4QfDjw3rNzpvjTw94r8VxWl3Bo3h7w1qVpr002shXitotTl02ae10u1trorJqDXtxBMlvFMlvDc3Rit2AF/ZD+I3jH4pfBy18V+OtWGta9L4i12xe+XT9M0wG1s5YFtovsuk2djaDyw7DzBB5j5zI7YFAH1BQBy/jXxfo3gHwn4g8Z+IZzb6P4c0u51S9dcGWRIE/dW1upIEl3eTtFaWkWR5tzPFGCCwNAHwZ4V8e/to/Huwl8c/Dh/AHwv8DXV1c/8I5b+IoUvr3VrS0la3c/abjQNfuLpftEUsMl8LDRLaWaOQWiCNN7AHf8AwN/aF+It38UNS+Avx20DTtI+INrZz3uiazo0fl2GvQW1q+pyrJFFNPaMZ9LWS/sb7TzBbSRWl1aXdpa38LI4Bs/tQ/F74leBvEfwh8B/CZ9G/wCEs+JGs6pYka1Zpd20UNvJo9pZPKXYfZYHuNSnmmuVjlZYbKYhDt2uAcf9m/4KDf8AP/8AB7/viP8A+QaAPqjwdqvivw78MrbXPjRf6HaeI9F0vWdU8ZajpJ26Ha2lhdahdLdQ7Y0Iig0SK1efbFuMyTBVYkBgD4w0L4x/tTftG3Wtav8AAux8I/Dj4eaZfyaZp/iDxhEl5f6rcwfvJCWk0rXY2nMLwPPbWeiG009rgWr6reTpJIoBteB/2h/jF8Pfizonwf8A2lNI0JD4ue3t/CvjjQI1gsrq9u5RaWQmMGyzurG8v9unuy2OlX2l3s0Ul9bvYzpNAAfSH7QPxs0n4D/D+58X31oNV1O6u4dI8OaJ55t/7U1e4SWYLNcLHM1vZWdrBcXt5MI2JSFbaPFxcwUAfLml3v8AwUC8UaDB49sbz4c6HbXtp/adh4Au9PsrfVbq0nRri1iK3mmag1rNLD5fk2+oeKrG5QTot79nnV0QA9x/Zl/aGl+NmleINI8TaNH4Z+I3ge8TT/FWiRefHby7nlthqNpb3W65s9t9a3llfabLNdS6dcwR+Zcul5CqgHY/EP8AaP8Agz8MrbVT4h8d6DPq+kfaIZ/C+i6haax4na/gU/8AEsfSLGaWexu5JAIs6r9gtoXbdc3EEau6gHkH7G/xq8b/ABs0z4l654y1CO6isPFsEXh+wisNNs49F0m+t7i7j0xJrCztZr5bZfKhW61B7m8kEW6SYuzlgD6O+KnxH0T4TeAvEPj3Xw8tjoVoJIrOFglxqeo3EqWum6ZbsVcJLf3s0Nv5zI6W0bSXUymGB9oB8SeFPF37cnxm0ZPiF4Oufh38P/CuoPLd+HdC1i0ie71jTVfZA0c13o2v3TwzDd5d9czaGt60ZubaGGymt2YA9W/Z0/aL8UeOfFXib4QfF3w/a+F/it4RjknlSyDQ2WuWlu8a3TR2xluYre8tormxu0e0vbmz1awuv7QsUhtoHWgD7HoAKACgAoAKACgAoA//1P7+KAOW8b+FLHx14P8AE3g7UmeOy8TaJqOjTzxjM1r9utpII7yDlR9ospXS6tySAJokJoA4/wCFniefxt4PutG8X2tsfF/hie68D/EXR5kWe3k1qxto4rm8WCdMXGi+KtMuLXX9MZ0eG40zVo4maR4pwoBFoXwO8AeH9V0vU7WHxFex+Hpzc+F9E1zxh4o17wz4VuPLeFJ/Dvh3V9WvNJ02a2hkeGwkiti+lwuYtMa2jwKAML4kynx5428JfCOwHnWFje6L8SPiTcKSYrLw74d1ZL/wpoE23C/bfFvivTbaRYGdXGh6DrMxjYSwMwB73QAUAfmZ8ev+T8/gB/2L3hL/ANSnx1QB+mdAHD/EvxfN4B8BeK/GFrpVxrl5oOj3N5YaPapPJNqeonEOn2IW2innCXF7NBFNJFDK8ULSShG2YoA+EfBHwi/ab+OehWXxD8c/tB+LvhoniBbi707wj4Ws9W0l7CzS5misxd2Ona34bgs8iPzoY511K+ktTbyXd4bqSQxAGr8EPG3xc+Gv7Rl/+zp8SfHU3xJ0q+0KbVfD3iHUvOm1WKWHTG1qCV7q6mudQjS5sIb63v7DUb3UhDdW9rJYXghaR7sA+d/j/wDCz4p6F8bvgpo2vfGbVfEus+MPFaReD9ensbmKbwHLceJ9Ht7aazjfVJnuDaXF3aXSiKaz3NYoBtLKVAP1D+Dvgrxr4D8K3Gi+PfiFe/EvWpdZu9Qi8QX9tNazQ6fPa2MMGmCOe9vnMdtNbXNwriYKWu2AjUqWcA9WoA+Qvh54O/aM079oLxj4i8Y+Irq8+D97deLX8N6RJ4jhvILaC81JJPDypo6kyW32ey3IisB9mHyEnIFAH17QByXjvxt4f+HPhLXfGnii7FlougWMl5dONpmnfIjtbG0jZlE19f3Tw2dlBuXzbmeJCyqWdQD4LsvBviDxd8JPj1+0r8TLQw+MPH/wr8Z23gfQ590kfgj4d/2BqL6XaWgkVRHd6xAVupp0iikltJPtTeVPrOpW6gHvv7Fn/Jsvwx/64+K//U58T0AfIGo/FXW9K/bP+L2veEPAWtfE3xRYeH4/A3hXw9pRKWthLp8Xhu21rVdWvlinexsbO4sdTi3CDy5J9SFtJc2u5ZqAPWLr9r/4sfDLV9Mi+P8A8Crzwl4b1W6a2j8Q+Hrx7+O1YjeiqGmvtM1KeKP557VNYsbswrLPBbymL7OwB996Tquna7pena1pF5BqGlatZWupabfWziS3vLG9hS4tbmFwBujngkSRDgHa3IBBFAGhQBT1DULLSbC+1XUrqGx07TbO51C/vbhxHb2llZwvcXV1PI3yxwwQRvLI54VEYnpQB+f/AMBob79pP4yeJv2hfFdtNL4G8EX0/hn4O6Jfx/6Na3MTJLPrwtH/AHf9oW9sYLye4bzyNZ1NUhuFPh6zSAA+2/HHgDwf8SdEHhzxxodv4g0Rb2DUBYXUt3DELy1SVIJ99lc2s26NZ5gB5uw7zuU4BoA/M/8Aa2+DXwy+Fmt/Aq5+H/hKy8NT6x48aDU5LS41Kc3cVnd+H5LZHF/fXaqInnmZTGEJ3ncT8ooA/WGgDwH4SfAPSvhT4z+KXjeHXrvX9V+KGtnWbz7ZYW1p/ZBk1XW9WubSzlhlmkmhurjWF80ybM/YLZtpYnaAeQft0/Erw5oPwh1X4cG4h1Dxp8QJNGstH0C3C3V9FZW2uWOoz6vc2ibpYrcnTjY6c5TzbnVJovsgkW1u2gAOT+K9l4g+EP7B1r4WvpJLLxC/hvw14b1ONpPMltW8R61bXOuaSXB2t5WnXGoaRIFLxrErrGXQK1AHL/DH9o2+8J/D3wV8M/gV8HfEvxbu/CPhPRP+Ev1vTGubPw7p3iXVbL+2NctIrm00vUTdzf2xd6gm+4lsFmmjmWyN7HH5zAHqnwx/aI+Hvxe+I2n+Dfif8JI/APxf0GUy+GLXxlpNnq99b3VvENUePRtX1PRNO1XQtTCW41G2ga1tVuEihms725uTGjAHnHw+VfE//BQz4p6hfDzX8J+D7ltKDneLRrTS/BfhljADny/Mh1fUHcLt+e7nJzuYMAfopq2mWmtaVqejX8azWOrafe6ZeRMAyy2l/bSWtxGynIKvDK6kEYIODnJoA/O7/gnmzav8Jfib4Qv5JWsYvF1zCwSQh4otd8PWdleLAWB8r/jwMiYXHmu8m0sW3AHmfwa+NGj/ALG+v+Pfgp8R4/7f0geJ5tf0nxN4JutJ1uaKa5tLXS5YNb04alBNp00lppNjJLp08ianpl0twklpPaXVpe0AamqeONI/bU+PHw003w3Lb+FvBXwtuJPEE0nia/02w8V+KJbm/wBLvdQs9C0OC8u5rjdF4ftYUeCWZNOtmutS1J4ZJLLT5wD9WaACgAoA/Mz9kr/k6v8Aam/7GHxl/wCrH1CgD9M6APmD9qX4+z/A3wfpn/CP2Vvq3j3xjfy6T4T0y4jmuIkMCRG/1aW1gZJbxLF7qwtoLNJEe5vtQtFIkgS4VQDxXSf2c/2pPEmlW3ibxZ+1B4o8MeMbiH7YnhrR4r+XQdOmmzILG/Oma7o2lyyAMkVwttoc9pZyLIlu1/CqO4B0P7Ovxx+IsXxH1/8AZ3+OrWc/j7QLea48P+I7dRH/AMJNa20CX8kMpigtoLySTR5V1bTdQjtbSW40+3u01KFdRgdpQDzf4w/Fv41aH+1hcfDv4a6nc38/iLwtpOk+G/D2o3Tt4Z0jWNV01Li78U3tiIpklTRbKC/1SbMUqL5JmlguYopLWcA0/G37P/7S3hLwtrnxC0z9qPxnr/ivQ9Nu/EF34dZdUsPD95Fp0DX95ZadBJ4gvdKDpHDP9jgm8PW9nevst5IbFJHdQD2P4aftKxan+zHP8bvGkMJ1Lw1ZapY65bWhjs4dY8QaddrYabBZgCVLSTxDPdaUu3yvJs7q+lEcRtoULgHhHgXwH+0v+0vpQ+KXiD44a/8ACHw7r01zceEfC/g0atCo0yKaSG2uZbbTdd8OlbTzot1pc6lc6nqOoxI10WtoJ7WRgDzj4jeO/wBpX4QeP/hn8JfF/wAQdQ1mxl8W6JqGj+ONNmlsbvxZ4Y1LVbHTbzRtfH7yS6msLiGTzI72e4vbcXb+ZeahZ3VjNEAfrxQBzPjW11y+8G+LbLwxO1r4kvPDOvWvh65ScWr2+uXGlXUWkzrcni3aG/e3kWc8RFfMP3TQB4d+zJ4Y+OHhfw14jtvjlrdxrmt3OuRT6LNca5FrrQ6ULCCOSJZ4iRADdrK/lE5JO/AzmgD6XoA8q8ffBH4V/FHULLVfH3g6w8Sahp1mdPsrm7udTge3szPJcmBVsb+0jK+fNLJl0Z8uRu2gBQD4X8A+CfC3w9/b4vPC/g3R4NC0G08Byz22m20lzLFFLeeGbC5uXD3c9xOTLPI8jBpWALYUKuBQB+ntAHyJ8Sf2WvgDZeCfH2v2vw20mHWLTwr4q1i3vlvddMkWpQaRfXsN2qtqzQl0ulWYK0bR7hgoUJWgDD/YC/5N6sf+xt8T/wDo61oA+16AOD+Jnw70P4reC9X8B+JZ9Ut9D1xrA37aPdQ2d866dqVpqkEaXE9reRpG11ZQGZTATJGrR7lDEqAeceMfiZ8LP2WvBHgfw/4guNZttEt9MTw54ajtbB9Wv7uLw7Y2UTm7kgW2txdyxTQyzTy/ZYp55JXURj5KAPk34KXb/tK/tP3H7QFrLp3h/wAL+AtOk0bQvDdxq2nS+MtSQ6Rf6XBd6npFjcTz2ViX128vJ72Y/ZGm+y6PZXGpLDe3EABn/tA/EWXR/wBs/wAAz6f4X1nx1qHgHwUYdH8H6FH5l7qvivXLDxFdWgd1ime0tYINU0q/u7qO2u3toNP87yHQttAO41r9q/4/fDg22u/Fr9nSfRvBNxc20c+p6Nqr3F1pkFyQq/arhZNRsBeEn9zaaj/YguJdtsZYJG30Ae5fFvxHY/Fz9lrx34l+H13NqNj4j8A6nqOnmCIm9eGzHnatpc1rH5jJqEcdnf6XeWimR47lZYUaQhGYA5n9hnxH4e1j9nrwrpOkXFudT8MXevab4isEdftVpqF5r+qatbz3CYWQx6hYX1tc285Bib97apI72cyoAfN//BR/xXosdz8LNBsLyFvF+iXeteIJPs8im70fT7pdMjsXmxzC2o3lkZ7dGO4jTTKVCNGzAG7+2FdDxl8TP2SfDF6obS/EfiCwur+1yfIuB4j1/wAGae6yRNuDCK3+0xxlgxVLmZf42DAH6aUAfm34AVfDH/BQ34p6TYDyrLxH4VkurmFDtje51DQfB/ie6nZBhWlbU0upA5Bb/SJTn53oA+q/EH7MXwH8U63qniPX/hzpWpa3rV7NqGqX8t7riS3d5cOXmndINVhhVpGJJEUSIOyr0YA+YP8AgnjBFa6J8ZLaBBHBb+PLWCGMEkRxQ2l3HGgLEsQqKFBJJ45JzmgC5/wUe1Se2+Evg7SonZItU8fW89yFYjzY9N0HWikMgHDx+fdxT7TkCWCJuqgqAfenh7SbbQNA0PQrKNYrPRdI03SbSJAAkdtp1lBZwRqAAAqRQqoAAGBwB0oA/PP4qKvhn/goD8GtVsB5UnifwrYRansOz7VLeJ408MOZsYEgWwttPChy3zW8R48tKAP0koAKACgAoAKACgAoA//V/v4oAKAPAfiED8NfG+kfGC1HleG9WTTvBXxaiTIii0mW6aLwj45mRdoMnhTVbx9M1e5be6+GdauLiTEOjRbQD1bxp4u0rwL4V1vxdrLSHT9FsXumht1827vrh2WCw0ywiXm41DVb6a207ToF5nvLqCIffoA5D4R+EdV8P6Hf694sWJviB491E+K/GskbeallfXMMcOmeGbOY5ZtL8I6PFZ6BYgM0cr2l1friS+mLAHq9ABQB+Znx6/5Pz+AH/YveEv8A1KfHVAH6Z0AeQfHX4r2vwW+Gev8Aj2ex/tS5sPsllpOmGUQJfavqVwlpYxTS/eS1hZ2vLwxZmNnbTiEGYoKAPmnwb4U/av8AjT4a0jxr4k+OFh8K9E8U2Frrek+GfBXhCzu76DSdSiW6sWn1K4ubHUrKW4tJIpoo31fVZIYpIzMyXJmhQA8U8CeAk+HH7eXhbw6/i/xB451M+FtV1PXPEXie8W91i71XUPBeuyus8nzSRRJZizNtBPPdSxwMmbh42jVQD039q/8A5OW/ZG/7HPTv/U08K0AfonQAUAFABQB+ef7YNl8VfEPxC+G+k6P8MPEfxM+Fnh22tfF2ueH9FS6t7DxB4nF/qtrBpmtajDZakv2WwsrWzl+yLaF5LXVr+NnRrq3uLUA5n4o/tCfHnW/hl490HWP2WvEvhjRNU8G+IdM1LXptU1B7XQtNutJura51OSF/DNtE0Gn27vcNG08CFIipljBDqAb/AOxB8R/iDceDfAfw8n+FGq2vgKz07xVPafFFru5OmX039vazqP2dLU6Ylsrf2hc3GlZXUpD5tq77dxMaAH1h8MPiN8KfiBrXjlPh+louv+HNUi0/xsR4ek0PUzqElzqccZvpp7O1n1LN3ZakPPLTKJkmJYM+XAOf/artNDu/2e/imNfSBrW38MXN3ZNOsZ8rXLeWF9AeFpMbJ21j7HDGyESN5rRLu8xkYA5n9imbUZv2avh02oA5QeJobJ2YmSTTofFuuR2pcHG0RhWhhUcG2igYZDZoA+qaAPiT9vX4hS+EPgq/hqwkdNU+IurW/h5fLyJE0a0A1PWnUjqLhYLPSpY8MZINUlAAwWUA+iPgl4Aj+F/wp8DeBxGiXWi6Fbf2sUxtk12/LalrsoYfeR9Wu7wxEliIfKTcQqlgD1OgD87P29v+Qp+zv/2UG7/9KPDNAH6J0AfFXxq/aB8cXXjv/hQ37PmkQ638Sng3+JPEt2kcmjeCLWSGGVpWabNk17aw3EMt3dXyT2VjLLbafHZarq10LK1ANz4MfsoaH4C1z/hYvxD1y7+J/wAWbqUXs/iXWnnuLDSb1kw0mjW15JLNNdQgiKHVtQLXEUcUR0600cb4mAHftw6Fda3+zn4wezhM8uiX3h/XZEXlxa2esWsF7Mo7i2tLua4lzjbBFK3O3FAHQ/sgadoWnfs7fDY6CluI9Q0u51HU5oQu+51241G7XV3uZAWaS4gu4nsf3jM0UFpDbgJHDHFEAfNP7ccFrYfFX9mvXdCWJPHL+J5LaFoDsu7qz0/xB4Wn0SO5ePMqwwanfX6WeV/eG8vwu/ymRQB/guZPB/8AwUP+IdjqBEC+OPCk0OjyTMsS3b3ui+FPEREG44lIl0HUbUKMM0ttIACyigD9EPEWt2fhrQNb8RajLHDYaFpOo6xeSyyLFGltptpLdzM8jEKg8uFvmJ496APy1/ZRuPEfhH9k/wDaD8e6KJ4NWMniOfRrpF2yQS6J4UtpJdWtyeG/s6S/nuFf5kE1jIuGKOlAHsv7C3ws8Bv8I9P+I97o+l6/408U6tr76nrGr2sGp32mJp2s3enQadaS3kUrWYmjs49UupIQLi8mvw1zPLHFbJAAcF+3r8PvCXgfR/A/xb8GWdn4P8eW3jWy0wXegQw6VJqSf2bqWrW+pTRWcUaPqOj3ekWyw3+1JGgvGt7qWYLZRxAH6S6Bd3l/oWi32ow/ZtQvNJ027vrfG37PeXFnDNdQ7cnHlTu8eMnG3GTjNAGtQAUAfmZ+yV/ydX+1N/2MPjL/ANWPqFAH6Z0Afmv+2XNFo/x8/Zb8R67IieFbPxNaPdSTny7W0Om+L/DV5qtzcTNiIRpZTWc0iOVDRWsgZghdkAP0oBzyOQeQR3oA/NfxvNFrP/BRL4aQeH5EN3oHhNIfEcsLCUQSxeH/ABnqN1HdbP8AUPNoep6baBZCCDcwf89UVgDRuI0f/gozYMyhmi8BPJGSMlHPg29iLL6MY5XTP91yO5oA+6fiB/yIfjb/ALFHxJ/6Zr2gD8gdFtL66/4J7eK5LPeYrL4sQ3eoqilt1j9r8O2w3Y+6qXtzZysxyAE5x1oA/Uz9n3UtO1b4G/CS70p43s1+HvhSx/dHcsV3pWjWml6jalu8lpqNndWsxPJlhfPJNAHxf+3Nq+mzfFn9mfQY3ifWLDxLNq15GD++t9O1bxJ4RtNOdwOsd3c6NqYQseGs3243NQB+mFABQAUAFABQB+dln/ykX1X/ALJ9H/6iWm0AfonQBwfxT/5Jj8R/+xD8X/8AqPajQB8yfsBf8m9WP/Y2+J//AEda0Afa9AHkfxr+Lul/BLwS3jjWdG1XXNPi1Ww0ue20hrRbqE6gJxFcubyaCHyVliSFwH37548AgNtAO20m70Dxz4c0HxCtlZalpevaPY6zpwvra1vALPWLKC7jGHWeIF4ZY1mCMUYrglgFoA/N74/eFdG+EP7U3wC8TfC6zt/D2s+N9es7HxDoGhxpY2N9FP4h0rR7mZrCBY7eBPEVjq17ZXYj2QSzaeb4xR3YmuJQD7k8O/Eb4U+Ivit4q8FaOlp/wtDwxYPJ4gabw9JZamdNjfTLdvL1uaziOoWoF5peI4LqZPJe2YAogZQDtfiBaaHf+BfGNp4mSCTw9P4Y1xdaFysbQrpo025a8kfzcRr5MKvKkjFfLdFkDKyhlAPh/wD4J+6z/ZPwB8bap4gnS08N6F438Q35vrne0drp1p4Y8PX2sSMio7fY7ULJcsyK+6SS6GCUIUAsW/7J3wt8e3U/xM/Z9+Lfif4fWOuzXMUk3gW+nl0XzoLl49Qt7FYrvR9UsojcK5Ng+oSWds5xaQQ2nkw0AfP37UXwQ8KfC3wn8Pfh74f1DVfF3xN+KPxDttQ1PxT4ilS98Q6smnWMmhWtmJVDyWemPqXiW3aC0Mk015cI8lzd3rWNuLUA9Y/bXgj8GfEX9lzxo4YaL4X8SxQXdw+Eit4/D+ueEdWgE0vCo09pDeyLxjbaStxtxQB+lqOkqJJG6yRyKrxyIwdHRxuV0ZcqyspDKynBByMg0Afmx8Mpk8Y/8FA/i34h04ifTfDfhy50+e5hYSwLd6VpXhPwfPbmVdyCVr+C/IjB3A2068+U+0A/SmgD87P+CfH/ACC/jV/2UGH/ANJ72gC5/wAFG9HuLz4QeFNXgjeSPRvHtol2VXIhttT0TWYVuJD/AAoLuC1t885kuYl7gqAfcvhHXrTxR4V8N+JLCWOez17QtK1e2likWWNotQsYLpcSLkMV83a3cMpBwwIoA/Pj4kTJ4w/4KDfCnStOInPgzwxYjVzCwkFpNY2ni7xW32krkQFrfVNNi2vg77iEDDSoGAP0poAKACgAoAKACgAoA//W/v4oAKAKGqaXp+t6ZqGjataQ3+l6tZXWm6jZXC74LyxvYJLa7tpk43RTwSPE4zyrHpQB8veC/BPj7U/FWieBvHUFxceAPgdfJqXhvXruUyv8Sr51f/hXdzfAqpeX4faK0qa0ZGP2/wAXQ6Xqi7hbZoA+sKACgAoA+Ef2if2cvjD8RPjN4S+LHwt8UeCvDl34T8MaTpljceIrnUhf2+sadrXiLUTdxWUXhTxDplxa+RrFukf2pmLSrOr2oRI3lAMr/hXH/BQr/ou/wu/8Ftl/852gC037Pv7SvxL8PeL/AAZ8ffiv4Q17w1q2gB/DT+GtOtTe6N42sNY0q+0jV7mJPAnhNrjTVsrfVNPv7ddTLywagyxRJN5d5agGd4R+Gn7dHg/Q7HwDo/xD+FsPhzQ7SLTNF8QahBNqOoWWkWq+RZWdsk3heaaU2tsqR20Wo2k4hjSO2F60MUe0A5y4/ZD+Nngr4neEvip8OviNovifxgILp/Gmv+PGu4pbrW9UTVLHVL21sYbDVkfRm0O9g0+0tGuGvbS4tVnhHkyIlkAe9ftM/s/eK/i9qHw78XfD7xRpnhbxv8ONTuNQ0m51iKdrCZ5LvS9QtLgzW9lqbw3OmX+kxTwRy6deWt0s80dxGFUbgDt/CPhb45S/CLxp4a+JnjHw3rHxJ1my8U2Hh7xH4bM+l6Zp0Op6DHZaG809j4c0K6trmx1Zrq7nu7TSpriKJopIJJ5I0iQAxP2ZPhd8U/hZ4a8R6X8VfGVv4z1TU9civ9MvLfxD4i8RLZ2CWEFu9s1x4jsNPuLdmuI5JfJt0khIbeXDsVoA5n4efBn40eG/2gvGPxG8S+P7XVvhvrd14tl0TwpH4q8V38+nw6xqSXOjI+hX+mQaDaGwtVaF1tL2VbUny7UyxktQB9e0AFAHCfFHwvf+Nvht498HaVNZ2+p+KfCHiHQNPn1CSaKwhvNW0q6sbaW8ltre7uI7ZJZ1ad4LW4lWMMY4ZWASgDlP2e/h1rfwn+D/AIO+H/iK60u91nw9Hra3tzos93c6ZKdS8SaxrEH2Wa+sdNunCW2oQRy+bZQbZ1lVPMjVJXAPn74hfswfELRfiZqnxh/Zy8c6b4K8S+IpJpvFHhvXYpToOqz3cv2nUrhJY7HWIZV1O7Vb2fTr7TCkWovNfWmp2jNHFAAc3rH7Pf7THxwk0vSvj38TPDOkeArS7jvb3w14BhcX+ozW+PLM7PpNpYmRj81vcXt5q0FhIxng0rzSFUA+ofiX8NdY1H4Kap8MfhPf23gvU4tI0PR/Ct6NS1PR7fSLbSdT0uZ1Op6Rb3mqQtLp1ncQPNDBNNcyzsLhts80tACfs/8Agbx38Ovhrp3hf4j+I4fFfim21HVrm51iDWNY1yOa2vLx57OEahrtpZajIYIGWMxyQLHERsiZkANAHlX7SX7P/jL43eNfhFqGmal4Zs/CPgbVZb7xDaave6rDql7FqGraHPqK6ba2ei39ncyDTNH8q3F5f2KtcTGNnhiZ5qAPsCgAoA+U/wBpr4GeLfjPefCq48L6j4dsE8D+KZ9b1Ya/d6natcWksujusenDTtI1VZrgDT5spctZxgtFiY7nKgH1Z/n/AD1/l+dAH5jeEP2av2wPh/4k8aeKfBnxF+EOkar481SXVNfurl9V1q5uJJL6+v1iS41n4bX0sEYn1Cd5FtzEtw3lPOHMMGwA9G/4QX/goN/0Wb4Pf+CuP/51FAH1l4d8N69qnw3tPCnxbuNI8Ua5qfh+60bxtPpsZi0fWDfx3FtfLbRpYaO0dvPaTCL5LCydTlkRGCvQB8VaP+z5+0/8DbzVNK+AvxF8M6x8P9QvZr2z8P8AjZP9L0yWc4z5cmmXNokyIVFxeaZfWEWpSRCe50pHCRqAdz8Lf2ZvHV18SrX41ftDeM7Pxr450gRjwxoujA/8I9oXkrP9mmPmWGnQhrGS4lubGw06wtbW21Ivqs11qF5KXUA6z9oz9mqf4ual4b8e+CPEcfgv4p+DTB/YuuSpN9iv4rS8+36fb6hNbJNd2cumXrz3NjfW9te7Rc3Ntc2VzHLC9uAeO+J/gz+2d8WdJ/4QT4j/ABI+HWj+C7ie0j1+88PW076nrdrbzRzhxbW2hacbkCWNJjYyXug2lxJGonVkULQB9m+Bfhb4S8A/DnT/AIYaZYpd+GbXSLnSb+G/RJX1tdSSYa1c6oAoSaXV5Lm5ku0VRCqz/Z4Y47eOOJAD420/9mz9oT4H6vrTfs5/Efw7N4J1m8m1F/BvjiKYtZ3DlUijhY6dqlrdTx20UNtJq0V3od1dwxQRXcM/kRyKAaGlfsu/Fn4oeN9C8a/tPeO9D8R6b4YmS50XwF4XglbRnYvHNJb35fTtItLW3nnt7Y6ksNtql3q9vElrPqUMMSIoB7D+114e8Za78HNRufBXjCHwRd+FtQHi3V9Yl13WPDxl0DRtH1o3unW95oltc3M13dz3FmLSxlENtdTxxq86SrEGAO//AGf31iX4J/C6fxBcX13rF34K0K9vrnU57i51CeS9so7tJbye6Z7iWeSKaNpGmZpM8NyCFAPX6APzQH7MH7UnhL4p/E34g/Cn4i/Dbwsnj7xP4l1PN5Lf3982j6t4ivNbsbS9ttS+Het2VtdQieL7R9illCzK8aXU0WHcA6z/AIVx/wAFCv8Aou/wu/8ABbZf/OdoA9Fb9n7x38Vfg9qngP8AaS8YaT4k8XjxRPrfhfxd4QtrWP8AsC2j0uxtdOjEP/CM+F4rlhdtrI1G0ksW+12N7Gi6lFPHbSaeAeb6T8Mf26fB2l2/gzw78Vvh1q+gW0P2LS/EOuRXNxrWlWEOUgjle98MahdSOsZCwpM2u/Z0SOGO5SKKMKAev/s8/s2RfB+813xr4r8R3Hjn4peLUkTX/EtwZXgt4J7kXlzaaa92DfzteXMcEuoaheust2bW1WK0soonScAik+Bni1/2r4PjqNR8O/8ACIxeFzojacbvU/8AhIzdHQptM8wWn9jnTDb/AGiQPvOsCTyQW8ouBHQB9H+KNMuNb8M+ItGtXhjutX0LV9MtpLhnS3S4v7C4tYXnaOOaRYVkmVpGjilcIGKxuwCUAfMnwQ+CkXwg+AniX4dfGPVfCE+jarqWvz67fW2rXMPh9NG1+007TBHcaprdhoT2dz5kbKkvkoIZ3tnt7hpyuwA8b8Mfs/8A7Q3w2tLqL9mz46eEdS+GetXc+o6NZ+J1ttTjto7vCfarK8t/DvifTLiSF1PnXWmPp1rqEkX2ifTTMzQUAeD/ABZ+D2s+D/iv8BI/G3jmT4g/GP4kfEjSb/xNqiiSKy0nRrLW/DmnaPZ6daFYdmnedPqTrcfY7CHy9ONva2FnDZuZQD9FP2kfh18Rvid4Bs/Dvwv8VQeD/EUHibT9Un1W41zXfD6SaVbWGrW9zZC+8PWV/fO01xeWcot3hW2cQGSSQPFErAG5oXgnxvp/wKj8Aah4hiuviCvw91Lw23iddW1eaE+JLrSLuztdW/tqe1TWykN7NDcG+Nn9vjCebHA0qojAHGfsyfC74p/Czw14j0v4q+Mrfxnqmp65Ff6ZeW/iHxF4iWzsEsILd7ZrjxHYafcW7NcRyS+TbpJCQ28uHYrQB9L0AFAHynB8DPFsX7V178dW1Hw6fCNz4WTRE05bvUz4jF2uh2mmGR7M6QNMFv59u7711hpPJKt5IcsigH1ZQBzXjTRrrxH4O8WeHrGSCK917w1rujWkt08kdrHdappd1Y28ly8MU8qQJLOjTPFBNIsYYpFIwCMAePfsw/CXxH8FfhZbeCPFN7ol/q0OuaxqbXHh+5v7rTjBqEkLQosuo6bpNyZlEbeaptFRSRskfJNAHMftQ/CL4v8AxXtvBcXwn8c23gqXQ59ek1x7nxN4n8Of2jHqEekLpyo3hvTdQa8+ytZ3hYXgiWD7QDBvMsuwA9p8VfDvSPiB8N7v4d+NE+32eq6DaaZqVzFIZJ4tQtoIDDq9jcXMZb7bZajBHqFncTwnM8UbTwuhlicA+NPD3wX/AGxfg7ZP4R+F3xL8CeJvA1vPMugw+LbeaLU9It7hzI2LefSL9bSKOR2kFpa61qFn5hklhsYTM8VAHbfCj9l7xZb/ABHi+NPx88bWnxB+INmpGiWGmwyjw/oUse8Wl1BJPa6asjWKTXDafp9ro2nWGn3ksl+v2q8MVxEAaHxv/Zk8Q+KfHtj8Zvg14yh+H/xUs4YILya8jmOj6+Le3WxhnvZoLfUHtpxpirp11FLpWqWGp2UFrBcWsDRSTzgHn+ufBr9sb4uacPCHxP8Aid4E8NeCbmaKHX08I2sz6vrVnG+5w0cGkaek8EwGGs5tV060kOw3VjMqbGAPsjwN8MfCXw/+H+n/AA10SxL+GrLTLnTbiK8KS3GrLqAl/tW61SWKOBZ7vVJLieS7eOOGMeaYoIoII4YkAPjHTf2d/wBpD4Halrdt+z18QfDF94C1i+n1GHwp42jk+06XczEInkmTT7+3mlgt0igl1K3v9LfUViiN7prvDG6gHXfCv9mf4gXPxOtvjX+0N4z0/wAZ+NNJjRPDOi6Mj/2JokkKulpcsTY6VaobASyzWen6fpkdumpStq095d3252APoL43/Bvw98cvAl54K16aSwk+0w6lomt28KXF1omsWqyJBexQSPElxFJBPcWd5atLF9os7mZI5oJ/IuIgD5Q0j4Xft0+E/D9t8PvD/wATPhrceHbKy/srSPEl79qOs6VpMKfZ7W3E0/he5u0mgttqW5aLVJbRUjjgvwsMTqAe+fs3fs76Z8BPD2ppNqn/AAknjPxPcQXnifxE0LxJK0CyG30zT1mkln+w2s1xdTNcXDC61C5uJLq5SJRb2lqAfSVAHyn+y38DPFvwRs/iFb+K9R8O6g/izxTHrenHw9d6ndrDaJFcIY706lpGktHcZmXCQLcx4BzNwBQB7v8AEXwDoHxP8F6/4F8TQvLpHiCyNtLJFtFzZ3EbpcWOo2burol5p17DBeWxkV4jLCqTRyQs8bgHxB4Y+C37ZPwg0x/A/wAMviP8PNa8ERXN2NBn8SWs8epaJb3U0lw8n2W40TURah5pHmNlDf63aRzSSyQwr5rrQB69+zr+zTd/CnWfEnxF8feJIvG3xV8Xm4Gq6zBHN/Z+mxXl19s1CLTprqK3ubufUriO3kur2WysBHDbwWNnZW0C3D3YB9bUAFABQAUAFABQAUAf/9f+/igAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAOM8cfD/wn8R9MsdE8Z6WNa0ax1iy1waXLcXMNleXunJcC0TUobaaEahZRvcGaTT7oyWdxLFD9pgmRPLYA7FESJEjjRY441VI40UIkaIAqIiqAqqqgKqqAAAAAAMUAOoAKACgAoAKACgAoAKAOf8AFfhjRvGnhrXPCfiG2N5oniHTLvSdTt1keF3tbyJopDDNHiSCePcJbeeM74ZkjlTDItAHxJoP7O/7TXwntpfDPwb+Ovh9/Awnnl0zSvHOhLPd6Kl1NJNJHZN/Y3iGLcjyNM620mnafd3bzXL6ZbvO9AHd/Cf9mLWNB+Ib/GP4wePrj4m/EqOGS30mcWiWWhaHFLbval7S1ZF3TQwz3UdjHa22ladYC6uZY9PkvJEuogD7AoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD//0P7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKAP/2Q==', null, '', 'standard', '642B4DC3-EF8E-4A12-A94B-0CC7B1B83C47', '355', '2015-12-21 10:11:37', '1', '', '', '004');

-- ----------------------------
-- Table structure for `zse_v1_orders_archive`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_orders_archive`;
CREATE TABLE `zse_v1_orders_archive` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `record_code` varchar(32) DEFAULT '',
  `record_destination_code` varchar(64) DEFAULT '',
  `ipad_udid` varchar(255) NOT NULL DEFAULT '',
  `agent_code` varchar(255) NOT NULL DEFAULT '',
  `date_access` datetime DEFAULT '0000-00-00 00:00:00',
  `date_order` datetime DEFAULT '0000-00-00 00:00:00',
  `status` varchar(32) NOT NULL DEFAULT 'nuovo',
  `management_status` varchar(32) NOT NULL DEFAULT 'nuovo',
  `inserted_by` varchar(64) NOT NULL DEFAULT '',
  `order_code` varchar(64) NOT NULL DEFAULT '',
  `management_code` varchar(128) DEFAULT '',
  `management_external_ref` varchar(128) DEFAULT NULL,
  `subject` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `data` mediumtext,
  `sign_image` mediumtext,
  `link_pdf` varchar(255) DEFAULT NULL,
  `transfer_message` varchar(255) DEFAULT '',
  `type` varchar(64) NOT NULL DEFAULT 'standard',
  `entity_id` varchar(64) NOT NULL DEFAULT '',
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `parent_id` varchar(64) NOT NULL DEFAULT '',
  `document_type` varchar(64) NOT NULL DEFAULT '',
  `document_type_id` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_order`),
  UNIQUE KEY `codice_ordine` (`order_code`),
  KEY `codice_agente` (`agent_code`),
  KEY `codice_gestionale` (`management_code`),
  KEY `record_code` (`record_code`),
  KEY `record_destination_code` (`record_destination_code`),
  KEY `record_code_and_destination` (`record_code`,`record_destination_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_orders_archive
-- ----------------------------

-- ----------------------------
-- Table structure for `zse_v1_records`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_records`;
CREATE TABLE `zse_v1_records` (
  `id_record` int(11) NOT NULL AUTO_INCREMENT,
  `id_locality` int(11) NOT NULL DEFAULT '0',
  `id_classification` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL DEFAULT '',
  `code_destination` varchar(64) NOT NULL DEFAULT '',
  `business_name` varchar(255) NOT NULL DEFAULT '',
  `legal_status` varchar(16) NOT NULL DEFAULT '',
  `type` varchar(64) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(64) NOT NULL DEFAULT '',
  `phone` varchar(32) NOT NULL DEFAULT '',
  `fax` varchar(32) NOT NULL DEFAULT '',
  `mobile` varchar(32) NOT NULL DEFAULT '',
  `inserted_by` varchar(64) NOT NULL DEFAULT '',
  `assigned_to` varchar(64) NOT NULL DEFAULT '',
  `origin` varchar(16) NOT NULL DEFAULT 'web',
  `is_contact` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_customer` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_supplier` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_prospect` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_destination` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_reseller` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_intrested` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `authorize_send` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `coordinates` varchar(128) NOT NULL DEFAULT '',
  `external_code` varchar(64) NOT NULL DEFAULT '',
  `icon` varchar(255) NOT NULL DEFAULT 'organizzazione_default.jpeg',
  `country` varchar(64) NOT NULL DEFAULT '',
  `province` varchar(64) NOT NULL DEFAULT '',
  `city` varchar(64) NOT NULL DEFAULT '',
  `postal_code` varchar(8) NOT NULL DEFAULT '',
  `vat_code` varchar(11) NOT NULL DEFAULT '',
  `fiscal_code` varchar(16) NOT NULL DEFAULT '',
  `pricelist` varchar(20) NOT NULL DEFAULT '',
  `options` text,
  `person_in_charge` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `direct_mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `direct_phone` varchar(255) DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_record`),
  KEY `id_anagrafica` (`id_record`,`id_locality`),
  KEY `id_localita` (`id_locality`),
  KEY `codice` (`code`),
  KEY `codice_destinazione` (`code_destination`),
  KEY `id_classification` (`id_classification`),
  KEY `code_and_destination` (`code`,`code_destination`),
  CONSTRAINT `zse_v1_records_ibfk_1` FOREIGN KEY (`id_classification`) REFERENCES `zse_v1_records_classifications` (`id_classification`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_records
-- ----------------------------
INSERT INTO `zse_v1_records` VALUES ('1', '0', '1', 'C054', '', 'Anagrafica Test Maggiorazione Caso 8', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '22.8167,89.55', 'C054', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G54\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('2', '0', '1', 'C053', '', 'Anagrafica Test Maggiorazione Caso 7', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '45.73766,11.40737', 'C053', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G53\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('3', '0', '1', 'C052', '', 'Anagrafica Test Maggiorazione Caso 6', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '45.7134505,11.357168', 'C052', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('4', '0', '1', 'C051', '', 'Anagrafica Test Maggiorazione Caso 5', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7100835,11.3547951', 'C051', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('5', '0', '1', 'C050', '', 'Anagrafica Test Maggiorazione Caso 4', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.7120527,11.3622259', 'C050', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('6', '0', '1', 'C049', '', 'Anagrafica Test Maggiorazione Caso 3', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.7107216,11.3528983', 'C049', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('7', '0', '1', 'C048', '', 'Anagrafica Test Maggiorazione Caso 2', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.7174929,11.3652977', 'C048', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('8', '0', '1', 'C047', '', 'Anagrafica Test Maggiorazione Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.716093,11.3475367', 'C047', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('9', '0', '1', 'C056', '', 'Anagrafica Test Maggiorazione Caso 10', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.7143194,11.3581171', 'C056', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G56\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('10', '0', '1', 'C055', '', 'Anagrafica Test Maggiorazione Caso 9', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '45.7094334,11.3655718', 'C055', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G55\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('11', '0', '1', 'C004', '', 'Anagrafica Test Sconto Caso 4', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C004', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('12', '0', '1', 'C003', '', 'Anagrafica Test Sconto Caso 3', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C003', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('13', '0', '1', 'C002', '', 'Anagrafica Test Sconto Caso 2', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C002', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('14', '0', '1', 'C001', '', 'Anagrafica Test Sconto Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C001', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('15', '0', '1', 'C005', '', 'Anagrafica Test Sconto Caso 5', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C005', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('16', '0', '1', 'C006', '', 'Anagrafica Test Sconto Caso 6', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C006', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('17', '0', '1', 'C007', '', 'Anagrafica Test Sconto Caso 7', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C007', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('18', '0', '1', 'C008', '', 'Anagrafica Test Sconto Caso 8', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C008', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('19', '0', '1', 'C009', '', 'Anagrafica Test Sconto Caso 9', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C009', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G09\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('20', '0', '1', 'C010', '', 'Anagrafica Test Sconto Caso 10', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C010', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G10\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('21', '0', '1', 'C011', '', 'Anagrafica Test Sconto Caso 11', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C011', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G11\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('22', '0', '1', 'C012', '', 'Anagrafica Test Sconto Caso 12', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C012', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G12\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('23', '0', '1', 'C013', '', 'Anagrafica Test Sconto Caso 13', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C013', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G13\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('24', '0', '1', 'C014', '', 'Anagrafica Test Sconto Caso 14', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C014', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G14\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('25', '0', '1', 'C015', '', 'Anagrafica Test Sconto Caso 15', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C015', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G15\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('26', '0', '1', 'C016', '', 'Anagrafica Test Sconto Caso 16', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C016', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G16\"]}', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('27', '0', '1', 'C017', '', 'Anagrafica Test Sconto Caso 21', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C017', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('28', '0', '1', 'C018', '', 'Anagrafica Test Sconto Caso 22', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '1', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C018', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('29', '0', '1', 'C019', '', 'Anagrafica Test Sconto Caso 23', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C019', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('30', '0', '1', 'C020', '', 'Anagrafica Test Sconto Caso 24', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C020', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('31', '0', '1', 'C021', '', 'Anagrafica Test Sconto Caso 25', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C021', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G21\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('32', '0', '1', 'C022', '', 'Anagrafica Test Sconto Caso 26', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C022', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G22\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('33', '0', '1', 'C023', '', 'Anagrafica Test Sconto Caso 27', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C023', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('34', '0', '1', 'C058', '', 'Anagrafica Test Sconto Caso 17', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C058', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('35', '0', '1', 'C024', '', 'Anagrafica Test Sconto Caso 28', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C024', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('36', '0', '1', 'C059', '', 'Anagrafica Test Sconto Caso 18', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C059', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('37', '0', '1', 'C060', '', 'Anagrafica Test Sconto Caso 19', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C060', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('38', '0', '1', 'C061', '', 'Anagrafica Test Sconto Caso 20', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C061', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('39', '0', '2', 'nuovo_cliente_01', '', 'Anagrafica Test Nuovo Cliente Caso 1', '', '', '', '', '', '', '', '', '', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '44.200797,24.5022981', 'nuovo_cliente_01', 'organizzazione_default.jpg', '', '', '', '', '', '', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('40', '0', '2', 'nuovo_cliente_02', '', 'Nuovo Cliente con Listino 002', '', '', '', '', '', '', '', '', '', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '44.200797,24.5022981', 'nuovo_cliente_02', 'organizzazione_default.jpg', '', '', '', '0', '0', '', 'L002', '', null, null, null, null, '2016-03-20 12:56:05', '0');
INSERT INTO `zse_v1_records` VALUES ('41', '0', '1', 'C025', '', 'Anagrafica Test Prezzi Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C025', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('42', '0', '1', 'C040', '', 'Anagrafica Test Prezzi Caso 16', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C040', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('43', '0', '1', 'C039', '', 'Anagrafica Test Prezzi Caso 15', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C039', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('44', '0', '1', 'C038', '', 'Anagrafica Test Prezzi Caso 14', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C038', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('45', '0', '1', 'C037', '', 'Anagrafica Test Prezzi Caso 13', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C037', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('46', '0', '1', 'C036', '', 'Anagrafica Test Prezzi Caso 12', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C036', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G36\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('47', '0', '1', 'C035', '', 'Anagrafica Test Prezzi Caso 11', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C035', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G35\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('48', '0', '1', 'C034', '', 'Anagrafica Test Prezzi Caso 10', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C034', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G34\"]}', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('49', '0', '1', 'C033', '', 'Anagrafica Test Prezzi Caso 9', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C033', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G33\"]}', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('50', '0', '1', 'C032', '', 'Anagrafica Test Prezzi Caso 8', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C032', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('51', '0', '1', 'C031', '', 'Anagrafica Test Prezzi Caso 7', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C031', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('52', '0', '1', 'C030', '', 'Anagrafica Test Prezzi Caso 6', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C030', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('53', '0', '1', 'C029', '', 'Anagrafica Test Prezzi Caso 5', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C029', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('54', '0', '1', 'C028', '', 'Anagrafica Test Prezzi Caso 4', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '0', '1', '0', '1', '0', '47.6112823,-122.2065124', 'C028', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('55', '0', '1', 'C027', '', 'Anagrafica Test Prezzi Caso 3', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C027', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('56', '0', '1', 'C026', '', 'Anagrafica Test Prezzi Caso 2', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C026', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('57', '0', '1', 'C057', '', 'Anagrafica Test Supplemento Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C057', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('58', '0', '1', 'C041', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C041', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('59', '0', '1', 'C043', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 3', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C043', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G43\"]}', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('60', '0', '1', 'C044', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 4', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C044', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"gruppi\":[\"G44\"]}', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('61', '0', '1', 'C045', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 5', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C045', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('62', '0', '1', 'C046', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 6', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C046', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('63', '0', '1', 'C042', '', 'Anagrafica Test Sconto Gruppi Articolo Cascata Caso 2', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C042', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('64', '0', '1', 'C062', '', 'Anagrafica Test Dati Aggiuntivi Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '0', '0', '0', '1', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C062', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"note_aggiuntive\":[{\"field_id\":\"FATTURATO\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Fatturato :\",\"valore\":\"Descrizione Linea   Anno corrente    Anno precedente\\\\\\\\n                    Qt\\u00e0     Valore    Qt\\u00e0     Valore\\\\\\\\\\\\\\\\nTotale Fatturato       0          0      0          0\"},{\"field_id\":\"RIM_DIR_ATTIVE\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Da pagare\",\"valore\":\"Ft.23531\\/06 del  2\\/09\\/15 Eur 29,61 Scd.12\\/09\\/15\\\\\\\\nFt.24546\\/06 del 11\\/09\\/15 Eur 13,14 Scd.21\\/09\\/15\\\\\\\\nFt.25028\\/06 del 18\\/09\\/15 Eur 52,19 Scd.28\\/09\\/15\\\\\\\\nFt.25281\\/06 del 22\\/09\\/15 Eur 20,44 Scd. 2\\/10\\/15\\\\\\\\nFt.26528\\/06 del  9\\/10\\/15 Eur 28,84 Scd.19\\/10\\/15\\\\\\\\n\"},{\"field_id\":\"SCADENZE\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Scadenzario :\",\"valore\":\"Scadenza N.Doc   Data     Tipo    Rate      Importo  Nt\"}],\"contesti_note_aggiuntive\":{\"S\":[\"FATTURATO\",\"RIM_DIR_ATTIVE\",\"SCADENZE\"]}}', null, null, null, null, '2016-03-20 12:56:19', '1');
INSERT INTO `zse_v1_records` VALUES ('65', '0', '1', 'C063', '', 'Anagrafica Test File Allegati Caso 1', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C063', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('66', '0', '1', 'C073', '', 'Anagrafica Test Geolocalizzazione Caso 10', '', '', 'Via Garibaldi, 1', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7143194,11.3581171', 'C073', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('67', '0', '1', 'C072', '', 'Anagrafica Test Geolocalizzazione Caso 9', '', '', 'Via Paraiso,5', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7094334,11.3655718', 'C072', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('68', '0', '1', 'C071', '', 'Anagrafica Test Geolocalizzazione Caso 8', '', '', 'Via Fogazzaro,32', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7103982,11.3645201', 'C071', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('69', '0', '1', 'C070', '', 'Anagrafica Test Geolocalizzazione Caso 7', '', '', 'Via Martiri della libertà ,18', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.73766,11.40737', 'C070', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('70', '0', '1', 'C069', '', 'Anagrafica Test Geolocalizzazione Caso 6', '', '', 'Via Fratelli Pasini, 21', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7134505,11.357168', 'C069', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('71', '0', '1', 'C068', '', 'Anagrafica Test Geolocalizzazione Caso 5', '', '', 'Viale Trento e Trieste, 31', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7100835,11.3547951', 'C068', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('72', '0', '1', 'C067', '', 'Anagrafica Test Geolocalizzazione Caso 4', '', '', 'Via Daniele Manin, 25', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7120527,11.3622259', 'C067', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('73', '0', '1', 'C066', '', 'Anagrafica Test Geolocalizzazione Caso 3', '', '', 'Via Cimatori, 41', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7107216,11.3528983', 'C066', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('74', '0', '1', 'C065', '', 'Anagrafica Test Geolocalizzazione Caso 2', '', '', 'Via Giorgione, 15', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.7174929,11.3652977', 'C065', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('75', '0', '1', 'C064', '', 'Anagrafica Test Geolocalizzazione Caso 1', '', '', 'Via Rovereto, 51', 'test@examplemail.com', '011/4634234', '', '', '', 'VI', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '45.716093,11.3475367', 'C064', 'organizzazione_default.jpg', 'IT', 'VI', 'Schio', '10024', '01234556755', '01234556755', 'L001', '', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('76', '0', '1', 'C074', '', 'Anagrafica Test Dati Aggiuntivi Caso 2', '', '', '1St Street', 'test@examplemail.com', '011/4634234', '', '', '', 'WA', 'importazione', '1', '0', '0', '0', '0', '0', '1', '0', '47.6112823,-122.2065124', 'C074', 'organizzazione_default.jpg', 'USA', 'WA', 'Seattle', '10024', '01234556755', '01234556755', 'L001', '{\"note_aggiuntive\":[{\"field_id\":\"FATTURATO\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Fatturato :\",\"valore\":\"Descrizione Linea   Anno corrente    Anno precedente\\\\\\\\n                    Qt\\u00e0     Valore    Qt\\u00e0     Valore\\\\\\\\\\\\\\\\nTotale Fatturato       0          0      0          0\"},{\"field_id\":\"RIM_DIR_ATTIVE\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Da pagare\",\"valore\":\"Ft.23531\\/06 del  2\\/09\\/15 Eur 29,61 Scd.12\\/09\\/15\\\\\\\\nFt.24546\\/06 del 11\\/09\\/15 Eur 13,14 Scd.21\\/09\\/15\\\\\\\\nFt.25028\\/06 del 18\\/09\\/15 Eur 52,19 Scd.28\\/09\\/15\\\\\\\\nFt.25281\\/06 del 22\\/09\\/15 Eur 20,44 Scd. 2\\/10\\/15\\\\\\\\nFt.26528\\/06 del  9\\/10\\/15 Eur 28,84 Scd.19\\/10\\/15\\\\\\\\n\"},{\"field_id\":\"SCADENZE\",\"tipo\":\"TESTO_ESTESO\",\"label\":\"Scadenzario :\",\"valore\":\"Scadenza N.Doc   Data     Tipo    Rate      Importo  Nt\"}],\"contesti_note_aggiuntive\":{\"S\":[\"FATTURATO\",\"RIM_DIR_ATTIVE\",\"SCADENZE\"]}}', null, null, null, null, '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('77', '34534', '2', 'C075', '', 'Contact Test 1 Person', 'test 1', '', '1St Street', 'ertertret', '345345345', 'dfgdfg', '345345345', 'dfgdgdfg', 'dfgdfgdfg', 'dfgdfg', '1', '0', '0', '0', '0', '0', '1', '255', '47.6112823,-122.2065124', 'we43', 'woman.png', '', 'werwer', 'werwer', 'werwer', 'werwe', 'werew', '', '', 'werewr', 'werwe', 'rwerwer', '3453453', '2016-03-20 12:55:33', '127');
INSERT INTO `zse_v1_records` VALUES ('84', '0', '1', 'C076', '', 'Contact Test 2 Company', 'test 2', '', '1St Street', '', '', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', 'organizzazione_default.jpeg', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('85', '0', '1', 'C077', '', 'Contact Test 3 Company', 'test 3', '', '1St Street', '', '', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', 'organizzazione_default.jpeg', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('88', '0', '6', 'C078', '', 'Contact Test 5 Company', 'test 4', '', '', '', '', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', 'organizzazione_default.jpeg', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('89', '0', '11', 'C079', '', 'Contact Test 6 Company', 'test 5', '', '', 'ertertret@f.com', '67676', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', 'organizzazione_default.jpeg', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('90', '0', '13', 'C080', '', 'Contact Test 7 Company', 'test 6', '', '', 'ertertret', '345345', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', '', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('91', '0', '13', 'C081', '', 'Contact Test 8 Company', 'test 7', '', '', 'ertertret', '345345', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', '', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('92', '0', '13', 'C082', '', 'Contact Test 9 Company', 'test 8', '', '', 'ertertret', '345345', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', '', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('93', '0', '11', 'C083', '', 'Contact Test 10 Company', 'test 9', '', '', 'ertertret@f.com', '23423423', '', '', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', '', 'organizzazione_default.jpeg', '', '', '', '', '', '', '', null, '', null, '', '', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('94', '0', '2', 'C084', '', 'Contact Test 11 Company', 'test 10', '', 'address 1', 'ertertret@f.com', '345345', 'dfgdfg', '23423432', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '22.845641,89.5403279', 'we43', 'organizzazione_default.jpeg', 'China', 'dsadsa', 'dsd', 'SP11 0JE', 'werwe', 'werew', 'Listino test sconto', null, 'werewr', null, 'rwerwer', '3453453', '2016-03-20 12:55:51', '1');
INSERT INTO `zse_v1_records` VALUES ('95', '0', '12', 'C085', '', 'Contact Test 12 Person', 'test 11', '', 'fsdf', 'ertertret@f.com', 'fdg', 'sadf', '23423432', '', '', 'web', '1', '0', '0', '0', '0', '0', '1', '0', '0,0', 'we43', 'organizzazione_default.jpeg', 'France', 'dsadsa', 'sdfq', 'fdsf', 'werwe', 'werew', 'Listino test sconto', '', 'sdf', null, 'rwerwer', '3453453', '2016-03-20 12:55:33', '1');
INSERT INTO `zse_v1_records` VALUES ('96', '0', '3', 'C086', '', 'Contact Test 13 Destination', 'test 12', '', 'address 1', 'ertertret@f.com', '345345', 'dfgdfg', '23423432', '', '', 'web', '0', '0', '0', '0', '1', '0', '1', '0', '', 'we43', 'organizzazione_default.jpeg', 'Mexico', 'dsadsa', 'test', 'ng1 1lx', 'werwe', 'werew', 'Listino netto caso 1', '{\"contesti_note_aggiuntive\":{\"S\":[\"sdasfgasd\",\"qwert\"]},\"note_aggiuntive\":[{\"field_id\":\"sdasfgasd\",\"label\":\"adadfsa\",\"tipo\":\"TESTO\",\"valore\":\"fasd fas df sd fds fa \"},{\"field_id\":\"qwert\",\"label\":\"qwert\",\"tipo\":\"TESTO_ESTESO\",\"valore\":\"asd fasd fsda f rewadsf wefsdafwerfsdfsda \"}]}', 'werewr', null, 'rwerwer', '3453453', '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('97', '0', '40', 'C087', '', 'Contact Test 14 Destination', 'test 13', '', 'address 1', 'ertertret@f.com', '345345', 'dfgdfg', '23423432', '', '', 'web', '0', '0', '0', '0', '1', '0', '1', '0', '', 'we43', 'organizzazione_default.jpeg', 'India', 'dsadsa', 'dsd', 'SP11 0JE', 'werwe', 'werew', 'Listino test sconto', '{\"contesti_note_aggiuntive\":{\"S\":[\"hizibizi\",\"hizibizi2\",\"hjsadfkj\",\"CONDIZIONE_DI_PAGAMENTO\",\"CONDIZIONE_DI_RESA\",\"CONDIZIONE_IVA\"]},\"note_aggiuntive\":[{\"field_id\":\"hizibizi\",\"label\":\"hizibizi\",\"tipo\":\"TESTO\",\"valore\":\"hizibizi hizibizi hizibizi hizibizi\"},{\"field_id\":\"hizibizi2\",\"label\":\"hizibizi2\",\"tipo\":\"TESTO_ESTESO\",\"valore\":\"hizibizi hizibizi hizibizi\\r\\nhizibizi hizibizi\\r\\nhizibizi\"},{\"field_id\":\"hjsadfkj\",\"label\":\"dsfdasf\",\"tipo\":\"TESTO\",\"valore\":\"hizibizi hizibizi hizibizi hizibizi\"},{\"field_id\":\"CONDIZIONE_DI_PAGAMENTO\",\"label\":\"Tipo Pagamento\",\"tipo\":\"TESTO\",\"valore\":\"B40\"},{\"field_id\":\"CONDIZIONE_DI_RESA\",\"label\":\"PORTO ASSEGNATO\",\"tipo\":\"TESTO\",\"valore\":\"20\"},{\"field_id\":\"CONDIZIONE_IVA\",\"label\":\"Condizione Iva\",\"tipo\":\"TESTO\",\"valore\":\"20.0\"}],\"dati_convenzionati\":{\"CONDIZIONE_DI_PAGAMENTO\":\"B40\",\"CONDIZIONE_DI_RESA\":\"20\"},\"gruppi\":[\"g10\"]}', 'werewr', null, 'rwerwer', '3453453', '2016-03-20 12:56:05', '1');
INSERT INTO `zse_v1_records` VALUES ('98', '0', '10', 'C088', '', 'Contact Test 15 Company', 'test 14', '', '1 The Mount, Acton High Street, Acton, London', 'ertertret@f.com', '345345', 'dfgdfg', '23423432', '', '', 'web', '0', '1', '0', '0', '0', '0', '1', '0', '', 'we43', 'organizzazione_default.jpeg', 'India', 'dsadsa', 'test', 'SP11 0JE', 'werwe', 'werew', 'Listino test sconto', '{\"contesti_note_aggiuntive\":{\"S\":[\"test\",\"test2\",\"CONDIZIONE_DI_PAGAMENTO\",\"CONDIZIONE_DI_RESA\",\"CONDIZIONE_IVA\"]},\"note_aggiuntive\":[{\"field_id\":\"test\",\"label\":\"test\",\"tipo\":\"TESTO\",\"valore\":\"test test test\"},{\"field_id\":\"test2\",\"label\":\"test2\",\"tipo\":\"TESTO_ESTESO\",\"valore\":\"Enter Value Here (You can use \\\\n as return.)\\r\\ntest test test\"},{\"field_id\":\"CONDIZIONE_DI_PAGAMENTO\",\"label\":\"Tipo Pagamento\",\"tipo\":\"TESTO\",\"valore\":\"B32\"},{\"field_id\":\"CONDIZIONE_DI_RESA\",\"label\":\"PORTO FRANCO\",\"tipo\":\"TESTO\",\"valore\":\"10\"},{\"field_id\":\"CONDIZIONE_IVA\",\"label\":\"Condizione Iva\",\"tipo\":\"TESTO\",\"valore\":\"10.0\"}],\"dati_convenzionati\":{\"CONDIZIONE_DI_PAGAMENTO\":\"B32\",\"CONDIZIONE_DI_RESA\":\"10\"},\"gruppi\":[\"g100\"]}', 'werewr', null, 'rwerwer', '3453453', '2016-03-20 12:55:51', '1');

-- ----------------------------
-- Table structure for `zse_v1_records_agents`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_records_agents`;
CREATE TABLE `zse_v1_records_agents` (
  `record_code` varchar(32) NOT NULL DEFAULT '0',
  `destination_code` varchar(64) NOT NULL DEFAULT '0',
  `agent_code` varchar(10) NOT NULL DEFAULT '0',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`record_code`,`destination_code`,`agent_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_records_agents
-- ----------------------------
INSERT INTO `zse_v1_records_agents` VALUES ('C002', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C004', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C019', '0', '000', '2016-02-26 11:40:39', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C020', '0', '000', '2016-02-26 11:40:39', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C026', '0', '000', '2016-02-26 11:50:37', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C026', '0', '001', '2016-03-14 19:24:33', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C027', '0', '000', '2016-02-26 11:50:37', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C027', '0', '001', '2016-03-14 19:24:33', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C041', '0', '001', '2016-03-14 19:24:33', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C047', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C048', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C049', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C050', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C050', '0', '001', '2016-02-26 11:40:50', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C052', '0', '000', '2016-02-26 11:50:37', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C052', '0', '001', '2016-03-14 19:24:33', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C053', '0', '000', '2016-02-26 11:40:39', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C054', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C054', '0', '001', '2016-02-26 11:40:50', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C055', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C056', '0', '000', '2016-03-14 19:10:14', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C057', '0', '001', '2016-03-14 19:24:33', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C070', '', '000', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C070', '', '001', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C071', '', '000', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C071', '', '001', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C072', '', '000', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C072', '', '001', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C073', '', '000', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C073', '', '001', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C074', '', '000', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C074', '', '001', '2015-12-21 10:22:38', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C086', '0', '000', '2016-03-19 13:02:28', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C087', '0', '001', '2016-03-19 13:02:06', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('C088', '0', '001', '2016-03-19 13:01:59', '1');
INSERT INTO `zse_v1_records_agents` VALUES ('nuovo_cliente_01', '', '001', '2015-12-21 10:22:38', '1');

-- ----------------------------
-- Table structure for `zse_v1_records_classifications`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_records_classifications`;
CREATE TABLE `zse_v1_records_classifications` (
  `id_classification` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `color` varchar(68) DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_classification`),
  UNIQUE KEY `label` (`label`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_records_classifications
-- ----------------------------
INSERT INTO `zse_v1_records_classifications` VALUES ('1', 'gggggg', 'gggggg', null, '2016-02-28 14:11:18', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('2', 'pppppppppppppp', 'pppppppppppppp', null, '2016-02-28 14:10:54', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('3', 'FERRAMENTA', 'FERRAMENTA', null, '2016-02-28 14:13:17', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('4', 'Arredamento', 'Arredamento', null, '2016-02-28 11:50:27', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('5', 'dddddddd', 'dddddddd', null, '2016-02-28 12:49:35', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('6', 'Alimentazione', 'Alimentazione', null, '2016-02-28 14:10:52', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('7', 'Audio e Hi Fi', 'Audio e Hi Fi', null, '2016-02-28 11:53:25', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('8', 'Settore Meccanico', 'Settore Meccanico', null, '2016-02-28 12:51:54', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('9', 'Settore Orafo', 'Settore Orafo', null, '2016-02-28 11:49:08', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('10', 'Formazione', 'Formazione', '#7FFFD4', '2016-03-04 16:39:42', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('11', 'Test Destinazioni', 'Test Destinazioni', '#000080', '2016-03-04 16:39:50', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('12', 'test', 'test', '#EE82EE', '2016-03-04 16:39:55', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('13', 'test3', '', null, '2016-02-28 14:10:46', '0');
INSERT INTO `zse_v1_records_classifications` VALUES ('36', 'en', 'en', null, '2016-02-28 00:00:00', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('37', 'bn', 'bn', null, '2016-02-28 00:00:00', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('38', 'xdvxvxcvx', 'xcvxcvxcvxcvxcv', null, '2016-02-28 00:00:00', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('39', 'ddddddddddd', 'ddddddddddd', '#008000', '2016-03-04 16:40:02', '1');
INSERT INTO `zse_v1_records_classifications` VALUES ('40', 'sfda', 'sfda', '#004080', '2016-03-14 18:59:14', '1');

-- ----------------------------
-- Table structure for `zse_v1_records_links`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_records_links`;
CREATE TABLE `zse_v1_records_links` (
  `id_record` int(11) NOT NULL DEFAULT '0',
  `id_link` int(11) NOT NULL DEFAULT '0',
  `role` varchar(255) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_record`,`id_link`),
  KEY `id_link` (`id_link`),
  CONSTRAINT `zse_v1_records_links_ibfk_1` FOREIGN KEY (`id_record`) REFERENCES `zse_v1_records` (`id_record`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `zse_v1_records_links_ibfk_2` FOREIGN KEY (`id_link`) REFERENCES `zse_v1_records` (`id_record`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_records_links
-- ----------------------------
INSERT INTO `zse_v1_records_links` VALUES ('1', '1', '', '2016-03-06 18:20:26', '1');

-- ----------------------------
-- Table structure for `zse_v1_records_recurrings`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_records_recurrings`;
CREATE TABLE `zse_v1_records_recurrings` (
  `id_recurring` int(11) NOT NULL AUTO_INCREMENT,
  `record_code` varchar(32) NOT NULL DEFAULT '',
  `card_code` varchar(128) NOT NULL DEFAULT '',
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  `ordered` text,
  PRIMARY KEY (`id_recurring`),
  UNIQUE KEY `record_code` (`record_code`,`card_code`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_records_recurrings
-- ----------------------------
INSERT INTO `zse_v1_records_recurrings` VALUES ('1', 'C001', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('2', 'C002', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('3', 'C003', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('4', 'C004', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('5', 'C005', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('6', 'C005', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('7', 'C006', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('8', 'C007', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('9', 'C007', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('10', 'C008', 'A001', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('11', 'C008', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('12', 'C008', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('13', 'C009', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('14', 'C009', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('15', 'C010', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('16', 'C010', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('17', 'C011', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('18', 'C011', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('19', 'C012', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('20', 'C013', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('21', 'C014', 'A005', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('22', 'C014', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('23', 'C015', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('24', 'C016', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('25', 'C016', 'A020', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('26', 'C017', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('27', 'C017', 'A020', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('28', 'C018', 'A010', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('29', 'C018', 'A020', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('30', 'C019', 'A020', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('31', 'C019', 'A024', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('32', 'C020', 'A020', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('33', 'C021', 'A024', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');
INSERT INTO `zse_v1_records_recurrings` VALUES ('34', 'C022', 'A024', '2015-11-02 17:25:22', '1', 'Ordinato : 99999 pz Valore 9999999 U$D ult.dt/ord : XX/XX/XXXX');

-- ----------------------------
-- Table structure for `zse_v1_reports_tree`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_reports_tree`;
CREATE TABLE `zse_v1_reports_tree` (
  `id_report_tree` int(11) NOT NULL AUTO_INCREMENT,
  `root` int(11) NOT NULL,
  `nleft` int(11) NOT NULL DEFAULT '0',
  `nright` int(11) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  `label` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `pattern` varchar(32) NOT NULL,
  `frequency` varchar(32) NOT NULL DEFAULT '',
  `limit` int(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_report_tree`),
  KEY `nleft` (`nleft`),
  KEY `nright` (`nright`),
  KEY `level` (`level`),
  KEY `root` (`root`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_reports_tree
-- ----------------------------
INSERT INTO `zse_v1_reports_tree` VALUES ('5', '5', '1', '4', '1', '', 'fatture', 'Da cancellare', '000', '', '0');
INSERT INTO `zse_v1_reports_tree` VALUES ('8', '5', '2', '3', '2', '', 'Provvigioni Mensili', '', 'PROVVMENS', 'aaaamm', '0');
INSERT INTO `zse_v1_reports_tree` VALUES ('9', '9', '1', '4', '1', '', 'fatture', 'Test Da cancellare', '002', '', '0');
INSERT INTO `zse_v1_reports_tree` VALUES ('10', '9', '2', '3', '2', '', 'Fatture acquisto 002', '', 'FATTURE_AQUISTO', 'aaaamm', '12');
INSERT INTO `zse_v1_reports_tree` VALUES ('11', '11', '1', '4', '1', '', 'Zona AR001', 'Test Da cancellare', 'AR001', '', '0');
INSERT INTO `zse_v1_reports_tree` VALUES ('12', '11', '2', '3', '2', '', 'FATTCRED', '', 'FATTCRED', 'aaaammgg', '30');
INSERT INTO `zse_v1_reports_tree` VALUES ('13', '13', '1', '8', '1', '', 'Agente 000', '', '000', '', '0');
INSERT INTO `zse_v1_reports_tree` VALUES ('14', '13', '2', '3', '2', '', 'Fatturato', 'Mensile', 'FATTMENS', 'aaaamm', '12');
INSERT INTO `zse_v1_reports_tree` VALUES ('15', '13', '4', '5', '2', '', 'Budget', 'Annuale', 'BUDGET', 'aaaa', '4');
INSERT INTO `zse_v1_reports_tree` VALUES ('16', '13', '6', '7', '2', '', 'Girovista', 'Settimanale', 'GIROVISITA', 'aaaammdd', '7');

-- ----------------------------
-- Table structure for `zse_v1_reports_tree_access`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_reports_tree_access`;
CREATE TABLE `zse_v1_reports_tree_access` (
  `id_report_tree` int(11) NOT NULL,
  `id_account` int(11) NOT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_report_tree`,`id_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_reports_tree_access
-- ----------------------------
INSERT INTO `zse_v1_reports_tree_access` VALUES ('5', '355', '2014-06-17 10:38:28', '1');
INSERT INTO `zse_v1_reports_tree_access` VALUES ('9', '357', '2015-06-08 10:24:40', '1');
INSERT INTO `zse_v1_reports_tree_access` VALUES ('11', '355', '2015-06-15 09:34:00', '1');
INSERT INTO `zse_v1_reports_tree_access` VALUES ('13', '355', '2015-12-16 10:00:41', '1');

-- ----------------------------
-- Table structure for `zse_v1_repository_files`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_repository_files`;
CREATE TABLE `zse_v1_repository_files` (
  `id_file` int(11) NOT NULL AUTO_INCREMENT,
  `mimetype` varchar(255) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `origin` tinyint(4) NOT NULL DEFAULT '1',
  `size` int(11) NOT NULL DEFAULT '0',
  `id_account` int(11) DEFAULT NULL,
  `date_insertion` datetime DEFAULT '0000-00-00 00:00:00',
  `entity_id` varchar(64) DEFAULT NULL,
  `date_modification` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `record_status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_file`),
  KEY `nome_file` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_repository_files
-- ----------------------------
INSERT INTO `zse_v1_repository_files` VALUES ('1', 'image/png', 'icona_a009.png', '2', '2302', null, '2014-02-13 09:28:16', '82eedd0cfd0ea0bac7f74688dc51129c869bd104', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('2', 'image/png', 'icona_a025.png', '2', '2302', null, '2014-02-13 09:28:16', 'f49833f7d1b844475b50b4cfd6cd100a382cf05d', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('3', 'application/msword', 'esempio_word.doc', '2', '25088', null, '2014-02-13 09:28:16', '21ff56e03717b256e7e071407ee162162d61c269', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('4', 'application/pdf', 'asd_vicenza_20131102.pdf', '2', '10658', null, '2014-02-13 09:28:16', '1eb866d1b059a7447762f29a6686234ca53e9cfe', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('5', 'image/png', 'BSK.png', '2', '32116', null, '2014-02-13 09:28:16', 'ef78cc682c9fe27413df83595689e82017ccae4b', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('6', 'image/png', 'cat_ico_003.png', '2', '2824', null, '2014-02-13 09:28:16', 'bc53edf4fd38d961a7949b68646fcb62f94fe602', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('7', 'image/png', 'cat002sub003.png', '2', '3445', null, '2014-02-13 09:28:16', 'de1db5171cb109bc15e6bdecf81f0f12247507e0', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('8', 'image/png', 'icona_a001.png', '2', '2302', null, '2014-02-13 09:28:16', '555f601d5dd98de287dc7d169de0a9b154770b5c', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('9', 'image/png', 'icona_a026.png', '2', '2302', null, '2014-02-13 09:28:16', '97aaac5b47aa4983d32212dd1169ed0ebaa9d2d1', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('10', 'image/png', 'galleria_a001_01.png', '2', '1904082', null, '2014-02-13 09:28:16', 'fa87a5bb634ed6b7742f8ba4738921814b4337a8', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('11', 'image/png', 'icona_a012.png', '2', '2302', null, '2014-02-13 09:28:16', '3a44d5a5a5b1ee649292824931ec20099fa1d96e', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('12', 'image/png', '025.png', '2', '1782217', null, '2014-02-13 09:28:16', '483d9fd80bfba28f30ca41f84e2ccb5150821a6b', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('13', 'image/png', '007.png', '2', '1782217', null, '2014-02-13 09:28:16', '5c78d84c2d9a939c05c3cfa1e5bf7c17e2f90d4c', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('14', 'application/pdf', 'asd_vicenza_20131101.pdf', '2', '10658', null, '2014-02-13 09:28:16', '22c9d6a60bde513f172fc75275c888cb7263cd6a', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('15', 'image/png', '005.png', '2', '1782217', null, '2014-02-13 09:28:16', '37cee8490078190a42cd3bbef03b4f3f17141a6d', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('16', 'image/png', '006.png', '2', '1782217', null, '2014-02-13 09:28:16', '71b708d42af73397077d5d784edbec30821bcfaf', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('17', 'image/png', 'c006001.png', '2', '2528', null, '2014-02-13 09:28:16', '98b9ef4bf86eb8ae39357eddce75e129d5ac9338', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('18', 'image/png', '026.png', '2', '1782217', null, '2014-02-13 09:28:16', '71cd9948978704b50e449c2f665d0917dcadd0e9', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('19', 'image/png', 'icona_a011.png', '2', '2302', null, '2014-02-13 09:28:16', '343685104adfc3f623ebecae6604b67c79de7672', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('20', 'image/png', 'cat001sub003.png', '2', '3313', null, '2014-02-13 09:28:16', '4c77d2c6de2a0b8f74a0411f4377ba2ee59006f4', '2014-02-13 09:28:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('21', 'image/png', '014.png', '2', '1782217', null, '2014-02-13 09:28:17', '2666e57045b25af0f9be4916257398117a614a45', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('22', 'image/png', '031.png', '2', '1782217', null, '2014-02-13 09:28:17', '0e6241aeb5ae0223e2f8cbf5228a667ce979c1d7', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('23', 'image/png', '011.png', '2', '1782217', null, '2014-02-13 09:28:17', '8ab93831b6b33d9255c894c3062bd3acd0d3d0cf', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('24', 'image/png', '027.png', '2', '1782217', null, '2014-02-13 09:28:17', '43f7e6477035a224b028117328e19d5fe296fe99', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('25', 'image/png', 'icona_a030.png', '2', '2302', null, '2014-02-13 09:28:17', '703ad6aba9e00ffc18e7a4940ef6066f9aba8c9d', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('26', 'text/plain', 'anagrafiche_opzioni_aggiuntive.csv', '2', '18344', null, '2014-02-13 09:28:17', '4889a5a1c7912116926291e487ff655d4a0218ed', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('27', 'image/png', 'r005001.png', '2', '2362', null, '2014-02-13 09:28:17', '9615cc65208c2a87c92e4a8c0a5df065ed06e39d', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('28', 'image/png', '002_aggiuntiva_00.png', '2', '1882470', null, '2014-02-13 09:28:17', '96ce3c12058d5c962a9f99bfecba3c04d2c1610c', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('29', 'image/png', '016.png', '2', '1782217', null, '2014-02-13 09:28:17', '1a07478cb9817b8a0a335ebf3926cf2a36c28a68', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('30', 'image/png', '032.png', '2', '1782217', null, '2014-02-13 09:28:17', 'a0559ba533e3b014de381089fe30dffa8b9f0bd1', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('31', 'image/png', 'cat_ico_002.png', '2', '2766', null, '2014-02-13 09:28:17', 'fd6e7b85a3b5baf53d830704ba76ac0fdccb6bf2', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('32', 'image/png', 'c005001.png', '2', '2528', null, '2014-02-13 09:28:17', 'b40621ddda977b31304ab6f48607d8b3abf28373', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('33', 'image/png', 'cat001sub002.png', '2', '3271', null, '2014-02-13 09:28:17', '91b6b3145a0c492e872bc5b9d8b06280d5ffee01', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('34', 'image/png', '002.png', '2', '1782217', null, '2014-02-13 09:28:17', 'e6164cd7a9a57edf140405726961050afe33b82b', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('35', 'application/vnd.ms-office', 'esempio_powerpoint.ppt', '2', '3412992', null, '2014-02-13 09:28:17', 'ee78a5cbdfc9de8ba713cffdd6705001ad9296e4', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('37', 'image/png', '001.png', '2', '1782217', null, '2014-02-13 09:28:17', 'd1c2d5b09912277d266a7f1fbc3831a1ee7b8e39', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('38', 'image/png', 'cat002sub002.png', '2', '3404', null, '2014-02-13 09:28:17', '845a40decccc940cb9f676354c18f247a951fc39', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('39', 'image/png', 'icona_a003.png', '2', '2302', null, '2014-02-13 09:28:17', 'd7ae0ff77b2affbccca60227766108323a93b7e4', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('40', 'image/png', '030.png', '2', '1782217', null, '2014-02-13 09:28:17', 'f691fd5a1cbefb5f4c24f3df1d591445e4f5ad7b', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('41', 'image/png', 'icona_a002.png', '2', '2302', null, '2014-02-13 09:28:17', 'eb6426cdd13edcecfaf3f0faa82d0b4bb64ef5bd', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('42', 'image/png', 'c006002.png', '2', '2528', null, '2014-02-13 09:28:17', '80e736fff98b36d6c04e89ce678f887d427a1ad9', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('43', 'image/png', 'galleria_a001_00.png', '2', '1909155', null, '2014-02-13 09:28:17', '099cd1573f7461cb3cb58866c1868f8916aec8d1', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('44', 'image/png', 'icona_a027.png', '2', '2302', null, '2014-02-13 09:28:17', '8171468877ae59d0b705fb1c8e13fbc0d8be8995', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('45', 'application/pdf', '111_SCADNZ_20131102.pdf', '2', '9773', null, '2014-02-13 09:28:17', '7ecbab5a9be387d88abaa94dabc9068412b42b71', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('46', 'image/png', 'icona_a022.png', '2', '2302', null, '2014-02-13 09:28:17', '042c2daaed1379cf4dfc2f90db30d85a8154e23b', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('47', 'image/png', 'icona_a010.png', '2', '2302', null, '2014-02-13 09:28:17', '54c34e16a0163084aaeef5a465dc0f38bfd038f6', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('48', 'image/png', '015.png', '2', '1782217', null, '2014-02-13 09:28:17', 'ea4b401b044f906b5670a84c3fbb38801db8373e', '2014-02-13 09:28:17', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('49', 'application/pdf', 'is_roma_2013.pdf', '2', '9886', null, '2014-02-13 09:28:18', '6b0989db5472a4078350451f063125f224571071', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('50', 'image/png', 'r004001.png', '2', '2362', null, '2014-02-13 09:28:18', 'ca6380a30679cbba28033725c57c6e15e8f0d882', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('51', 'image/png', 'BSK_thumb.png', '2', '4973', null, '2014-02-13 09:28:18', '9a28570495338f7f6953bc150b429781f8c21307', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('52', 'image/png', '020.png', '2', '1782217', null, '2014-02-13 09:28:18', 'bf6bba0b325cc35eb5ce76c1992ca02d890e4d17', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('53', 'image/png', '022.png', '2', '1782217', null, '2014-02-13 09:28:18', '95077500703f4d39ea7a46a71c2bfd3b502cbcb6', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('54', 'image/png', 'galleria_a001_02.png', '2', '1903969', null, '2014-02-13 09:28:18', '2f1f8ff88e216825d67e463435377ffa8b2e3ea2', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('55', 'application/pdf', '111_FATCLI_20131101.pdf', '2', '9285', null, '2014-02-13 09:28:18', '75a237310dac014462a244ebeeba08419e39f431', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('56', 'image/png', '017.png', '2', '1782217', null, '2014-02-13 09:28:18', '8d9dc25baf8448f77a45ab57b70076c45c171af7', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('57', 'image/png', '018.png', '2', '1782217', null, '2014-02-13 09:28:18', '4f2b1ce3c8673b241f27168bc83358cad8f9f82a', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('58', 'image/png', 'icona_a013.png', '2', '2302', null, '2014-02-13 09:28:18', 'cc813f5ecfa1562edc542528de7939ea7b949704', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('59', 'image/png', 'c003001.png', '2', '2528', null, '2014-02-13 09:28:18', 'ed379203ef37b121146e51b6dd12d2c19b89ae22', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('60', 'image/png', 'icona_a028.png', '2', '2302', null, '2014-02-13 09:28:18', '93493542022d80069028d4a92f3499c5ffe3c6c3', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('61', 'image/png', 'r006001.png', '2', '2362', null, '2014-02-13 09:28:18', '036b9383a45401e34686d6d6588301761e2c39c1', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('62', 'image/png', '023.png', '2', '1782217', null, '2014-02-13 09:28:18', 'c7ad09ae1c8437faf5a2edeac169b6419c059603', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('63', 'image/jpeg', 'organizzazione_default.jpeg', '2', '4942', null, '2014-02-13 09:28:18', 'ab12b3ccd4a53fa3c93790780e652e458bc2b9f5', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('64', 'image/png', 'icona_a019.png', '2', '2302', null, '2014-02-13 09:28:18', 'd02fa9ccb6afb44d0bb07e61aa5414255d420548', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('65', 'application/zip', 'esempio_html5.zip', '2', '1181391', null, '2014-02-13 09:28:18', '4cce53a179787b63345317c444a45062e2d13b29', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('66', 'image/png', 'icona_a018.png', '2', '2302', null, '2014-02-13 09:28:18', 'efb2a16dd0b91595ef35269d9ee70e7c7e885a67', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('67', 'image/png', 'r006002.png', '2', '2362', null, '2014-02-13 09:28:18', 'f0a7b7a3920bf79b8046eb3e8524b5ffef84e147', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('68', 'image/png', 'icona_a006.png', '2', '2302', null, '2014-02-13 09:28:18', '91bd96e3b8f0eaaf9be64ba2e5e8f89aba9cb535', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('69', 'image/png', 'icona_a014.png', '2', '2302', null, '2014-02-13 09:28:18', '90a30d2e4af79bdbaccf52b3e0a62b3064f1bf34', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('70', 'application/pdf', '111_FATCLI_20131102.pdf', '2', '9285', null, '2014-02-13 09:28:18', '4c64a9bb69eabc6b81d32483f7349660ca9669e0', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('71', 'image/png', '003.png', '2', '1782217', null, '2014-02-13 09:28:18', '179c7f8c060ea390a8c9b2db850cfc10655a8773', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('72', 'image/png', '012.png', '2', '1782217', null, '2014-02-13 09:28:18', '1e2373d8ea5ffb79ee7e0608e2ee3e092f04541b', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('73', 'image/png', 'icona_a029.png', '2', '2302', null, '2014-02-13 09:28:18', '86e00d65b93bd8fdd0f311545b3d4a5e3d69bba7', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('74', 'application/pdf', 'is_roma_2014.pdf', '2', '9886', null, '2014-02-13 09:28:18', '9e6cdd1b999f57736e43c77e72ce038e250a5eaa', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('75', 'image/png', 'cat001sub001.png', '2', '3090', null, '2014-02-13 09:28:18', '1ebfd06e3d2c8e6c607e8ff56ad0c13c2a23eba4', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('76', 'image/png', '008.png', '2', '1782217', null, '2014-02-13 09:28:18', '15831a42a656d4955fe8f62dea703c648c0aec12', '2014-02-13 09:28:18', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('77', 'image/png', 'cat_ico_001.png', '2', '2588', null, '2014-02-13 09:28:19', 'df332097d4d25fc871b43d106cc5169b3a263997', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('78', 'image/png', 'icona_a031.png', '2', '2302', null, '2014-02-13 09:28:19', 'ff15f88db219897e4ef3d9cd4f7e13687ac5951f', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('79', 'text/plain', 'esempio_testo.txt', '2', '881', null, '2014-02-13 09:28:19', 'bdcddb18409885bb4fba0601b96be8508dd677e9', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('80', 'image/png', '021.png', '2', '1782217', null, '2014-02-13 09:28:19', '59e9cee3bb3b371121ce5b444d2847a73df6377c', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('81', 'image/png', 'cat002sub001.png', '2', '3223', null, '2014-02-13 09:28:19', 'c65be5c7db5b2d169c994a46fd54ea4d8ce2f76c', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('82', 'image/png', '024.png', '2', '1782217', null, '2014-02-13 09:28:19', 'bf2f42a5b13fce785f9849a443b8fefd5e046620', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('83', 'image/png', '028.png', '2', '1782217', null, '2014-02-13 09:28:19', '2baf55ffcd6bd77b70763fb2c7bd1ca98b5e8f2a', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('84', 'image/png', 'cat_ico_004.png', '2', '2667', null, '2014-02-13 09:28:19', 'f784b27dd1591e090294da91ce897236cc8c8e45', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('85', 'image/png', 'icona_a015.png', '2', '2302', null, '2014-02-13 09:28:19', '30b4c90a98c1e353c8e7a3be385450346ae6584c', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('86', 'image/png', 'r005002.png', '2', '2362', null, '2014-02-13 09:28:19', 'b371f6025b34e78c9415c384af22d44bb964dd7d', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('87', 'image/png', '029.png', '2', '1782217', null, '2014-02-13 09:28:19', '345bbe73d95af1a5fe902cbcb10e33639273c4d0', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('88', 'image/png', 'icona_a016.png', '2', '2302', null, '2014-02-13 09:28:19', 'f859469f947a8447ad1b0c79622e7f49d246f172', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('89', 'application/pdf', '111_SCADNZ_20131101.pdf', '2', '9773', null, '2014-02-13 09:28:19', 'ae71c0bc50b5833086e54b4d83df32f53de5afc5', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('90', 'image/png', 'cat_ico_005.png', '2', '2778', null, '2014-02-13 09:28:19', '72500cc92ba4f82118f37bf594f8770bf9655fc9', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('91', 'image/png', 'icona_a017.png', '2', '2302', null, '2014-02-13 09:28:19', 'bba7fcaf613378fbac559f501b9add75d0c5b7fd', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('92', 'image/png', 'icona_a032.png', '2', '2302', null, '2014-02-13 09:28:19', 'f963eaf7c7cc5b60114516a103f5da07ddcd6a71', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('93', 'image/png', 'icona_a024.png', '2', '2302', null, '2014-02-13 09:28:19', '7b1bc7165d4ec0a57b1a2a491a426acd4292a36b', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('94', 'image/png', 'icona_a008.png', '2', '2302', null, '2014-02-13 09:28:19', 'a069561e80f3a7260065b5e7e94c4916951e310a', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('95', 'image/png', 'icona_a021.png', '2', '2302', null, '2014-02-13 09:28:19', '09543be0c18673867ade328d00d370f9f5b4d0d7', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('96', 'image/png', '013.png', '2', '1782217', null, '2014-02-13 09:28:19', '0c38158546c95fbef5920877b9c3f88912797345', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('97', 'image/png', 'c004001.png', '2', '2528', null, '2014-02-13 09:28:19', '005c2ae26cedb416ab12604ee3832d6275c80a6d', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('98', 'image/png', 'c005002.png', '2', '2528', null, '2014-02-13 09:28:19', 'e95e648f3f6cbfaadc683e927b9f5c11cbf7b9b7', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('99', 'image/png', 'icona_a005.png', '2', '2302', null, '2014-02-13 09:28:19', '40db1519fd8f7a128e25a5fadab5cf566e353ae6', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('100', 'image/png', 'r004002.png', '2', '2362', null, '2014-02-13 09:28:19', '04a66bb495ab9310eb49e5b12558f92729f26e64', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('101', 'image/jpeg', 'persona_default.jpeg', '2', '1330', null, '2014-02-13 09:28:19', 'b7d1a5a350209db1c21aa9c43ed8fef950c4dc9a', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('102', 'image/png', 'r006003.png', '2', '2362', null, '2014-02-13 09:28:19', 'ebe45769388e44138931e6823aa9ddd05134a93b', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('103', 'image/png', 'icona_a004.png', '2', '2302', null, '2014-02-13 09:28:19', '86af2044dc01ccf0246b3aafb62e6b1d4c6545ce', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('104', 'image/png', '009.png', '2', '1782217', null, '2014-02-13 09:28:19', 'b3e2c0b623d2a4154f8a45aa6a24e91e086a284b', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('105', 'image/png', '019.png', '2', '1782217', null, '2014-02-13 09:28:19', '0e8cab716c81e738174dce5427a15e5156821950', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('106', 'image/png', '004.png', '2', '1782217', null, '2014-02-13 09:28:19', '5e637581ddc8f1ff10a3532ab031e9ed0c8bf4a0', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('107', 'image/png', 'icona_a023.png', '2', '2302', null, '2014-02-13 09:28:19', 'd26c95615f0ee003e89ee992974aeebc6fbb1dd7', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('108', 'application/pdf', 'pdf_a003_00.pdf', '2', '346710', null, '2014-02-13 09:28:19', '45eed7a0b8c5412526fe503592bd08cd7d4bae98', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('109', 'application/pdf', '111_PROVVMENS_201311.pdf', '2', '10119', null, '2014-02-13 09:28:19', 'fc17d54a691a0f7031b3342f356de86435761ce6', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('110', 'application/pdf', 'pdf_c001_00.pdf', '2', '346753', null, '2014-02-13 09:28:19', '818c581d97b246178b2f3a54779cc5a9e40db4f6', '2014-02-13 09:28:19', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('111', 'image/png', 'icona_a007.png', '2', '2302', null, '2014-02-13 09:28:20', 'e289609ddb94670f9ab85becf6c960842eefd2b6', '2014-02-13 09:28:20', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('112', 'image/png', 'icona_a020.png', '2', '2302', null, '2014-02-13 09:28:20', '8d556148120e29b647a6c58bb8dcda8101b6a64d', '2014-02-13 09:28:20', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('113', 'application/vnd.ms-office', 'esempio_excell.xls', '2', '19968', null, '2014-02-13 09:28:20', '18de0008b57fa036012b5f3b1e2294a4ea93cd35', '2014-02-13 09:28:20', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('114', 'application/pdf', '111_PROVVMENS_201312.pdf', '2', '10119', null, '2014-02-13 09:28:20', 'ba25d5b3cebf62e7c88e6c3a60544422e50bc1a3', '2014-02-13 09:28:20', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('115', 'image/png', '010.png', '2', '1782217', null, '2014-02-13 09:28:20', 'd2199d7cf6e2ff877c7b56f417c7653f86f1c0c8', '2014-02-13 09:28:20', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('116', 'image/jpeg', '3_slider_landscape.jpg', '1', '179468', '393', '2014-03-12 12:34:02', '05c4651025d935d1f83c892da59a8eb28a5f07bc', '2014-03-12 12:34:02', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('117', 'image/jpeg', '2_slider_portrait.jpg', '1', '234431', '393', '2014-03-12 12:34:02', '03a80c2aa8d09311ad22e850c88aa28aa4dac363', '2014-03-12 12:34:02', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('118', 'image/jpeg', '3_slider_portrait.jpg', '1', '233950', '393', '2014-03-12 12:34:03', '5fefaac6892298f8458ff2a4812dfb0b0b917b5e', '2014-03-12 12:34:03', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('119', 'image/jpeg', '1_slider_landscape.jpg', '1', '138522', '393', '2014-03-12 12:34:03', '6cd74258cb771cd307a326a49e6b001aa844f0e6', '2014-03-12 12:34:03', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('120', 'image/jpeg', '2_slider_landscape.jpg', '1', '202197', '393', '2014-03-12 12:34:03', '0af7c1fd5a5e637a4f6f21408a1b3e74d5a0fd66', '2014-03-12 12:34:03', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('121', 'image/jpeg', '1_slider_portrait.jpg', '1', '201261', '393', '2014-03-12 12:34:04', '86e1d9c503840ed58be619d0bdc625b41437ab6e', '2014-03-12 12:34:04', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('122', 'image/jpeg', '4_slider_landscape.jpg', '1', '245248', '393', '2014-03-12 12:34:07', '78a010968ef7c1b87898a810c1612e06f410afaa', '2014-03-12 12:34:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('123', 'image/jpeg', '4_slider_portrait.jpg', '1', '311406', '393', '2014-03-12 12:34:09', '57b32073d992c2fd3ecbcc4efda77173045cf4da', '2014-03-12 12:34:09', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('124', 'image/jpeg', 'pdf.jpg', '1', '4666', '393', '2014-03-12 12:59:12', '83de727d6e5baa1aaa812c45632283da315b76b9', '2014-03-12 12:59:12', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('125', 'image/jpeg', 'mov-avi.jpg', '1', '4591', '393', '2014-03-12 12:59:13', '0fd5c45243d2cfd231b09b9e561bf82bde8304c4', '2014-03-12 12:59:13', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('126', 'image/jpeg', '046.jpg', '2', '50330', null, '2014-03-19 16:44:30', 'a7b22012e2634fc8ff85ded1072ed697133f1c42', '2014-03-19 16:44:30', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('128', 'application/vnd.ms-office', 'powerpoint_a045_00.ppt', '2', '68608', null, '2014-03-19 16:44:30', '3315dc8023c2b8b3bd36acbb5c869e7dbfcb4193', '2014-03-19 16:44:30', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('129', 'image/jpeg', 'img1_a045_00.jpg', '2', '48362', null, '2014-03-19 16:44:31', '77a35caf57bc2215ae59f8b7d03e72967a6bfe1c', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('130', 'image/jpeg', 'img2_a045_00.jpg', '2', '38091', null, '2014-03-19 16:44:31', '8f641b3c158c5c156d19d69f689023f41d977074', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('131', 'text/plain', 'txt_a045_00.txt', '2', '1694', null, '2014-03-19 16:44:31', '418ed866f37a76d96e404c34b64fcbec260816bc', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('132', 'image/jpeg', '045.jpg', '2', '52755', null, '2014-03-19 16:44:31', '16ba8da37d7690b9f595fb334dcdeceec5df3a58', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('133', 'image/jpeg', 'icona_a045.jpg', '2', '6308', null, '2014-03-19 16:44:31', '0fd842d382028965f572fc6bd30208297ae0be73', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('134', 'application/zip', 'html5_a045_00.zip', '2', '3111424', null, '2014-03-19 16:44:31', '7e6e60a4c0bb2eabe3b04c07292b137c48a32e11', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('135', 'image/jpeg', 'icona_a046.jpg', '2', '5906', null, '2014-03-19 16:44:31', 'b5b79b65c359c4d1996819fe77a14156e162c775', '2014-03-19 16:44:31', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('136', 'image/jpeg', 'img3_a045_00.jpg', '2', '38306', null, '2014-03-19 16:44:32', '4c2e18e8e54d5aea6b9a5926cfc3a5f794ac0fce', '2014-03-19 16:44:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('137', 'application/vnd.ms-office', 'xls_a045_00.xls', '2', '10752', null, '2014-03-19 16:44:32', '6b1902f441036a1d844fcc83c0ba9ace534ddaac', '2014-03-19 16:44:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('138', 'application/vnd.ms-office', 'doc_a045_00.doc', '2', '16384', null, '2014-03-19 16:44:32', 'b16303a0e893ca687dc2cbe8d4d78f8545ef09c9', '2014-03-19 16:44:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('139', 'application/pdf', 'pdf_a045_00.pdf', '2', '11210', null, '2014-03-19 16:44:32', '14352967c7839ad41c1cd77e1b831c74a8c6b6b0', '2014-03-19 16:44:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('140', 'image/jpeg', 'logo_ordine.jpg', '1', '5832', '393', '2014-04-04 10:23:22', 'be6f3e22064c46efa559518015835ae19820cce4', '2014-04-04 10:23:22', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('141', 'application/pdf', '111_PROVVMENS_201405.PDF', '1', '18779', '393', '2014-07-07 17:49:25', '0ed1284ce36b3f804fb722d6e62df2362e27dda3', '2014-07-07 17:49:25', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('143', 'image/png', 'logo_euronda_500.png', '1', '7062', '393', '2014-10-22 12:02:41', '52343ddcbea6b6dc5fc4b6fdbcf8aa1f838f45b7', '2014-10-22 12:02:41', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('144', 'image/png', 'blabla.png', '2', '146521', null, '2014-10-22 17:19:46', '4fdbb734acc619601e0baf0a061391ba7a582e2f', '2014-11-03 08:55:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('145', 'image/png', '02-promotions.png', '1', '2893', '393', '2014-10-27 16:48:59', '10f4fc181e1f8b80dc471b18a97de57e9e2f8202', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('146', 'image/png', '06-dealers.png', '1', '3514', '393', '2014-10-27 16:48:59', '2f7e7e2021fe85b4028c5e8a3e91b2efab058a0f', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('147', 'image/png', '05-video.png', '1', '3068', '393', '2014-10-27 16:48:59', '46fec554ff20e3463eb384e257fbebb8bd58697b', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('148', 'image/png', '07-training.png', '1', '3640', '393', '2014-10-27 16:48:59', 'd677a56f3df74970e002faf010f602c5915d2183', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('149', 'image/png', '03-news.png', '1', '3410', '393', '2014-10-27 16:48:59', 'db3012b76340bc2b57f72eb053db94ffc3c2cb29', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('150', 'image/png', '08-myeuronda.png', '1', '4407', '393', '2014-10-27 16:48:59', '408e1a6a9be07638051fdf5eaed027e74fe2bea3', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('151', 'image/png', '01-catalogue.png', '1', '2689', '393', '2014-10-27 16:48:59', '00e8be25e12b673d2ca9b08be308d297b01bdec7', '2014-10-27 16:48:59', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('152', 'image/png', '09-viral.png', '1', '4865', '393', '2014-10-27 16:49:00', '25aa6f529aac0cc559b71b97baf2a6f7d0592b84', '2014-10-27 16:49:00', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('153', 'image/png', '04-documents.png', '1', '2618', '393', '2014-10-27 16:49:01', '43195ac3f6c50c638194ae2a866328d0cd660551', '2014-10-27 16:49:01', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('154', 'image/png', 'dashboard_landscape.png', '1', '2026146', '393', '2014-10-30 09:04:40', 'bc99cbb34e8839d05eaa7f9697838c170a3fd7bc', '2014-10-30 09:04:40', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('155', 'image/png', 'dashboard_portrait.png', '1', '2047828', '393', '2014-10-30 09:04:43', '7969a29501efb66a940fe9b36c06b29fc5229172', '2014-10-30 09:04:43', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('156', 'application/zip', 'MuseExport.zip', '1', '49400', '393', '2014-11-06 19:43:34', '8b11b09388f2da10123103d22f43ab3f14bc2aec', '2014-11-06 19:43:34', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('157', 'application/pdf', 'Catalogo_test_2014.pdf', '1', '7805812', '393', '2014-11-13 11:31:07', '95dba6bb29d85d97f9982ed0406028db26bf53a8', '2014-11-13 11:31:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('158', 'application/octet-stream', 'dega_tab_C20141109.xls', '1', '16101', '393', '2014-11-14 09:29:42', 'b5c5de9c3ae6656074a067e4f0cca4dfc9ad5010', '2014-11-14 09:29:42', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('159', 'application/vnd.ms-excel', 'dega_tab_C20141109_2.xls', '1', '32256', '393', '2014-11-14 11:48:28', '556c28171e07db3f4cfe4560b725ac4ecae721d6', '2014-11-14 11:48:28', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('160', 'application/vnd.ms-excel', 'dega_tab_C20141109_3.xls', '1', '32256', '393', '2014-11-14 11:48:28', 'd7de6f14cf91f8d929f6a978b57869bbd1c475c9', '2014-11-14 11:48:28', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('162', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'dega_tab_C20141109_2.xlsx', '1', '51843', '393', '2014-11-14 12:03:37', '93f46a8e52e8df52ede80d6f8efe714394f64b8f', '2014-11-14 12:03:37', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('163', 'image/png', 'hospot_icon_personalizzata.png', '1', '1247', '393', '2014-11-17 11:29:12', 'be807670926c20da781006bbfcefbe7736d0393b', '2014-11-17 11:29:12', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('164', 'application/octet-stream', 'dega_tab_C2014110988888.xls', '1', '16101', '393', '2014-11-17 15:39:38', '1a2d227f3e34b203b03770bd4481c216405dad85', '2014-11-17 15:39:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('165', 'image/png', 'Logo_zotsell_500x500.png', '1', '9358', '393', '2014-11-19 12:52:48', 'faf83948459856d8052e7dfa0ecbfcc4872399cd', '2014-11-19 12:52:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('166', 'application/octet-stream', 'dega_tab_C20141109_0.xls', '1', '16101', '393', '2014-11-21 08:38:09', '398198c73e3dda90047a618d1cf3ba1f47484605', '2014-11-21 08:38:09', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('167', 'application/vnd.ms-office', 'clienti_C20141106_b.xls', '1', '115712', '161', '2014-11-25 10:22:24', 'bfce749f1d1c1cd429feb4b684852cb911f9d9b9', '2014-11-25 10:22:24', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('168', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'provaprovaprova.xlsx', '1', '51843', '393', '2014-11-25 10:24:44', '07ea4ea0e6b307aeb38d92cd67a1d8c24f4834fa', '2014-11-25 10:24:44', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('169', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'provaprovaprova2.xlsx', '1', '51843', '393', '2014-11-25 10:50:49', '69db09ef2ccabc378fa6cf5fc8c1191be95adb0e', '2014-11-25 10:50:49', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('170', 'image/png', 'zot_activity_reportage.png', '1', '4196', '393', '2014-12-11 09:08:37', '6f24a985abf44401722e4e95d2b150a939d7d01c', '2014-12-11 09:08:37', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('171', 'image/png', 'zot_activity_sendFile.png', '1', '4112', '393', '2014-12-11 09:08:38', '678815a157fa02014aa74940a12105cb3e28cd69', '2014-12-11 09:08:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('172', 'image/png', 'zot_activity_questionaire.png', '1', '4182', '393', '2014-12-11 09:08:39', 'e2e6a028d6550fa7b3342a622578057e5c86e0f3', '2014-12-11 09:08:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('173', 'image/png', 'zot_activity_visit.png', '1', '4019', '393', '2014-12-11 09:08:39', '934f47ac6f74e96dc7857238dded4f99c4a8a083', '2014-12-11 09:08:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('174', 'image/png', 'zot_activity_task.png', '1', '3714', '393', '2014-12-11 09:08:39', '87f7a8ca43f61ccd3c7913b4e5e64e1007536047', '2014-12-11 09:08:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('175', 'image/png', 'zot_activity_serviceTicket.png', '1', '4221', '393', '2014-12-11 09:08:39', '1e0a52878adec4672665d50f2b012bda31ff39e2', '2014-12-11 09:08:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('176', 'image/png', 'zot_activity_task_chiusuracontratto.png', '1', '2446', '393', '2014-12-17 10:58:38', 'b69ec31fc0c3086dfad8ca3c8ab872cbfe9f4431', '2014-12-17 10:58:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('177', 'image/png', 'zot_activity_task_appuntamento.png', '1', '2060', '393', '2014-12-17 10:58:38', '1c907f5fa770b4d2f215cccdaffb528e24c55ecc', '2014-12-17 10:58:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('178', 'image/png', 'zot_activity_task_visitaaziendale.png', '1', '2113', '393', '2014-12-17 10:58:38', '8241630f2bd358b6e140680c1e2c5261eda974bc', '2014-12-17 10:58:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('179', 'image/png', 'zot_activity_task_inviopresentazione.png', '1', '1598', '393', '2014-12-17 10:58:38', 'e135832ee55940b1e4964ffec6e3d6fc06ccfac9', '2014-12-17 10:58:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('180', 'image/png', 'zot_activity_task_invioemail.png', '1', '1446', '393', '2014-12-17 10:58:39', '88f04aa70f987ed8f9ffe5af9e5bca6574e4d9e9', '2014-12-17 10:58:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('181', 'image/png', 'zot_activity_task_telefonata.png', '1', '1917', '393', '2014-12-17 10:58:39', '0a659373507602aac2c2b1c249f2ed0a33af3343', '2014-12-17 10:58:39', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('184', 'application/zip', 'wistia.zip', '1', '1657', '393', '2015-03-31 11:23:38', '654d0e45e5a5813404388693af98d785af9d2c3d', '2015-03-31 11:23:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('185', 'application/pdf', '01003738_ultime_fatture.pdf', '1', '98565', '393', '2015-04-01 09:50:10', '3ff56837d057512238cc1749f7f47ce401ac19cd', '2015-04-01 09:50:10', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('186', 'image/jpeg', 'logo_ordine_zotsell.jpg', '1', '5198', '393', '2015-04-03 10:39:32', '268036c9d64fe51d2e516fd12d26c574f76b2e79', '2015-04-03 10:39:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('187', 'image/jpeg', 'orderLogo.jpg', '1', '5525', '393', '2015-04-03 14:45:32', 'd21945ed7cbfc4ce38d3183a3c1ae222c237ca5d', '2015-04-03 14:45:32', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('188', 'application/zip', 'pdf_sfogliabile.zip', '1', '2566854', '393', '2015-04-15 18:12:57', '54713830fa050c26c8065fcc5a4a962c43bf6dff', '2015-04-15 18:12:57', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('189', 'application/zip', 'sterilisation_process.zip', '1', '575', '161', '2015-05-05 11:22:26', '3f763565189fbebeb3a926db9cc1039978b0b443', '2015-05-05 11:22:26', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('192', 'application/pdf', 'AR001_FATTCRED_20150609.PDF', '1', '244911', '393', '2015-06-12 18:08:36', '311e9d7e6b5832514c504e3e889ead539676d496', '2015-06-12 18:08:36', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('193', 'application/pdf', 'AR001_FATTCRED_20150605.PDF', '1', '245616', '393', '2015-06-15 09:24:38', '0f7a30c4591c2bc7e0718e5272b46d791144db9c', '2015-06-15 09:24:38', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('194', 'image/png', '06b-dealers.png', '1', '5242', '393', '2015-06-15 11:07:06', 'be7e8cca3ad74bfa06320bc7c14b1b86ea27ccad', '2015-06-15 11:07:06', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('195', 'application/pdf', 'Stella Bianca_cat2015.pdf', '1', '11985043', '393', '2015-07-17 09:16:01', '8597d07dcb5f8f32d78fc478b9f69cd78684c8db', '2015-07-17 09:16:01', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('196', 'image/png', 'landscape.png', '1', '159635', '393', '2015-07-30 14:56:48', '1225f99a6c13c7553641b903426a7afdbdaba66b', '2015-07-30 14:56:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('197', 'image/png', 'portrait.png', '1', '420008', '393', '2015-07-30 14:56:49', 'ae4f41ff6c1d74d751c963d13ad3d44202d821cf', '2015-07-30 14:56:49', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('198', 'application/pdf', 'pdf_example_corrotto.pdf', '1', '885063', '393', '2015-09-21 10:10:43', '66c356cad28775c9c2cbcadb39cf75e925f2ca0d', '2015-09-21 10:10:43', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('199', 'application/pdf', 'pdf_example_ok.pdf', '1', '887654', '393', '2015-09-21 10:10:47', '0158be2819527fdc5c3eacd7300264f35a4375e9', '2015-09-21 10:10:47', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('200', 'image/png', 'transport_site_new.png', '1', '6458', '393', '2015-10-15 11:17:25', '7ec4ce71e8597a2045190126923f4d1639b1a280', '2015-10-15 11:17:25', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('201', 'application/pdf', '00001R_PROVVIGIONI_20151109.pdf', '1', '58033', '161', '2015-11-11 12:29:58', 'd6ea7ec2244f07fc01f0b46f32a68a5bfd116791', '2015-11-11 12:29:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('202', 'application/pdf', '000_FATTMENS_201505.pdf', '2', '18315', null, '2015-12-16 10:22:06', '680635882614219df4ade5a2955c19e51cf28d1d', '2015-12-16 10:22:06', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('203', 'application/pdf', '000_GIROVISTA_20151212.pdf', '2', '18693', null, '2015-12-16 10:22:06', 'a3759ab86fae6cccdc407d8094bc5946bd084de1', '2015-12-16 10:22:06', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('204', 'application/pdf', '000_FATTMENS_201510.pdf', '2', '18314', null, '2015-12-16 10:22:06', 'c0beb40c75db7e118c14521037a4eb4fec232c35', '2015-12-16 10:22:06', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('205', 'application/pdf', '000_GIROVISTA_20151210.pdf', '2', '18695', null, '2015-12-16 10:22:06', '257c4efcc069756d96f1836c07b070f02e4e316d', '2015-12-16 10:22:06', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('206', 'application/pdf', '000_GIROVISTA_20151213.pdf', '2', '18976', null, '2015-12-16 10:22:07', '835fdd16342933c8829cf1a18b422e8e9b885c3c', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('207', 'application/pdf', '000_BUDGET_2015.pdf', '2', '18072', null, '2015-12-16 10:22:07', '44bcd791e2115876f1c529c2bafcc8f7a033812a', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('208', 'application/pdf', '000_GIROVISTA_20151211.pdf', '2', '18711', null, '2015-12-16 10:22:07', 'a304e314c3d6e511a83da593ef07ff8abc7fa7a8', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('209', 'application/pdf', '000_GIROVISTA_20151215.pdf', '2', '18696', null, '2015-12-16 10:22:07', '1e3875c6ebfcd1c4af067950d762accf63a9eeee', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('210', 'application/pdf', '000_FATTMENS_201502.pdf', '2', '18310', null, '2015-12-16 10:22:07', '8c7059a70235452babe0fcb9138fdd03e1fc38da', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('211', 'application/pdf', '000_GIROVISTA_20151214.pdf', '2', '18832', null, '2015-12-16 10:22:07', 'fa0fbbd4ab16fab2ee9ffc676dd8a9b98df3b948', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('212', 'application/pdf', '000_FATTMENS_201511.pdf', '2', '18328', null, '2015-12-16 10:22:07', '4f225536f536a47c68ae402028f6b8a91ee7f78a', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('213', 'application/pdf', '000_FATTMENS_201503.pdf', '2', '18597', null, '2015-12-16 10:22:07', '4747095b685f7b27edae09a31577bb6d6b3a15b4', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('214', 'application/pdf', '000_FATTMENS_201501.pdf', '2', '18373', null, '2015-12-16 10:22:07', '1bd1206efe3e763d0e65fae459092b2c899fa794', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('215', 'application/pdf', '000_GIROVISTA_20151217.pdf', '2', '18847', null, '2015-12-16 10:22:07', 'bfbadbaccc91f200e32ad212cd5157d290a4a2a1', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('216', 'application/pdf', '000_GIROVISTA_20151216.pdf', '2', '18946', null, '2015-12-16 10:22:07', '5a9d1d49fccd5a62a331a923af478a03f67c09a7', '2015-12-16 10:22:07', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('217', 'application/pdf', '000_FATTMENS_201506.pdf', '2', '18582', null, '2015-12-16 10:22:08', '461cd246be25474e63bca233a2e53023c5fe433d', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('218', 'application/pdf', '000_FATTMENS_201507.pdf', '2', '18457', null, '2015-12-16 10:22:08', 'f9a635f883e4e0fcb3fcaa264d3020d42dfdc580', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('219', 'application/pdf', '000_FATTMENS_201512.pdf', '2', '18315', null, '2015-12-16 10:22:08', 'bc65899e5334ab9ee9426fd4a11a3a69137b41ef', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('220', 'application/pdf', '000_FATTMENS_201508.pdf', '2', '18623', null, '2015-12-16 10:22:08', 'c520b329df48831e076993820deb42f1cda90aac', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('221', 'application/pdf', '000_BUDGET_2013.pdf', '2', '18092', null, '2015-12-16 10:22:08', 'c3e89a8b13ea3c9a1a0b427575449ea5827c84d0', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('222', 'application/pdf', '000_FATTMENS_201504.pdf', '2', '18458', null, '2015-12-16 10:22:08', '83310221dbb63004bd668b5e54e8c176bfcbce0f', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('223', 'application/pdf', '000_BUDGET_2012.pdf', '2', '17802', null, '2015-12-16 10:22:08', '269d461ce7ba41a1d866f84c3bef179c861be8d8', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('224', 'application/pdf', '000_BUDGET_2014.pdf', '2', '17950', null, '2015-12-16 10:22:08', '94116af095ba63e64c605b1378936c0334a35bf2', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('225', 'application/pdf', '000_FATTMENS_201509.pdf', '2', '18573', null, '2015-12-16 10:22:08', 'ee97ef16fb09e2f64832b212d29d68661e97aaf8', '2015-12-16 10:22:08', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('226', 'application/pdf', '000_GIROVISITA_20151214.pdf', '2', '18832', null, '2015-12-16 10:27:47', '470c3d3ea835a3f2ee9f0f8b7192e3aa2b9857b9', '2015-12-16 10:27:47', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('227', 'application/pdf', '000_GIROVISITA_20151216.pdf', '2', '18946', null, '2015-12-16 10:27:47', '730d83021c637c163550609b2b8baf3fad9f1527', '2015-12-16 10:27:47', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('228', 'application/pdf', '000_GIROVISITA_20151217.pdf', '2', '18847', null, '2015-12-16 10:27:48', '83c19fd395dfff504c0a88217275e93c45bbc1aa', '2015-12-16 10:27:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('229', 'application/pdf', '000_GIROVISITA_20151210.pdf', '2', '18695', null, '2015-12-16 10:27:48', '54c29e66db31e31f458b2ef8adbedbd8bbf6ab1d', '2015-12-16 10:27:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('230', 'application/pdf', '000_GIROVISITA_20151213.pdf', '2', '18976', null, '2015-12-16 10:27:48', '2f0d4ee0b2c4b489efc0e88a7bbe770cae6aaeab', '2015-12-16 10:27:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('231', 'application/pdf', '000_GIROVISITA_20151211.pdf', '2', '18711', null, '2015-12-16 10:27:48', 'f9ac5bb95699b428c1322ec470bf56b63a2ffcf4', '2015-12-16 10:27:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('232', 'application/pdf', '000_GIROVISITA_20151212.pdf', '2', '18693', null, '2015-12-16 10:27:48', '08501cec14c1f8a8d9682b031e8e8b7d6c9c4785', '2015-12-16 10:27:48', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('233', 'application/pdf', '000_GIROVISITA_20151215.pdf', '2', '18696', null, '2015-12-16 10:27:49', '6e68470f174acb3c6062f7baef78462dcdba2e36', '2015-12-16 10:27:49', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('234', 'image/jpeg', 'galleria1_test_file.jpg', '2', '193580', null, '2015-12-16 17:07:58', 'ac368f7bd202e8ac8ddc7d914df363dc6d938a00', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('235', 'application/msword', 'word_test_file.doc', '2', '25088', null, '2015-12-16 17:07:58', '893ead98454fd45c32fc3fb972f1cf0e1b0ae614', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('236', 'image/png', 'icona_file_test.png', '2', '23820', null, '2015-12-16 17:07:58', 'd94205f00bd99b747f0ae07968424cd98d9844a4', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('237', 'application/vnd.ms-office', 'xls_test_file.xls', '2', '19968', null, '2015-12-16 17:07:58', '0df8649984fc18a05ef60323859774c48149fdb5', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('238', 'image/png', 'immagine_test_file.png', '2', '1782217', null, '2015-12-16 17:07:58', '0563c0f641a96885a20abed9a97654907efb15b7', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('239', 'text/plain', 'txt_test_file.txt', '2', '881', null, '2015-12-16 17:07:58', '5ebf812d18576463bfd1629efa9d9c5b730f9af0', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('240', 'image/jpeg', 'galleria2_test_file.jpg', '2', '170155', null, '2015-12-16 17:07:58', '9bb2cdce6d847f444b944fba6c38ed6044c833d0', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('241', 'video/mp4', 'movie_test_file.mp4', '2', '5253880', null, '2015-12-16 17:07:58', 'e2d49afa35f48f89e097e02a269bddbc6f703ea8', '2015-12-21 10:07:16', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('242', 'image/jpeg', 'galleria3_test_file.jpg', '2', '77582', null, '2015-12-16 17:07:58', '44a91af42b32b792642acf7c88ee950dea33b5c4', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('243', 'application/vnd.ms-office', 'ppt_test_file.ppt', '2', '3412992', null, '2015-12-16 17:07:58', '48e8864bd6b978587b65301ee5ce65903e65c70e', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('244', 'application/pdf', 'pdf_test_file.pdf', '2', '346710', null, '2015-12-16 17:07:58', '0f2e81deb94b7ce3191c41ed3439a2c0a468e803', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('245', 'application/zip', 'html5_test_file.zip', '2', '1181391', null, '2015-12-16 17:07:58', 'd509f4c0512c87bfc7ad1497a7696f2fce792ceb', '2015-12-16 17:07:58', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('246', 'image/jpeg', 'icona_galleria_test_file.jpg', '2', '9964', null, '2015-12-16 17:29:22', '025cdb3a554273e51575db6362de2c1096e5969c', '2015-12-16 17:29:22', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('247', 'image/png', 'articolo_thumb.png', '2', '2302', null, '2015-12-21 10:22:29', 'a72e4f5f01b9ca70a511d7c222b1d7c07edbf75c', '2015-12-21 10:22:29', '1');
INSERT INTO `zse_v1_repository_files` VALUES ('248', 'image/png', 'articolo.png', '2', '1782217', null, '2015-12-21 10:22:29', '592e3d85f787e345bb68c86377aa3cd97a7d880b', '2015-12-21 10:22:29', '1');

-- ----------------------------
-- Table structure for `zse_v1_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `zse_v1_sessions`;
CREATE TABLE `zse_v1_sessions` (
  `id` char(32) CHARACTER SET latin1 NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zse_v1_sessions
-- ----------------------------
INSERT INTO `zse_v1_sessions` VALUES ('59jvjh6l79dvljp4jo6he2sbs6', '1450881424', '');
