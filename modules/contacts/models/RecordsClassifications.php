<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_records_classifications".
 *
 * @property integer $id_classification
 * @property string $label
 * @property string $description
 * @property string $color
 * @property string $date_modification
 * @property integer $record_status
 *
 * @property ZseV1Records[] $zseV1Records
 */
class RecordsClassifications extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_records_classifications';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_modification'], 'safe'],
            [['record_status'], 'integer'],
            [['label'], 'string', 'max' => 64],
            [['description','color'], 'string', 'max' => 255],
            [['label'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_classification' => 'Id Classification',
            'label' => 'Label',
            'description' => 'Description',
            'color' => 'Color',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1Records()
    {
        return $this->hasMany(ZseV1Records::className(), ['id_classification' => 'id_classification']);
    }
}
