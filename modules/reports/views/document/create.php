<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\grid\GridView;

$this->title = 'Reports';

$this->params['headerTitle'] = "Reports Media Create/update";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$fileParentFolder = ($media->folders) ? $media->folders[0] : null;

if ($fileParentFolder) {
    $parentsFolder = $fileParentFolder->parents()->all();
    foreach ($parentsFolder as $parentFolder) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentFolder->label,
            'url' => ['folder/view', 'id' => $parentFolder->id_report_tree]
        ];
    }

    $this->params['breadcrumbs'][] = [
        'label' => $fileParentFolder->label,
        'url' => ['folder/view', 'id' => $fileParentFolder->id_report_tree]
    ];
} else {
    $currentFolder = \app\modules\reports\models\ReportsTree::find()->roots()->one();
    $this->params['breadcrumbs'][] = [
        'label' => $currentFolder->label,
        'url' => ['folder/view', 'id' => $currentFolder->id_report_tree]
    ];
}


$this->params['breadcrumbs'][] = [
    'label' => ($media->name) ? $media->name : 'File name',
    'template' => "<li><b>{link}</b></li>\n"
];

$counterIdDropzone = 0;
?>

<div class="col-md-3">
    <div class="hpanel panel-group">
        <div class="panel-body">
            <div class="text-center text-muted font-bold">Select translation or add new</div>

        </div>
        <div class="panel-section">

            <div class="input-group">
                <input type="text" class="form-control" placeholder="Select Area...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i
                            class="glyphicon glyphicon-plus small"></i></button>
                </span>
            </div>
            <button type="button" data-toggle="collapse" data-target="#notes"
                    class="btn-sm visible-xs visible-sm collapsed btn-default btn btn-block m-t-sm">
                All notes <i class="fa fa-angle-down"></i>
            </button>
        </div>

        <div id="notes" class="collapse">
            <div class="panel-body note-link active_contact_side_bar">
                <a href="#default" data-toggle="tab">
                    <small class="pull-right text-muted">WORLD</small>
                    <h5>Default</h5>

                    <div class="small">
                        <strong><?= $media->name ?></strong>
                    </div>
                    <div class="small">
                        <?= $media->description ?>
                    </div>
                </a>
            </div>

            <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                <div class="panel-body note-link <?php if (!$media->id_media) echo 'disabledTab'; ?>">
                    <a href="#<?= $key ?>" data-toggle="tab">
                        <small class="pull-right text-muted"><?= $languageRelation['country'] ?></small>
                        <h5><?= $languageRelation['language'] ?></h5>

                        <div class="small">
                            <strong><?php
                                /* if(isset($languageRelation['model'])) */
                                echo $languageRelation['model']->name;
                                ?></strong>
                        </div>
                        <div class="small">
                            <?php
                            /* if(isset($languageRelation['model'])) */
                            echo $languageRelation['model']->description;
                            ?>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<div class="col-md-9">
    <div class="hpanel">

        <div class="panel-body">

            <div class="text-center hidden">
                We couldn't find any Document. Add one
            </div>

            <div class="tab-content">
                <div id="default" class="tab-pane active">
                    <div class="pull-right text-muted m-l-lg">
                        WORLD
                    </div>
                    <h3>Default</h3>
                    <hr/>
                    <div class="note-content">
                        <div class="row">
                            <div class="col-lg-4">
                                <?=
                                $this->render('_createForm', [
                                    'media' => $media,
                                    'idContainer' => $idContainer,
                                ]);
                                ?>
                            </div>

                            <!-- Choose file -->
                            <div class="col-lg-4">
                                <h3> Choose file</h3>

                                <?=
                                $this->render('_searchForm', [
                                    'key' => 'default',
                                    'searchModel' => $searchModel
                                ]);
                                ?>

                                <?php Pjax::begin(['id' => 'search-result', 'timeout' => 5000, 'options' => ['class' => 'pjax-search-result']]) ?>
                                <?=
                                $this->render('_chooseFile', [
                                    'key' => 'default',
                                    'dataProvider' => $dataProvider,
                                    'media' => $media,
                                ]);
                                ?>                               
                                <?php Pjax::end() ?>
                            </div>

                            <!-- Upload File -->
                            <?=
                            $this->render('_uploadFile', [
                                'key' => 'default',
                                'dropZoneConfig' => $dropZoneConfig
                            ]);
                            ?>

                        </div>

                    </div>

                    <?php $formDelete = ActiveForm::begin(['id' => 'delete-media', 'method' => 'GET', 'action' => ['delete']]); ?>
                    <?= Html::hiddenInput('id', $media->id_media); ?>
                    <?php ActiveForm::end(); ?>

                    <div class="btn-group">
                        <button class="btn btn-sm btn-default" form="create-media"><i class="fa fa-thumbs-o-up"></i> Save</button>
                        <button class="btn btn-sm btn-default" form="delete-media"><i class="fa fa-trash"></i> Remove</button>
                    </div>
                </div>


                <!-- Translations -->
                <?php foreach ($mediaTranslations as $key => $languageRelation): ?>
                    <div id="<?= $key ?>" class="tab-pane">
                        <div class="pull-right text-muted m-l-lg">
                            <?= $languageRelation['country'] ?>
                        </div>
                        <h3><?= $languageRelation['language'] ?></h3>
                        <hr/>
                        <!-- build translation body -->
                        <div class="note-content">
                            <div class="row">
                                <div class="col-lg-4">
                                    <?=
                                    $this->render('_createTranslationForm', [
                                        'key' => $key,
                                        'media' => $media,
                                        'model' => $languageRelation['model'],
                                        'idContainer' => $idContainer,
                                    ]);
                                    ?>
                                </div>

                                <!-- Choose file -->
                                <div class="col-lg-4">
                                    <h3> Choose file</h3>
                                    <?=
                                    $this->render('_searchForm', [
                                        'key' => $key,
                                        'searchModel' => $searchModel
                                    ]);
                                    ?>

                                    <?php Pjax::begin(['id' => 'search-result-' . $key, 'timeout' => 5000, 'options' => ['class' => 'pjax-search-result']]) ?>
                                    <?=
                                    $this->render('_chooseFile', [
                                        'key' => $key,
                                        'dataProvider' => $dataProvider,
                                        'media' => $media,
                                    ]);
                                    ?>
                                    <?php Pjax::end() ?>

                                </div>

                                <!-- Upload File -->
                                <?=
                                $this->render('_uploadFile', [
                                    'key' => $counterIdDropzone,
                                    'dropZoneConfig' => $dropZoneConfig
                                ]);
                                ?>
                            </div>

                        </div>
                        <!-- /build translation body -->
                        <?php $formDelete = ActiveForm::begin(['id' => "delete-media-{$key}", 'method' => 'GET', 'action' => ['delete-translation', 'id' => $media->id_media, 'lang' => $key]]); ?>
                        <?= Html::hiddenInput('id', $media->id_media); ?>
                        <?php ActiveForm::end(); ?>

                        <div class="btn-group">
                            <button class="btn btn-sm btn-default" form="create-mediaTr-<?= $key ?>"><i
                                    class="fa fa-thumbs-o-up"></i> Save
                            </button>
                            <button class="btn btn-sm btn-default" form="delete-media-<?= $key ?>"><i class="fa fa-trash"></i>
                                Remove
                            </button>
                        </div>
                    </div>
                    <?php $counterIdDropzone++; ?>
                <?php endforeach; ?>
            </div>

        </div>

    </div>
</div>

<?php
// pjax events debug
$this->registerJs(<<<JS

   $('.pjax-search-result').on('pjax:error', function (event) {
       //console.log('Failed to load the page Search-result. Server is busy. Try later');
       event.preventDefault();
    });

   $('.pjax-search-result').on('pjax:timeout', function() {
       //console.log('pjax:timeout');
    });
                    
   $('.pjax-search-result').on('pjax:complete', function() {
       console.log('pjax:complete'+this.id);
       if($(this).closest(".note-content").find(".choosen-list").val()!=''){ //before
           setCheckedColumns($(this).closest(".note-content").find(".grid-search-result"), $(this).closest(".note-content").find(".choosen-list").val().split("\\n"));
       }
});
JS
);
?>

<?php
$idMedia = ($media->id_media) ? $media->id_media : '';
$this->registerJs('
var urlAction = \'' . Yii::$app->controller->action->id . '\';
var mediaType = \'' . $media->type . '\';
var idFolder = \'' . $idContainer . '\';
var id = \'' . $idMedia . '\'; //media id (if isset)
//console.log ("id:"+id);
$(document).ready(function(){
$.pjax.defaults.timeout = 5000; //update timeout value (long response from server)
    $(\'body\').on(\'click\', \'.m-t-n-xs\',  function(){ 
        //console.log(\'clicked:\'+$(this).attr(\'id\'));
        //console.log(\'found input without id: \'+$(this).closest(\'.search-form\').find(\'.form-control\').val());
        if($(this).closest(\'.search-form\').find(\'.form-control\').val() !=""){
            var url = urlAction+"?SearchRepositoryFiles%5Bname%5D="+$(this).closest(\'.search-form\').find(\'.form-control\').val()+"&type="+mediaType;

            if(idFolder>0) url = url+"&id_folder="+idFolder;

            if(id>0 ) url = url+"&id="+id;

            var pjaxResultContainer = $(this).closest(".note-content").find(".pjax-search-result");

            $.pjax({url: url, container: "#"+pjaxResultContainer.attr("id")}); //before $.pjax({url: url, container: \'#search-result\'});
        }
        else {
            //console.log("empty search field");
        }
    });
});', \yii\web\View::POS_READY);
?>

<?php
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/reports/customIcon.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile(Yii::$app->request->baseUrl . '/js/reports/chooseFile.js', ['depends' => [\yii\web\JqueryAsset::className()]]); ///!!!
?>