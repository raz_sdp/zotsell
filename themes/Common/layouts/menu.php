<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:29 AM
 */?>
<div id="navigation">
    <div class="profile-picture">


        <div class="stats-label text-color">
            <span class="font-extra-bold font-uppercase">Robert Razer</span>

            <div class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <small class="text-muted">Admin user <b class="caret"></b></small>
                </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="<?php echo Yii::$app->homeUrl?>contacts/records/list">Contacts</a></li>
                    <li><a href="profile.html">Profile</a></li>
                    <li><a href="analytics.html">Analytics</a></li>
                    <li class="divider"></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>


        </div>
    </div>

    <ul class="nav" id="side-menu">
        <li>
            <a href="../index.html"> <span class="nav-label">Dashboard</span> <span
                    class="label label-success pull-right">3</span> </a>
        </li>
        <li>
            <a href="../News/index.html"> <span class="nav-label">News</span><span
                    class="label label-warning pull-right">8</span> </a>
        </li>

        <li class="active">
            <a href="<?php echo Yii::$app->homeUrl?>contacts/records/list"> <span class="nav-label">Contacts</span> </a>
        </li>

        <li>
            <a href="../#"><span class="nav-label">Sales</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
                <li><a href="../Items/index.html">Items</a></li>
                <li><a href="../Cart/index.html">Cart</a></li>
                <li><a href="../Orders/index.html">Orders</a></li>
                <li><a href="../Offers/index.html">Offers</a></li>
            </ul>
        </li>

        <li>
            <a href="../Promotions/index.html"> <span class="nav-label">Promotions</span> </a>
        </li>


        <li>
            <a href="../Catalog/index.html"> <span class="nav-label">Catalog</span> </a>
        </li>


        <li>
            <a href="../documents/index.html"> <span class="nav-label">Documents</span> </a>
        </li>
        <li>
            <a href="../Reports/index.html"> <span class="nav-label">Reports</span> </a>
        </li>

        <li>
            <a href="../#"><span class="nav-label">CRM</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
                <li><a href="../Tasks/index.html">Tasks</a></li>
                <li><a href="../Activities/index.html">Activities</a></li>
                <li><a href="../Maps/index.html">Maps</a></li>
                <li><a href="../Itinerary/index.html">Itinerary</a></li>
                <li><a href="../Accounts/index.html">Accounts</a></li>


            </ul>
        </li>
        <li>
            <a href="../Payments/index.html"> <span class="nav-label">Payments</span> </a>
        </li>


        <li>
            <a href="../#"><span class="nav-label">Development</span><span class="fa arrow"></span> </a>
            <ul class="nav nav-second-level">
                <li><a href="../Notifications/index.html">Notifications</a></li>
                <li><a href="../Tools/index.html">Tools</a></li>
                <li><a href="../FormComposer/index.html">Form composer</a></li>
                <li><a href="../Settings/index.html">Settings</a></li>

            </ul>
        </li>
    </ul>
</div>