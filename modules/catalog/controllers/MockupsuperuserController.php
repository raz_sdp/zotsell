<?php

namespace app\modules\catalog\controllers;

use yii\web\Controller;

class MockupsuperuserController extends Controller
{
       public function actionIndex()
    {
        return $this->render('index');
    }
  
    public function actionDetails()
    {
        return $this->render('details');
    }
    
    public function actionSelectcustomer()
    {
        return $this->renderAjax('selectcustomer');
    }
    
    
	public function actionBuyitem()
    {
        return $this->renderAjax('buyitem');
    }

	public function actionBuykit()
    {
        return $this->render('buykit');
    }
        
    
    public function actionItemusers()
    {
        return $this->render('itemusers');
    }
    
    public function actionBulk()
    {
        return $this->render('bulk');
    }

    
        public function actionEdit()
    {
        return $this->render('edit');
    }

     	public function actionCart()
    {
		$this->layout = 'cart';
        return $this->render('cart');
    }
    
             public function actionFolders()
    {
        return $this->render('folders');
    }
    
    
    
            public function actionFlipbook()
    {
        return $this->render('flipbook');
    }
     
        
            public function actionSquares()
    {
        return $this->render('squares');
    }
     
     
    
}

