<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:14 AM
 */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title="Contacts List View";
$this->subtitle="Use List view to show Contacts in a sortable list .";
$this->breadCrums= [['label'=>'Contacts','link'=>''],'List View','root'];
?>
<div class="col-md-3">
    <?php echo $this->render('../layouts/left-hpanel')?>
    <?php echo $this->render('../elements/_contact-list-filter',[
        'classification_list'=>$classification_list
    ])?>

</div>

<div class="col-md-9">
    <div class="hpanel">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                <a class="closebox"><i class="fa fa-times"></i></a>
            </div>
            Standard table
        </div>
        <div class="panel-body">
            <table class="table table-striped table-bordered table-hover js-contact-list-table">
                <thead>
                <tr>
                    <th>Type</th>
                    <!--<th>ID</th>-->
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Phone</th>
                    <th>Country</th>
                    <th>Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>