<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/26/16
 * Time: 11:52 PM
 */
use \app\modules\contacts\models\Records;
?>
<div class="hpanel">
    <div class="panel-body">
        <div class="m-b-md">
            <h4>
                New Folder
            </h4>
            <small>
                Create a new folder for user and assign contacts to it.
            </small>
        </div>


        <form role="form" id="form_new_contact" action="<?php echo Yii::$app->homeUrl;?>contacts/records/associate_new" method="post">
            <p> <h5> Choose User</h5> </p>
            <div class="form-group">
                <input type="text" id="name" name="agent" placeholder="Enter user code" pattern="[A-Za-z0-9]+" class="form-control">
            </div>
            <p> <h5> Choose Contact</h5> </p>
            <div class="form-group">
                <?php
                $record_codes = Records::getAllRecordCode();
//                \app\components\BaseController::_setTrace($record_codes);
                ?>
                <select id="name" name="contact" class="form-control">
                    <option value="">--Please Select One--</option>
                    <?php
                    $tbl_row='';
                    foreach($record_codes as $row){
                        echo '<option value ="'.$row['code'].'">'.$row['code'].'</option>';
                    }
                    ?>
                </select>
<!--                <input type="text" id="name" name="contact" placeholder="Enter contact code" pattern="[A-Za-z0-9]+" class="form-control" required>-->
            </div>
            <!--<div class="table-responsive slim_scroll">
                <table class="table table-striped js-connection-search-result-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2">No Result Found</td>
                    </tr>
                    </tbody>
                </table>
            </div>-->
            <div>
                <button class="btn btn-block btn-success m-t-n-xs" type="submit">
                    <strong>Add</strong>
                </button>
            </div>
        </form>
    </div>
</div>