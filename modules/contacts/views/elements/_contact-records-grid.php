<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/28/16
 * Time: 12:30 PM
 */

use \app\modules\contacts\models\RecordsClassifications;
use app\modules\contacts\components\AppHelper;
use yii\helpers\ArrayHelper;
foreach($data as $key => $item) { ?>
    <?php if($key%4==0) echo "<div class='row'>";?>
    <div class="col-lg-3">
        <?php
        $classification = RecordsClassifications::find()->select('color')->where(['id_classification' => $item->id_classification])->one();
        if(isset($classification->color)){
            $color = $classification->color;
        }else{
            $color = '#62cb31';
        }
        ?>
        <!--<div class="hpanel hgreen contact-panel custom_table_brake">-->
        <div class="hpanel contact-panel custom_table_brake">
            <div class="panel-body" style="border-top: 2px solid <?php echo $color?>">
                <!--<span class="label label-success pull-right">NEW</span>-->
                <?php
                if(isset($item->record_status) && $item->record_status == 0){?>
                    <span class="label label-warning pull-right">BLOCKED</span>
                <?php }
                /*switch($item->type) {
                    case "Company":
                        echo "<i class=\"fa fa-building-o fa-3x\"></i>"; break;
                    case "Prospect":
                        echo "<i class=\"fa fa-user-secret fa-3x\"></i>"; break;
                    case "Destination":
                        echo "<i class=\"fa fa-truck fa-3x\"></i>"; break;
                    case "Person":
                        echo "<i class=\"fa fa-user fa-3x\"></i>"; break;
                    default:
                        echo ""; break;
                }*/
                $type_extra = '';
                if(isset($icon_type) && $icon_type != ''){ //for destination type only
                    echo AppHelper::convertTypeIntoImage($icon_type,false);
                    $type_extra = '&d';
                }else{
                    echo AppHelper::getIconFromArrayVal(ArrayHelper::toArray($item), false, false);
                }
                ?>

                <h3><a href="<?= Yii::$app->homeUrl?>contacts/records/view?id=<?= $item->id_record . $type_extra;?>"> <?= $item->business_name;?> </a></h3>

                <div class="small"> <?= $item->address;?> </div>
                <p>
                    <!--Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices
                    accumsan.-->
                </p>
            </div>
            <div class="panel-footer contact-footer">

            </div>
        </div>
    </div>
    <?php if($key%4==3 || count($data)==$key+1) echo "</div>";?>
<?php } ?>
    <?php
    $total_page_number=ceil($data_count/$limit);
    $current_page_number=round(($current_page_offset+$limit)/$limit);
    $previous_page_offset=$current_page_offset-$limit;
    $next_page_offset=$current_page_offset+$limit;
    $onclick_previous='';
    $onclick_next='';
    $disable_previous='disabled';
    $disable_next='disabled';
    if($current_page_number>1){
        $onclick_previous='getContactRecordGridDataByPagination('.$previous_page_offset.')';
        $disable_previous='';
    }
    if($current_page_number<$total_page_number){
        $onclick_next='getContactRecordGridDataByPagination('.$next_page_offset.')';
        $disable_next='';
    }
    if($total_page_number>1){?>
    <div class="contacts-grid-pagination">
        <ul class="pagination">
            <li class="page-item <?php echo $disable_previous?>">
                <a class="page-link" href="javascript:void(0)" onclick="<?php echo $onclick_previous ?>" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>
            <?php
            $offset=0;
            for($i=1;$i<=$total_page_number;$i++){
                $active=($current_page_number==$i)?'active':'';
                echo "<li class='page-item js-page-offset $active'><a class='page-link' href='javascript:void(0)' onclick='getContactRecordGridDataByPagination($offset)'>$i</a></li>";
                $offset+=$limit;
            }
            ?>
            <li class="page-item <?php echo $disable_next?>">
                <a class="page-link" href="javascript:void(0)" onclick="<?php echo $onclick_next?>" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        </ul>
    </div>
    <?php }?>

