<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media".
 *
 * @property integer $id_media
 * @property string $type
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property integer $cansend
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['multimedia'], 'string'],
            [['cansend', 'record_status'], 'integer'],
            [['entity_id'], 'required'],
            [['date_modification'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['icon', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'type' => 'Type',
            'icon' => 'Icon',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'cansend' => 'Cansend',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }
}
