<?php

namespace app\modules\documents\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "zse_v1_docs_tree".
 *
 * @property integer $id_folder
 * @property integer $root
 * @property integer $nleft
 * @property integer $nright
 * @property integer $level
 * @property string $icon
 * @property string $label
 * @property string $description
 *
 * @property DocsTreeTr[] $docsTreeTrs
 */
class MediaFolderSearch extends \app\modules\documents\models\DocsTree
{
    public $searchstring;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_docs_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['root', 'level'], 'required'],
            [['root', 'nleft', 'nright', 'level'], 'integer'],
            [['description'], 'string'],
            [['icon', 'label'], 'string', 'max' => 255],
            [['searchstring'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_folder' => 'Id Folder',
            'root' => 'Root',
            'nleft' => 'Nleft',
            'nright' => 'Nright',
            'level' => 'Level',
            'icon' => 'Icon',
            'label' => 'Label',
            'description' => 'Description',
        ];
    }

    public function search($params)
    {
        Yii::trace('params in model: '.print_r($params,true));
        $queryFolders = DocsTree::find()
            ->select([
                'id_folder as id',
                'label as name',
                "COALESCE(null,'folder') as type"
            ])
            ->orderBy(['label'=> SORT_ASC]);

        $queryMedias = Media::find()
            ->select([
                'zse_v1_media.id_media as id',
                'zse_v1_media.name as name',
                'zse_v1_media.type as type'
            ])
            ->joinWith('mediaLinks')
            ->groupBy('zse_v1_media.id_media')
            ->orderBy(['name'=> SORT_ASC]);

        $unionQuery = (new \yii\db\Query())
            ->from(['docs' => $queryFolders->union($queryMedias, true)]);

        $dataProvider = new ActiveDataProvider([
            'query' => $unionQuery,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' =>[
                'attributes'=>['name','type','sharable','size']
            ]]);


        $dataProvider->setSort([
            'attributes' => [
                'name',
                'type'
            ]
        ]);

        if (!($this->load($params,'') && $this->validate())) {
            return $dataProvider;
        }


        $unionQuery->orFilterWhere(['like', 'name', $this->searchstring]);

        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocsTreeTrs()
    {
        return $this->hasMany(DocsTreeTr::className(), ['id_folder' => 'id_folder']);
    }
}
