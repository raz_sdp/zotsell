<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:14 AM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\contacts\components\AppHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\Records */
/* @var $form yii\widgets\ActiveForm */


$type_id = Yii::$app->request->getQueryParam('type',1);
$type = \app\modules\contacts\components\Enum::getContactType($type_id);

if(yii::$app->controller->action->id == 'company_create'){
    $secondary_tab_disable = 'disabledTab';
    $title = 'Contact (' . $type . ') Create';
    $bred_crumbs_last = 'Create new contact';
} else {
    $type = ucfirst(AppHelper::getIconFromArrayVal(ArrayHelper::toArray($model), true));
    $secondary_tab_disable = '';
    $title = 'Contact (' . $type . ') Update';
    $bred_crumbs_last = 'Update contact'.$model->business_name;
}
$this->title=$title;

$this->breadCrums= [['label'=>'Contacts','link'=>Yii::$app->urlManager->createUrl('contacts/records/list')],$bred_crumbs_last];
?>

<div class="col-md-3">
    <?php echo Yii::$app->controller->renderPartial('../elements/_contact_tab',['secondary_tab_disable'=>$secondary_tab_disable]); ?>
</div>
<div class="col-md-9">
<div class="hpanel">
<div class="panel-body">
<?php if(!empty($msg)):?>
<div class="alert alert-success text-center js-update-alert-success" role="alert">
    <?php
        echo $msg;
    ?>
</div>
<?php endif; ?>

<div class="text-center hidden">
    We couldn't find any Document. Add one
</div>
<div class="tab-content">
<div id="note0" class="tab-pane active">
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3><?= $type;?> Info</h3>
<hr/>
<div class="note-content">
<div class="row">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="col-lg-4">
        <p> <h3>Contact Type</h3> </p>
        <div class="form-group" id="type_checklist">
            <?php
                $selected_types = array($type);
                if($model->is_customer==1 && $model->is_destination==2) {
                    $selected_types = array("Company", "Destination");
                }
            ?>
            <?= Html::checkboxList('type_options', $selected_types, \app\modules\contacts\components\Enum::getContactTypesSelect(), ['itemOptions'=>['class' => 'i-checks','labelOptions'=>['class'=>'col-lg-12']]]);?>
            <?php //$form->field($model,'type')->dropDownList(\app\modules\contacts\components\Enum::getContactTypesSelect()) ?>
        </div>        
        <p> <h3>Add Meta data</h3> </p>
        <div class="form-group">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'placeholder' => "Enter Contact ID" ])->label('Contact Id'); ?>
            <!--<label for="name">Contact ID </label>
            <input type="text" id="name" name="name" placeholder="Enter Contact ID " class="form-control" required>-->
        </div>
        <div class="form-group">
            <!--<label for="last_name">Destination ID </label>
            <input type="text" id="last_name" placeholder="Enter Destination ID " class="form-control" name="last_name">-->
            <?= $form->field($model, 'code_destination')->textInput(['maxlength' => true, 'placeholder' => "Enter Destination ID"])->label('Destination Id'); ?>
        </div>
        <p>
        <h3> Classification <span style="cursor: pointer" data-toggle="modal" data-target="#myModal4" class="label h-bg-green pull-right js-add-classification"><i
                    class="fa fa-pencil"></i></span></h3> </p>
        <div class="form-group slim_scroll">
            <label class="control-label">Classify this contact:</label>

            <div class="input-group">
                <div class="radio radio-primary">
                    <input class="id_classification" name="classification" value="" checked id="radio1" type="radio">
                    <label for="classification" class="text-primary">
                        None
                    </label>
                </div>
                <?php
                $classificationArray = array();
                if(isset($classifications) && !empty($classifications)) {
                    foreach ($classifications as $key => $item) {
                        $classificationArray[$item['id_classification']] = $item['label'];
                        $checked = '';
                        if(isset($model->id_classification)){
                            if($model->id_classification == $item['id_classification']){
                                $checked = 'checked';
                            }
                        }
                        ?>
                        <div class="radio radio-primary">
                            <input name="classification" <?=$checked?> value="<?php echo $item['id_classification'];?>" id="radio1" class="id_classification" type="radio">
                            <label for="classification" class="text-primary" style="color: <?= $item['color'];?>">
                                <?php echo $item['label'];?>
                            </label>
                        </div>
                    <?php }
                } ?>
                <?php //echo $form->field($model, 'id_classification')->radioList($classificationArray); ?>
            </div>
        </div>
        <?= $form->field($model, 'id_classification')->hiddenInput(['maxlength' => true]); ?>


        <p>

        <h3>Contact Owners <span data-toggle="modal" data-target="#myModal3" class="label h-bg-green pull-right"><i
                    class="fa fa-exchange"></i></span></h3> </p>

        <div class="form-group">
            <table class="table table-striped table-border" id="contact-owner">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tbody id="tbl-contact-owner-body">
                <?php               
                $owner_list_token_as_array=array();
                $owner_list_token='';                
                if(isset($contact_owner_list_of_this_record) && !empty($contact_owner_list_of_this_record)){
                    foreach($contact_owner_list_of_this_record as $owner_list){
                        echo "<tr>
                                <td>
                                    <span>".$owner_list['username']."</span>
                                </td>
                                <td>".$owner_list['usercode']."</td>
                              </tr>";
                        $owner_list_token .= ",".$owner_list['usercode'];
                        $owner_list_token_as_array[] = $owner_list['usercode'];
                    }

                    $owner_list_token = substr($owner_list_token,1);
                    #print_r($owner_list_token);die;
                }?>
                </tbody>
            </table>
            <input type="hidden" id="total-contact-owner-list" name="owner_list" value="<?php if(isset($owner_list_token) && $owner_list_token != '')echo $owner_list_token;?>"/>
        </div>
        <?php
        $status_checked = '';
        $status_checked_up = '';
        $status_checked_down = '';
        if(isset($model->record_status)){
            if($model->record_status == 0){
                $status_checked_down = 'checked';
                $status_checked_up = '';
            }else{
                $status_checked_down = '';
                $status_checked_up = 'checked';
                $status_checked = '';
            }
            $value = $model->record_status;
        }else{
            $status_checked = 'checked';
            $value = 1;
        }
        ?>
        <?= $form->field($model, 'record_status')->hiddenInput(['maxlength' => true,'value'=>$value]); ?>
        <div class="form-group">
            <label class="control-label">Status:</label>

            <div class="input-group">
                <div class="radio radio-primary">
                    <input class="status_record" name="status_record" value="1" <?=$status_checked?> <?=$status_checked_up?> id="radio1" type="radio">

                    <label for="radio1">
                        Active
                    </label>
                </div>
                <div class="radio radio-primary">

                    <input class="status_record" name="status_record" value="0" <?=$status_checked_down?> id="radio1" type="radio">
                    <label for="radio1">
                        Blocked
                    </label>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php
            $icon_checked = '';
            $icon_checked_up = '';
            $icon_checked_down = '';
            if(isset($model->icon)){
                if($model->icon != 'organizzazione_default.jpeg'){
                    $icon_checked_down = 'checked';
                    $icon_checked_up = '';
                    $value_icon = 2;
                }else{
                    $icon_checked_down = '';
                    $icon_checked_up = 'checked';
                    $icon_checked = '';
                    $value_icon = 1;
                }
            }else{
                $icon_checked = 'checked';
                $value_icon = 1;
            }
            ?>
            <input type="radio" class="standard_image icon_status_record" style="cursor: pointer" name="radio1" id="radio1" value="1" <?=$icon_checked?> <?=$icon_checked_up?>>
            <label for="radio1">
                Standard Icon
            </label>
            <input type="radio" class="custom_image icon_status_record" style="cursor: pointer" name="radio1" id="radio2" value="2" <?=$icon_checked_down?>>
            <label for="radio2">
                <a type="button"  data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                   aria-controls="collapseExample">
                    Custom image
                </a>
            </label>
            <div class="custom_image_field" style="display: none">
                <?= $form->field($model, 'icon')->fileInput(['class'=>'col-lg-12']) ?>
            </div>
            <?php
            $remove_image = '';
            if(isset($model->icon)){
                $remove_image = $model->icon;
                $remove_image_path = Yii::$app->homeUrl . 'uploads/'.$model->id_record.'-'.$model->icon;
            }
            ?>
            <input value="<?=$remove_image?>" type="hidden" name="remove_image">
            <?php if(isset($model->icon) && $value_icon == 2){ ?>
                <img style="width: 50px;height:50px" src="<?=str_replace('/web','',$remove_image_path)?>">
            <?php } ?>
            <input value="<?=$value_icon?>" name="icon_status" type="hidden" class="icon_status">
        </div>
        <div style="display: none" class="collapse out" id="collapseExample">
            <div class="upload-drop-zone" id="drop-zone">
                Drag and drop 100 x 100 icon here
            </div>
        </div>
    </div>
<div class="col-lg-4">
    <p> <h3>Add Info data</h3> </p>
    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true,'placeholder'=>'Enter Name']) ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true,'placeholder'=>'Enter Address']) ?>
    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true,'placeholder'=>'Enter Zip']) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true,'placeholder'=>'Enter City']) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,'placeholder'=>'Enter Phone']) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'Enter Email']) ?>
    <?php if(0): ?>
        <?= $form->field($model, 'legal_status')->textInput(['maxlength' => true,'placeholder'=>'Enter Legal Status']) ?>
    <?php endif; ?>
    <?= $form->field($model, 'fax')->textInput(['maxlength' => true,'placeholder'=>'Enter Fax']) ?>
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true,'placeholder'=>'Enter Mobile']) ?>
    <?= $form->field($model, 'person_in_charge')->textInput(['maxlength' => true,'placeholder'=>'Enter Person In Charge']) ?>
    <?= $form->field($model, 'role')->textInput(['maxlength' => true,'placeholder'=>'Enter Person Role']) ?>
    <?= $form->field($model, 'direct_mail')->textInput(['maxlength' => true,'placeholder'=>'Enter Direct Email']) ?>
    <?= $form->field($model, 'direct_phone')->textInput(['maxlength' => true,'placeholder'=>'Enter Direct Phone']) ?>
    <?php if(0): ?>
        <?= $form->field($model, 'external_code')->textInput(['maxlength' => true,'placeholder'=>'Enter External Code'])?>
        <?= $form->field($model, 'type')->hiddenInput(['maxlength' => true,'value'=>$type]) ?>
    <?php endif; ?>
    <?= $form->field($model, 'province')->textInput(['maxlength' => true,'placeholder'=>'Enter Province']) ?>
    <?= $form->field($model, 'vat_code')->textInput(['maxlength' => true,'placeholder'=>'Enter Vat Code']) ?>
    <?= $form->field($model, 'fiscal_code')->textInput(['maxlength' => true,'placeholder'=>'Enter Fiscal Code']) ?>

    <div class="form-group">
            <?= $form->field($model,'country')->dropDownList(\app\modules\contacts\components\Enum::getCountryList()) ?>
    </div>

    <div class="form-group">
        <?= $form->field($model,'pricelist')->dropDownList(\app\modules\contacts\components\Enum::getPriceList()) ?>
    </div>


    <?php
        $options = isset($model->options)?json_decode($model->options,1):array();
        $gruppi = isset($options['gruppi'][0]) ? $options['gruppi'][0] : "";
        $pagamento = isset($options['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO']) ? $options['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO'] : "";
        $resa = isset($options['dati_convenzionati']['CONDIZIONE_DI_RESA']) ? $options['dati_convenzionati']['CONDIZIONE_DI_RESA'] : "";
        $iva = "";

        $options_aa = isset($options['note_aggiuntive']) ? $options['note_aggiuntive'] : array();
        foreach($options_aa as $item) {
            if($item['field_id']=='CONDIZIONE_IVA') {
                $iva = isset($item['valore']) ? $item['valore'] : "";
            }
        }
    ?>
    <div class="form-group">
        <label class="control-label" for="records-pricelist">Payment Condition:</label>
        <?= Html::dropDownList('payment_condition', $pagamento, ArrayHelper::map($payment_conditions, 'code_external', 'label'), array('class'=>'form-control', 'prompt'=>'Choose Payment Condition')) ?>
    </div>

    <div class="form-group">
        <label class="control-label" for="records-pricelist">Incoterms:</label>
        <?= Html::dropDownList('incoterm', $resa, ArrayHelper::map($incoterms, 'code_external', 'label'), array('class'=>'form-control', 'prompt'=>'Choose Incoterms')) ?>
    </div>

    <div class="form-group">
        <label class="control-label" for="records-pricelist">VAT Condition:</label>
        <?= Html::dropDownList('vat_condition', $iva, ArrayHelper::map($vat_conditions, 'label', 'label'), array('class'=>'form-control', 'prompt'=>'Choose VAT Condition')) ?>
    </div>

    <div class="form-group">
        <label class="control-label" for="records-fiscal_code">Contact Group:</label>
        <input type="text" id="records-contact-group" value="<?= $gruppi;?>" class="form-control" name="contact_group" maxlength="16" placeholder="Enter Group Code">
    </div>

</div>
<div class="col-lg-4" id="extended_datas">
    <p>
        <h3>
            Add Extended data
            <span data-toggle="modal" data-target="#myModal5" class="label h-bg-green pull-right">
                <i class="fa fa-pencil"></i>
            </span>
        </h3>
    </p>

    <?php
        $options = isset($options['note_aggiuntive']) ? $options['note_aggiuntive'] : array();
        //print_r($options); die();
        if(!empty($options)) {
        foreach($options as $item) {
            if($item['field_id']=='CONDIZIONE_DI_PAGAMENTO'
                || $item['field_id']=='CONDIZIONE_DI_RESA'
                || $item['field_id']=='CONDIZIONE_IVA') {

                continue;
            }
    ?>
            <div class="form-group">
                <label for="<?= $item['label'];?>"><?= $item['label'];?></label>
                <input type="hidden" name="Records[options_label][]" value="<?= $item['label'];?>">
                <input type="hidden" name="Records[options_field_id][]" value="<?= $item['field_id'];?>">

                <?php if($item['tipo'] == "TESTO") { ?>

                    <input type="hidden" name="Records[options_type][]" value="TESTO">
                    <input type="text" id="<?= $item['label'];?>_value" name="Records[options][]" value="<?= $item['valore'];?>" class="form-control">";

                <?php } else if($item['tipo'] == "TESTO_ESTESO") { ?>

                    <input type="hidden" name="Records[options_type][]" value="TESTO_ESTESO">
                    <textarea id="<?= $item['label'];?>_value" name="Records[options][]" class="form-control" rows="2" style="font-family: Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;"><?= $item['valore'];?></textarea>

                <?php } ?>
            </div>
    <?php }
    } ?>

</div>

</div>
<div class="col-lg-12">
    <div class="btn-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : '<i class="fa fa-thumbs-o-up"></i> Save', ['class' => $model->isNewRecord ? 'btn btn-success js-recoed-save-btn' : 'btn btn-sm btn-default js-recoed-save-btn']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
</div>
<div class="btn-group" style="display: none">
    <button class="btn btn-sm btn-default"><i class="fa fa-thumbs-o-up"></i> Save</button>
    <button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> Remove</button>
</div>
</div>

<div id="note1" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_connections', ['connections' => $connections]); ?>
</div>

<div id="note2" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_documents', ['documents' => $documents]); ?>
</div>

<div id="note3" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_sales', ['orders' => $orders]); ?>
</div>

<div id="note4" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_activities', ['activities' => $activities]); ?>
</div>

<div id="note5" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_maps', ['coordinates'=>$model->coordinates]); ?>
</div>
</div>

</div>
</div>
</div>
<!--extended_data_modal-->
<?php echo Yii::$app->controller->renderPartial('../elements/_extended_data_modal'); ?>

<!--classification_modal-->
<!-- myModal4-->
<?php echo Yii::$app->controller->renderPartial('../elements/_classification_modal',array('classifications'=>$classifications)); ?>

<!--contacts_owner modal-->
<!-- myModal3-->
<?php echo Yii::$app->controller->renderPartial('../elements/_contacts_owner_modal',['owner_list_token_as_array' => $owner_list_token_as_array]); ?>

<!-- Footer-->

<style>
    .modal-backdrop {
        z-index: 0 !important;
    }
    #contact-owner{
        table-layout: fixed;
        word-wrap: break-word;
    }
    
</style>