<?php

use yii\db\Migration;

class m160223_153409_drop_contrain_activities extends Migration
{
    public function up()
    {
       $this->execute('SET foreign_key_checks = 0');
	
		try {
			$this->execute('ALTER TABLE zse_v1_activities DROP FOREIGN KEY zse_v1_activities_ibfk_1;');
		} catch(Exception $e) {
			echo "Non ho potuto togliere la chiave esterna zse_v1_activities_ibfk_1 dalla tabella zse_v1_activities\n";
			return;
		}	

		try {
			$this->execute('ALTER TABLE zse_v1_activities DROP FOREIGN KEY zse_v1_activities_ibfk_2;');
		} catch(Exception $e) {
			echo "Non ho potuto togliere la chiave esterna zse_v1_activities_ibfk_2 dalla tabella zse_v1_activities\n";
			return;
		}
		$this->execute('SET foreign_key_checks = 1');
    }

    public function down()
    {
       
    }
}
