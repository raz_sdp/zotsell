<?php

namespace app\modules\reports;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\reports\controllers';
    public $defaultRoute = 'main';


    public function init()
    {
        parent::init();

        $this->layout = 'main';
    }
}
