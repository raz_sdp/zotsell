<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:14 AM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\Records */
/* @var $form yii\widgets\ActiveForm */
$this->title = "Contact (Company) Create/update";
//$this->subtitle="Use Folder view to show Contacs in a sortable folder .";
$this->breadCrums = [['label'=>'Contacts','link'=>''],'Folder View','root'];
?>
<div class="col-md-3">
    <?php echo Yii::$app->controller->renderPartial('../elements/_contact_tab'); ?>
</div>
<div class="col-md-9">
<div class="hpanel">

<div class="panel-body">

<div class="text-center hidden">
    We couldn't find any Document. Add one
</div>

<div class="tab-content">
<div id="note1" class="tab-pane active">
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3>Info</h3>
<hr/>
<div class="note-content">
<div class="row">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<div class="col-lg-4">
    <p>
    <h3>Add Meta data</h3> </p>
        <div class="form-group">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            <!--<label for="name">Contact ID </label>
            <input type="text" id="name" name="name" placeholder="Enter Contact ID " class="form-control" required>-->
        </div>
        <div class="form-group">
            <!--<label for="last_name">Destination ID </label>
            <input type="text" id="last_name" placeholder="Enter Destination ID " class="form-control" name="last_name">-->
            <?= $form->field($model, 'code_destination')->textInput(['maxlength' => true]) ?>
        </div>
        <p>
        <h3> Classification <span data-toggle="modal" data-target="#myModal4" class="label h-bg-green pull-right"><i
                class="fa fa-pencil"></i></span></h3> </p>
        <div class="form-group">
            <label class="control-label">Classify this contact:</label>

            <div class="input-group">
                <div class="radio radio-primary">
                    <input name="classification" checked id="radio1" type="radio">
                    <label for="classification" class="text-primary">
                        None
                    </label>
                </div>
                <?php
                $classificationArray = array();
                if(isset($classifications) && !empty($classifications)) {
                    foreach ($classifications as $key => $item) {
                        $classificationArray[$item['id_classification']] = $item['label'];
                        ?>
                        <div class="radio radio-primary">
                            <input name="classification" value="<?php echo $item['id_classification'];?>" id="radio1" class="id_classification" type="radio">
                            <label for="classification" class="text-primary" style="color: <?= $item['color'];?>">
                                <?php echo $item['label'];?>
                            </label>
                        </div>
                    <?php }
                } ?>
                <?php //echo $form->field($model, 'id_classification')->hiddenInput()->label(false); ?>
            </div>
        </div>


        <p>

        <h3>Contact Owners <span data-toggle="modal" data-target="#myModal3" class="label h-bg-green pull-right"><i
                class="fa fa-exchange"></i></span></h3> </p>

        <div class="form-group">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Code</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        Enrico Talin</span>
                    </td>
                    <td>001</td>
                </tr>
                <tr>
                    <td>
                        John Doe
                    </td>
                    <td>K23f</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="form-group">
            <?= $form->field($model, 'record_status')->radioList(['1' => 'Active', '0' => 'Blocked']) ?>
        </div>

        <div class="form-group">
            <input type="radio" name="radio1" id="radio1" value="option1" checked="">
            <label for="radio1">
                Standard Icon
            </label>
            <input type="radio" name="radio1" id="radio2" value="option2">
            <label for="radio2">
                <a type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false"
                   aria-controls="collapseExample">
                    Custom image
                </a>
            </label>

        </div>
        <div class="collapse out" id="collapseExample">
            <div class="upload-drop-zone" id="drop-zone">
                Drag and drop 100 x 100 icon here
            </div>
        </div>
</div>

<div class="col-lg-4">
    <p>
    <h3>Add Info data</h3> </p>
    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'legal_status')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'person_in_charge')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'direct_mail')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'direct_phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'external_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'vat_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fiscal_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'pricelist')->textInput(['maxlength' => true]) ?>

    <?php if(0): ?>
        <div class="form-group"><label for="name">Business Name </label> <input type="text" id="name" name="name"
                                                                                placeholder="Enter Name "
                                                                                class="form-control" required></div>
        <div class="form-group"><label for="last_name"> Address</label> <input type="text" id="last_name"
                                                                               placeholder="Enter Address"
                                                                               class="form-control" name="last_name">
        </div>
        <div class="form-group"><label for="last_name"> Zip</label> <input type="text" id="last_name"
                                                                           placeholder="Enter Zip" class="form-control"
                                                                           name="last_name"></div>
        <div class="form-group"><label for="last_name"> City</label> <input type="text" id="last_name"
                                                                            placeholder="Enter City"
                                                                            class="form-control" name="last_name"></div>
        <div class="form-group"><label for="last_name"> Phone</label> <input type="text" id="last_name"
                                                                             placeholder="Enter Phone"
                                                                             class="form-control" name="last_name">
        </div>
        <div class="form-group"><label for="last_name"> Email</label> <input type="text" id="last_name"
                                                                             placeholder="Enter Email"
                                                                             class="form-control" name="last_name">
        </div>


        <div class="form-group"><label for="name">Legal Status </label> <input type="text" id="name" name="name"
                                                                               placeholder="Enter Legal Status "
                                                                               class="form-control" required></div>
        <div class="form-group"><label for="last_name"> Fax</label> <input type="text" id="last_name"
                                                                           placeholder="Enter Fax" class="form-control"
                                                                           name="last_name"></div>
        <div class="form-group"><label for="last_name"> Mobile</label> <input type="text" id="last_name"
                                                                              placeholder="Enter Mobile"
                                                                              class="form-control" name="last_name">
        </div>
        <div class="form-group"><label for="last_name"> Person In Charge</label> <input type="text" id="last_name"
                                                                                        placeholder="Enter Person In Charge"
                                                                                        class="form-control"
                                                                                        name="last_name">
        </div>
        <div class="form-group"><label for="last_name"> Direct Mail</label> <input type="text" id="last_name"
                                                                                   placeholder="Enter Direct Mail"
                                                                                   class="form-control"
                                                                                   name="last_name"></div>
        <div class="form-group"><label for="last_name"> Direct Phone</label> <input type="text" id="last_name"
                                                                                    placeholder="Enter Direct Phone"
                                                                                    class="form-control"
                                                                                    name="last_name"></div>
        <div class="form-group"><label for="External Code">External Code </label> <input type="text" id="name"
                                                                                         name="name"
                                                                                         placeholder="Enter External Code  "
                                                                                         class="form-control" required>
        </div>
        <div class="form-group"><label for="last_name"> Province</label> <input type="text" id="last_name"
                                                                                placeholder="Enter Province"
                                                                                class="form-control" name="last_name">
        </div>
        <div class="form-group"><label for="last_name">Vat Code</label> <input type="text" id="last_name"
                                                                               placeholder="Enter Vat Code"
                                                                               class="form-control" name="last_name">
        </div>
        <div class="form-group"><label for="last_name">Fiscal Code</label> <input type="text" id="last_name"
                                                                                  placeholder="Enter Fiscal Code"
                                                                                  class="form-control" name="last_name">
        </div>
    <?php endif; ?>

        <div class="form-group">
            <div class="input-group">
                <?php
                $country =  array(
                    ''=>'Select Country',
                    'Italy'=>'Italy',
                    'Usa'=>'Usa',
                    'Angola'=>'Angola',
                    'Argentina'=>'Argentina',
                );
                ?>
                <?= $form->field($model,'country')->dropDownList($country) ?>
            </div>
        </div>

        <div class="form-group">
            <?php
            $pricelist =  array(
                ''=>'Select Price List',
                'Listino test sconto'=>'Listino test sconto',
                'Listino netto caso 1'=>'Listino netto caso 1',
            );
            ?>
            <?= $form->field($model,'pricelist')->dropDownList($pricelist) ?>
        </div>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
</div>
<div class="col-lg-4">
    <p>
    <h3>Add Extended data <span data-toggle="modal" data-target="#myModal5" class="label h-bg-green pull-right"><i
            class="fa fa-pencil"></i></span></h3> </p>

        <div class="form-group"><label for="name">Partitario</label> <textarea class="form-control" rows="2"
                                                                               Style="font-family: Courier New,Courier,Lucida Sans Typewriter,Lucida Typewriter,monospace;">
            multi-line data example\r
            Invoice Date Amount Status\r
            5465464 17/8/2015 $354 unpaid\r
            5465464 17/7/2015 $354 paid\r
            5465464 17/7/2015 $354 paid\r</textarea>
            <label for="distinta-base">Distinta Base </label> <input type="text" id="distinta-base" name="name"
                                                                     placeholder="Enter Distinta Base "
                                                                     class="form-control" required></div>



</div>

</div>
</div>
<div class="btn-group">
    <button class="btn btn-sm btn-default"><i class="fa fa-thumbs-o-up"></i> Save</button>
    <button class="btn btn-sm btn-default"><i class="fa fa-trash"></i> Remove</button>
</div>
</div>

<div id="note3" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_connections', ['connections' => $connections]); ?>
</div>

<div id="note4" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_documents', ['documents' => $documents]); ?>
</div>

<div id="note5" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_sales', ['orders' => $orders]); ?>
</div>

<div id="note6" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_activities', ['activities' => $activities]); ?>
</div>

<div id="note7" class="tab-pane">
    <?php echo Yii::$app->controller->renderPartial('../elements/_maps'); ?>
</div>
</div>

</div>
</div>
</div>

<!--extended_data_modal-->
<?php echo Yii::$app->controller->renderPartial('../elements/_extended_data_modal'); ?>

<!--classification_modal-->
<!-- myModal4-->
<?php echo Yii::$app->controller->renderPartial('../elements/_classification_modal',array('classifications'=>$classifications)); ?>

<!--contacts_owner modal-->
<!-- myModal3-->
<?php echo Yii::$app->controller->renderPartial('../elements/_contacts_owner_modal'); ?>

<!-- Footer-->

<style>
    .modal-backdrop {
        z-index: 0 !important;
    }
</style>
