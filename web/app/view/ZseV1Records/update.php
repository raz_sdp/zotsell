<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ZseV1Records */

$this->title = 'Update Zse V1 Records: ' . ' ' . $model->id_record;
$this->params['breadcrumbs'][] = ['label' => 'Zse V1 Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_record, 'url' => ['view', 'id' => $model->id_record]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zse-v1-records-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
