<?php

namespace app\modules\contacts;

use yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\contacts\controllers';
    //public $defaultRoute = 'main';
    public $layout = 'main';

    public function init()
    {
        parent::init();
        //echo yii::$app->basepath;die();
        //\Yii::$app->view->theme->pathMap = ['@app/modules/contacts/views' => '@app/themes/themeLTE']; //don't using
        //$this->layoutPath = \Yii::getAlias('@app/themes/themeLTE/layouts');
        //$this->layout = 'main';

        //}
        //app\assets\AppAsset::register($this);
        //app\modules\contacts\assets\AppAssetContacts::register($this);
        
        // apply theme only in module (http://www.yiiframework.com/forum/index.php/topic/56890-solved-yii-2-module-theme/)
        
        //\Yii::$app->view->theme = new \yii\base\Theme([
        //    'pathMap' => ['@app/views' => '@app/themes/themeLTE'],
            //'baseUrl' => '@web/themes/themeLTE',
        //]);
        
    }
}
