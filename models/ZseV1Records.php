<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zse_v1_records".
 *
 * @property integer $id_record
 * @property integer $id_locality
 * @property integer $id_classification
 * @property string $code
 * @property string $code_destination
 * @property string $business_name
 * @property string $legal_status
 * @property string $type
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $fax
 * @property string $mobile
 * @property string $inserted_by
 * @property string $assigned_to
 * @property string $origin
 * @property integer $is_contact
 * @property integer $is_customer
 * @property integer $is_supplier
 * @property integer $is_prospect
 * @property integer $is_destination
 * @property integer $is_reseller
 * @property integer $is_intrested
 * @property integer $authorize_send
 * @property string $coordinates
 * @property string $external_code
 * @property string $icon
 * @property string $country
 * @property string $province
 * @property string $city
 * @property string $postal_code
 * @property string $vat_code
 * @property string $fiscal_code
 * @property string $pricelist
 * @property string $options
 * @property string $person_in_charge
 * @property string $role
 * @property string $direct_mail
 * @property string $direct_phone
 * @property string $date_modification
 * @property integer $record_status
 *
 * @property ZseV1AccountsServicesLicenses[] $zseV1AccountsServicesLicenses
 * @property ZseV1Orders[] $zseV1Orders
 * @property ZseV1RecordsClassifications $idClassification
 * @property ZseV1RecordsLinks[] $zseV1RecordsLinks
 * @property ZseV1RecordsLinks[] $zseV1RecordsLinks0
 */
class ZseV1Records extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_records';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_locality', 'id_classification', 'is_contact', 'is_customer', 'is_supplier', 'is_prospect', 'is_destination', 'is_reseller', 'is_intrested', 'authorize_send', 'record_status'], 'integer'],
            [['options'], 'string'],
            [['date_modification'], 'safe'],
            [['code', 'phone', 'fax', 'mobile'], 'string', 'max' => 32],
            [['code_destination', 'type', 'email', 'inserted_by', 'assigned_to', 'external_code', 'country', 'province', 'city'], 'string', 'max' => 64],
            [['business_name', 'address', 'icon', 'person_in_charge', 'role', 'direct_mail', 'direct_phone'], 'string', 'max' => 255],
            [['legal_status', 'origin', 'fiscal_code'], 'string', 'max' => 16],
            [['coordinates'], 'string', 'max' => 128],
            [['postal_code'], 'string', 'max' => 8],
            [['vat_code'], 'string', 'max' => 11],
            [['pricelist'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_record' => 'Id Record',
            'id_locality' => 'Id Locality',
            'id_classification' => 'Id Classification',
            'code' => 'Code',
            'code_destination' => 'Code Destination',
            'business_name' => 'Business Name',
            'legal_status' => 'Legal Status',
            'type' => 'Type',
            'address' => 'Address',
            'email' => 'Email',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'mobile' => 'Mobile',
            'inserted_by' => 'Inserted By',
            'assigned_to' => 'Assigned To',
            'origin' => 'Origin',
            'is_contact' => 'Is Contact',
            'is_customer' => 'Is Customer',
            'is_supplier' => 'Is Supplier',
            'is_prospect' => 'Is Prospect',
            'is_destination' => 'Is Destination',
            'is_reseller' => 'Is Reseller',
            'is_intrested' => 'Is Intrested',
            'authorize_send' => 'Authorize Send',
            'coordinates' => 'Coordinates',
            'external_code' => 'External Code',
            'icon' => 'Icon',
            'country' => 'Country',
            'province' => 'Province',
            'city' => 'City',
            'postal_code' => 'Postal Code',
            'vat_code' => 'Vat Code',
            'fiscal_code' => 'Fiscal Code',
            'pricelist' => 'Pricelist',
            'options' => 'Options',
            'person_in_charge' => 'Person In Charge',
            'role' => 'Role',
            'direct_mail' => 'Direct Mail',
            'direct_phone' => 'Direct Phone',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1AccountsServicesLicenses()
    {
        return $this->hasMany(ZseV1AccountsServicesLicenses::className(), ['id_record' => 'id_record']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1Orders()
    {
        return $this->hasMany(ZseV1Orders::className(), ['record_code' => 'code', 'record_destination_code' => 'code_destination']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClassification()
    {
        return $this->hasOne(ZseV1RecordsClassifications::className(), ['id_classification' => 'id_classification']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1RecordsLinks()
    {
        return $this->hasMany(ZseV1RecordsLinks::className(), ['id_record' => 'id_record']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZseV1RecordsLinks0()
    {
        return $this->hasMany(ZseV1RecordsLinks::className(), ['id_link' => 'id_record']);
    }
}
