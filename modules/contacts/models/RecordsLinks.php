<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_records_links".
 *
 * @property integer $id_record
 * @property integer $id_link
 * @property string $role
 * @property string $date_modification
 * @property integer $record_status
 *
 * @property ZseV1Records $idRecord
 * @property ZseV1Records $idLink
 */
class RecordsLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_records_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_record', 'id_link'], 'required'],
            [['id_record', 'id_link', 'record_status'], 'integer'],
            [['date_modification'], 'safe'],
            [['role'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_record' => 'Id Record',
            'id_link' => 'Id Link',
            'role' => 'Role',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRecord()
    {
        return $this->hasOne(ZseV1Records::className(), ['id_record' => 'id_record']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdLink()
    {
        return $this->hasOne(ZseV1Records::className(), ['id_record' => 'id_link']);
    }

    /**
     * @return array
     * get all connections of a record
     */
    public function getContactConnections($id, $fields = [], $filter_where = [], $limit= null, $offset = null, $sort = "") {
        $command = (new \yii\db\Query())
                    #->select(['`zse_v1_records`.*', '`zse_v1_records_links`.id_link'])
                    ->select($fields)
                    ->from('`zse_v1_records_links`')
                    ->innerJoin('`zse_v1_records`', '`zse_v1_records_links`.`id_link` = `zse_v1_records`.`id_record`')
                    ->Where(['`zse_v1_records_links`.`id_record`'=> $id])
                    ->andFilterWhere($filter_where)
                    ->limit($limit)
                    ->offset($offset)
                    ->orderBy($sort)
                    ->createCommand();

        return $command->queryAll();
    }

    /**
     * Get contact connection count
     * @param $id
     * @param array $filter_where
     * @return int|string
     */
    public function countContactConnections($id, $filter_where = []) {
        $command = (new \yii\db\Query())
            ->from('`zse_v1_records_links`')
            ->innerJoin('`zse_v1_records`', '`zse_v1_records_links`.`id_link` = `zse_v1_records`.`id_record`')
            ->Where(['`zse_v1_records_links`.`id_record`'=> $id])
            ->andFilterWhere($filter_where)
            ->count();

        return $command;
    }
    /**
     * @param $data
     * saves the selected record codes against agent code
     */
    public function saveRecordLinks($data)
    {
        $id_record = $data['id'];
        if (empty($data['connections']))
            $connections = array();
        else
            $connections = $data['connections'];

        foreach ($connections as $item) {
            Yii::$app->db->createCommand()
                ->insert('zse_v1_records_links', [
                    'id_record' => $id_record,
                    'id_link' => $item,
                ])->execute();
        }
    }
}
