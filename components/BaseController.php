<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:50 AM
 */

namespace app\components;
use yii\web\Controller;

/**
 * RecordsController implements the CRUD actions for Records model.
 */
class BaseController extends Controller
{
   //public $subtitle;
   public static function _setTrace($data = null, $die = true)
    {
        if (is_string($data)) {
            print $data;
        } else {

            print "<pre>";
            print_r($data);
            print "</pre>";
        }
        print "<hr />";
        if ($die) {
            exit();
        }
    }
}