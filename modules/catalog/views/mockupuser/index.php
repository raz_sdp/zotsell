


<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\boostrap\Modal;
    $this->title = 'Catalog List';
    $this->params['breadcrumbs'][] = $this->title;
?>




<!-- 
  <h1>Catalog list</h1>
  <p class="lead">Manage all your  %number% Articles</p>
 -->
  
  


<br>

  
  

<div class="row">

<div class="nav navbar-nav navbar-left col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Orientation</a></li>
            <li><a href="#">Classification</a></li>
            <li><a href="#">Status</a></li>
            <li><a href="#">Kind</a></li>
             <li><a href="#">Favorites</a></li>
              <li><a href="#">Type</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">remove filters</a></li>
          </ul>
        </li>
      </div>


<form class="col-md-10" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder=" Search">
  </div>
</form>



<div class="nav navbar-nav navbar-right col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
       <li><a href="/zot/web/catalog/<?=  $mockup  ?>"><i class="fa fa-check"></i>  List</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/folders">  Folders</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/flow">Flow</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/squares">Squares</a></li>
             <li><a href="/zot/web/catalog/<?=  $mockup  ?>/flipbook"> Flipbook</a></li>
              <li><a href="/zot/web/catalog/<?=  $mockup  ?>/frequent">Frequents</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="pdf">Download pdf</a></li>
            <li><a href="csv">Download csv</a></li>
        </li>
      </div>

    
   </div>    





  
  











<table class="table table-hover">




<thead> <tr> <th class="col-md-1"></th> <th> </th> <th class="col-md-2"> </th><th> </th>  </tr> </thead>

<tr>


<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>





<address>
  <strong>Article 1 (no customer selected) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			   
 			   
 			   
	<!-- 
	<a href="<?= Url::to('mockupuser/details') ?>" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> 
			<a href="/zot/web/catalog/default/edit" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="/zot/web/catalog/default/bulk" title="move" aria-label="Update" data-pjax="0"><i class="fa fa-arrows"></i></a> 
				<a href="#" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
 -->
				
				<br>
				<?= Html::button('400 &euro;',  ['value' => Url::to($mockup.'/selectcustomer'),'class' => 'btn btn-default modalButton']) ?><br>
				
				
				
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">45 </span>
				
				</td>
<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>				
				<br><i class="fa fa-chevron-right"></i>
</td> 	
    
</tr> 
  
  
  <tr> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<address>
  <strong>Article 2 (basic purchase) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 	<!-- 
		   <a href="/zot/web/catalog/default/details" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> 
				<a href="/zot/web/catalog/default/edit" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
				<a href="/zot/web/catalog/default/bulk" title="move" aria-label="Update" data-pjax="0"><i class="fa fa-arrows"></i></a> 
				<a href="#" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
 -->

<br>
				<?= Html::button('400 &euro;',  ['value' => Url::to('mockupuser/buyitem'),'class' => 'btn btn-default modalButton']) ?><br>
			<!-- 
	<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">30 12/12/2016</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">30 </span>
 -->

</td> 	

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>				
				<br><i class="fa fa-chevron-right"></i>
</td> 	
    
</tr> 

<tr> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<address>
  <strong>Article 3 (kit purchase) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
<!-- 
 			   <a href="/zot/web/catalog/default/details" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> 
				<a href="/zot/web/catalog/default/edit" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a> 
				<a href="/zot/web/catalog/default/bulk" title="move" aria-label="Update" data-pjax="0"><i class="fa fa-arrows"></i></a> 
				<a href="#" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>

 -->
<br>
				 <a  href="<?=  $mockup  ?>/buykit" class="btn btn-default" type="button"  ><i class="fa fa-shopping-cart"></i> Buy </a><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">50 12/12/2016</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">- </span>

</td> 	

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>				
				<br><i class="fa fa-chevron-right"></i>
</td> 	
    
</tr> 

<tr> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<address>
  <strong>Article 4</strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
<!-- 
 			   <a href="/zot/web/catalog/default/details" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> 
				<a href="/zot/web/catalog/default/edit" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a> 
				<a href="/zot/web/catalog/default/bulk" title="move" aria-label="Update" data-pjax="0"><i class="fa fa-arrows"></i></a> 
				<a href="#" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
 -->

<br>
				<a  href="#" class="btn btn-default" type="button"  > 400 &euro; </a><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">- </span>

</td> 
<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>				
				<br><i class="fa fa-chevron-right"></i>
</td> 	
    
</tr> 

<tr> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>
<address>
  <strong>Article 5</strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
<!-- 
 			   <a href="/zot/web/catalog/default/details" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a> 
				<a href="/zot/web/catalog/default/edit" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a> 
				<a href="/zot/web/catalog/default/bulk" title="move" aria-label="Update" data-pjax="0"><i class="fa fa-arrows"></i></a> 
				<a href="#" title="Delete" aria-label="Delete" data-confirm="Are you sure you want to delete this item?" data-method="post" data-pjax="0"><span class="glyphicon glyphicon-trash"></span></a>
 -->

<br>
				<a  href="#" class="btn btn-default" type="button"  > 400 &euro; </a><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">-</span>

</td> 
<td class='clickable-row' data-href='<?=  $mockup  ?>/details'>				
				<br><i class="fa fa-chevron-right"></i>
</td> 	
    
</tr> 




  
  

</table>


<nav>
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>


