<?php

namespace app\modules\documents\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the search model class for table "zse_v1_repository_files".
 *
 * @property integer $id_file
 * @property string $mimetype
 * @property string $name
 * @property integer $origin
 * @property integer $size
 * @property integer $id_account
 * @property string $date_insertion
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class SearchRepositoryFiles extends \app\modules\documents\models\RepositoryFiles
{
    public $type;
    //configuration for dropzone file's uploading 'Upload file' (http://www.dropzonejs.com/#configuration-options)
    public static $dropzoneConfig = [
        'maxFilesize' => '2', // max file's size (Mb)
        'maxFiles' => '3', // max file for upload in one zone
        'dictDefaultMessage' => 'Just drag and drop files here', //default message before uploading
        'url' => 'upload', // controller DocumentController.php, actionUpload
    ];

    /**
     * @var array list of media types and related mime-types
     * - icon: icon's style
     * - label: string, optional, specifies the menu item label.
     * - urlNew: array, optional, specifies the URL for creating new item. It will be processed by [[Url::to]].
     * - mimeTypes: array of available mimetypes for this type. if array element == '*' => any mimetype
     */
    public static $typeRelations = [
        'pdf' => [
            'icon' => 'fa fa fa-file-pdf-o',
            'label' => 'Pdf',
            'urlNew' => ['document/create', 'type' => 'pdf'],
            'mimeTypes' => ['application/pdf'],
        ],
        'movie' => [
            'icon' => 'fa fa-file-movie-o',
            'label' => 'Movie',
            'urlNew' => ['document/create', 'type' => 'movie'],
            'mimeTypes' => ['video/mpeg', 'video/mp4', 'video/ogg', 'video/quicktime', 'video/webm', 'video/x-ms-wmv', 'video/x-flv', 'video/3gpp', 'video/3gpp2'],
        ],
        'html5' => [
            'icon' => 'fa fa-file-code-o',
            'label' => 'HTML5',
            'urlNew' => ['document/create', 'type' => 'html5'],
            'mimeTypes' => ['text/html']
        ],
        'gallery' => [
            'icon' => 'fa fa-file-image-o',
            'label' => 'gallery',
            'urlNew' => ['document/create', 'type' => 'gallery'],
            'mimeTypes' => ['image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/svg+xml', 'image/tiff', 'image/vnd.microsoft.icon', 'image/vnd.wap.wbmp']
        ],
        'powerpoint' => [
            'icon' => 'fa fa-file-powerpoint-o',
            'label' => 'PowerPoint',
            'urlNew' => ['document/create', 'type' => 'powerpoint'],
            'mimeTypes' => ['application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation']
        ],
        'excel' => [
            'icon' => 'fa fa-file-excel-o',
            'label' => 'Excel',
            'urlNew' => ['document/create', 'type' => 'excel'],
            'mimeTypes' => ['application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']
        ],
        'word' => [
            'icon' => 'fa fa-file-word-o',
            'label' => 'Word',
            'urlNew' => ['document/create', 'type' => 'word'],
            'mimeTypes' => ['application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
        ],
        'file' => [
            'icon' => 'fa fa-file',
            'label' => 'File',
            'urlNew' => ['document/create', 'type' => 'file'],
            'mimeTypes' => ['*']
        ],
    ];

    public static function tableName()
    {
        return 'zse_v1_repository_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin', 'size', 'id_account', 'record_status', 'date_insertion'], 'integer'],
            [['date_insertion', 'date_modification', 'type'], 'safe'],
            [['mimetype', 'name'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name'], 'required', 'on' => ['create']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['name'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_file' => 'Id File',
            'mimetype' => 'Mimetype',
            'name' => 'Name',
            'origin' => 'Origin',
            'size' => 'Size',
            'id_account' => 'Id Account',
            'date_insertion' => 'Date Insertion',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * search model in repository files
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepositoryFiles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);


        $dataProvider->setSort([
            'attributes' => [
                'name',
            ]
        ]);

        if (!($this->load($params, '') && $this->validate())) {
            return $dataProvider;
        }

        if (isset($params['SearchRepositoryFiles'])) {
            $this->name = ($params['SearchRepositoryFiles']['name']) ? $params['SearchRepositoryFiles']['name'] : '';
        }

        if ($this->name) $query->andWhere('name LIKE "%' . $this->name . '%"');

        if ($this->type) {
            $mimeTypesList = self::getMimetypesByType($this->type); //mimetypes list for this media type
            if ($mimeTypesList) {
                $mimeTypes = "'" . implode("','", $mimeTypesList) . "'";
            } else $mimeTypes = "'" . $this->type . "'"; //if type not isset in $typeRelations

            if ($mimeTypes != "'*'") $query->andWhere('mimetype IN (' . $mimeTypes . ')'); // set mimetypes for search (not 'file')

        }

        return $dataProvider;
    }

    /**
     * Get array of available mimetypes by media type
     * @param $type
     * @return array
     */
    public static function getMimetypesByType($type)
    {
        return ArrayHelper::getValue(self::$typeRelations, $type . '.mimeTypes');
    }

    //deprecated
    public static function listMediaTypes()
    {
        return array_keys(self::$typeRelations);
    }

    /**
     * Get Media's type by file's mimetype
     * @param $fileMimeType
     * @return bool|int|string
     */
    public static function getTypeByMimeType($fileMimeType)
    {
        if (!$fileMimeType) return false;

        foreach (SearchRepositoryFiles::$typeRelations as $type => $params) {
            if ($type == 'gallery') continue;

            foreach (ArrayHelper::getValue($params, 'mimeTypes') as $mimeType) {
                if ($fileMimeType == $mimeType) return $type;
            }
        }
        return false;
    }

}
