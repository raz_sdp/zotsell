<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Search';
$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['main/index']];
$this->params['breadcrumbs'][] = ['label'=>$this->title];
?>
<h1>Documents</h1>
<p class="lead">Manage your Docs and Folders</p>

<button data-toggle="modal" data-target="#iconModal" class="btn btn-success" type="button"><i class="fa fa-plus"></i> Add Folder </button>
<button data-toggle="modal" data-target="#iconModal" class="btn btn-success" type="button"><i class="fa fa-plus"></i> Add Media</button>

<hr>

<?php if(isset($searchModel)):?>
<?php $form = ActiveForm::begin([
        'id' => 'login-form2',
        'method' => 'post',
        'action' => ['main/search'],
        'options' => [
            'class' => 'form-search pull-right',
        ],
])
?>
    <div class="input-append">
        <input type="text" name='searchstring' value="<?php if(isset($searchModel->searchstring)) echo $searchModel->searchstring;?>" class="span2 search-query">
        <button type="submit" class="btn btn-success">Search</button>
    </div>
<?php ActiveForm::end() ?>
<?php endif; ?>

<?php Yii::trace('searchModel:'.print_r($searchModel,true));?>
<?=GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'columns' => [
        [
            'attribute'=>'name',
            'format'=>'html',
            'value' => function($model) {
                if ($model['type']=='folder') return Html::a($model['name'], ['folder/view', 'id'=>$model['id']]);
                else return Html::a($model['name'], ['document/view', 'id'=>$model['id']]);
            }
        ],
        [
            'attribute'=>'type',
            'value'=> 'type'
        ]
    ],
    'summary' => 'Displaying {begin}-{end} of {totalCount} results', 
    'layout' => '{summary}{items}{pager}',
])
?>
