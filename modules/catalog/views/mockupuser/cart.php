


<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\boostrap\Modal;
    $this->title = 'Shopping Cart';
    $this->params['breadcrumbs'][] = $this->title;
?>


  <h1>Shopping Cart</h1>
  <p class="lead">Manage all your  Purchases</p>
  
  



<table class="table table-condensed">


<thead> <tr> <th class="col-md-1"></th> <th> </th> <th class="col-md-2"> </th>  </tr> </thead>


<tr> 

<td  class="col-md-1">  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Twitter, Inc.</strong><br>
  1355 Market Street, Suite 900<br>
  San Francisco, CA 94103<br>
  <abbr title="Phone">P:</abbr> (123) 456-7890
</address>
 </td> 

 
<td>
<a href="/zot/web/contacts/default/edit" type="button" class="btn btn-default"><i class="fa fa-pencil"></i> Edit customer info</a></td> 	
</td> 	
    
    
    
</tr> 

</table>


		
        
            <div class="row bs-wizard" style="border-bottom:0;">
                
                <div class="col-xs-2 bs-wizard-step complete">
                  <div class="text-center bs-wizard-stepnum">Step 1</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Review Items</div>
                </div>
                
                <div class="col-xs-2 bs-wizard-step complete"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Step 2</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Choose Shipping</div>
                </div>
                
                <div class="col-xs-2 bs-wizard-step active"><!-- complete -->
                  <div class="text-center bs-wizard-stepnum">Step 3</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center">Add data</div>
                </div>
                
                <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Step 4</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"> Sign/pay</div>
                </div>
                
                
                    <div class="col-xs-2 bs-wizard-step disabled"><!-- active -->
                  <div class="text-center bs-wizard-stepnum">Step 5</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#" class="bs-wizard-dot"></a>
                  <div class="bs-wizard-info text-center"> Confirm</div>
                </div>
            </div>
        
        
        
        
        








<table class="table table-hover">




<thead> <tr> <th class="col-md-1"></th> <th> </th> <th class="col-md-2"> </th>  </tr> </thead>

<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 1 (no customer selected) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			 
				
				
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">45 </span>
				 <td> 
				</td>
 <td> 
<?= Html::button('Edit',  ['value' => Url::to('buyitem'),'class' => 'btn btn-default modalButton']) ?><br>
</td> 	
    
</tr> 
  
<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 1 (no customer selected) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			 
				
				
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">45 </span>
				 <td> 
				</td>
 <td> 
<?= Html::button('Edit',  ['value' => Url::to('buyitem'),'class' => 'btn btn-default modalButton']) ?><br>
</td> 	
    
</tr> 
  
  
  <tr> 
  
  <tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 1 (no customer selected) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			 
				
				
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">45 </span>
				 <td> 
				</td>
 <td> 
<?= Html::button('Edit',  ['value' => Url::to('buyitem'),'class' => 'btn btn-default modalButton']) ?><br>
</td> 	
    
</tr> 
  
  
  <tr> 
  
  <tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 1 (no customer selected) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			 
				
				
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Future availabitity">-</span><br>
				<span type="button" data-toggle="tooltip" data-placement="left" title="Current availabitity">45 </span>
				 <td> 
				</td>
 <td> 
<?= Html::button('Edit',  ['value' => Url::to('buyitem'),'class' => 'btn btn-default modalButton']) ?><br>
</td> 	
    
</tr> 
  
  
  <tr> 

  

  
  

</table>





