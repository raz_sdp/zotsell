<?php
    $this->title = 'Contacts Users';
    $this->params['breadcrumbs'][] = $this->title;
?>


  <h1>Item to Users </h1>
  <p class="lead">Associate your Items to Users</p>
  
  
  

<table class="items table table-striped table-bordered table-condensed">
<thead>
<tr>
<th id="develop-data-grid_c0"><a class="sort-link" href="/admin/managecontacts/contactsAssign?AccountServiceLicenses_sort=email">Username<span class="caret"></span></a></th><th id="develop-data-grid_c1"><a class="sort-link" href="/admin/managecontacts/contactsAssign?AccountServiceLicenses_sort=usercode">User Code<span class="caret"></span></a></th><th id="develop-data-grid_c2"><a class="sort-link" href="/admin/managecontacts/contactsAssign?AccountServiceLicenses_sort=userzone">User Zone<span class="caret"></span></a></th><th class="button-column" id="develop-data-grid_c3">&nbsp;</th></tr>
<tr class="filters">
<td><div class="filter-container"><input name="AccountServiceLicenses[email]" id="AccountServiceLicenses_email" type="text"></div></td><td><div class="filter-container"><input name="AccountServiceLicenses[usercode]" id="AccountServiceLicenses_usercode" type="text" maxlength="255"></div></td><td><div class="filter-container"><input name="AccountServiceLicenses[userzone]" id="AccountServiceLicenses_userzone" type="text" maxlength="255"></div></td><td>&nbsp;</td></tr>
</thead>
<tbody>
<tr class="odd">
<td>enterpriseuser003@zotsell.com</td><td>012</td><td>001</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="even">
<td>alessandro.gervasi@mixcomunicazione.it</td><td>001</td><td>&nbsp;</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="odd">
<td>marco.ruaro@tradenet.it</td><td>&nbsp;</td><td>&nbsp;</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="even">
<td>selluser001@zotsell.com</td><td>001</td><td>&nbsp;</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="odd">
<td>marco.zenere@tradenet.it</td><td>001</td><td>&nbsp;</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="even">
<td>enterpriseadmin@zotsell.com</td><td>000</td><td>001</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="odd">
<td>enterprisebuyer001@zotsell.com</td><td>&nbsp;</td><td>&nbsp;</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="even">
<td>enterpriseuser001@zotsell.com</td><td>001</td><td>000</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="odd">
<td>enterpriseuser002@zotsell.com</td><td>002</td><td>002</td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
<tr class="even">
<td>raffaele.viola@tradenet.it</td><td>999</td><td></td><td class="button-column"><a href="bulk" type="button" class="btn btn-success"><i class="fa fa-link"></i> Assign Items</a></td></tr>
</tbody>
</table>
