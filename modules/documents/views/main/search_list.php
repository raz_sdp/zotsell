<?php
use yii\helpers\Html;
use fedemotta\datatables\DataTables;

?>

<div class="hpanel">
    <div class="panel-body">
        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'tableOptions' => [
                'class' => 'table table-striped table-bordered table-hover dataTable no-footer',
            ],
            'clientOptions' => [
                'dom' => 'lftrip',
                'searching' => false,
                'paging' => true,
                'aaSorting' => [],
                //'language' => [
                //    "search" => "Search:"
                //],
                'columnDefs' => [
                    [
                        "type" => "num-html", "targets" => 0
                    ],
                    [
                        "type" => "num-html", "targets" => 3
                    ],
                    [
                        "type" => "file-size",
                        "targets" => 4
                    ],
                    [
                        "orderable" => false,
                        "targets" => -1
                    ]
                ]
            ],
            //'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'icon',
                    'format' => 'html',
                    'value' => function ($model) {
                        return $model['icon'];
                        //return Html::img(Yii::getAlias('@web').'/images/'. $data['image'], ['width' => '70px']); //for images instead icons
                    }
                ],

                [
                    //'header' => '<span class="glyphicon glyphicon-user"></span>',
                    'attribute' => 'name',
                    'format' => 'html',
                    'value' => function ($model) {
                        if ($model['type'] == 'folder') return Html::a($model['name'], ['list', 'id_folder' => $model['id']], ['class' => 'list-link']); //'nested'
                        else return $model['name'];
                    }
                ],
                'type',
                [
                    'attribute' => 'sharable',
                    'format' => 'html',
                    'value' => function ($model) {
                        switch ($model['sharable']) {
                            case 0:
                                return '<i class="fa fa-square-o"></i>';
                                break;
                            case 1:
                                return '<i class="fa fa-check-square-o"></i>';
                                break;
                            case 2:
                                return '<i class="fa fa-minus-square-o"></i>';
                                break;
                        }
                    }
                ],
                [
                    'attribute' => 'size',
                    'value' => function ($model) {
                        return ($model['size']) ? $model['size'] . ' kb' : '0 b';
                    }
                ],
                [
                    'header' => 'Actions',
                    'headerOptions' => ['class' => ''],
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{download} {view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            if ($model['type'] == 'folder') $uri = 'folder';
                            else $uri = 'document';
                            return Html::a(
                                '<span class="fa fa-eye"></span>',
                                [$uri . '/view', 'id' => $model['id']]
                            );
                        },
                        'update' => function ($url, $model) {
                            return Html::tag('span', '', ['class' => "fa fa-pencil"]);
                        },
                        'download' => function ($url, $model) {
                            return Html::tag('span', '', ['class' => "fa fa-download"]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::tag('span', '', ['class' => "fa fa-trash"]);
                        },
                    ],
                ],
            ],

        ]); ?>
    </div>
</div>`