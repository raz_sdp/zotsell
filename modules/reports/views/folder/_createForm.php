<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(['id' => 'create-folder', 'options' => ['class' => 'create-update-folder', 'role' => 'form']]); ?>
    <div class="col-lg-4">
        <?php /**
         * $form->field($model, 'lang')->hiddenInput(['value' => 'default'])->label(false) */?>
        <h3>Add meta</h3>

        <div class="form-group">
            <?= $form->field($model, 'label')->textInput(['placeholder' => 'Enter your label'])->label('Folder title') ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'description')->textInput(['placeholder' => 'Enter Short Description'])->label('Short Description') ?>
        </div>

        <?php
        $iconCurrent = '';
        if ($model->icon != '') {
            $iconCurrent = $model->icon; // for load current custom image value on radio value
            $model->icon = 'custom';
        }
        ?>
        <?php
        echo $form->field($model, 'icon', [
            'inline' => true,
            //'enableLabel' => false
        ])->radioList([
            '' => 'Standard Icon',
            'custom' => 'Custom image'
        ], [
            'id' => 'icon_block',
            'class' => 'form-group',
            //'data-toggle' => 'buttons',
            //'unselect' => null, // remove hidden field
            'item' => function ($index, $label, $name, $checked, $value) use ($iconCurrent) {
                if ($value == 'custom') {
                    return "<label><a type=\"button\" data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\"
                           aria-controls=\"collapseExample\">" . Html::radio($name, $checked, ['id' => "radio_custom", 'value' => ($iconCurrent != '') ? $iconCurrent : $value, 'class' => 'project-status-btn']) . $label . "</a></label>";
                } else {
                    return "<label>" . Html::radio($name, $checked, ['id' => "radio_standard", 'value' => $value, 'class' => 'project-status-btn']) . $label . "</label>";
                }
            },
        ]);
        ?>



        <div class="collapse out" id="collapseExample">
            <div class="upload-drop-zone" id="drop-zone-icon">
                <?php
                echo \kato\DropZone::widget([
                    'id' => 'iconUploader',
                    'dropzoneContainer' => 'iconUploader',
                    'previewsContainer' => 'previewsIcon',
                    'options' => [
                        'dictDefaultMessage' => 'Drag and drop 100 x 100 icon here',
                        'url' => yii\helpers\Url::to(['document/upload', 'id_folder' => $idContainer]),//'/reports/document/upload?id_folder=' . $idContainer,
                        'maxFilesize' => '2', // to config
                        'maxFiles' => '1', // to array map config
                        'acceptedFiles' => 'image/*', // to array map config
                    ],
                    'clientEvents' => [
                        'complete' => "function(file){console.log(file); eventAfterIconUpload(file);}",
                        'removedfile' => "function(file){alert(file.name + ' is removed')}"
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <h3>FTP Batch Loading</h3>

        <div class="form-group">
            <?= $form->field($model, 'pattern')->textInput(['placeholder' => 'Enter Pattern'])->label('Pattern') ?>
            <!--<label for="name">Pattern</label> <input type="text" id="name" name="name" placeholder="Enter Pattern" class="form-control" required>-->
        </div>
        <div class="form-group">
            <?= $form->field($model, 'frequency')->textInput(['placeholder' => 'Enter Frequency'])->label('Frequency') ?>
            <!--<label for="last_name">Frequency</label> <input type="text" id="last_name" placeholder="Enter Frequency" class="form-control" name="last_name">-->
        </div>
        <div class="form-group">
            <?= $form->field($model, 'limit')->textInput(['placeholder' => 'Enter Document Quantity Limit'])->label('Document Quantity Limit') ?>
            <!--<label for="name">Document Quantity Limit</label> <input type="text" id="name" name="name" placeholder="Enter Document Quantity Limit" class="form-control" required>-->
        </div>
    </div>

    <div class="col-lg-4"></div>
<?php ActiveForm::end(); ?>