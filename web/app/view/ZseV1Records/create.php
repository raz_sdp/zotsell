<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ZseV1Records */

$this->title = 'Create Zse V1 Records';
$this->params['breadcrumbs'][] = ['label' => 'Zse V1 Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zse-v1-records-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
