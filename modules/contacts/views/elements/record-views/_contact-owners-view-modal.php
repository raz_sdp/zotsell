<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:29 PM
 */
use \app\modules\contacts\models\RecordsAgents;

?>
<div class="modal fade" id="contact-owner-view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="z-index: 999999">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Contacts owners</h4>
            </div>
            <div class="modal-body row col-md-12" style="clear: both; margin: auto">
                <!-- <div class="form-group pull-left"> -->
                <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Code</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $owner_list_token_as_array=array();
                            $owner_list_token='';
                            if(isset($contact_owner_list_of_this_record) && !empty($contact_owner_list_of_this_record)){
                                foreach($contact_owner_list_of_this_record as $owner_list){
                                    echo "<tr>
                                            <td>
                                               <span>".$owner_list['usercode']." (".$owner_list['username'].")</span>
                                            </td>
                                            <td>".$owner_list['usercode']."</td>
                                          </tr>";
                                }
                            }?>
                            </tbody>
                        </table>
                </div>
                <!-- </div> -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>