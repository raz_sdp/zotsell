<?php

namespace app\modules\contacts\controllers;

use app\modules\contacts\components\AppHelper;
use app\modules\contacts\models\AccountsServicesLicenses;
use Yii;
use app\modules\contacts\models\Records;
use app\modules\contacts\models\RecordsAgents;
use app\modules\contacts\models\RecordsLinks;
use app\modules\contacts\models\RecordsMedia;
use app\modules\contacts\models\RecordsActivities;
use app\modules\contacts\models\RecordsOrders;
use app\modules\contacts\models\SearchRecords;
use app\modules\contacts\models\RecordsClassifications;
use app\modules\contacts\models\CatalogLabels;
//use yii\web\Controller;
use  app\components\BaseController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\Session;

/**
 * RecordsController implements the CRUD actions for Records model.
 */
class RecordsController extends BaseController
{
    public $enableCsrfValidation = false;
    public $defaultAction = 'list';
    public $check = 0;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Records models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchRecords();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //print_r($dataProvider); die();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Records model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $data = ArrayHelper::toArray($model);
        if(isset($_GET['d'])){
            $type_icon = AppHelper::convertTypeIntoImage('destination', false);         # false means return large icon
        }else{
            $type_icon = AppHelper::getIconFromArrayVal($data, false, false);         # false means return large icon
        }
        $status_icon = AppHelper::convertTypeIntoImage($model->record_status);
        $contact_owner_icon = AppHelper::convertTypeIntoImage('contact-owner');
        $classification = RecordsClassifications::find($model->id_classification)->one();
        $contact_owner_list_of_this_record= RecordsAgents::getContactOwnersOfCurrentRecord($id);
        #BaseController::_setTrace($contact_owner_list_of_this_record);
        return $this->render('view', [
            'model' => $model,
            'type_icon' => $type_icon,
            'classification' => $classification->label,
            'status' => $status_icon,
            'contact_owner_list_of_this_record' => $contact_owner_list_of_this_record,
            'contact_owner_icon' => $contact_owner_icon
        ]);
    }

    /**
     * Creates a new Records model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Records();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_record]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Records model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $message = '';
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $options_format = array();
            $req_data = Yii::$app->request->post();
            #BaseController::_setTrace($req_data);
            $options_values = isset($req_data['Records']['options'])?$req_data['Records']['options']:"";
            $options_types = isset($req_data['Records']['options_type'])?$req_data['Records']['options_type']:'';
            $options_labels = isset($req_data['Records']['options_label'])?$req_data['Records']['options_label']:'';
            $options_field_ids = isset($req_data['Records']['options_field_id'])?$req_data['Records']['options_field_id']:'';

            $c = 0;
            if(!empty($options_values)) {
                foreach($options_values as $key => $value) {
                    $options_format['contesti_note_aggiuntive']['S'][$key] = $options_field_ids[$key];
                    $options_format['note_aggiuntive'][$key]['field_id'] = $options_field_ids[$key];
                    $options_format['note_aggiuntive'][$key]['label'] = $options_labels[$key];
                    $options_format['note_aggiuntive'][$key]['tipo'] = $options_types[$key];
                    $options_format['note_aggiuntive'][$key]['valore'] = $options_values[$key];
                    $c++;
                }
            }

            if(!empty($req_data['payment_condition'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_DI_PAGAMENTO';

                /*$predefined_data = CatalogLabels::find()->where(['code_external' => $req_data['payment_condition']])->one();
                BaseController::_setTrace($predefined_data);*/

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_DI_PAGAMENTO';
                $options_format['note_aggiuntive'][$c]['label'] = 'Tipo Pagamento';
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['payment_condition'];
                $c++;

                $options_format['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO'] = $req_data['payment_condition'];
            }
            if(!empty($req_data['incoterm'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_DI_RESA';

                $predefined_data = CatalogLabels::find()->where(['code_external' => $req_data['incoterm']])->one();

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_DI_RESA';
                $options_format['note_aggiuntive'][$c]['label'] = $predefined_data->label;
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['incoterm'];
                $c++;

                $options_format['dati_convenzionati']['CONDIZIONE_DI_RESA'] = $req_data['incoterm'];
            }
            if(!empty($req_data['vat_condition'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_IVA';

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_IVA';
                $options_format['note_aggiuntive'][$c]['label'] = 'Condizione Iva';
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['vat_condition'];
                $c++;
            }
            if(!empty($req_data['contact_group'])) {
                $options_format['gruppi'][0] = $req_data['contact_group'];
            }

            $model->options = json_encode($options_format);

            if(UploadedFile::getInstance($model, 'icon')){
                $image = UploadedFile::getInstance($model, 'icon');
                $model->icon = $image->name;
            } else{
                $image_remove = $_POST['remove_image'];
                $icon_status = $_POST['icon_status'];
                if(($icon_status == 1) && ($image_remove != 'organizzazione_default.jpeg')) {
                    $model->icon = 'organizzazione_default.jpeg';
                    $image_remove_path = Yii::$app->basePath . '/uploads/'.$model->id_record.'-'.$image_remove;
                    unlink($image_remove_path);
                } elseif(($icon_status == 1) && ($image_remove == 'organizzazione_default.jpeg')) {
                    $model->icon = 'organizzazione_default.jpeg';
                } else {
                    $model->icon = $image_remove;
                }
            }

            $model->is_customer = 0;
            $model->is_destination = 0;
            $model->is_prospect = 0;
            $model->is_contact = 0;
            switch($req_data['type_options'][0]) {
                case "Company":
                    $model->is_customer = 1;
                    break;
                case "Destination":
                    $model->is_destination = 1;
                    break;
                case "Prospect":
                    $model->is_prospect = 1;
                    break;
                case "Person":
                    $model->is_contact = 1;
                    break;
            }
            if(in_array("Company", $req_data['type_options']) && in_array("Destination", $req_data['type_options'])) {
                $model->is_customer = 1;
                $model->is_destination = 2;
            }
            $model->type = "";

            if($model->save()) {
                $message = 'Contact Updated Successfully!';
                if(isset($image)){
                    $path = Yii::$app->basePath . '/uploads/'.$model->id_record.'-'.$model->icon;
                    $image->saveAs($path);
                }
                $submitted_data=Yii::$app->request->post();                
                if(isset($submitted_data) && !empty($submitted_data)){                    
                    AppHelper::saveContactAgents($submitted_data);
                }                
            }
        }

        $classification_list = RecordsClassifications::find()->select(['id_classification','color','label','record_status'])->all();
        $payment_conditions = CatalogLabels::find()->where(['context' => "CONDIZIONE_DI_PAGAMENTO"])->all();
        $incoterms = CatalogLabels::find()->where(['context' => "CONDIZIONE_DI_RESA"])->all();
        $vat_conditions = CatalogLabels::find()->where(['context' => "CONDIZIONE_IVA"])->all();

        $link_model = new RecordsLinks();
        $connections = $link_model->getContactConnections($id);

        $activity_model = new RecordsActivities();
        $activities = $activity_model->getActivitiesData($model->code);

        $doc_model = new RecordsMedia();
        $documents = $doc_model->getContactDocuments($id);

        $orders = RecordsOrders::find()->where(['record_code' => $model->code])->all();
        $contact_owner_list_of_this_record= RecordsAgents::getContactOwnersOfCurrentRecord($id);
        //BaseController::_setTrace($contact_owner_list_of_this_record);
        //not need to add company-update//use company-create [rahul]
        return $this->render('company-create', [
            'model' => $model,
            'classifications' => $classification_list,
            'payment_conditions' => $payment_conditions,
            'incoterms' => $incoterms,
            'vat_conditions' => $vat_conditions,
            'connections' => $connections,
            'activities' => $activities,
            'documents' => $documents,
            'orders' => $orders,
            'contact_owner_list_of_this_record'=>$contact_owner_list_of_this_record,
            'msg'=>$message
        ]);
    }
    /**
     * Deletes an existing Records model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['list']);
    }

    /**
     * Finds the Records model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Records the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Records::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /*==================================Custom Code start from here============================================*/

    /**
     * Contacts index.
     * @return mixed
     */
    public function actionList()
    {
        $data = Records::find()->all();
        $classification_list = RecordsClassifications::find()->select(['id_classification','label'])->all();
        #BaseController::_setTrace($data);
        return $this->render('contact-list', [
            'data' => $data,
            'classification_list' => $classification_list,
        ]);
    }

    /**
     * @return string
     */
    public function actionFlow()
    {
        $data = Records::find()->all();
        $classification_list = RecordsClassifications::find()->select(['id_classification','label'])->all();
        return $this->render('contact-flow', [
            'data' => $data,
            'classification_list'=>$classification_list,
        ]);
    }

    /**
     * @return string
     */
    public function actionFolder()
    {
        $search=Yii::$app->request->post('search');
        $model = new Records();
        $where1 = [];
        $where2 = [];
        if(isset($search) && $search != ''){
            $where1[] = 'or';
            $where2[] = 'or';
            $where1[] = ['like', 'zse_v1_records.business_name', trim($search)];
            $where2[] = ['like', 'business_name', trim($search)];
        };

        $agent_contact_data = $model->getAgentContactData($where1);
        $type_contacts = $model->getTypesAllContactData($where2);

        $msg = '';
        $success = 0;
        $session = new Session;
        $session->open();

        if($session->hasFlash('msg')) {
            $msg = $session->getFlash('msg');
            $success = $session->getFlash('success');
        }

        #BaseController::_setTrace($agent_contact_data['agents_contacts_codes']['f35e0316cc']['Person']);
        #BaseController::_setTrace($agent_contact_data);

        return $this->render('contact-folder', [
            'agents_contacts' => $agent_contact_data['agents_contacts'],
            'associate_emails' => $agent_contact_data['associate_emails'],
            'agents_contacts_codes' => $agent_contact_data['agents_contacts_codes'],
            'type_contacts' => $type_contacts,
            'search' => $search,
            'msg' => $msg,
            'success' => $success,
        ]);
    }

    /**
     * @return string
     */
    public function actionCompany_create()
    {
        $classification_list = RecordsClassifications::find()->select(['id_classification','color','label','record_status'])->all();

        $payment_conditions = CatalogLabels::find()->where(['context' => "CONDIZIONE_DI_PAGAMENTO"])->all();
        $incoterms = CatalogLabels::find()->where(['context' => "CONDIZIONE_DI_RESA"])->all();
        $vat_conditions = CatalogLabels::find()->where(['context' => "CONDIZIONE_IVA"])->all();

        $model = new Records();
        if ($model->load(Yii::$app->request->post())) {
            $options_format = array();
            $req_data = Yii::$app->request->post();
            $options_values = isset($req_data['Records']['options'])?$req_data['Records']['options']:"";
            $options_types = isset($req_data['Records']['options_type'])?$req_data['Records']['options_type']:'';
            $options_labels = isset($req_data['Records']['options_label'])?$req_data['Records']['options_label']:'';
            $options_field_ids = isset($req_data['Records']['options_field_id'])?$req_data['Records']['options_field_id']:'';

            $c = 0;

            if(!empty($options_values)) {
                foreach($options_values as $key => $value) {
                    $options_format['contesti_note_aggiuntive']['S'][$key] = $options_field_ids[$key];
                    $options_format['note_aggiuntive'][$key]['field_id'] = $options_field_ids[$key];
                    $options_format['note_aggiuntive'][$key]['label'] = $options_labels[$key];
                    $options_format['note_aggiuntive'][$key]['tipo'] = $options_types[$key];
                    $options_format['note_aggiuntive'][$key]['valore'] = $options_values[$key];
                    $c++;
                }
            }

            if(!empty($req_data['payment_condition'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_DI_PAGAMENTO';

                /*$predefined_data = CatalogLabels::find()->where(['code_external' => $req_data['payment_condition']])->one();
                BaseController::_setTrace($predefined_data);*/

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_DI_PAGAMENTO';
                $options_format['note_aggiuntive'][$c]['label'] = 'Tipo Pagamento';
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['payment_condition'];
                $c++;

                $options_format['dati_convenzionati']['CONDIZIONE_DI_PAGAMENTO'] = $req_data['payment_condition'];
            }
            if(!empty($req_data['incoterm'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_DI_RESA';

                $predefined_data = CatalogLabels::find()->where(['code_external' => $req_data['incoterm']])->one();
                //BaseController::_setTrace($predefined_data);

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_DI_RESA';
                $options_format['note_aggiuntive'][$c]['label'] = $predefined_data->label;
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['incoterm'];
                $c++;

                $options_format['dati_convenzionati']['CONDIZIONE_DI_RESA'] = $req_data['incoterm'];
            }
            if(!empty($req_data['vat_condition'])) {
                $options_format['contesti_note_aggiuntive']['S'][$c] = 'CONDIZIONE_IVA';

                $options_format['note_aggiuntive'][$c]['field_id'] = 'CONDIZIONE_IVA';
                $options_format['note_aggiuntive'][$c]['label'] = 'Condizione Iva';
                $options_format['note_aggiuntive'][$c]['tipo'] = 'TESTO';
                $options_format['note_aggiuntive'][$c]['valore'] = $req_data['vat_condition'];
                $c++;
            }
            if(!empty($req_data['contact_group'])) {
                $options_format['gruppi'][0] = $req_data['contact_group'];
            }

            $model->options = json_encode($options_format);
            #BaseController::_setTrace($model->options);

            if(UploadedFile::getInstance($model, 'icon')){
                $image = UploadedFile::getInstance($model, 'icon');
                $model->icon = $image->name;
            }else{
                $model->icon = 'organizzazione_default.jpeg';
            }

            $model->is_customer = 0;
            $model->is_destination = 0;
            $model->is_prospect = 0;
            $model->is_contact = 0;
            switch($req_data['type_options'][0]) {
                case "Company":
                    $model->is_customer = 1;
                    break;
                case "Destination":
                    $model->is_destination = 1;
                    break;
                case "Prospect":
                    $model->is_prospect = 1;
                    break;
                case "Person":
                    $model->is_contact = 1;
                    break;
            }
            if(in_array("Company", $req_data['type_options']) && in_array("Destination", $req_data['type_options'])) {
                $model->is_customer = 1;
                $model->is_destination = 2;
            }

            if(!empty($model->code)) {
                $model->external_code = $model->code;
            }

            $model->type = "";

            if($model->save()) {
                if(isset($image)){
                    $path = Yii::$app->basePath . '/uploads/'.$model->id_record.'-'.$model->icon;
                    $image->saveAs($path);
                }
                return $this->redirect(['update', 'id' => $model->id_record]);
            } else {
                return $this->render('company-create', [
                    'classifications' => $classification_list,
                    'payment_conditions' => $payment_conditions,
                    'incoterms' => $incoterms,
                    'vat_conditions' => $vat_conditions,
                    'model' => $model,
                    'connections' => array(),
                    'activities' => array(),
                    'documents' => array(),
                    'orders' => array()
                ]);
            }
        } else {
            return $this->render('company-create', [
                'classifications' => $classification_list,
                'payment_conditions' => $payment_conditions,
                'incoterms' => $incoterms,
                'vat_conditions' => $vat_conditions,
                'model' => $model,
                'connections' => array(),
                'activities' => array(),
                'documents' => array(),
                'orders' => array()
            ]);
        }
    }

    public function actionEdit($id)
    {
        $data = Records::find($id)->all();
        print_r($data); die;
        return $this->render('company-create', [
            'data' => $data
        ]);
    }

    /**
     *
     */
    public function actionAssociate()
    {
        $data = Yii::$app->request->post();
        $agent_code = $data['agent'];
        $type = $data['type'];

        $type_field = '';
        if($type && $type != ''){
            $type_field = AppHelper::typeToQueryConversion($type);
        }

        $type_records = Records::find('code')->where([$type_field => 1])->all();
        #BaseController::_setTrace($type_records);

        foreach ($type_records as $item) {
            RecordsAgents::deleteAll(['agent_code' => $agent_code, 'record_code' => $item->code]);
        }
        $model = new RecordsAgents();
        $model->saveAgentsRecords($data);

        Yii::$app->response->redirect(array('contacts/records/folder'));
    }

    public function actionAssociate_new()
    {
        $data = Yii::$app->request->post();
        $agent_code = $data['agent'];       #user code
        $contact = $data['contact'];        # record code

        $session = new Session;
        $session->open();

        $is_valid_user_code = AccountsServicesLicenses::find()->where(['usercode' => $agent_code])->one();
        if(!empty($is_valid_user_code)) {
            $agent_exist = RecordsAgents::find()->where(['agent_code' => $agent_code])->one();
            if(empty($agent_exist)) {
                Yii::$app->db->createCommand()
                    ->insert('zse_v1_records_agents', [
                        'agent_code' => $agent_code,
                        'destination_code' => '',
                        'record_code' => $contact,
                    ])->execute();

                $_SESSION['msg'] = 'New folder has been created.';
                $_SESSION['success'] = 1;

                $session->setFlash('msg', 'New folder has been created.');
                $session->setFlash('success', 1);

            } else {
                $_SESSION['msg'] = 'Folder already exists.';
                $_SESSION['success'] = 0;
                $session->setFlash('msg', 'Folder already exists.');
                $session->setFlash('success', 0);
            }
        } else{
            $_SESSION['msg'] = 'Invalid User code.';
            $_SESSION['success'] = 0;
            $session->setFlash('msg', 'Invalid User code.');
            $session->setFlash('success', 0);
        }
        Yii::$app->response->redirect(array('contacts/records/folder'));
    }
}
