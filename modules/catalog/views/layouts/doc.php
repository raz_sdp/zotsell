<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Modal;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php
	Modal::begin([
		'header'=>'<h4>title</h4>',
		'id'=>'modal',
		'size'=>'modal-lg',
		]);
		
	echo "<div id='modalcontent'></div>";
	
	Modal::end();

 ?>

<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        //'brandLabel' => 'Zot',
        //'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-default navbar-fixed-top',
        ],
    ]);
    ?>
    <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"></a>
    </div>
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"> </i> <i class="fa fa-caret-down"> </i></a>
    <?php
        echo \yii\bootstrap\Dropdown::widget([
            'items' => [
                ['label' => 'Catalog', 'url' => ['default/']],
            ],
        ]);
    ?>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="default/cart" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="badge" style="background-color:red;" >4</span>  <i class="fa fa-shopping-cart fa-2x"> </i>  <i class="fa fa-caret-down"> </i> </a>
              
              <ul class="dropdown-menu">
             
			<li> <a tabindex="-1" href="default/cart"> article 1 - 100 &euro;</a> </li>
			 <li> <a tabindex="-1" href="default/cart"> article 2 - 100 &euro;</a> </li>
			 <li> <a tabindex="-1" href="default/cart"> article 3 - 100 &euro;</a> </li>
			 <li> <a tabindex="-1" href="default/cart"> article 4 - 100 &euro;</a> </li>
                <li role="separator" class="divider"></li>
                 <li> <strong>   Total 5 - 500 &euro;</strong></li>
                <li role="separator" class="divider"></li>
              <li> <a tabindex="-1" href="cart"> Go to shopping cart </a> </li>
              </ul>
            </li>
          </ul>

    <?php
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label'=> 'Home',
                'url' => Yii::$app->homeUrl,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        
       
    
    <div class="wrapper">
        <?= $content ?>
    </div>

    

    
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Tradenet Services srl <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
</div> 
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
