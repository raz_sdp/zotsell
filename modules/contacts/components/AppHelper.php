<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/28/16
 * Time: 10:32 AM
 */
namespace app\modules\contacts\components;

use app\modules\contacts\models\RecordsAgents;
use yii\base\Exception;
use yii;

class AppHelper{

    /**
     * Function for getting any model data using parameter
     * @param $model
     * @param array $fields
     * @param string $limit
     * @param int $offset
     * @param array $where
     * @param string $short
     * @return mixed
     */
    public static function findDataFromModel($model, $fields = [], $limit = '', $offset = 0, $where = [], $short = '', $extra_filter = []){
        $result = $model->find()
           ->select($fields)
           ->limit($limit)
           ->offset($offset)
           ->andFilterWhere($where)
           ->andwhere($extra_filter)
           ->orderBy($short)
           ->all();
       return $result;
   }

    /**
     * Function for getting cont of model data
     * @param $model
     * @param array $fields
     * @param string $limit
     * @param int $offset
     * @param array $where
     * @param string $short
     * @return mixed
     */
    public static function countDataFromModel($model, $fields = [], $limit = '', $offset = 0, $where = [], $short = '', $extra_filter = []){
        $count = $model->find()
            ->select($fields)
            ->limit($limit)
            ->offset($offset)
            ->andFilterWhere($where)
            ->andwhere($extra_filter)
            ->orderBy($short)
            ->count();
        return $count;
    }

    /**
     * Function for making condition
     * @param $params
     * @param array $conditions
     * @return array
     */
    public static function makeConditionsForQuery($params,$conditions = []){
        foreach($params as $key => $val){
            if ($val!='' || is_numeric($val)) {
                $conditions[$key] = $val;
            }else{

            }
        }
        return $conditions;
    }

    /**
     * function for getting icon dependent on contact type
     * @param $type
     * @return mixed
     */
    public static function convertTypeIntoImage($type,$small=true){
        if($small){
            $type_to_image=array(
                "company" => "<i class='fa fa-building-o'></i>",
                "destination" => "<i class='fa fa-truck'></i>",
                "prospect" => "<i class='fa fa-user-secret'></i>",
                "person" => "<i class='fa fa-user'></i>",
                "gallery" => "<i class = 'fa fa-file-archive-o'></i>",
                "txt" => "<i class = 'fa fa-file-text-o'></i>",
                "application/msword" => "<i class = 'fa fa-file-word-o'></i>",
                "html5" => "<i class = 'fa fa-file-code-o'></i>",
                "application/vnd.ms-excel" => "<i class = 'fa fa-file-excel-o'></i>",
                "pdf" => "<i class = 'fa fa-file-pdf-o'></i>",
                "application/vnd.ms-powerpoint" => "<i class = 'fa fa-file-powerpoint-o'></i>",
                "folder" => "<i class = 'fa fa-folder-o'></i>",
                "movie" => "<i class = 'fa fa-file-video-o'></i>",
                "image" => "<i class = 'fa fa-file-image-o'></i>",
                "" => "<i class = 'fa fa-file-o'></i>",
                "cansend-1" => "<i class = 'fa fa-check-square-o'></i>",
                "cansend-0" => "<i class = 'fa fa-minus-square-o'></i>",
                "1" => "<i class='fa fa-minus-square-o'>",
                "contact-owner" => "<i class='fa fa-arrows-alt fa-2x'></i>"
            );
        } else{
            $type_to_image=array(
                "company" => "<i class='fa fa-building-o fa-3x'></i>",
                "destination" => "<i class='fa fa-truck fa-3x'></i>",
                "prospect" => "<i class='fa fa-user-secret fa-3x'></i>",
                "person" => "<i class='fa fa-user fa-3x'></i>",
                "" => "<i class = 'fa fa-file fa-3x'></i>"
            );
        }
        return isset($type_to_image[strtolower($type)]) ? $type_to_image[strtolower($type)] : '';
    }

    /**
     * save contact agents
     * @param $data
     */
    public static function saveContactAgents($data){
        #print_r($data);die;
        RecordsAgents::deleteAll(['record_code' => $data['Records']['code']]);
        $agents=explode(',',$data['owner_list']);
        foreach ($agents as $agent_code) {
            Yii::$app->db->createCommand()
                ->insert('zse_v1_records_agents', [
                    'agent_code' => $agent_code,
                    'record_code' => $data['Records']['code'],
                    'destination_code'=> $data['Records']['code_destination'],
                    'record_status'=> $data['Records']['record_status'],
                    'date_modification'=> date('Y-m-d h:i:s')
                ])->execute();
        }
    }

    /**
     * Function for converting type with appropriate column of zse_v1_records table
     * @param $type_string
     * @param bool $complete_qry
     * @return string
     */
    public static function typeToQueryConversion($type_string, $qry = false , $qry_select = false){

        $return = '';
        if($type_string && $type_string != ''){

            $type_string = strtolower($type_string);

            $all_tbl_column = array(
                'company' => '`is_customer`',
                'destination' => '`is_destination`',
                'prospect' => '`is_prospect`',
                'person' => '`is_contact`'
            );

            if(array_key_exists($type_string,$all_tbl_column)){
                $tbl_column = $all_tbl_column[$type_string];
                if($qry){
                    if($qry_select){
                        return '`zse_v1_records`'.'.'.$tbl_column.' as type';
                    }else{
                        return '`zse_v1_records`'.'.'.$tbl_column.' = 1';
                    }
                }else{
                    return $tbl_column;
                }
            }
        }
        return $return;
    }

    /**
     * getting corresponding field name for type
     * @param $key
     * @return mixed
     */
    public static function getTypeFromFild($key){
        $all_tbl_column = array(
            'is_customer' => 'company',
            'is_destination' => 'destination',
            'is_prospect' => 'prospect',
            'is_contact' => 'person'
        );

        return $all_tbl_column[$key];
    }

    /**
     * check if corresponding db field exist for any type
     * @param $key
     * @return bool
     */
    public static function fieldArrayExist($key){
        $fieldArray = array('is_customer','is_destination','is_prospect','is_contact');
        if(in_array($key,$fieldArray)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * get icon for each type
     * @param $row
     * @param bool $only_type
     * @param bool $icon_size
     * @return mixed|string
     */
    public static function getIconFromArrayVal($row ,$only_type = false, $icon_size = true){
        foreach ($row as $key => $val) {
            if (AppHelper::fieldArrayExist($key)) {
                if($val == 1){
                    $type = AppHelper::getTypeFromFild($key);
                    if($only_type){
                        return $type;
                    }
                    $icon = AppHelper::convertTypeIntoImage($type, $icon_size);
                    return $icon;                   
                }
            }
        }
        return '';
    }

    /**
     * get plural form of a text
     * @param $key
     * @return mixed
     */
    public static function getPlural($key){
        $plural_array = array(
            'company' => 'Companies',
            'destination' => 'Destinations',
            'prospect' => 'Prospects',
            'person' => 'People'
        );
        return $plural_array[strtolower($key)];
    }
}