<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ZseV1Records */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zse-v1-records-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_locality')->textInput() ?>

    <?= $form->field($model, 'id_classification')->textInput() ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code_destination')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'legal_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'inserted_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assigned_to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_contact')->textInput() ?>

    <?= $form->field($model, 'is_customer')->textInput() ?>

    <?= $form->field($model, 'is_supplier')->textInput() ?>

    <?= $form->field($model, 'is_prospect')->textInput() ?>

    <?= $form->field($model, 'is_destination')->textInput() ?>

    <?= $form->field($model, 'is_reseller')->textInput() ?>

    <?= $form->field($model, 'is_intrested')->textInput() ?>

    <?= $form->field($model, 'authorize_send')->textInput() ?>

    <?= $form->field($model, 'coordinates')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'external_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'province')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vat_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fiscal_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pricelist')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'options')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'person_in_charge')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direct_mail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'direct_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_modification')->textInput() ?>

    <?= $form->field($model, 'record_status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
