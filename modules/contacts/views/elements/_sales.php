<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:23 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3>Sales </h3>
<hr/>
<div class="note-content">
    <div class="row">
        <?php if(!isset($type)): ?>
        <div class="col-lg-4">
            <p>
            <h3> Choose Orders</h3>
            </p>
            <form role="form" id="form_order">
                <div class="form-group">
                    <label for="keyword_o">Search</label>
                    <input type="text" id="keyword_o" name="keyword_o" placeholder="Enter keyword" class="form-control" required>
                </div>
                <div>
                    <button class="btn btn-block btn-success m-t-n-xs" type="submit"><strong>Search</strong>
                    </button>
                </div>
            </form>

            <div class="table-responsive slim_scroll">
                <table class="table table-striped js-order-search-result-table custom_table_brake">
                    <thead>
                    <tr>
                        <th class="col-lg-9">Order Code</th>
                        <th>Type</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="2">No Result Found</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <?php endif; ?>
        <div class="col-lg-8">
        <?php if(!isset($type)): ?>
            <p>
            <h3> Added Orders</h3>
            </p>
            <?php endif; ?>

            <script type="text/javascript">
                var added_orders = [];
            </script>            
            <table id="sales-table-update" class="table table-striped table-bordered table-hover js-order-table custom_table_brake">
                <thead>
                <tr>
                    <th>Order Code</th>
                    <th>Date</th>
                    <th>Type</th>
                    <th>Subject</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($orders)) { ?>
                    <!--<tr class="order_no_res_msg">
                        <td colspan="6">No Sales.</td>
                    </tr>-->
                <?php } else { ?>
                    <?php foreach($orders as $order) : ?>
                        <tr id="sales_<?= $order['id_order']?>" rel="<?= $order['id_order'];?>">
                            <td><?= $order['order_code'];?></td>
                            <td><?= $order['date_order'];?></td>
                            <td><?= $order['type'];?></td>
                            <td><?= $order['subject'];?></td>
                            <td><?= $order['status'];?></td>
                            <td><a id="<?=$order['id_order'];?>"><i class="fa fa-minus"  onclick="delete_from_orders_table(this)"></i></a></td>
                        </tr>

                        <script type="text/javascript">
                            added_orders.push("<?php echo $order['id_order'];?>");
                        </script>

                    <?php endforeach; ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php if(!isset($type)): ?>
<div class="btn-group save_btn_group" id="order_save_btn_group">
    <button class="btn btn-sm btn-default" id="save_orders"><i class="fa fa-thumbs-o-up"></i> Save</button>
</div>
<?php endif; ?>
<?php
$active = 0;
if(isset($orders) && !empty($orders)){
    $active = 1;
}
?>
<input value="<?=$active?>" id="active_orders" type="hidden">