<?php
namespace app\modules\contacts\controllers;

use app\modules\contacts\models\RecordsClassifications;
use yii;
//use yii\web\Controller;
use  app\components\BaseController;
use  app\modules\contacts\components\AppHelper;
use app\modules\contacts\models\Records;
use app\modules\contacts\models\RecordsLinks;
use app\modules\contacts\models\RecordsMedia;
use app\modules\contacts\models\RecordsActivities;
use app\modules\contacts\models\RecordsOrders;
use yii\helpers\ArrayHelper;

class AjaxController extends BaseController
{
    public $enableCsrfValidation = false;

    /**
     * For getting contacts record data of Data table in contact-list page with ajax
     */
    public function actionGetcontacsrecords()
    {

        $baseUrl = yii::$app->request->baseUrl;
        /********************************get posted data start***********************/
        $from_action = Yii::$app->request->post('from_action');
        $classification = Yii::$app->request->post('classification');
        $status = Yii::$app->request->post('status');
        $type = Yii::$app->request->post('type');
        //$genre = Yii::$app->request->post('genre');
        //$favourites = Yii::$app->request->post('favourites');
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $search = Yii::$app->request->post('search');
        $sort = Yii::$app->request->post('sort');
        /*********************************get posted data end************************/


        if($type && $type != ''){
            $type_filed = AppHelper::typeToQueryConversion($type);
        }

        /****where condition making for filtering contacts record list table start****/
        $where = [];
        $extra_filter = [];
        if ($search != '') {

            $where[] = 'or';
            if ($from_action == 'flow') {
                $where[] = ['like', 'business_name', trim($search)];
            } else {
                //$where[] = ['like', 'type', $search];
                //$where[] = ['like', 'id_record', $search];
                $where[] = ['like', 'code', $search];
                $where[] = ['like', 'business_name', $search];
                $where[] = ['like', 'city', $search];
                $where[] = ['like', 'phone', $search];
                $where[] = ['like', 'country', $search];

                $search_type_fild = AppHelper::typeToQueryConversion($search);
                if(isset($search_type_fild) && $search_type_fild != ''){
                    if(strtolower($search) == 'destination'){
                        $where[] = ['>', $search_type_fild , 0];
                    }else{
                        $where[] = ['like', $search_type_fild , 1];
                    }
                }
            }
        } else {

           /* if (!isset($favourites)) {
                $is_intrested = '';
            } else {
                $is_intrested = ($favourites == 'on') ? 1 : 0;
            }*/

            $condition_params = [
                'id_classification' => $classification,
                'record_status' => $status,
                //'type' => $type,
                //'genre' => $genre,
                //'is_intrested' => $is_intrested
            ];

            if(isset($type_filed) && $type_filed != ''){
                if(strtolower($type) == 'destination'){
                    $extra_filter = ['>', '`is_destination`', 0];
                }else{
                    $condition_params[$type_filed] = 1;
                }
            }

            $where = AppHelper::makeConditionsForQuery($condition_params, $where);
        }
        /****where condition making for filtering contacts record list table end****/
        $short_qry = '';
        if ($from_action == 'list') {
            $field_list = ['is_customer','is_destination','is_prospect','is_contact', 'id_record', 'code', 'business_name', 'city', 'phone', 'country'];
            /********************************sorting query start*********************/
            if (isset($sort['column']) && $sort['dir']) {
                $sort_column = $field_list[$sort['column']];
                $sort_order = $sort['dir'];
                $short_qry = $sort_column . ' ' . strtoupper($sort_order);
            }
            /********************************sorting query end*********************/
        } else if ($from_action == 'flow') {
            $field_list = ['is_customer','is_destination','is_prospect','is_contact', 'id_record', 'code', 'business_name', 'city', 'phone', 'country', 'address' ,'id_classification', 'record_status'];
        }


        /********query execution for getting filter data start*********************/

        $model = new Records;
        $data = AppHelper::findDataFromModel($model, $field_list, $limit, $offset, $where, $short_qry,$extra_filter);
        $data_count = AppHelper::countDataFromModel($model, $field_list, $limit, $offset, $where, $short_qry, $extra_filter);
        /********query execution for getting filter data start*********************/

        /**********making json data from active record data for DataTable start***/
        $total_data = [];
        if ($from_action == 'list') {
            $data = ArrayHelper::toArray($data);
            foreach ($data as $row_key => $row_val) {
                $row_data = [];
                $select_from_four_type = true;
                foreach ($row_val as $key => $val) {
                    $entry_flag = true;
                    if (AppHelper::fieldArrayExist($key)) {                 //this code block is only for type icon generating purpose
                        if($val >0 && $select_from_four_type){                                      //here 4 type field exists. 1. is_customer, 2.is_contact, 3. is_destination, 4. is_prospect. Generally with wich type we search the related field value will 1. But for destination is_destination field value may be 2 but is_customer value will be 1.
                            if(isset($search) && $search != ''){            //if search works then left filter will not work
                                if(strtolower($search) == 'destination'){   //if from search held for string 'destination' then icon will generate only for destination
                                    $type_record = 'destination';
                                }else{
                                    $type_record = AppHelper::getTypeFromFild($key);
                                }
                            }else if(isset($type) && strtolower($type) == 'destination'){
                                $type_record = 'destination';
                            }else{
                                $type_record = AppHelper::getTypeFromFild($key);
                            }
                            $val = AppHelper::convertTypeIntoImage($type_record);
                            $select_from_four_type = false;
                        }else{
                            $entry_flag = false;
                        }
                    }
                    if ($key != 'id_record' && $entry_flag) {
                        $row_data[] = $val;
                    }
                }
                $id_record = $row_val['id_record'];

                $type_extra = '';
                if(isset($type_record) && strtolower($type_record) == 'destination'){
                    $type_extra = '&d';
                }

                $view_url = $baseUrl . '/contacts/records/view?id=' . $id_record.$type_extra;
                $update_url = $baseUrl . '/contacts/records/update?id=' . $id_record;
                $row_data[] = "<a href='" . $view_url . "'><i class='fa fa-eye'></i></a> <a href='" . $update_url . "'><i class='fa fa-pencil'></i></a> <a href='javascript:void(0)' onclick='deleteTableRow($id_record)'><i class='fa fa-trash'></i></a>";
                $total_data[] = $row_data;
            }
        } else if ($from_action == 'flow') {
            $icon_type = '';
            if(isset($type) && strtolower($type) == 'destination'){
                $icon_type = 'destination';
            }
            $total_data = $this->renderPartial('../elements/_contact-records-grid', ['data' => $data, 'data_count' => $data_count, 'limit' => $limit, 'current_page_offset' => $offset, 'icon_type' => $icon_type]);
        }
        $final_data = array('data' => $total_data, "recordsTotal" => $data_count, "recordsFiltered" => $data_count);

        /**********making json data from active record data for DataTable start******/

        die(json_encode($final_data));
    }

    /**
     * For deleting contacts record data from Data table in contact-list page with ajax
     */
    public function actionDeletecontacsrecords()
    {
        $id_record = Yii::$app->request->post('id_record');
        if (Records::findOne($id_record)->delete()) {
            die(json_encode(array('success' => true)));
        } else {
            die(json_encode(array('success' => false)));
        }
    }

    public function actionEntryclassification()
    {
        $status = 0;
        $id_classification = '';
        $text_color = Yii::$app->request->post('text_color');
        $text_label = Yii::$app->request->post('text_label');
        $text_id = Yii::$app->request->post('text_id');
        $classification = new RecordsClassifications();
        $classification->label = $text_label;
        $classification->color = $text_color;
        $classification->description = $text_label;
        $classification->date_modification = date('y-m-d');
        $classification->record_status = 1;

        if ($text_id && $text_id != '') {
            $update = Yii::$app->db->createCommand('UPDATE zse_v1_records_classifications SET color="' . $text_color . '",label="' . $text_label . '",description="' . $text_label . '" WHERE id_classification=' . $text_id)
                ->execute();
            if ($update) {
                $status = 2;
            }
        } else {
            if ($classification->save()) {
                $status = 1;
                $id_classification = $classification->id_classification;
            }
        }
        die(json_encode(array('status' => $status, 'id_classification' => $id_classification)));
    }

    public function actionDeleteclassification()
    {
        $status = false;
        $id_record = Yii::$app->request->post('delete_id');
        Yii::$app->db->createCommand('DELETE FROM zse_v1_records WHERE id_classification=' . $id_record)
            ->execute();
        if (RecordsClassifications::findOne($id_record)->delete()) {
            $status = true;
        }
        die(json_encode(array('success' => $status)));
    }

    /**
     * For searching contacts record data from connection tab with ajax
     */
    public function actionSearchcontact()
    {
        $name = Yii::$app->request->post('name');
        $model = new Records();
        $data = $model->searchContactByName($name);
        $data = ArrayHelper::toArray($data);

        if (!empty($data)) {
            die(json_encode(array('success' => true, 'data' => $data)));
        } else {
            die(json_encode(array('success' => false)));
        }
    }

    /**
     * For searching documents from document tab with ajax
     */
    public function actionSearchdocuments()
    {
        $name = Yii::$app->request->post('file_name');
        $model = new RecordsMedia();
        $data = $model->searchDocumentByName($name);
        $data = ArrayHelper::toArray($data);

        if (!empty($data)) {
            die(json_encode(array('success' => true, 'data' => $data)));
        } else {
            die(json_encode(array('success' => false)));
        }
    }

    /**
     * For searching orders from sales tab with ajax
     */
    public function actionSearchorders()
    {
        $keyword = Yii::$app->request->post('keyword_o');
        $model = new RecordsOrders();
        $data = $model->searchOrdersByKeyword($keyword);
        $data = ArrayHelper::toArray($data);

        if (!empty($data)) {
            die(json_encode(array('success' => true, 'data' => $data)));
        } else {
            die(json_encode(array('success' => false)));
        }
    }

    /**
     * For searching orders from sales tab with ajax
     */
    public function actionSearchactivities()
    {
        $keyword = Yii::$app->request->post('keyword');
        $model = new RecordsActivities();
        $data = $model->searchActivitiesByKeyword($keyword);
        $data = ArrayHelper::toArray($data);

        if (!empty($data)) {
            die(json_encode(array('success' => true, 'data' => $data)));
        } else {
            die(json_encode(array('success' => false)));
        }
    }

    public function actionSaveconnections()
    {
        $data['id'] = Yii::$app->request->post('id');
        $data['connections'] = Yii::$app->request->post('connections');

        RecordsLinks::deleteAll(['id_record' => $data['id']]);
        $model = new RecordsLinks();
        $model->saveRecordLinks($data);

        die(json_encode(array('success' => true)));
    }
    public function actionDeleteconnections($id_link)
    {
        RecordsLinks::deleteAll(['id_link' => $id_link]);
        die(json_encode(array('success' => true)));
    }

    public function actionDeletedocuments($id_document)
    {
        RecordsMedia::deleteAll(['id_media' => $id_document]);
        die(json_encode(array('success' => true)));
    }

    public function actionDeleteorders($id_order)
    {
        RecordsOrders::deleteAll(['id_order' => $id_order]);
        die(json_encode(array('success' => true)));
    }

    public function actionDeleteactivities($id_activity)
    {
        RecordsActivities::deleteAll(['id_activity' => $id_activity]);
        die(json_encode(array('success' => true)));
    }

    public function actionSavedocuments()
    {
        $id = Yii::$app->request->post('id');
        $documents = Yii::$app->request->post('documents');

        RecordsMedia::deleteAll(['id_container' => $id, 'context' => 3]);
        if (!empty($documents)) {
            foreach ($documents as $item) {
                $media = new RecordsMedia;
                $media->id_container = $id;
                $media->id_media = $item;
                $media->context = 3;
                $media->save();
            }
        }

        die(json_encode(array('success' => true)));
    }

    /**
     * function for save orders
     */
    public function actionSaveorders()
    {
        $id = Yii::$app->request->post('id');
        $orders = Yii::$app->request->post('orders');
        $record = Records::find('code')->where(['id_record' => $id])->one();

        /*
        *   not deleting. beacuse there is no junction table.
        *   set the record_code field null for all of this record orders 
        *   just update the orders record_code field.
        */

        RecordsOrders::updateAll(['record_code' => null], ["record_code" => $record->code]);

        if (!empty($orders)) {
            foreach ($orders as $item) {
                $order = RecordsOrders::findOne($item);
                $order->record_code = $record->code;
                $order->update();
            }
        }

        die(json_encode(array('success' => true)));
    }

    /**
     * save activities
     */
    public function actionSaveactivities()
    {
        $id = Yii::$app->request->post('id');
        $activities = Yii::$app->request->post('activities');
        $record = Records::find('code')->where(['id_record' => $id])->one();

        /*
        *   not deleting. beacuse there is no junction table.
        *   set the record_code field null for all of this record activities 
        *   just update the activities record_code field.
        */

        RecordsActivities::updateAll(['record_code' => null], ["record_code" => $record->code]);
        if (!empty($activities)) {
            foreach ($activities as $item) {
                $activity = RecordsActivities::findOne($item);
                $activity->record_code = $record->code;
                $activity->update();
            }
        }

        die(json_encode(array('success' => true)));
    }

    /**
     * action for saving map data
     */
    public function actionSavemap()
    {
        $id = Yii::$app->request->post('id');
        $coordinates = Yii::$app->request->post('coordinates');

        $record = Records::findOne($id);
        $record->coordinates = $coordinates;
        $record->save();

        /*Yii::$app->db->createCommand('UPDATE zse_v1_records SET coordinates = "' . $coordinates . '" WHERE id_record=' . $id)
            ->execute();*/

        die(json_encode(array('success' => true)));
    }


    /**
     * Function list of connection tab in view page view tab.
     * @param $id
     */
    public function actionGetconnections($id)
    {
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $search = Yii::$app->request->post('search');
        $sort = Yii::$app->request->post('sort');

        /********************************select fields***************************/
        $field_list = ['is_customer','is_destination','is_prospect','is_contact', 'zse_v1_records_links.id_link', 'code', 'business_name', 'city', 'country'];
        /********************************sorting query start*********************/
        $short_qry = '';
        if (isset($sort['column']) && $sort['dir']) {
            $sort_column = $field_list[$sort['column']];
            $sort_order = $sort['dir'];
            $short_qry = $sort_column . ' ' . strtoupper($sort_order);
        }
        /***********************************End sort***************************************8*/
        /**************************************Filter ************************************8*/
        $filter_where = [];
        if ($search != '') {
            $filter_where[] = 'or';
            //$filter_where[] = ['like', '`zse_v1_records`.`type`', $search];
            $filter_where[] = ['like', '`zse_v1_records`.`code`', $search];
            $filter_where[] = ['like', '`zse_v1_records`.`business_name`', $search];
            $filter_where[] = ['like', '`zse_v1_records`.`city`', $search];
            $filter_where[] = ['like', '`zse_v1_records`.`country`', $search];

            $search_type_fild = AppHelper::typeToQueryConversion($search);
            if(isset($search_type_fild) && $search_type_fild != ''){
                if(strtolower($search) == 'destination'){
                    $filter_where[] = ['>', "`zse_v1_records`.$search_type_fild" , 0];
                }else{
                    $filter_where[] = ['like', "`zse_v1_records`.$search_type_fild" , 1];
                }
            }
        }
        /**************************************************************************8*/
        $link_model = new RecordsLinks();
        $data = $link_model->getContactConnections($id, $field_list, $filter_where, $limit, $offset, $short_qry);
        $data_count = $link_model->countContactConnections($id, $filter_where);

        $total_data = [];
        $data = ArrayHelper::toArray($data);
        foreach ($data as $row_key => $row_val) {
            $row_data = [];
            $select_from_four_type = true;
            foreach ($row_val as $key => $val) {
                $entry_flag = true;
                if (AppHelper::fieldArrayExist($key)) {
                    if($val >0 && $select_from_four_type){
                        if(isset($search) && $search != ''){
                            if(strtolower($search) == 'destination'){   //if from search held for string 'destination' then icon will generate only for destination
                                $type = 'destination';
                            }else{
                                $type = AppHelper::getTypeFromFild($key);
                            }
                        }else{
                            $type = AppHelper::getTypeFromFild($key);
                        }
                        $val = AppHelper::convertTypeIntoImage($type);
                        $select_from_four_type = false;
                    }else{
                        $entry_flag = false;
                    }
                }
                if ($key != 'id_link' && $entry_flag) {
                    $row_data[] = $val;
                }
            }
            $id_link = $row_val['id_link'];
            $action_url = '<a href="javascript:void(0)" id="' . $id_link . '" onclick="delete_connection(this)"> <i class="fa fa-minus"></i></a>';
            $row_data[] = "$action_url";
            $total_data[] = $row_data;
        }
        $final_data = array('data' => $total_data, "recordsTotal" => $data_count, "recordsFiltered" => $data_count);

        /**********making json data from active record data for DataTable start******/

        die(json_encode($final_data));
    }

    /**
     * Function list of documents tab in view page view tab.
     * @param $id
     */
    public function actionGetdocument($id)
    {
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $search = Yii::$app->request->post('search');
        $sort = Yii::$app->request->post('sort');
        $short_qry = '';
        $field_list = ['`zse_v1_media`.`type`','`zse_v1_media_links`.`id_media`','`zse_v1_media`.`type`','`zse_v1_media`.`name`','`zse_v1_media`.`description`','`zse_v1_media`.`cansend`'];
        /********************************sorting query start*********************/
        if (isset($sort['column']) && $sort['dir']) {
            $sort_column = $field_list[$sort['column']];
            $sort_order = $sort['dir'];
            $short_qry = $sort_column . ' ' . strtoupper($sort_order);
        }
        /***********************************End sort***************************************8*/
        /**************************************Filter ************************************8*/
        $where = ['zse_v1_media_links.id_container' => $id, 'zse_v1_media_links.context'=>3];
        $filter_where = [];
        if ($search != '') {
            $filter_where[] = 'or';
            $filter_where[] = ['like', '`zse_v1_media`.`type`', $search];
            $filter_where[] = ['like', '`zse_v1_media_links`.`id_media`', $search];
            $filter_where[] = ['like', '`zse_v1_media`.`type`', $search];
            $filter_where[] = ['like', '`zse_v1_media`.`name`', $search];
            $filter_where[] = ['like', '`zse_v1_media`.`description`', $search];
            $filter_where[] = ['like', '`zse_v1_media`.`cansend`', $search];
        }
        /**************************************************************************8*/
        $document_model = new RecordsMedia();

        $data = $document_model->getContactDocuments($id, $filter_where, $limit, $offset, $short_qry);

        $data_count = $document_model->countContactDocuments($id, $filter_where);
        #BaseController::_setTrace($data_count);
        $total_data = [];
        $data = ArrayHelper::toArray($data);

        foreach ($data as $row_key => $row_val) {
            $row_data = [];
            foreach ($row_val as $key => $val) {
                if ($key == 'type' && !empty($val)) {
                    $icon = AppHelper::convertTypeIntoImage($val);
                    $row_data[] = $icon;
                }
                if ($key == 'cansend' && !empty($val)) {
                    $val = AppHelper::convertTypeIntoImage('cansend-'.$val);
                }
                if ($key != 'id_media') {
                    $row_data[] = $val;
                }
            }
            $id_media = $row_val['id_media'];
            $action_url = '<a href="javascript:void(0)" id="' . $id_media . '" onclick="delete_document(this)"> <i class="fa fa-minus"></i></a>';
            $row_data[] = "$action_url";
            $total_data[] = $row_data;
        }
        $final_data = array('data' => $total_data, "recordsTotal" => $data_count, "recordsFiltered" => $data_count);

        /**********making json data from active record data for DataTable start******/

        die(json_encode($final_data));
    }

    /**
     * Function list of orders tab in view page view tab.
     * @param $id
     */
    public function actionGetorder($id)
    {
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $search = Yii::$app->request->post('search');
        $sort = Yii::$app->request->post('sort');
        $short_qry = '';
        $field_list = ['`order_code`','`id_order`','`date_order`','`type`','`subject`','`description`','`status`'];
        /********************************sorting query start*********************/
        if (isset($sort['column']) && $sort['dir']) {
            $sort_column = $field_list[$sort['column']];
            $sort_order = $sort['dir'];
            $short_qry = $sort_column . ' ' . strtoupper($sort_order);
        }
        /***********************************End sort****************************************/
        /**************************************Filter *************************************/

        $filter_where = [];
        if ($search != '') {
            $filter_where[] = 'or';
            $filter_where[] = ['like', '`order_code`', $search];
            $filter_where[] = ['like', '`type`', $search];
            $filter_where[] = ['like', '`date_order`', $search];
            $filter_where[] = ['like', '`subject`', $search];
            $filter_where[] = ['like', '`description`', $search];
            $filter_where[] = ['like', '`status`', $search];
        }
        $record_model = Records::find()->where(['id_record' => $id])->one();
        $where = ['record_code' => $record_model->code];
        //$where = ['record_code' => 'C034'];
        /***************************************************************************/

        $data = RecordsOrders::getContactOrders($field_list, $where,$filter_where, $limit, $offset, $short_qry);
        $data_count = RecordsOrders::countContactOrders($where, $filter_where);
        #BaseController::_setTrace($data_count);
        $total_data = [];
        $data = ArrayHelper::toArray($data);

        foreach ($data as $row_key => $row_val) {
            $row_data = [];
            foreach ($row_val as $key => $val) {
                if ($key != 'id_order') {
                    $row_data[] = $val;
                }
            }
            $id_order = $row_val['id_order'];
            $action_url = '<a href="javascript:void(0)" id="' . $id_order . '" onclick="delete_order(this)"> <i class="fa fa-minus"></i></a>';
            $row_data[] = "$action_url";
            $total_data[] = $row_data;
        }
        $final_data = array('data' => $total_data, "recordsTotal" => $data_count, "recordsFiltered" => $data_count);

        /**********making json data from active record data for DataTable start******/

        die(json_encode($final_data));
    }

    /**
     * Function list of activities tab in view page view tab.
     * @param $id
     */
    public function actionGetactivity($id)
    {
        $limit = Yii::$app->request->post('limit');
        $offset = Yii::$app->request->post('offset');
        $search = Yii::$app->request->post('search');
        $sort = Yii::$app->request->post('sort');
        $short_qry = '';
        $field_list = ['`date_activity`','`id_activity`','`type`','`user`','`description`','`status`'];
        /********************************sorting query start*********************/
        if (isset($sort['column']) && $sort['dir']) {
            $sort_column = $field_list[$sort['column']];
            $sort_order = $sort['dir'];
            $short_qry = $sort_column . ' ' . strtoupper($sort_order);
        }
        /***********************************End sort****************************************/
        /**************************************Filter *************************************/

        $filter_where = [];
        if ($search != '') {
            $filter_where[] = 'or';
            $filter_where[] = ['like', '`date_activity`', $search];
            $filter_where[] = ['like', '`type`', $search];
            $filter_where[] = ['like', '`user`', $search];
            $filter_where[] = ['like', '`description`', $search];
            $filter_where[] = ['like', '`status`', $search];
        }
        $record_model = Records::find()->where(['id_record' => $id])->one();
        $where = ['record_code' => $record_model->code];
        //$where = ['record_code' => 'C034'];
        /***************************************************************************/

        $data = RecordsActivities::getContactActivities($field_list, $where,$filter_where, $limit, $offset, $short_qry);
        $data_count = RecordsActivities::countContactActivities($where, $filter_where);
        #BaseController::_setTrace($data_count);
        $total_data = [];
        $data = ArrayHelper::toArray($data);

        foreach ($data as $row_key => $row_val) {
            $row_data = [];
            foreach ($row_val as $key => $val) {
                if ($key != 'id_activity') {
                    $row_data[] = $val;
                }
            }
            $id_activity = $row_val['id_activity'];
            $action_url = '<a href="javascript:void(0)" id="' . $id_activity . '" onclick="delete_activity(this)"> <i class="fa fa-minus"></i></a>';
            $row_data[] = "$action_url";
            $total_data[] = $row_data;
        }
        $final_data = array('data' => $total_data, "recordsTotal" => $data_count, "recordsFiltered" => $data_count);

        /**********making json data from active record data for DataTable start******/

        die(json_encode($final_data));
    }

}



