<?php

namespace app\modules\documents\controllers;

use app\modules\documents\models\DocsTree;
use app\modules\documents\models\Media;
use app\modules\documents\models\SearchRepositoryFiles;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use dosamigos\arrayquery\ArrayQuery;
use yii\helpers\Html;
use Yii;

/**
 * Default controller for module 'Documents'
 */
class MainController extends Controller
{

    public $defaultAction = 'list';
    public static $nestedFolders; //array of nested folders
    public static $relationArray;
    public static $modalContentUri = 'folder/move-files'; //uri for content into Modal (click Massive move button)

    public function init()
    {
        self::$relationArray = [];
    }

    /**
     * @var array list of items (folder, files) and options
     * - icon: icon's style
     * - label: string, optional, specifies the menu item label.
     * - urlNew: array, optional, specifies the URL for creating new item. It will be processed by [[Url::to]].
     */
    public $entities = [
        'folder' => [
            'icon' => 'fa fa-folder-o',
            'label' => 'Folder',
            'urlNew' => ['folder/create'],
        ],
        'pdf' => [
            'icon' => 'fa fa fa-file-pdf-o',
            'label' => 'Pdf',
            'urlNew' => ['document/create', 'type'=>'pdf'],
        ],
        'movie' => [
            'icon' => 'fa fa-file-movie-o',
            'label' => 'Movie',
            'urlNew' => ['document/create', 'type'=>'movie'],
        ],
        'html5' => [
            'icon' => 'fa fa-file-code-o',
            'label' => 'HTML5',
            'urlNew' => ['document/create', 'type'=>'html5'],
        ],
        'gallery' => [
            'icon' => 'fa fa-file-image-o',
            'label' => 'gallery',
            'urlNew' => ['document/create', 'type'=>'gallery'],
        ],
        'powerpoint' => [
            'icon' => 'fa fa-file-powerpoint-o',
            'label' => 'PowerPoint',
            'urlNew' => ['document/create', 'type'=>'powerpoint'],
        ],
        'excel' => [
            'icon' => 'fa fa-file-excel-o',
            'label' => 'Excel',
            'urlNew' => ['document/create', 'type'=>'excel'],
        ],
        'word' => [
            'icon' => 'fa fa-file-word-o',
            'label' => 'Word',
            'urlNew' => ['document/create', 'type'=>'word'],
        ],
        'default' => [
            'icon' => 'fa fa-file',
            'label' => 'File',
            'urlNew' => ['document/create', 'type'=>'file'],
        ],
    ];

    /**
     * Constuct list datatable
     * @return type
     */
    public function actionList()
    {
        //apply filters
        if (Yii::$app->getRequest()->getQueryParam('sharable') || Yii::$app->getRequest()->getQueryParam('type')) {
            $documents = DocsTree::find()
            ->joinWith('media.repositoryFile')
            ->orderBy(['zse_v1_docs_tree.id_folder' => SORT_ASC])->limit(10000);

            $modelsProvider = [];
            foreach ($documents->all() as $folder) {
                $sizeFolder = 0;
                $countSharable = [
                    0 => 0, //count not sharable files in folder
                    1 => 0 //count sharable files in folder
                ];
                if (isset($folder->media)) {
                    foreach ($folder->media as $media) {
                        if ($this->mediaAlreadyExists($modelsProvider, "id", $media->id_media, $media->type))
                            continue; //if media allready in array (earlier added from another linked folder)

                        if (isset($media->repositoryFile)) { //calculate size for this media
                            $modelsProvider[] = $this->parseFile($media);
                            $sizeFolder += $media->repositoryFile->size;
                        } elseif ($media->type === 'gallery') { //calculate size for gallery item
                            $galleryItems = explode("\n", $media->multimedia);
                            $gallerySize = 0;
                            foreach ($galleryItems as $item) {
                                $itemSize = \app\modules\documents\models\RepositoryFiles::find()->select('size')->where(['name' => $item])->scalar();
                                $sizeFolder += $itemSize;
                                $gallerySize += $itemSize;
                            }
                            $modelsProvider[] = $this->parseFile($media, $gallerySize);
                        }
                        $countSharable[$media->cansend] ++;
                    }
                }

                $modelsProvider[] = $this->parseFolder($folder, $sizeFolder, $countSharable);
            }

            //check "Type" filter
            if (Yii::$app->getRequest()->getQueryParam('type')) {
                $filterType = $this->getFilterType(Yii::$app->getRequest()->getQueryParam('type'));
            }

            if (count($modelsProvider) > 0) {
                $query = new ArrayQuery($modelsProvider);
                if ($filterType)
                    $query->addCondition('type', $filterType['value']); //apply type filter
                if (Yii::$app->getRequest()->getQueryParam('sharable'))
                    $query->addCondition('sharable', 1); //apply sharable filter

                $modelsProvider = $query->find();
            }
        } else { //get array of nested folders and medias (default action (without filters))
            $folderId = (Yii::$app->getRequest()->getQueryParam('id_folder')) ? Yii::$app->getRequest()->getQueryParam('id_folder') : 0;
            $currentFolder = ($folderId > 0) ? DocsTree::findOne(['id_folder' => $folderId]) : DocsTree::find()->roots()->one(); //for one folder in root
            $modelsProvider = $this->getNestedList($currentFolder);
        }

        $provider = new ArrayDataProvider([
            'allModels' => $modelsProvider,
            'sort' => [
                'attributes' => ['icon', 'name', 'type', 'sharable', 'size'],
            ],
        ]);

        return $this->render('list', [
            'folder' => isset($currentFolder) ? $currentFolder : false,
            'newEntities' => isset($currentFolder) ? $this->parseNewEntities($currentFolder) : $this->parseNewEntities(),//items for dropdown list (button NEW) -> only inside folder!!!
            'dataProvider' => $provider,
        ]);
    }

    /**
     * Set image icon (if isset)
     * @param type $image
     * @return type
     */
    public function setImgIcon($image)
    {
        return Html::img(Yii::getAlias('@web') . '/images/' . $image, ['width' => '64px', 'height' => '64px']);
        //Html::img(Yii::getAlias('@web').'/images/'. $data['image']
    }

    /**
     * Set icon style by item's type
     * @param type $type
     * @param type $addClass add class name to current name
     * @return type
     */
    public function setStyleIcon($type, $addClass = null)
    {
        $classStyle = (isset($this->entities[$type]['icon'])) ? $this->entities[$type]['icon'] : $this->entities['default']['icon'];
        if ($addClass)
            $classStyle .= ' ' . $addClass;
        return "<i class='{$classStyle}'></i>";
    }

    /**
     * Get nested list of folders and media (filters don't applied)
     *
     * @param $currentFolder DocsTree Object 
     * @return array
     */
    public function getNestedList($currentFolder, $addIconStyle = null)
    {
        $queryChildrenFolders = $currentFolder->children(1); //only children 1-st level

        $folderChildren = $queryChildrenFolders->all(); //subfolders for current folder
        $modelsProvider = []; //models array for provider
        $index = 0; //key for array $modelsProvider
        foreach ($folderChildren as $folder) {
            $modelsProvider[$index] = [
                'icon' => (is_null($addIconStyle)) ? $this->setStyleIcon('folder') : $this->setStyleIcon('folder', $addIconStyle),
                'id' => $folder->id_folder,
                'name' => $folder->label,
                'type' => 'folder',
                'sharable' => $folder->sharable,
                'size' => $folder->size,
            ];
            $index++;
        }

        if ($currentFolder) {
            $queryMedias = Media::find()
            ->joinWith('mediaLinks')
            ->joinWith('repositoryFile')
            ->andWhere(['zse_v1_media_links.id_container' => $currentFolder->id_folder])
            ->orderBy(['zse_v1_media_links.weight' => SORT_DESC, 'zse_v1_media_links.id_media' => SORT_DESC])->limit(10000);

            $mediaInFolder = $queryMedias->all(); //medias in current folder
            foreach ($mediaInFolder as $media) {
                $modelsProvider[$index] = [
                    'icon' => (is_null($addIconStyle)) ? $this->setStyleIcon($media->type) : $this->setStyleIcon($media->type, $addIconStyle), //$this->setStyleIcon($media->type),
                    'id' => $media->id_media,
                    'name' => $media->name,
                    'type' => $media->type,
                    'sharable' => $media->cansend,
                    'size' => ($media->repositoryFile) ? $media->repositoryFile->size : 0,
                    'repFile' => ($media->repositoryFile) ? $media->repositoryFile->name : null,
                ];
                $index++;
            }
        }
        return $modelsProvider;
    }

    /**
     * Parse folder for list datatable
     * @param type $folderObject
     * @param type $size
     * @param type $sharable
     * @return type
     */
    public function parseFolder($folderObject, $size, $sharable)
    {
        return [
            'icon' => ($folderObject->icon != '') ? $this->setImgIcon($folderObject->icon) : $this->setStyleIcon('folder'),
            'id' => $folderObject->id_folder,
            'name' => $folderObject->label,
            'type' => 'folder',
            'sharable' => DocsTree::sharableStatus($sharable),
            'size' => $size,
            'repositoryFile'=> ''
        ];
    }

    /**
     * Parse media for list datatable
     * @param type $fileObject
     * @return type
     */
    public function parseFile($fileObject, $size = null)
    {
        Yii::trace('parseFile'.print_r($fileObject->repositoryFile,true));
        return [
            'icon' => ($fileObject->icon != '') ? $this->setImgIcon($fileObject->icon) : $this->setStyleIcon($fileObject->type),
            'id' => $fileObject->id_media,
            'name' => $fileObject->name,
            'type' => $fileObject->type,
            'sharable' => $fileObject->cansend,
            'size' => ($size) ? $size : $fileObject->repositoryFile->size,
            'repFile' => ($fileObject->repositoryFile) ? $fileObject->repositoryFile->name : null,
        ];
    }

    /**
     * Check media exists in provider array
     * @param $array
     * @param $key
     * @param $val
     * @return bool
     */
    public function mediaAlreadyExists($array, $key, $val, $type)
    {
        foreach ($array as $item)
            if (isset($item[$key]) && $item[$key] == $val && $item['type'] == $type)
                return true;
        return false;
    }

    /**
     * get parameter for filter condition
     * using dosamigos\arrayquery\ArrayQuery;
     * @param type $getType
     * @return boolean | array
     * return:
     * false => don't apply filter
     * [
     * 'value' => 'filterValue',
     * 'operator' => 'operator' //from dosamigos\arrayquery\ArrayQuery addCondition method();
     * ]
     */
    public function getFilterType($getType)
    {
        switch ($getType) {
            case 'files':
                return [
                    'value' => '<>folder',
                ];
                break;

            case 'all':
            case '---':
                return false;
                break;


            default:
                if (array_key_exists($getType, $this->entities)) { // $this->iconType
                    return [
                        'value' => $getType,
                    ];
                } else
                    return false;
                break;
        }
    }

    /**
     * build Folders
     * @return type
     */
    public function actionFolders()
    {
        $rootFolder = DocsTree::find()->roots()->one();

        self::$nestedFolders = [];
        self::$nestedFolders[$rootFolder->id_folder] = ['label' => '<span class="label h-bg-navy-blue"><i class="fa fa-folder"></i></span> ' . $rootFolder->label, 'options' => ['data-id' => $rootFolder->id_folder]]; //['label' => $rootFolder->label, 'options' => ['data-id' => $rootFolder->id_folder]];

        foreach (SearchRepositoryFiles::$typeRelations as $type => $params) {
            $typeId = "type_{$rootFolder->id_folder}_{$type}";
            self::setArray(self::$nestedFolders, "{$rootFolder->id_folder}.items.{$typeId}", ['label' => '<span class="label h-bg-navy-blue"><i class="'.$params['icon'].'"></i></span> ' . $params['label'] . self::setMassiveMoveBtn($rootFolder->id_folder, $type, $params['label']), 'options' => ['data-id' => $typeId]]);
        }
        //build recursively nested folder's structure
        Yii::trace('folders nested begin:'.print_r(self::$nestedFolders,true));
        self::folderNestedRecursive($rootFolder->children(1)->all(), "{$rootFolder->id_folder}"); //true|false

        return $this->render('folders', [
            'folders' => self::$nestedFolders,
            'newEntities' => $this->parseNewEntities(),//items for dropdown list (button NEW)
        ]);
    }


    /**
     * Build nested structure of folders and medias
     * @param array $array
     * @param type $path string (array's deep, example: "key1.key2.key3"
     */
    public static function folderNestedRecursive(array $array, $path = null)
    {
        $path = "{$path}.items";
        foreach ($array as $k => $v) {
            $children = $v->children(1)->all(); //children folders
            //set information about folder (build folder menu's item)
            self::setArray(self::$nestedFolders, "{$path}.{$v->id_folder}", ['label' => '<span class="label h-bg-navy-blue"><i class="fa fa-folder"></i></span> ' . $v->label, 'options' => ['data-id' => $v->id_folder]]);

            //set pannels for moving by types:
            foreach (SearchRepositoryFiles::$typeRelations as $type => $params) {
                $typeId = "type_{$v->id_folder}_{$type}";
                //$controller = new MainController('main', 'documents');
                self::setArray(self::$nestedFolders, "{$path}.{$v->id_folder}.items.{$typeId}", ['label' => '<span class="label h-bg-navy-blue"><i class="'.$params['icon'].'"></i></span> ' . $params['label'] . self::setMassiveMoveBtn($v->id_folder, $type, $params['label']), 'options' => ['data-id' => $typeId]]);
            }

            if (count($children) > 0) {
                $fullpath = "{$path}.{$v->id_folder}";
                self::folderNestedRecursive($children, $fullpath);
            }


            //set information about medias in folder (build media menu's item). Nested structure
            /*
            if (isset($v->media)) {
                if (count($v->media) > 0) { //insert folder's media
                    foreach ($v->media as $media) {
                        $idMedia = "media_" . $media->id_media;
                        $controller = new MainController('main', 'documents');

                        //set information about media
                        self::setArray(self::$nestedFolders, "{$path}.{$v->id_folder}.items.{$idMedia}", ['label' => '<span class="label h-bg-navy-blue">' . $controller->setStyleIcon($media->type) . '</span> ' . $media->name, 'options' => ['data-id' => $idMedia]]);
                    }
                }
            }
            */
        }
    }

    /**
     * Set multidimensional array deep
     * @param $array
     * @param $keys
     * @param $value
     *
     * example:
     * $array = Array();
     * self::setArray($array, "key", Array('value' => 2));
     * self::setArray($array, "5.15.15", 3);
     * print_r($array);
     *
     */
    public static function setArray(&$array, $keys, $value)
    {
        $keys = explode(".", $keys);
        $current = &$array;
        foreach ($keys as $key) {
            $current = &$current[$key];
        }
        $current = $value;
    }

    /**
     * Set style for button "Massive Move" (for opening Modal)
     * @param type $id
     * @param type $label folder's label
     * @return type
     */
    public static function setMassiveMoveBtn($id, $type, $label)
    {
        $url = \yii\helpers\Url::to([self::$modalContentUri, 'id' => $id, 'type' => $type]);
        return '<span class="showModalButton label h-bg-green pull-right"><input id="folder_' . $id . '" type="hidden" value="' . $label . '|' . $url . '"><i class="fa fa-exchange"></i></span>';
    }

    /**
     * Build flow
     * @param type $id_folder
     * @return type
     */
    public function actionFlow($id_folder = null)
    {
        $folderId = (!is_null($id_folder)) ? $id_folder : 0;
        $currentFolder = ($folderId > 0) ? DocsTree::findOne(['id_folder' => $folderId]) : DocsTree::find()->roots()->one(); //for one folder in root
        $items = $this->getNestedList($currentFolder, 'text-info');
        return $this->render('flow', [
            'folder' => $currentFolder,
            'columns' => $this->buildFlowGrid($items),
            'newEntities' => $this->parseNewEntities(),//items for dropdown list (button NEW)
        ]);
    }

    /**
     * Build multidimensional array for flow view
     * @param type $items
     * @param type $columnsLimit
     * @return array $out 
     */
    public function buildFlowGrid($items, $columnsLimit = 4)
    {
        $out = [];
        $columnCounter = 0;
        foreach ($items as $item) {
            $out[$columnCounter][] = $item;
            $columnCounter++;

            if ($columnCounter >= $columnsLimit)
                $columnCounter = 0;
        }
        return $out;
    }


    /**
     * Parse $entities array for droppdown list (click on NEW button)
     * @return array
     */
    public function parseNewEntities($folder = false)
    {
        if(!$folder){ //not inside folder => block NEW button
            return false;
        }
              
        $newEntities = [];
        foreach ($this->entities as $key=>$entity) {
            $newEntities[] = [
                'label' => $this->setStyleIcon($key).' '.$entity['label'],
                'url' => array_merge($entity['urlNew'], ['id_folder'=>$folder['id_folder']]),
            ];
        }
        return $newEntities;
    }

    /**
     * Search by name from field on 'folders' and 'flow' pages
     * @return string
     */
    public function actionSearch()
    {
        $searhString = Yii::$app->request->post('search');
        $documents = DocsTree::find()
        ->joinWith('media.repositoryFile')
        ->orderBy(['zse_v1_docs_tree.id_folder' => SORT_ASC])->limit(10000);

        $modelsProvider = [];
        foreach ($documents->all() as $folder) {
            $sizeFolder = 0;
            $countSharable = [
                0 => 0, //count not sharable files in folder
                1 => 0 //count sharable files in folder
            ];
            if (isset($folder->media)) {
                foreach ($folder->media as $media) {
                    if ($this->mediaAlreadyExists($modelsProvider, "id", $media->id_media, $media->type))
                        continue; //if media allready in array (earlier added from another linked folder)

                    if (isset($media->repositoryFile)) { //calculate size for this media
                        $modelsProvider[] = $this->parseFile($media);
                        $sizeFolder += $media->repositoryFile->size;
                    } elseif ($media->type === 'gallery') { //calculate size for gallery item
                        $galleryItems = explode("\n", $media->multimedia);
                        $gallerySize = 0;
                        foreach ($galleryItems as $item) {
                            $itemSize = \app\modules\documents\models\RepositoryFiles::find()->select('size')->where(['name' => $item])->scalar();
                            $sizeFolder += $itemSize;
                            $gallerySize += $itemSize;
                        }
                        $modelsProvider[] = $this->parseFile($media, $gallerySize);
                    }
                    $countSharable[$media->cansend] ++;
                }
            }

            $modelsProvider[] = $this->parseFolder($folder, $sizeFolder, $countSharable);
        }

        if (count($modelsProvider) > 0) {
            $query = new ArrayQuery($modelsProvider);
            if ($searhString) {
                $query->addCondition('name', '~'.$searhString); //apply type filter
            }

            $modelsProvider = $query->find();
            $provider = new ArrayDataProvider([
                'allModels' => $modelsProvider,
                'sort' => [
                    'attributes' => ['icon', 'name', 'type', 'sharable', 'size'],
                ],
            ]);

            return $this->renderAjax('search_list', [
                'dataProvider' => $provider,
            ]);
        }
    }    
}        