<?php

namespace app\modules\reports\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "zse_v1_repository_files".
 *
 * @property integer $id_file
 * @property string $mimetype
 * @property string $name
 * @property integer $origin
 * @property integer $size
 * @property integer $id_account
 * @property string $date_insertion
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class RepositoryFiles extends \yii\db\ActiveRecord
{

    public $idContainer; // container_id for generate entity_id

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'zse_v1_repository_files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['origin', 'size', 'id_account', 'record_status', 'date_insertion'], 'integer'],
            [['date_insertion', 'date_modification'], 'safe'],
            [['mimetype', 'name'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name'], 'required', 'on' => ['create']]
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['name'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_file' => 'Id File',
            'mimetype' => 'Mimetype',
            'name' => 'Name',
            'origin' => 'Origin',
            'size' => 'Size',
            'id_account' => 'Id Account',
            'date_insertion' => 'Date Insertion',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['date_insertion', 'date_modification'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['date_modification'],
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ]
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->loadDefaultValues(); // load default attributes from db table
                $this->entity_id = sha1(mt_rand(10000, 99999) . time() . $this->idContainer); // set idContainer
            } else { // update
            }
            return true;
        } else
            return false;
    }

    public function setIdContainer($idContainer)
    {
        $this->idContainer = $idContainer;
    }

    public function getIdContainer()
    {
        return $this->idContainer;
    }

}
