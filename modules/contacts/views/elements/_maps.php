<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:25 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3>Maps </h3>
<hr/>
<div class="note-content">


    <?php
        $coordinates_string = $coordinates;
        $coordinates = explode(',', $coordinates);
    ?>

    <script>
        var myLatLng;
        <?php if(!empty($coordinates[0]) && !empty($coordinates[1])) { ?>
                myLatLng = {lat: <?= $coordinates[0]?>, lng: <?= $coordinates[1]?>};
        <?php } else { ?>
                myLatLng = {};
        <?php } ?>
    </script>

    <div class="row">
        <?php if(!isset($type)): ?>
        <div class="col-lg-4">
            <p> <h3> Add Coordinates </h3> </p>

            <form role="form" id="form_map">
                <div class="form-group"><label for="coordinates">Coordinates</label>
                    <input type="text" id="coordinates" name="coordinates" value="" class="form-control" required>
                </div>

                <button class="btn btn-block btn-success m-t-n-xs" type="submit"><strong>Add</strong></button>
            </form>


        </div>
        <?php endif; ?>
        <div class="col-lg-8">
            <?php if(!isset($type)): ?>
            <p> <h3> See in map </h3> </p>
            <?php endif; ?>
            <section id="map">
                <div id="map-area" style="height: 200px"></div>
            </section>
        </div>    
        <script src="https://maps.googleapis.com/maps/api/js"
                async defer></script>
    </div>
    <?php if(!isset($type)): ?>
    <div class="btn-group save_btn_group" id="map_save_btn_group">
        <button class="btn btn-sm btn-default" id="save_map"><i class="fa fa-thumbs-o-up"></i> Save</button>
    </div>
    <?php endif; ?>
</div>
