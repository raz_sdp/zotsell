<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:14 AM
 */
$this->title = "Contacts Flow View";
$this->subtitle = "Use Flow view to show Contacts in a sortable flow .";
$this->breadCrums = [['label' => 'Contacts', 'link' => Yii::$app->urlManager->createUrl('contacts/records/list')], 'Flow View'];
?>
<div class="col-md-3">
    <?php echo $this->render('../layouts/left-hpanel') ?>
    <?php echo $this->render('../elements/_contact-list-filter',[
        'classification_list'=>$classification_list
    ])?>
</div>

<div class="col-md-9">

    <div class="row">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-body">
                    <div class="input-group">
                        <input class="form-control" id="serch-contact-records-value-flow" type="text" placeholder="Search contacts..">
                        <div class="input-group-btn">
                            <button class="btn btn-default" id="serch-contact-records-button-flow"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="pagination-offset" value="0">
    <input type="hidden" id="filter-form-submitted" value="0">
    <input type="hidden" id="submitted-by-click" value="0">
    <input type="hidden" id="submitted-by-search-flow" value="0">
    <div class="js-contact-records-grid"></div>
</div>
