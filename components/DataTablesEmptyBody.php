<?php

namespace app\components;

use Yii;
use fedemotta\datatables\DataTables;

/**
 * Datatables Yii2 widget extends fedemotta\datatables\DataTables
 * override \yii\helpers\Html::renderTableBody()
 * to generate empty <tbody></tbody> when provider return empty models
 */

class DataTablesEmptyBody extends DataTables
{
    public function renderTableBody()
    {
        $models = array_values($this->dataProvider->getModels());
        $keys = $this->dataProvider->getKeys();
        $rows = [];
        foreach ($models as $index => $model) {
            $key = $keys[$index];
            if ($this->beforeRow !== null) {
                $row = call_user_func($this->beforeRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }

            $rows[] = $this->renderTableRow($model, $key, $index);

            if ($this->afterRow !== null) {
                $row = call_user_func($this->afterRow, $model, $key, $index, $this);
                if (!empty($row)) {
                    $rows[] = $row;
                }
            }
        }

        if (empty($rows)) {
            $colspan = count($this->columns);

            return "<tbody></tbody>";
            
        } else {
            return "<tbody>\n" . implode("\n", $rows) . "\n</tbody>";
        }
    }
}
