<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\contacts\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetContacts extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = '@webroot';
    public $css = [
        'css/contacts/custom.css',
    ];

    public $js = [
        'js/contacts/custom.js',
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
