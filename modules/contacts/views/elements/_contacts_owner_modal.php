<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:29 PM
 */
use \app\modules\contacts\models\RecordsAgents;
?>
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Define Contacts owner</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body row col-md-12" style="clear: both; margin: auto">
                <!-- <div class="form-group pull-left"> -->
                <?php
                $contact_owner_list = RecordsAgents::getContactOwners();
                $selected_owner_list = $owner_list_token_as_array;
                #print_r($selected_owner_list);die;
                ?>
                <div class="col-md-12">
                    <select multiple="multiple" id="contact-owner-list" class="searchable" name="my_select[]">
                        <?php
                        $tbl_row='';
                        foreach($contact_owner_list as $row){
                            $selected = (in_array($row['usercode'],$selected_owner_list)) ? 'selected' : '';
                            echo "<option ".$selected." value = '".json_encode(['working_code'=>$row['working_code'],'username'=>$row['username'],'usercode' => $row['usercode']])."'>".$row['usercode']." (".$row['username'].")  </option>";
                        }
                        ?>
                    </select>
                </div>
                <!-- </div> -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="contact-owner-list-button" class="btn btn-primary">Ok</button>
            </div>
        </div>
    </div>
</div>