<?php
/* @var $this yii\web\View */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->params['headerTitle'] = "Reports Folder \"{$model->label}\" Detail View";
$this->params['headerDescription'] = "Use List view to show only documents in a sortable list .";

$this->title = 'Reports';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

$parents = $model->parents()->all();
foreach ($parents as $parentFolder) {
    $this->params['breadcrumbs'][] = [
        'label' => $parentFolder->label,
        'url' => ['folder/view', 'id' => $parentFolder->id_report_tree]
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => $model->label,
    'template' => "<li><b>{link}</b></li>\n"
];


// class for sharable column
switch ($model->sharable) {
    case 0:
        $sharableClass = '<i class="fa fa-square-o"></i>';
        break;
    case 1:
        $sharableClass = '<i class="fa fa-check-square-o"></i>';
        break;
    case 2:
        $sharableClass = '<i class="fa fa-minus-square-o"></i>';
        break;
}

// sort two providers together
//$dataProviderMedia->sort->sortParam = 'media-sort';
//if (!is_null($dataProviderUsers)) $dataProviderUsers->sort->sortParam = 'users-sort';

?>

<div class="col-md-3">
    <div class="hpanel panel-group">
        <div class="panel-body">
            <div class="text-center text-muted font-bold">Select tabs</div>

        </div>


        <div id="notes" class="collapse">
            <div class="panel-body note-link active_contact_side_bar">
                <a href="#default" data-toggle="tab">
                    <small class="pull-right text-muted">Active</small>
                    <h5>Basic Info</h5>

                    <div class="small">
                        <strong><?= $model->label ?></strong>
                    </div>
                    <div class="small">
                        <?= $model->description ?>
                    </div>
                </a>
            </div>


            <div class="panel-body note-link <?php if ($model->type == 'folder') echo 'disabledTab'; ?>">
                <a href="#users" data-toggle="tab">
                    <small class="pull-right text-muted">
                        <?php if ($model->type == 'folder'): ?>
                            Disabled
                        <?php else: ?>
                            Active
                        <?php endif; ?>
                    </small>
                    <h5>Users</h5>

                    <div class="small">
                        <strong>User names</strong>
                    </div>
                    <div class="small">
                        Users that can browse documents
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="col-md-9">
    <div class="hpanel">
        <div class="panel-heading hbuilt">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                <a class="closebox"><i class="fa fa-times"></i></a>
            </div>
            <?= $model->label ?>
        </div>

        <div class="panel-body">
            <div class="tab-content">
                <div id="default" class="tab-pane active">
                    <div class="pull-right text-muted m-l-lg">
                        Active
                    </div>
                    <h3>Basic Info</h3>
                    <hr/>
                    <div class="note-content">
                        <div class="row">
                            <?php
                            echo DetailView::widget([
                                'model' => $model,
                                'attributes' => [
                                    [
                                        'label' => 'Field',
                                        'value' => 'Content',
                                    ],
                                    [
                                        'label' => 'Name',
                                        'attribute' => 'label'
                                    ],
                                    'description',
                                    [
                                        'label' => 'Type',
                                        'value' => ($model->type == 'userFolder') ? 'User Folder' : $model->type,
                                    ],
                                    [
                                        'label' => 'Sharable',
                                        'format' => 'html',
                                        'value' => $sharableClass,
                                    ],
                                    [
                                        'label' => 'Size',
                                        'format' => 'html',
                                        'value' => Yii::$app->formatter->asShortSize($model->size, 2), //($model->size) ? $model->size . ' b' : '-',
                                    ],
                                    'icon'
                                ],
                            ]);
                            ?>


                            <hr>
                            <p class="lead">This Folder contains these Media</p>
                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProviderMedia,
                                'tableOptions' => [
                                    'class' => 'table table-striped'
                                ],
                                'columns' => [
                                    [
                                        'attribute' => 'media-name',
                                        'format' => 'html',
                                        'value' => function ($model) {
                                            return Html::a($model['media-name'], ['document/view', 'id' => $model['id_media']], ['class' => 'list-link']);
                                        },
                                        'label' => 'Name'
                                    ],
                                    [
                                        'attribute' => 'media-type',
                                        'label' => 'Type'
                                    ],
                                ],
                                'layout' => '{items}',
                            ])
                            ?>

                        </div>
                    </div>
                </div>

                <div id="users" class="tab-pane">
                    <div class="pull-right text-muted m-l-lg">
                        Active
                    </div>
                    <h3>Users</h3>
                    <hr/>
                    <div class="note-content">
                        <div class="row">
                            <?php if ($model->type == 'userFolder'): ?>
                            <?=
                            GridView::widget([
                                'dataProvider' => $dataProviderUsers,
                                'tableOptions' => [
                                    'class' => 'table table-striped'
                                ],
                                'columns' => [
                                    [
                                        'attribute' => 'user-username',
                                        'label' => 'User name'
                                    ],
                                    [
                                        'attribute' => 'user-usercode',
                                        'label' => 'User code'
                                    ],
                                ],
                                'layout' => '{items}{pager}',
                            ])
                            ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>

<?php
if (isset($activeTab) && !is_null($activeTab)) Yii::$app->view->registerJs('var tab = "' . $activeTab . '"', \yii\web\View::POS_HEAD); ?>

<?php
$js = <<<JS
function activeTab(tab){
if (tab!=''){
    $('#notes a[href="#' + tab + '"]').tab('show');
    $('#notes a[href="#' + tab + '"]').parent().addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
}
};

$(document).ready(function(){
    $('.note-link').click(function() {
        $(this).addClass('active_contact_side_bar').siblings().removeClass('active_contact_side_bar');
    });
    if (typeof tab != 'undefined') activeTab(tab);
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>