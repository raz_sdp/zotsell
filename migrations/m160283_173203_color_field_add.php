<?php

use yii\db\Migration;

class m160283_173203_color_field_add extends Migration
{
    public function up()
    {
        $this->execute('ALTER TABLE zse_v1_records_classifications ADD color VARCHAR(68) DEFAULT NULL AFTER description;');
    }

    public function down()
    {

    }
}
