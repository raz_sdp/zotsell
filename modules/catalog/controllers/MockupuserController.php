<?php

namespace app\modules\catalog\controllers;

use yii\web\Controller;

class MockupuserController extends Controller
{
	var $mockup='mockupuser';
	

    public function actionIndex()
    {
        return $this->render('index',[
        	'mockup' => $this->mockup,
        ]);
    }
  
    public function actionDetails()
    {
    	     return $this->render('details',[
        	'mockup' => $this->mockup,
        ]);
       
    }
    
    public function actionSelectcustomer()
    {
        return $this->renderAjax('selectcustomer');

    }
    
    
	public function actionBuyitem()
    {
        return $this->renderAjax('buyitem');
    }

	public function actionBuykit()
    {
       
         	     return $this->render('buykit',[
        	'mockup' => $this->mockup,
        ]);
    }
        
    
    public function actionItemusers()
    {
      
         	     return $this->render('itemusers',[
        	'mockup' => $this->mockup,
        ]);
    }
    
    public function actionBulk()
    {
        
         	     return $this->render('bulk',[
        	'mockup' => $this->mockup,
        ]);
    }

    
        public function actionEdit()
    {
        
         	     return $this->render('edit',[
        	'mockup' => $this->mockup,
        ]);
    }

     	public function actionCart()
    {
		$this->layout = 'cart';
      
          return $this->render('cart',[
        	'mockup' => $this->mockup,
        ]);
    }
    
             public function actionFolders()
    {
        
         	     return $this->render('folders',[
        	'mockup' => $this->mockup,
        ]);
    }
    
    
    
            public function actionFlipbook()
    {
       
         	     return $this->render('flipbook',[
        	'mockup' => $this->mockup,
        ]);
    }
     
        
            public function actionSquares()
    {
        
         	     return $this->render('squares',[
        	'mockup' => $this->mockup,
        ]);
    }
     
     
    
}

