<div class="input-group">
    <input class="form-control" name="search" id="search" type="text" placeholder="Search Documents.." value="">
    <div class="input-group-btn">
        <button class="btn btn-default" id="searchButton"><i class="fa fa-search"></i></button>
    </div>
</div>

<?php
$script = <<<EOD
//$('#searchButton').click(function(){
$("#search").on("input",function(e){
//console.log($('#search').val());
    $.ajax({
        url:'search',
        type:'POST',
        data:'search='+$("#search").val(),
        dataType: 'html',
        success:function(data){
            $('#body-result').html(data);
        }
    });
});

EOD;
$this->registerJs($script, \yii\web\View::POS_READY);