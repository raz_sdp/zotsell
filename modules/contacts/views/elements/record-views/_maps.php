<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:25 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3>Maps </h3>
<hr/>
<div class="note-content">
    <?php
    $coordinates_string = $coordinates;
    $coordinates = explode(',', $coordinates);
    ?>

    <script>
        var myLatLng;
        <?php if(!empty($coordinates[0]) && !empty($coordinates[1])) { ?>
        myLatLng = {lat: <?= $coordinates[0]?>, lng: <?= $coordinates[1]?>};
        <?php } else { ?>
        myLatLng = {};
        <?php } ?>
    </script>

    <div class="row">
        <div class="col-lg-12">
            <section id="map">
                <div id="map-area" style="height: 200px"></div>
            </section>
        </div>
        <script src="https://maps.googleapis.com/maps/api/js"
                async defer></script>
    </div>
</div>
