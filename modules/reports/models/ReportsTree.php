<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_reports_tree".
 *
 * @property integer $id_report_tree
 * @property integer $root
 * @property integer $nleft
 * @property integer $nright
 * @property integer $level
 * @property string $icon
 * @property string $label
 * @property string $description
 * @property string $pattern
 * @property string $frequency
 * @property integer $limit
 */
class ReportsTree extends \kartik\tree\models\Tree
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_reports_tree';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['root', 'level', 'pattern'], 'required'],
            [['root', 'nleft', 'nright', 'level', 'limit'], 'integer'],
            ['limit', 'default', 'value' => 0],
            ['frequency', 'in', 'range' => ['', 'aaaammgg', 'aaaa', 'aaaamm', 'mm', 'mmgg', 'gg', 'yyyymmdd', 'yyyy', 'yyyymm', 'mm', 'mmdd', 'dd']],
            [['description'], 'string'],
            [['icon', 'label'], 'string', 'max' => 255],
            [['pattern', 'frequency'], 'string', 'max' => 32],
            [['label', 'description', 'pattern'], 'required', 'on' => ['create']],
            ['icon', 'compare', 'compareValue' => 'custom', 'operator' => '!=',
                'when' => function ($model) {
                    if ($model->icon == 'custom')
                        return true;
                },
                'whenClient' => "function (attribute, value) {
                    if($('input[name=\"ReportsTree[icon]\"]:checked').val()=='custom'){
                        return true;
                    }
                    }",
                'on' => ['create'],
                'message' => 'When set Custom, you have to upload icon'], //compare validator
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['label', 'description', 'icon', 'pattern', 'frequency', 'limit'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_report_tree' => 'Id Report Tree',
            'root' => 'Root',
            'nleft' => 'Nleft',
            'nright' => 'Nright',
            'level' => 'Level',
            'icon' => 'Icon',
            'label' => 'Label',
            'description' => 'Description',
            'pattern' => 'Pattern',
            'frequency' => 'Frequency',
            'limit' => 'Limit',
        ];
    }

    public function getMediaLinks()
    {
        return $this->hasMany(MediaLinks::className(), ['id_container' => 'id_report_tree'])->andOnCondition(['zse_v1_media_links.context' => 4]);
    }

    public function getMedia()
    {
        return $this->hasMany(Media::className(), ['id_media' => 'id_media'])
        ->via('mediaLinks');
    }

    public function getRepositoryFiles()
    {
        return $this->hasMany(RepositoryFiles::className(), ['name' => 'file'])
        ->via('media');
    }

    public function getReportsTreeAccess()
    {
        return $this->hasMany(ReportsTreeAccess::className(), ['id_report_tree' => 'id_report_tree']);
    }

    public function getAccounts()
    {
        return $this->hasMany(Accounts::className(), ['id_account' => 'id_account'])
        ->via('reportsTreeAccess');
    }

    /**
     * Get size of folder (include files inside subfolders)
     * @return type
     */
    public function getSize()
    {
        $size = 0;
        if ($this->media) {
            foreach ($this->media as $media) {
                $size += $media->size;
            }
        }

        foreach ($this->children()->all() as $subfolder) { //sum size of subfolders
            if ($subfolder->media) {
                foreach ($subfolder->media as $mediaSub) {
                    $size += $mediaSub->size;
                }
            }
        }

        return $size;
    }

    /**
     * Check files in folder for sharable
     * @return 0 (if all files aren't sharable) | 1 (if all files in folder are sharable) | 2 (some sharable, some not)
     */
    public function getSharable()
    {
        $medias = $this->getMedia()->all();
        $countCansend = 0;
        $countNotCansend = 0;
        foreach ($medias as $media) {
            if ($media->cansend == 1)
                $countCansend += 1;
            else
                $countNotCansend += 1;
        }

        switch (true) {
            case $countCansend > 0 && $countNotCansend == 0;
                return 1;
                break;

            case $countCansend > 0 && $countNotCansend > 0;
                return 2;
                break;

            default:
                return 0;
                break;
        }
    }

    /**
     * Get type of folder
     * @return string
     */
    public function getType()
    {
        return ($this->isRoot()) ? 'userFolder' : 'folder';
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->type == 'userFolder') {
                foreach ($this->reportsTreeAccess as $userAccess) {
                    $userAccess->delete();
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->level == 0)
                    $this->level = 1; // default level for root folders
            } else { // update
            }
            return true;
        } else
            return false;
    }

}
