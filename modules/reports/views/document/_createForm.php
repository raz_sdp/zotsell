<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<?php $form = ActiveForm::begin(['id' => 'create-media', 'options' => ['class' => 'create-update-media', 'role' => 'form']]); ?>
<?= $form->field($media, 'lang')->hiddenInput(['value' => 'default'])->label(false); //$mediaTr    ?>
    <h3>Add meta</h3>

    <div class="form-group">
        <?= $form->field($media, 'name')->textInput(['placeholder' => 'Enter your name'])->label('File name') ?>
    </div>
    <div class="form-group">
        <?= $form->field($media, 'description')->textInput(['placeholder' => 'Enter Short Description'])->label('Short Description') ?>
    </div>

<?= $form->field($media, 'type')->hiddenInput(['value' => $media->type])->label(false); ?>

<?php $checkboxTemplate = "<label class=\"control-label\">Status:</label><div class=\"input-group\"><div class=\"checkbox checkbox-primary\">\n{input}\n{beginLabel}\n{labelTitle}\n{endLabel}\n{error}\n{hint}\n</div></div>";
?>
<?= $form->field($media, 'cansend')->checkbox(['template' => $checkboxTemplate])->label('Sharable') ?>

<?php if ($media->type == 'gallery'): ?>
    <?= $form->field($media, 'multimedia')->textArea(['id' => 'choosen-gallery', 'readonly' => true, 'class' => 'form-control choosen-list', 'style' => 'min-height:120px;'])->label('Choosen Files:') ?>
<?php else: ?>
    <?= $form->field($media, 'file')->textInput(['id' => 'choosen-file', 'readonly' => true, 'class' => 'form-control choosen-list'])->label('Choosen File:'); ?>
<?php endif; ?>

<?php
$iconCurrent = '';
if ($media->icon != '') {
    $iconCurrent = $media->icon; // for load current custom image value on radio value
    $media->icon = 'custom';
}
?>
<?php
echo $form->field($media, 'icon', [
    'inline' => true,
    //'enableLabel' => false
])->radioList([
    '' => 'Standard Icon',
    'custom' => 'Custom image'
], [
    'id' => 'icon_block',
    'class' => 'form-group',
    //'data-toggle' => 'buttons',
    //'unselect' => null, // remove hidden field
    'item' => function ($index, $label, $name, $checked, $value) use ($iconCurrent) {
        if ($value == 'custom') {
            return "<label><a type=\"button\" data-toggle=\"collapse\" data-target=\"#collapseExample\" aria-expanded=\"false\"
                           aria-controls=\"collapseExample\">" . Html::radio($name, $checked, ['id' => "radio_custom", 'value' => ($iconCurrent != '') ? $iconCurrent : $value, 'class' => 'project-status-btn']) . $label . "</a></label>";
        } else {
            return "<label>" . Html::radio($name, $checked, ['id' => "radio_standard", 'value' => $value, 'class' => 'project-status-btn']) . $label . "</label>";
        }
    },
]);
?>



    <div class="collapse out" id="collapseExample">
        <div class="upload-drop-zone" id="drop-zone-icon">
            <?php
            echo \kato\DropZone::widget([
                'id' => 'iconUploader',
                'dropzoneContainer' => 'iconUploader',
                'previewsContainer' => 'previewsIcon',
                'options' => [
                    'dictDefaultMessage' => 'Drag and drop 100 x 100 icon here',
                    'url' => 'upload?id_folder=' . $idContainer,
                    'maxFilesize' => '2',
                    'maxFiles' => '1',
                    'acceptedFiles' => 'image/*',
                ],
                'clientEvents' => [
                    'complete' => "function(file){console.log(file); eventAfterIconUpload(file);}",
                    'removedfile' => "function(file){alert(file.name + ' is removed')}"
                ],
            ]);
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>