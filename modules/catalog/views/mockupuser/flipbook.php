


<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\boostrap\Modal;
    $this->title = 'Catalog Flipbook';
    $this->params['breadcrumbs'][] = $this->title;

	//$this->registerJs("\$('#magazine').turn({gradients: true, acceleration: true});");
?>


<!-- 
  <h1>Catalog Flipbook</h1>
  <p class="lead">Manage all your  %number% Articles</p>
  
  




 <a  href="default/edit" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Add New Item </a> 
 <a  href="default/newfolder" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Add New folder </a> 
    <a  href="default/bulk" class="btn btn-success" type="button"  ><i class="fa fa-arrows"></i> Bulk Move </a>  
  <a  href="default/itemusers" class="btn btn-success" type="button"  ><i class="fa fa-link"></i> Assign items </a>  
 -->

  
  <br>

<div class="row">

<div class="nav navbar-nav navbar-left col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-filter fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Orientation</a></li>
            <li><a href="#">Classification</a></li>
            <li><a href="#">Status</a></li>
            <li><a href="#">Kind</a></li>
             <li><a href="#">Favorites</a></li>
              <li><a href="#">Type</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">remove filters</a></li>
          </ul>
        </li>
      </div>


<form class="col-md-10" role="search">
  <div class="form-group">
    <input type="text" class="form-control" placeholder=" Search">
  </div>
</form>



<div class="nav navbar-nav navbar-right col-md-1">
  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-eye fa-2x"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">
        <li><a href="/zot/web/catalog/<?=  $mockup  ?>">  List</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/folders">  Folders</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/flow">Flow</a></li>
            <li><a href="/zot/web/catalog/<?=  $mockup  ?>/squares">Squares</a></li>
             <li><a href="/zot/web/catalog/<?=  $mockup  ?>/flipbook"><i class="fa fa-check"></i> Flipbook</a></li>
              <li><a href="/zot/web/catalog/<?=  $mockup  ?>/frequent">Frequents</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="pdf">Download pdf</a></li>
            <li><a href="csv">Download csv</a></li>
        </li>
      </div>

    
   </div>    




<hr>

implement using

http://www.turnjs.com
<hr>



<div class="flipbook-viewport">
	<div class="container">
		<div class="flipbook">
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
			<div style="background-image:url(https://s-media-cache-ak0.pinimg.com/736x/8a/53/b4/8a53b43a0aafc65de220301aee7a97e0.jpg)"></div>
		</div>
	</div>
</div>


<script type="text/javascript">

function loadApp() {

	// Create the flipbook

	$('.flipbook').turn({
			// Width

			width:922,
			
			// Height

			height:600,

			// Elevation

			elevation: 50,
			
			// Enable gradients

			gradients: true,
			
			// Auto center this flipbook

			autoCenter: true

	});
}



</script>





  </div>   





