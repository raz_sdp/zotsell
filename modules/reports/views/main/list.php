<?php

use yii\helpers\Html;
use app\components\DataTablesEmptyBody;
//use fedemotta\datatables\DataTables;


$this->title = 'List views';

// module root
$this->params['breadcrumbs'][] = [
    'label' => "Reports",
    'url' => ['']
];


if (isset($folder['id_report_tree']) && $folder['id_report_tree'] > 0) {
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/list']];

    $parents = $folder->parents()->all();
    foreach ($parents as $parentFolder) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentFolder->label,
            'url' => ['main/list', 'id_folder' => $parentFolder->id_report_tree]
        ];
    }

    $this->params['breadcrumbs'][] = [
        'label' => $folder['label'],
        'template' => "<li><b>{link}</b></li>\n"
    ];
} else {
    $this->params['breadcrumbs'][] = $this->title;
}

$this->params['headerTitle'] = "Reports List View";
$this->params['headerDescription'] = "Use List view to show only Reports in a sortable list.";

$this->params['newEntities'] = $newEntities; // for dropdown list on click button NEW

//$searchModel = new app\modules\documents\models\MediaFolderSearch();//for testing
//$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>

<div class="col-md-3">
    <div class="hpanel">
        <?= $this->render(
            '_menu'
        ) ?>
    </div>
    <div class="hpanel">
        <div class="panel-body">
            <div class="m-b-md">
                <h4>
                    Filters
                </h4>
                <small>
                    Filter your data based on different options below.
                </small>
            </div>

            <?= Html::beginForm(['main/list'], 'GET', ['id' => 'filter-form']); ?>
            <div class="form-group">
                <label class="control-label">Type:</label>

                <div class="input-group">
                    <?= Html::dropDownList('type', (Yii::$app->getRequest()->getQueryParam('type')) ? Yii::$app->getRequest()->getQueryParam('type') : '---',
                        [
                            'all' => 'All',
                            'files' => 'Files',
                            'folders' => 'Folders',
                            'userFolders' => 'User Folders',
                            '---' => '---',
                            'pdf' => 'Pdf',
                            'movie' => 'Movie',
                            'html5' => 'Html5',
                            'gallery' => 'Gallery',
                            'powerpoint' => 'Powerpoint',
                            'excel' => 'Excel',
                            'word' => 'Word',
                        ],
                        [
                            'class' => 'form-control m-b',
                        ]); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label">Status:</label>

                <div class="input-group">
                    <div class="checkbox checkbox-primary">
                        <?= Html::checkbox(
                            'sharable',
                            (Yii::$app->getRequest()->getQueryParam('sharable')) ? true : false,
                            ['id' => 'sharable']) ?>
                        <label for="sharable">
                            Sharable
                        </label>
                    </div>
                </div>
            </div>

            <?= Html::submitButton('Apply', ['class' => 'btn btn-success btn-block']); ?>

            <?= Html::endForm(); ?>

        </div>
    </div>

</div>

<div class="col-md-9">
    <div class="hpanel">
        <div class="panel-heading">
            <div class="panel-tools">
                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                <a class="closebox"><i class="fa fa-times"></i></a>
            </div>
            Standard table
        </div>

        <div class="panel-body">
            <?php
            /*
            $searchModel = new ModelSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            */
            ?>
            <?= DataTablesEmptyBody::widget([
                'dataProvider' => $dataProvider,
                'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                'tableOptions' => [
                    'class' => 'table table-striped table-bordered table-hover dataTable no-footer',
                ],
                'clientOptions' => [
                    'dom' => 'lftrip',
                    'searching' => true,
                    'paging' => true,
                    'aaSorting' => [],
                    'language' => [
                        "search" => "Search:",
                        "emptyTable" => "No matching records found",
                    ],
                    /*'columns' => [
                        null,
                        null,
                        null,
                        null,
                        [
                           'orderable'=>false //https://datatables.net/reference/option/columns.orderable
                        ]],
                    */
                    'columnDefs' => [
                        [
                            "type" => "num-html", "targets" => 0
                        ],
                        [
                            "type" => "num-html", "targets" => 3
                        ],
                        [
                            "type" => "file-size",
                            //"orderable"=> false,
                            "targets" => 4
                        ],
                        [
                            "orderable" => false,
                            "targets" => -1
                        ]
                    ]
                ],
                //'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'icon',
                        'format' => 'html',
                        'value' => function ($model) {
                            return $model['icon'];
                            //return Html::img(Yii::getAlias('@web').'/images/'. $data['image'], ['width' => '70px']); //for images instead icons
                        }
                    ],

                    [

                        //'header' => '<span class="glyphicon glyphicon-user"></span>',
                        'attribute' => 'name',
                        'format' => 'html',
                        'value' => function ($model) {
                            if (in_array($model['type'], ['folder','userFolder'])) return Html::a($model['name'], ['list', 'id_folder' => $model['id']], ['class' => 'list-link']); 
                            else return $model['name'];
                        }
                    ],
                    [
                        'attribute' => 'type',
                        'value' => function ($model) {
                            if ($model['type'] == 'userFolder') return 'User Folder'; 
                            else return $model['type'];
                        } 
                    ],
                    [
                        'attribute' => 'sharable',
                        'format' => 'html',
                        'value' => function ($model) {
                            switch ($model['sharable']) {
                                case 0:
                                    return '<i class="fa fa-square-o"></i>';
                                    break;
                                case 1:
                                    return '<i class="fa fa-check-square-o"></i>';
                                    break;
                                case 2:
                                    return '<i class="fa fa-minus-square-o"></i>';
                                    break;
                            }
                        }
                    ],
                    [
                        'attribute' => 'size',
                        'value' => function ($model) {
                            return  Yii::$app->formatter->asShortSize($model['size'], 2);//($model['size']) ?  $model['size']. ' b' : '0 b'; 
                        }
                    ],
                    'pattern',
                    [
                        'header' => 'Actions',
                        'headerOptions' => ['class' => ''],
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{rootFolder} {download} {view} {update} {delete}',
                        'buttons' => [
                            'rootFolder' => function ($url, $model) {
                                return ($model['type'] == 'userFolder') ? Html::a('<span class="fa fa-user"></span>', ['folder/update', 'id' => $model['id'], 'tab' => 'users']) : '';  
                            },
                            'view' => function ($url, $model) {
                                if (in_array($model['type'],['folder','userFolder'])) $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-eye"></span>',
                                    [$uri . '/view', 'id' => $model['id']]
                                );
                            },
                            'update' => function ($url, $model) {
                                if (in_array($model['type'],['folder','userFolder'])) $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-pencil"></span>',
                                    [$uri . '/update', 'id' => $model['id']]
                                );
                            },
                            'download' => function ($url, $model) {
                                if (in_array($model['type'],['folder','userFolder'])) return Html::tag('span', '', ['class' => "fa fa-download"]);
                                else {
                                    if (isset($model['repFile'])) {
                                        return Html::a(
                                            '<span class="fa fa-download"></span>',
                                            ['/uploads/' . $model['repFile']]
                                        );
                                    }
                                    return Html::tag('span', '', ['class' => "fa fa-download"]);
                                }

                            },
                            'delete' => function ($url, $model) {
                                if (in_array($model['type'],['folder','userFolder'])) $uri = 'folder';
                                else $uri = 'document';
                                return Html::a(
                                    '<span class="fa fa-trash"></span>',
                                    [$uri . '/delete', 'id' => $model['id']]
                                );
                            },
                        ],
                    ],
                ],

            ]); ?>
        </div>


    </div>


</div>
