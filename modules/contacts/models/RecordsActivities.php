<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_activities".
 *
 * @property integer $id_activity
 * @property string $agent_code
 * @property string $record_code
 * @property string $record_destination_code
 * @property string $associated_with
 * @property string $type
 * @property string $status
 * @property string $date_insertion
 * @property string $date_creation
 * @property string $date_activity
 * @property string $inserted_by
 * @property string $ipad_udid
 * @property string $management
 * @property string $subject
 * @property string $description
 * @property string $data
 * @property string $kind
 * @property string $delegated_to
 * @property string $transfer_status
 * @property string $transfer_message
 * @property string $entity_id
 * @property string $user
 * @property string $date_modification
 * @property integer $record_status
 * @property string $parent_id
 */
class RecordsActivities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_insertion', 'date_creation', 'date_activity', 'date_modification'], 'safe'],
            [['description', 'data'], 'string'],
            [['record_status'], 'integer'],
            [['agent_code', 'associated_with', 'ipad_udid', 'management', 'subject', 'delegated_to', 'transfer_message', 'user'], 'string', 'max' => 255],
            [['record_code', 'kind', 'transfer_status'], 'string', 'max' => 32],
            [['record_destination_code', 'type', 'status', 'inserted_by', 'entity_id', 'parent_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_activity' => 'Id Activity',
            'agent_code' => 'Agent Code',
            'record_code' => 'Record Code',
            'record_destination_code' => 'Record Destination Code',
            'associated_with' => 'Associated With',
            'type' => 'Type',
            'status' => 'Status',
            'date_insertion' => 'Date Insertion',
            'date_creation' => 'Date Creation',
            'date_activity' => 'Date Activity',
            'inserted_by' => 'Inserted By',
            'ipad_udid' => 'Ipad Udid',
            'management' => 'Management',
            'subject' => 'Subject',
            'description' => 'Description',
            'data' => 'Data',
            'kind' => 'Kind',
            'delegated_to' => 'Delegated To',
            'transfer_status' => 'Transfer Status',
            'transfer_message' => 'Transfer Message',
            'entity_id' => 'Entity ID',
            'user' => 'User',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * @return array
     * search all Activities by subject and description field
     */
    public function searchActivitiesByKeyword($keyword) {
        return RecordsActivities::find()
                ->select(['DATE(date_activity) as date_activity', 'type', 'record_code', 'subject', 'description', 'status'])
                ->Where(['LIKE', 'zse_v1_activities.subject', $keyword])
                ->orWhere(['LIKE', 'zse_v1_activities.description', $keyword])
                ->all(); 
    }

    /**
     * function for getting activities of a contact
     * @param $field_list
     * @param $where
     * @param array $filter_where
     * @param null $limit
     * @param null $offset
     * @param string $sort
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getContactActivities($field_list, $where, $filter_where = [], $limit= null, $offset = null, $sort = ""){
        $activities = RecordsActivities::find()
            ->select($field_list)
            ->where($where)
            ->andFilterWhere($filter_where)
            ->limit($limit)
            ->offset($offset)
            ->orderBy($sort)
            ->all();
        return $activities;
    }

    /**
     * function for getting count of activities of a contact
     * @param $where
     * @param array $filter_where
     * @return int|string
     */
    public static function countContactActivities($where, $filter_where = []){
        $count = RecordsActivities::find()
            ->where($where)
            ->andFilterWhere($filter_where)
            ->count();

        return $count;
    }

    public function getActivitiesData($code) {
        return RecordsActivities::find()
                ->select(['DATE(date_activity) as date_activity', 'type', 'record_code', 'subject', 'description', 'status'])
                ->Where(['record_code' => $code])
                ->all();

    }
}
