<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_accounts".
 *
 * @property string $id_account
 * @property string $username
 * @property string $working_code
 * @property integer $role
 *
 * @property ZseV1AccountsDetails $zseV1AccountsDetails
 * @property ZseV1AccountsDevices[] $zseV1AccountsDevices
 * @property ZseV1AccountsServicesLicenses[] $zseV1AccountsServicesLicenses
 * @property ZseV1ApnsAdmin[] $zseV1ApnsAdmins
 * @property ZseV1News[] $zseV1News
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role'], 'required'],
            [['role'], 'integer'],
            [['username'], 'string', 'max' => 100],
            [['working_code'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_account' => 'Id Account',
            'username' => 'Username',
            'working_code' => 'Working Code',
            'role' => 'Role',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountsServicesLicenses()
    {
        return $this->hasOne(AccountsServicesLicenses::className(), ['id_account' => 'id_account']);
    }
    
    /**
     * Get user code
     * @return string
     */
    public function getUsercode()
    {
        if (isset($this->accountsServicesLicenses)) { //calculate size for this media
            return $this->accountsServicesLicenses->usercode;
        } 
        return '';
    }

}
