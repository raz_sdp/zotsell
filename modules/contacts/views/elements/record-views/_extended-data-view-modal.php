<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:29 PM
 */
?>
<div class="modal fade" id="extended-data-view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="z-index: 999999">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Extended Data</h4>
            </div>
            <div class="modal-body row col-md-12" style="clear: both; margin: auto">
                <!-- <div class="form-group pull-left"> -->
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Label</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        ?>
                        </tbody>
                    </table>
                </div>
                <!-- </div> -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>