<?php

namespace app\modules\documents\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "zse_v1_media".
 * Model search for right grid on bulk (medias are in current folder)
 *
 * @property integer $id_media
 * @property string $type
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property integer $cansend
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class MediaSearchCurrent extends \app\modules\documents\models\Media
{

    public $name;
    public $type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['multimedia'], 'string'],
            [['cansend', 'record_status'], 'integer'],
        //    [['entity_id'], 'required'],
            [['date_modification'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['icon', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name', 'type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'type' => 'Type',
            'icon' => 'Icon',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'cansend' => 'Cansend',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    /**
     * search model in current folder
     * @param $params
     * @param $idFolder
     * @return ActiveDataProvider
     */
    public function search($params,$idFolder)
    {
        $query = Media::find()
            ->joinWith('mediaLinks')
        ->where(['zse_v1_media_links.id_container'=> $idFolder]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>false,
        ]);
        

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'type'
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andWhere('name LIKE "%' . $this->name . '%" AND type LIKE "%'.$this->type.'%"');

        return $dataProvider;
    }
}
