<?php

namespace app\modules\documents\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media".
 *
 * @property integer $id_media
 * @property string $type
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property integer $cansend
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class Media extends \yii\db\ActiveRecord
{

    public $size;
    public $idContainer; // container_id for generate entity_id
    public $lang; // value 'default'

    /**
     * Relation array by field 'zse_v1_media_tr'.'lang'
     * @var type
     */
    public static $translationLanguages = [
        'en-us' => [
            'language' => 'English',
            'country' => 'USA'
        ],
        'fr' => [
            'language' => 'French',
            'country' => 'FRANCE'
        ],
        'it' => [
            'language' => 'Italian',
            'country' => 'ITALY'
        ],
        'en' => [
            'language' => 'English',
            'country' => 'UK'
        ],
        'es' => [
            'language' => 'Spanish',
            'country' => 'SPAIN'
        ],
        'de' => [
            'language' => 'German',
            'country' => 'GERMANY'
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['multimedia'], 'string'],
            [['cansend', 'record_status'], 'integer'],
            [['entity_id'], 'required'],
            [['date_modification'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['icon', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name', 'description', 'file'], 'required', 'on' => ['create']],
            [['name', 'description', 'multimedia'], 'required', 'on' => ['create-gallery']],
            ['icon', 'compare', 'compareValue' => 'custom', 'operator' => '!=',
                'when' => function ($model) {
                    if ($model->icon == 'custom')
                        return true;
                },
                'whenClient' => "function (attribute, value) {
                    if($('input[name=\"Media[icon]\"]:checked').val()=='custom'){
                        return true;
                    }
                    }",
                'on' => ['create', 'create-gallery'],
                'message' => 'When set Custom, you have to upload icon'] //compare validator
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['name', 'description', 'cansend', 'icon', 'file', 'type'];
        $scenarios['create-gallery'] = ['name', 'description', 'cansend', 'icon', 'multimedia', 'type'];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'type' => 'Type',
            'icon' => 'Icon',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'cansend' => 'Cansend',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    public function getMediaLinks()
    {
        return $this->hasMany(MediaLinks::className(), ['id_media' => 'id_media'])->where(['zse_v1_media_links.context' => 1]);
    }

    public function getMediaTr()
    {
        return $this->hasMany(MediaTr::className(), ['id_media' => 'id_media']);
    }

    public function getFolder()
    {
        return $this->hasOne(DocsTree::className(), ['id_folder' => 'id_container'])
            ->via('mediaLinks');
    }

    //deprecated
    public function getFile()
    {
        return $this->hasOne(RepositoryFiles::className(), ['name' => 'file']);
    }

    //new
    public function getRepositoryFile()
    {
        return $this->hasOne(RepositoryFiles::className(), ['name' => 'file']);
    }

    public function getFileSize()
    {
        return $this->getFile()->select('size')->scalar();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->loadDefaultValues(); // load default attributes from db table
                if ($this->type == 'file') { //if type file, try to find type for media by 'repository_files'.'mimetype'
                    if ($this->repositoryFile) {
                        $detectedType = SearchRepositoryFiles::getTypeByMimeType($this->repositoryFile->mimetype);
                        if ($detectedType) $this->type = $detectedType;
                    }
                }
                $this->entity_id = ($this->idContainer) ? sha1(mt_rand(10000, 99999) . time() . $this->idContainer) : 0; // set idContainer
            } else { // update

            }
            return true;
        } else
            return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->mediaTr as $mediaTr) {
                $mediaTr->delete(); // delete related media translation models
            }
            foreach ($this->mediaLinks as $link) {
                $link->delete(); // delete related media translation models
            }
            return true;
        } else {
            return false;
        }
    }

    public function setIdContainer($idContainer)
    {
        $this->idContainer = $idContainer;
    }

    public function getIdContainer()
    {
        return $this->idContainer;
    }

    /**
     * Get languages relations array
     * @return type
     */
    public static function getLanguages()
    {
        return self::$translationLanguages;
    }

}
