<?php
/**
 * Created by PhpStorm.
 * User: Raaz
 * Date: 2/27/16
 * Time: 12:30 PM
 */
?>
<div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <h4 class="modal-title">Define Extended data</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">

                <p> <h3>Define Extended data</h3> </p>
                <form role="form" id="extended_data_formaaaaaaaaaa" method="post">
                    <div class="form-group">
                        <label for="extended_field_id">Data Unique ID</label>
                        <input type="text" id="extended_field_id" name="extended_field_id" placeholder="Enter Data Unique ID" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="extended_data_label"> Data Label </label>
                        <input type="text" id="extended_data_label" name="extended_data_label" placeholder="Enter Data Label" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Data Type :</label>
                        <div class="input-group">
                            <div class="radio radio-primary">
                                <input checked id="radio1" name="extended_data_type" value="s" onclick="change_field();" type="radio">
                                <label for="radio1">
                                    S (Simple Text)
                                </label>
                            </div>
                            <div class="radio radio-primary">
                                <input id="radio2" name="extended_data_type" value="e" onclick="change_field();" type="radio">
                                <label for="radio2">
                                    E (Extended you can add \n as return )
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="extended_data_value"> Value </label>
                        <textarea id="value_field_e" required name="extended_data_value_e" class="form-control">Enter Value Here (You can use \n as return.)</textarea>
                        <input type="text" required id="value_field_s" name="extended_data_value_s" placeholder="Enter Simple Text Value" class="form-control">
                    </div>

                    <script>
                        var radios = document.getElementsByName('extended_data_type');
                        var value_field_id;
                        for (var i = 0, length = radios.length; i < length; i++) {
                            if (!radios[i].checked) {
                                value_field_id = "value_field_" + radios[i].value;
                                document.getElementById(value_field_id).style.display = "none";
                            } else {
                                value_field_id = "value_field_" + radios[i].value;
                                document.getElementById(value_field_id).style.display = "";
                            }
                        }

                        function change_field () {
                            for (var i = 0, length = radios.length; i < length; i++) {
                                if (!radios[i].checked) {
                                    value_field_id = "value_field_" + radios[i].value;
                                    document.getElementById(value_field_id).style.display = "none";
                                } else {
                                    value_field_id = "value_field_" + radios[i].value;
                                    document.getElementById(value_field_id).style.display = "";
                                }
                            }
                        }
                    </script>

                    <div>
                        <button class="btn btn-block btn-success m-t-n-xs" type="submit"><strong>Add Extended data</strong></button>
                    </div>
                </form>

                <!--<div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Label</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <i class="fa fa-trash"></i>
                                <span class="text-danger font-bold">partitario</span>
                            </td>
                            <td>Partitario</td>
                            <td>E</td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-trash"></i>
                                <span class="text-danger font-bold">distinta-base</span>
                            </td>
                            <td>Distinta Base</td>
                            <td>S</td>
                        </tr>
                        </tbody>
                    </table>
                </div>-->


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>