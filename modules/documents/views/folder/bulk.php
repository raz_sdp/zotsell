<?php
/* @var $this yii\web\View */
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Documents';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['main/index']];

if($model->father->id_folder != $model->id_folder) { // folder has parent folder
    $this->params['breadcrumbs'][] = [
        'label' => $model->father->label, 
        'url' => ['main/index', 'id_folder'=>$model->father->id_folder]
    ];
}
$this->params['breadcrumbs'][] = ['label' => $model->label];

$dataProviderMediaAll->sort->sortParam = 'media-all-sort';

$dataProviderMediaCur->sort->sortParam = 'media-cur-sort';
?>
<h1>Documents</h1>
<p class="lead">Manage your Docs and Folders</p>

<hr>
<div class="row">
<div class="col-md-6">
    <?= GridView::widget([
        'dataProvider' => $dataProviderMediaAll,
        'filterModel' => $searchModelNotCurrent,
        'tableOptions' => [
            'class' => 'table table-striped  table-condensed'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    $key=$model['id_media'];
                    Yii::trace('checkbox key'.print_r($key,true));
                    return [
                        'onclick' =>"console.log('clicked:'+this.checked);",
                        'value'=> $model['id_media']
                    ];//['data-uid' => $model->user_id, 'data-jid'=>$model->job_id];
                }

            ],
            'name',
            'type'
        ],

        //'summary' => 'Displaying {begin}-{end} of {totalCount} results', //Displaying 1-12 of 24 results
        'layout' => '{items}',
    ])
    ?>
    <div class="pull-right">
        <?=yii\bootstrap\Button::widget([
            'label' => 'Add selected',
            'options' => [
                'id' => 'addButton',
                'class' => 'bulk-actions-btn btn btn-success btn-small',
                'disabled' => false
            ],
        ]);?>
    </div>
</div>

<div class="col-md-6">
    <?= GridView::widget([
        'dataProvider' => $dataProviderMediaCur,
        'filterModel' => $searchModelCurrent,
        'tableOptions' => [
            'class' => 'table table-striped  table-condensed'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function ($model, $key, $index, $column) {
                    return [
                        'onclick' =>"console.log('clicked:'+this.checked);",
                    ];//['data-uid' => $model->user_id, 'data-jid'=>$model->job_id];
                }
            ],
            'name',
            'type'
        ],

        //'summary' => 'Displaying {begin}-{end} of {totalCount} results', //Displaying 1-12 of 24 results
        'layout' => '{items}',
    ])
    ?>
    <!--<input type="button" class="btn btn-info" value="Multiple Delete" id="MyButton" >-->
    <div class="pull-right">
    <?=yii\bootstrap\Button::widget([
        'label' => 'Remove selected',
        'options' => [
            'id' => 'removeButton',
            'class' => 'bulk-actions-btn btn btn-danger btn-small',
            'disabled' => false
        ],
    ]);?>
    </div>
</div>

<?php

$this->registerJs('

    $(document).ready(function(){
    $(\'#addButton\').click(function(){

        var keys = $(\'#w0\').yiiGridView(\'getSelectedRows\');
        var mediaChecked = [];
        keys.forEach(function(entry) {
            var dataKey = \'[data-key=\'+entry+\']\';
            mediaChecked.push($(dataKey).find("input[type=checkbox]").val());
        });
        //console.log(mediaChecked);

          $.ajax({
            type: \'POST\',
            url : \'multiple-add\',
            data : {row_id: mediaChecked,folder_id:'.$model->id_folder.'},
            success : function() {
              $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
            }
        });

    });


    $(\'#removeButton\').click(function(){

        var HotId = $(\'#w1\').yiiGridView(\'getSelectedRows\');
          $.ajax({
            type: \'POST\',
            url : \'multiple-delete\',
            data : {row_id: HotId,folder_id:'.$model->id_folder.'},
            success : function() {
              $(this).closest(\'tr\').remove(); //or whatever html you use for displaying rows
            }
        });

    });
    });', \yii\web\View::POS_READY);

?>



