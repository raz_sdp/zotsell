

<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\boostrap\Modal;
    $this->title = 'item';
    $this->params['breadcrumbs'][] = $this->title;
?>





<div class="row">

<div class="nav navbar-nav navbar-left col-md-1">
  
<i class="fa fa-arrow-left "></i>
      </div>


<form class="col-md-10" role="search">

</form>



<div class="nav navbar-nav navbar-right col-md-1">
  
     <i class="fa fa-arrow-right "></i>

    
   </div>    


   </div>   

  

<p >

<table class="table table-condensed">


<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 3 (kit purchase) </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 		
<br>
	<?= Html::button('400 &euro;',  ['value' => Url::to('buyitem'),'class' => 'btn btn-default modalButton']) ?>
</td> 	
    
</tr> 

</table>



<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#image" aria-controls="home" role="tab" data-toggle="tab">Image</a></li>
    <li role="presentation" ><a href="#features" aria-controls="home" role="tab" data-toggle="tab">Features</a></li>
    <li role="presentation"><a href="#info" aria-controls="home" role="tab" data-toggle="tab">Sales info</a></li>
    <li role="presentation"><a href="#connections" aria-controls="profile" role="tab" data-toggle="tab">Related items</a></li>
    <li role="presentation"><a href="#documents" aria-controls="messages" role="tab" data-toggle="tab">Docs</a></li>
    <li role="presentation"><a href="#map" aria-controls="settings" role="tab" data-toggle="tab">Map</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
  
  <div role="tabpanel" class="tab-pane active" id="image">
  
  <img class=" " src=" https://upload.wikimedia.org/wikipedia/commons/thumb/2/23/Shoe-PlainDerby-Black.jpg/1920px-Shoe-PlainDerby-Black.jpg" style="width: 100%; height: 100%;">
  
  
  </div>
  
  <div role="tabpanel" class="tab-pane " id="features">



 NULL<br>
 
  <h1> %html5 object% </h1> 
  
  
   NULL<br>




   
  </div>
  
  
    <div role="tabpanel" class="tab-pane " id="info">
    
    
   
    
   
   
   <table class="detail-view table table-striped table-condensed" id="yw6"><tbody>

<tr class="even"><th>Codice articolo</th><td>5454</td></tr>
<tr class="odd"><th>Gruppi articolo</th><td>null</td></tr>
<tr class="even"><th>Confezione minima</th><td> </td></tr>
<tr class="odd"><th>Disponibilità</th><td> </td></tr>
<tr class="even"><th>Disponibilità programmata</th><td> </td></tr>
<tr class="odd"><th>Data disponibilità programmata</th><td> </td></tr>
<tr class="even"><th>Unita</th><td> </td></tr>
<tr class="odd"><th>Listino</th><td></td></tr>
<tr class="even"><th>Prezzo articolo</th><td>400</td></tr>
<tr class="odd"><th>Maggiorazioni articolo</th><td></td></tr>
<tr class="even"><th>Sconti Articolo</th><td></td> </tr>
<tr class="odd"><th>Varianti</th><td></td></tr>
</td></tr>
</tbody></table>
   
    
    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="connections">
    
    
    
    
    
    
    
    
    <table class="table table-hover">




<thead> <tr> <th class="col-md-1"></th> <th> </th> <th class="col-md-1"> </th>  </tr> </thead>

<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">
</td> 

<td>
<address>
  <strong>Article 5  </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 				<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Show </button></td> 	
    
</tr> 
  
  
  <tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">

<td>
<address>
  <strong>Article 6  </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 	<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Show </button></td> 	
    
</tr> 

<tr> 

<td>  
<img class="media-object img-thumbnail pull-left" data-src="holder.js/64x64" alt="64x64" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTI1Njc0ZWJlYyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1MjU2NzRlYmVjIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxMy45MjE4NzUiIHk9IjM2LjM2NDA2MjUiPjY0eDY0PC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="width: 64px; height: 64px;">

<td>
<address>
  <strong>Article 7  </strong><br>
  Lorem ipsum description est<br>
  Codice articolo 5454<br>
</address>
 </td> 
 <td>   
    	
 			 	<button type="button" class="btn btn-default"><i class="fa fa-share"></i> Show </button></td> 	
    
</tr> 
    
   </table>  
    
    
    
  
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="documents">
    
    
    
    <table class="table table-striped"><thead>
<tr><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=name" data-sort="name">Name</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=type" data-sort="type">Type</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=sharable" data-sort="sharable">Sharable</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=size" data-sort="size">Size</a></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>reportVendite</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>575 kb</td><td><button type="button" class="btn btn-default"><i class="fa fa-arrows-alt"></i> open</button></td> </td></tr>
<tr data-key="1"><td>reportmargini</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>2566854 kb</td><td> <button type="button" class="btn btn-default"><i class="fa fa-arrows-alt"></i> open</button></td> </td></tr>
<tr data-key="2"><td>fatture</td><td>pdf</td><td><i class="fa fa-check-square-o"></i></td><td>49400 kb</td><td> <button type="button" class="btn btn-default"><i class="fa fa-arrows-alt"></i> open</button></td> </td></tr>
</tbody></table>
    
    

    
    
    
    
    </div>
   
    <div role="tabpanel" class="tab-pane" id="sent">
    
    
    
    
            <table class="table table-striped"><thead>
<tr><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=name" data-sort="name">Filename</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=type" data-sort="type">Sent date</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=sharable" data-sort="sharable">Opened</a></th><th><a href="/zot/web/documents/main/index?id_folder=8&amp;sort=size" data-sort="size">Size</a></th><th>&nbsp;</th></tr>
</thead>
<tbody>
<tr data-key="0"><td>Esempio HTML5 Euronda</td><td>5/12/2015</td><td><i class="fa fa-check-square-o"></i></td><td>575 kb</td><td><button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="1"><td>pdf_sfogliabile.zip</td><td>4/11/2015</td><td><i class="fa fa-check-square-o"></i></td><td>2566854 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
<tr data-key="2"><td>MuseExport.zip</td><td>7/10/2015</td><td><i class="fa fa-check-square-o"></i></td><td>49400 kb</td><td> <button type="button" class="btn btn-success"><i class="fa fa-pencil"></i> edit</button></td> </td></tr>
</tbody></table>

    
    
    
    
    
    
    </div>
    <div role="tabpanel" class="tab-pane" id="map">
    

    
    
       <img class=" " src=" http://www.advancedcustomfields.com/wp-content/uploads/2013/11/acf-google-maps-field-2-700x384.png" style="width: 100%; height: 100%;">
       
       
       
    </div>
  </div>




</div>
  
  
  
  
  </p>
  
