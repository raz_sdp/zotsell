<?php

namespace app\modules\contacts\controllers;

use yii\web\Controller;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        //return $this->render('index');
        return $this->redirect(\Yii::$app->urlManager->createUrl("contacts/records/list"));
    }
  
    /*public function actionDetails()
    {
        return $this->render('details');
    }
    
        public function actionEdit()
    {
        return $this->render('edit');
    }
    public function actionContactsusers()
    {
        return $this->render('contactsusers');
    }
    
    public function actionBulk()
    {
        return $this->render('bulk');
    }*/
}
