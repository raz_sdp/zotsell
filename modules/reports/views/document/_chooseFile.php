<?php
use yii\grid\GridView;

echo GridView::widget([
    'id' => 'no-js-grid-' . $key,
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'tableOptions' => [
        'class' => 'table table-striped'
    ],
    'options' => ['class' => 'table-responsive grid-search-result'],
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) use ($media, $key) {
                return [
                    'onclick' => "clickCheckbox(this,'{$model->name}', '{$media->type}');", //$.pjax.reload({container:'#search-files'});
                    'value' => $model->name,
                    'id' => 'repo-file-' . $key . '-' . $model->id_file,
                ]; //['data-uid' => $model->user_id, 'data-jid'=>$model->job_id];
            }
        ],
        [
            'attribute' => 'name',
            'filter' => false,
        /* 'filterInputOptions' => [
          //'placeholder' => 'Type in some characters...',
          ] */
        ],
        [
            'attribute' => 'size',
            'value' => function ($model) {
                return ($model['size']) ? $model['size'] . ' b' : '-';
            },
            'filter' => false,
        ],
    ],
    'layout' => '{summary}{items}{pager}',
]);
?>