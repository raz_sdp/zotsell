<?php

namespace app\modules\documents\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "zse_v1_media".
 * Model search for left grid on bulk (medias are not in this folder)
 *
 * @property integer $id_media
 * @property string $type
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property integer $cansend
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class MediaSearchNotCurrent extends \app\modules\documents\models\Media
{

    public $name;
    public $type;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['multimedia'], 'string'],
            [['cansend', 'record_status'], 'integer'],
            //    [['entity_id'], 'required'],
            [['date_modification'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['icon', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name', 'type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'type' => 'Type',
            'icon' => 'Icon',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'cansend' => 'Cansend',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    public function search($params, $folderId)
    {
        /*
SELECT media.id_media FROM zse_v1_media media 
LEFT JOIN zse_v1_media_links links on media.id_media=links.id_media 
WHERE media.id_media NOT IN (SELECT id_media FROM zse_v1_media_links WHERE id_container = 1 AND zse_v1_media_links.context=1 group BY id_media) 
AND (links.context=1) GROUP BY media.id_media
         */

        //select id_media that are in current folder
        $subQuery = (new \yii\db\Query())
            ->select('id_media')
            ->from('zse_v1_media_links')
            ->where('id_container=:folder', [':folder' => $folderId])
            ->andWhere('context=:context', [':context' => 1])
            ->groupBy('id_media');

        $query = (new \yii\db\Query())
            ->select('media.*')
            ->from(['media' => 'zse_v1_media'])
            ->where(['not in',  'media.id_media', $subQuery])
            ->groupBy(['media.id_media']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>false,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name',
                'type'
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andWhere('name LIKE "%' . $this->name . '%" AND type LIKE "%'.$this->type.'%"');

        return $dataProvider;
    }

}