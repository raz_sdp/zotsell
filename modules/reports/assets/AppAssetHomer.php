<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\documents\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetHomer extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/homer';
    public $sourcePath = '@webroot';
    public $css = [
        'css/fontawesome/css/font-awesome.css',
        'css/metisMenu/dist/metisMenu.css',
        'css/animate.css/animate.css',
        'css/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css',
        'css/fonts/pe-icon-7-stroke/css/helper.css',
        'css/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css',
        'css/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',        
        'css/styles/static_custom.css', //for main/folders
        'css/styles/style.css',
    ];

    public $js = [
        'js/jquery-ui/jquery-ui.min.js',
        "js/slimScroll/jquery.slimscroll.min.js",
        "js/metisMenu/dist/metisMenu.min.js",
        "js/iCheck/icheck.min.js",
        "js/sparkline/index.js",
        'js/datatables/media/js/jquery.dataTables.min.js',
        'js/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js',
        'js/nestable/jquery.nestable.js',//for main/folders
        "js/scripts/homer.js",
        "//cdn.datatables.net/plug-ins/1.10.11/sorting/file-size.js", //delete
//        'js/scripts/folders_nestable.js', //for nestable folder -> call from view file
        //"//cdn.datatables.net/plug-ins/1.10.11/sorting/num-html.js"
        //"//cdn.datatables.net/plug-ins/1.10.11/sorting/enum.js"
//        'js/scripts/ajax-modal-popup.js' // folders  -> call from view file
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
