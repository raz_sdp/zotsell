<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media_links".
 *
 * @property integer $id_media
 * @property integer $id_container
 * @property integer $context
 * @property integer $weight
 * @property string $date_modification
 * @property integer $record_status
 */
class MediaLinks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_media', 'id_container'/*, 'context'*/], 'required'],
            [['id_media', 'id_container', 'context', 'weight', 'record_status'], 'integer'],
            [['date_modification'], 'safe'],
            ['context', 'default', 'value' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'id_container' => 'Id Container',
            'context' => 'Context',
            'weight' => 'Weight',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }
    
    public function getMedia()
    {
        return $this->hasOne(Media::className(), ['id_media' => 'id_media']);
    }
    
    public function getFolder()
    {
        return $this->hasOne(ReportsTree::className(), ['id_report_tree' => 'id_container']);
    }
    
    /**
     * Set weight of media in current id_container folder
     * (increasing the current maximum value)
     */
    public function setWeight()
    {
        $queryMedia = (new \yii\db\Query());
        $maxWeightInFolder = $queryMedia
            ->select('MAX(weight)')
            ->from(self::tableName())
            ->where(['id_container'=>$this->id_container, 'context'=>$this->context])
            ->scalar();
        $this->weight = $maxWeightInFolder+1;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->loadDefaultValues(); // load default attributes from db table
                $this->setWeight();
            }
            else { // update

            }
            return true;    
        } 
        else  return false;
    }
    
}
