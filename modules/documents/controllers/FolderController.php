<?php

namespace app\modules\documents\controllers;

use app\modules\documents\models\DocsTree;
use app\modules\documents\models\Media;
use app\modules\documents\models\MediaLinks;
use app\modules\documents\models\DocsTreeTr;
use app\modules\documents\models\SearchRepositoryFiles;

use yii\data\ActiveDataProvider;
use Yii;

/**
 * Controller for actions with folder
 */
class FolderController extends \yii\web\Controller
{

    /**
     * View information about this folder
     * @param type $id
     * @return type
     */
    public function actionView($id)
    {
        $model = DocsTree::findOne($id);

        // data provider for Media in this folder
        $dataProviderMedia = new ActiveDataProvider([
            'query' => Media::find()
                ->joinWith('mediaLinks.folder')
                ->where(['zse_v1_media_links.id_container' => $id])
                ->groupBy('zse_v1_media.id_media'),
            'sort' => [
                'attributes' => ['name', 'type', 'sharable', 'size']
            ]
        ]);

        // data provider for translation this folder
        $dataProviderTranslation = new ActiveDataProvider([
            'query' => DocsTreeTr::find()
                ->joinWith('docsTree')
                ->where(['zse_v1_docs_tree_tr.id_folder' => $id]),
            'sort' => [
                'attributes' => [
                    'label',
                    'lang',
                    'description'
                ]
            ],
        ]);

        $folderTranslations = $this->buildTranslationArray($model); // translation for this folder

        return $this->render('view', [
            'model' => $model,
            'dataProviderMedia' => $dataProviderMedia,
            'folderTranslations' => $folderTranslations,
        ]);
    }

    /**
     * Create folder
     */
    public function actionCreate()
    {
        Yii::trace("GET params begin create = ".print_r(Yii::$app->request->queryParams,true));

        if (!Yii::$app->getRequest()->getQueryParam('id_folder')) {
            return $this->redirect(['main/list']);
        }
        $idContainer = Yii::$app->getRequest()->getQueryParam('id_folder');

        Yii::trace("POST Data:" . print_r(Yii::$app->request->post(), true));

        $folder = new DocsTree();
        $folder->icon = ''; // standard icon by default
        $folder->scenario = 'create';

        if ($folder->load(Yii::$app->request->post())) {
            if ($folder->validate()) {
                // save nested folder
                $parentFolder = DocsTree::findOne(['id_folder' => $idContainer]);
                $folder->appendTo($parentFolder);
                //$folder->save(false);

                return $this->redirect(['update', 'id' => $folder->getPrimaryKey()]);
            } else {
                $errors['folder'] = $folder->errors;
                Yii::trace("Errors when save files: " . print_r($errors, true));
            }
        }


        $folderTranslations = $this->buildTranslationArray($folder); // translation for this folder

        return $this->render('create', [
            'model' => $folder,
            'idContainer' => $idContainer,
            'folderTranslations' => $folderTranslations
        ]);
    }

    /**
     * Update existing model Media
     * @param $id
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $model->scenario = 'create';

        $folderTranslations = $this->buildTranslationArray($model); // translation for this folder

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id_folder]);
        }


        $folderParent = $model->parents(1)->one();

        return $this->render('create', [
            'model' => $model,
            'idContainer' => ($folderParent) ? $folderParent->id_folder : $model->id_folder,// parent folder id or current folder's id
            'folderTranslations' => $folderTranslations
        ]);
    }

    /**
     * Update translation for id_folder
     * @param type $id value of field 'docs_tree'.'id_folder'
     * @param type $lang
     * @return type
     */
    public function actionUpdateTranslation($id, $lang)
    {
        $model = $this->findMediaTrModel($id, $lang);
        if (!$model){
            $model = new DocsTreeTr();
            $model->id_folder = $id;
            $model->lang = $lang;
        }

        if($model->load(Yii::$app->request->post())) {
            $model->scenario = 'create';

            if($model->validate()){
                if($model->save(false)) {
                    //Yii::$app->session->setFlash('success', 'Model has been saved');
                    Yii::trace('model docstree updated:'.print_r($model->getAttributes(),true));
                }
                else {
                    //Yii::$app->session->setFlash('error', 'Model could not be saved');
                    Yii::trace('ERROR! model docstree update failed:'.print_r($model->errors,true));
                }
            }
            else {
                Yii::trace("Validation docstree error: " . print_r($model->errors, true));
            }
        }
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * Delete media by primary key $id
     * and delete related media translation models via Media::beforeDelete()
     * @param $id value of database field 'id_media'
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model) $model->delete();

        return $this->redirect(['main/list']);
    }


    public function actionDeleteTranslation($id, $lang)
    {
        $model = $this->findMediaTrModel($id, $lang);
        if ($model) $model->delete();

        return $this->redirect(['update', 'id' => $id]);
    }

    /** Find Folder model by primary key $id
     * @param $id value of database field 'id_media'
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = DocsTree::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Find Folder's translation
     * @param $folderId
     * @param $lang
     * @return array|bool|null|\yii\db\ActiveRecord
     */
    protected function findMediaTrModel($folderId, $lang)
    {
        if(($modelDocsTreeTr = DocsTreeTr::find()->where(['id_folder' => $folderId, 'lang' => $lang])->one()) !==null) {
            return $modelDocsTreeTr;
        }
        return false;
    }

    /**
     * Update translation for id_media
     * @param type $id value of field 'media'.'id_media'
     * @param type $lang
     * @return type
     */
/*s
    public function actionUpdateTranslation($id, $lang)
    {
        $model = $this->findMediaTrModel($id, $lang);
        if (!$model){
            $model = new MediaTr();
            $model->id_media = $id;
            $model->lang = $lang;
        }

        if($model->load(Yii::$app->request->post())) {
            if($model->type == 'gallery') $model->scenario = 'create-gallery';
            else $model->scenario = 'create';

            if($model->validate()){
                if($model->save(false)) {
                    //Yii::$app->session->setFlash('success', 'Model has been saved');
                    Yii::trace('we update or save mediaTR:'.print_r($model->getAttributes(),true));
                }
                else {
                    //Yii::$app->session->setFlash('error', 'Model could not be saved');
                    Yii::trace('ERROR! update or save mediaTR:'.print_r($model->errors,true));
                }
            }
            else {
                Yii::trace("Errors when save model Tr: " . print_r($model->errors, true));
            }
        }
        return $this->redirect(['update', 'id' => $id]);
    }
*/


    /**
     * Generate data for form in Modal with medias for moving
     * @param type $id
     * @return type
     */
    public function actionMoveFiles($id,$type)
    {
        $model = \app\modules\documents\models\DocsTree::findOne(['id_folder' => $id]);

        $typesArray = SearchRepositoryFiles::getMimetypesByType($type);
        $typesArray[] = $type; //contains mime-types and GET type

        $mediaAll = Media::find()
            ->joinWith('mediaLinks')
            ->where(['context' => 1])
            ->andWhere(['type' => $typesArray])
            ->asArray()->all(); // all available media

        $mediaInFolder = Media::find()
            ->joinWith('mediaLinks')
            ->where(['id_container' => $id])
            ->andWhere(['context' => 1])
            ->andWhere(['type' => $typesArray])
            ->asArray()->all(); //media in current folder

        $items = [];
        $selection = [];

        foreach ($mediaAll as $media) {
            $items[$media['id_media']] = $media['name'];
        }
        foreach ($mediaInFolder as $mediaFolder) {
            $selection[$mediaFolder['id_media']] = $mediaFolder['name'];
        }

        return $this->renderAjax('_moveFiles', [
            'model' => $model,
            'items' => $items,
            'selection' => $selection,
        ]);
    }

    /**
     * Save moved files (from modal)
     * @return type
     */
    public function actionSaveMovedFiles()
    {
        $postData = Yii::$app->getRequest()->post();

        $idFolder = $postData['DocsTree']['id_folder'];
        $errors = [];

        $mediaInFolderBefore = Media::find()->joinWith('mediaLinks')->where(['id_container' => $idFolder])->andWhere(['context' => 1])->all();
        $mediaInFolderAfter = (isset($postData['multiForm'])) ? $postData['multiForm'] : [];

        foreach ($mediaInFolderBefore as $mediaBefore) {
            if (!in_array($mediaBefore->id_media, $mediaInFolderAfter)) { //media doesn't exist in folder after moving => delete media
                $modelDel = $this->findMediaLinksModel($mediaBefore->id_media, $idFolder);
                if ($modelDel) $modelDel->delete();
                else $errors[] = 'Don\'t remove media with id =' . $mediaBefore->id_media;
            } else $mediaInFolderAfter = $this->removeArrayElement($mediaInFolderAfter, $mediaBefore->id_media);//$mediaInFolderAfter = array_diff($mediaInFolderAfter, [$mediaBefore->id_media]);
        }


        if (count($mediaInFolderAfter) > 0) {//need to save this media in folder
            foreach ($mediaInFolderAfter as $idMediaAdd) {
                $model = new MediaLinks();
                $model->id_media = $idMediaAdd;
                $model->id_container = $idFolder;
                $model->context = 1;

                if (!$model->save()) $errors[] = 'Failed to save the media with id =' . $idMediaAdd;
            }
        }

        if (count($errors) > 0) return json_encode($errors);

        $success = true;
        return json_encode($success);
    }

    /**
     * Remove array's element by value
     * @param $array
     * @param $element
     * @return array
     */
    public function removeArrayElement($array, $element)
    {
        return array_diff($array, [$element]);
    }

    /**
     * Finds the MediaLinks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_media
     * @param integer $id_container
     * @param integer $context
     * @return MediaLinks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findMediaLinksModel($id_media, $id_container, $context = 1)
    {
        if (($model = MediaLinks::findOne(['id_media' => $id_media, 'id_container' => $id_container, 'context' => $context])) !== null) {
            return $model;
        } else {
            return false;//throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Build translation array with related models DocsTreeTr (if isset) for current DocsTree
     * @param type $folderModel
     * @return array translations with added models MediaTr as array element 'model'
     */
    public function buildTranslationArray($folderModel)
    {
        $translations = Media::getLanguages();
        foreach($folderModel->docsTreeTr as $docTr){
            $issetModel = $docTr;
            $issetModel->scenario = 'create';
            $translations[$docTr->lang]['model'] = $issetModel;
        }
        foreach ($translations as $key => $translation) {
            if (!isset($translation['model'])) {
                $emptyModel = new docsTreeTr();
                $emptyModel->scenario = 'create';
                $translations[$key]['model'] = $emptyModel;
            }
        }
        Yii::trace('buildTranslationArray Folder:'.print_r($translations,true));
        return $translations;
    }

}
