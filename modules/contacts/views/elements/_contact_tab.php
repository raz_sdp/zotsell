<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:17 PM
 */
?>
<div class="hpanel panel-group">
    <div class="panel-body">
        <div class="text-center text-muted font-bold">Select Contact Tab</div>

    </div>
    <div id="notes" class="collapse">
        <div class="panel-body note-link active_contact_side_bar">
            <a href="#note0" data-toggle="tab">
                <small class="pull-right text-muted">Active</small>
                <h5>Basic Info</h5>

                <div class="small">
                    add information shown on Default tab
                </div>
            </a>
        </div>

        <div class="panel-body  note-link <?=$secondary_tab_disable?>" id="js-tab-connections">
            <a href="#note1" data-toggle="tab">
                <small class="pull-right text-muted">Active</small>
                <h5>
                    Connections
                </h5>

                <div class="small">
                    add information shown on Connections tab
                </div>
            </a>
        </div>

        <div class="panel-body note-link <?=$secondary_tab_disable?>" id="js-tab-document">
            <a href="#note2" data-toggle="tab">
                <small class="pull-right text-muted">Active</small>
                <h5>
                    Documents
                </h5>

                <div class="small">
                    add information shown on Documents tab
                </div>
            </a>
        </div>

        <div class="panel-body note-link <?=$secondary_tab_disable?>" id="js-tab-order">
            <a href="#note3" data-toggle="tab">
                <small class="pull-right text-muted">Active</small>
                <h5>
                    Sales
                </h5>

                <div class="small">
                    add information shown on Sales tab
                </div>
            </a>
        </div>

        <div class="panel-body note-link <?=$secondary_tab_disable?>" id="js-tab-activity">
            <a href="#note4" data-toggle="tab">
                <small class="pull-right text-muted">Disabled</small>
                <h5>
                    Activities
                </h5>
                <div class="small">
                    add information shown on Activities tab
                </div>
            </a>
        </div>

        <div class="panel-body note-link <?=$secondary_tab_disable?>" id="js-map-tab">
            <a href="#note5" data-toggle="tab">
                <small class="pull-right text-muted">Active</small>
                <h5>
                    Maps
                </h5>
                <div class="small">
                    add information shown on Maps tab
                </div>
            </a>
        </div>
    </div>
</div>