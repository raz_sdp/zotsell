<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$this->registerJs(
    '$("document").ready(function(){
        //$("#search_file").on("pjax:end", function() { //before
        $(".pjax-search-file").on("pjax:end", function() {
            var containerId = $(this).closest(\'.note-content\').find(\'.pjax-search-result\').attr("id");
            //$.pjax.reload({container:"#search-result"});  //before Reload GridView
            $.pjax.reload({container: containerId});
        });
    });'
);
?>

    <div class="search-form">

        <?php yii\widgets\Pjax::begin(['id' => 'search_file-' . $key, 'timeout' => 5000, 'options' => ['class' => 'pjax-search-file']]) ?>
        <?php
        $form = ActiveForm::begin(
            [
                'method' => 'get',
                'options' => ['data-pjax' => true,]]);
        ?>
        <div class="form-group">
            <?= $form->field($searchModel, 'name')->textInput([/*'id' => 'search-field'*/
                    'class' => 'form-control', 'placeholder' => 'Enter file name'])->label('Search:') ?>
        </div>

        <?php ActiveForm::end(); ?>
        <?php yii\widgets\Pjax::end() ?>

        <div>
            <?= yii\bootstrap\Button::widget([
                'label' => '<strong>Search</strong>',
                'encodeLabel' => false,
                'options' => [
                    //'id' => 'searchButton',
                    'class' => 'btn btn-block btn-success m-t-n-xs',
                    'disabled' => false,
                ],
            ]);
            ?>
        </div>

    </div>

<?php
$this->registerJs(<<<JS
//$('#search_file').on('pjax:error', function (event) {
$('.pjax-search-file').on('pjax:error', function (event) {
window.alert('Failed to load the page');
console.log('error!');
event.preventDefault();
});
JS
);