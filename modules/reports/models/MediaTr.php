<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media_tr".
 *
 * @property integer $id_media
 * @property string $lang
 * @property string $type
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property string $entity_id
 */
class MediaTr extends \yii\db\ActiveRecord
{
    public $idContainer;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media_tr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_media', 'lang', /*'entity_id'*/], 'required'],
            [['id_media'], 'integer'],
            [['multimedia'], 'string'],
            [['lang', 'type', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['lang', 'id_media'], 'unique', 'targetAttribute' => ['lang', 'id_media'], 'message' => 'The combination of Id Media and Lang has already been taken.'],
            [['name', 'description'], 'required', 'on' => ['create']],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['name', 'description', 'file', 'lang', 'type'];
        $scenarios['create-gallery'] = ['name', 'description', 'multimedia', 'lang', 'type'];
        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'lang' => 'Lang',
            'type' => 'Type',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'entity_id' => 'Entity ID',
        ];
    }
    
    public function getMedia()
    {
        return $this->hasOne(Media::className(), ['id_media' => 'id_media']);
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->entity_id = $this->media['entity_id'];
                $this->file = ($this->file) ? $this->file : $this->media['file'];
                $this->multimedia = ($this->multimedia) ? $this->multimedia : $this->media['multimedia'];
                $this->type = ($this->type) ? $this->type : $this->media['type'];
            } else { // update

            }
            return true;
        } else  return false;
    }

    public function setIdContainer($idContainer)
    {
        $this->idContainer = $idContainer;
    }

    public function getIdContainer()
    {
        return $this->idContainer;
    }

    //new
    //public function getRepositoryFile()
    //{
    //    return $this->hasOne(RepositoryFiles::className(), ['name' => 'file']);
    //}
    
}
