<?php

namespace app\modules\reports\models;

use Yii;

/**
 * This is the model class for table "zse_v1_media".
 *
 * @property integer $id_media
 * @property string $type
 * @property string $icon
 * @property string $name
 * @property string $description
 * @property string $file
 * @property string $multimedia
 * @property integer $cansend
 * @property string $entity_id
 * @property string $date_modification
 * @property integer $record_status
 */
class Media extends \yii\db\ActiveRecord
{

    public $idContainer; // container_id for generate entity_id
    public $lang; // value 'default'

    /**
     * Relation array by field 'zse_v1_media_tr'.'lang'
     * @var type
     */
    public static $translationLanguages = [
        'en-us' => [
            'language' => 'English',
            'country' => 'USA'
        ],
        'fr' => [
            'language' => 'French',
            'country' => 'FRANCE'
        ],
        'it' => [
            'language' => 'Italian',
            'country' => 'ITALY'
        ],
        'en' => [
            'language' => 'English',
            'country' => 'UK'
        ],
        'es' => [
            'language' => 'Spanish',
            'country' => 'SPAIN'
        ],
        'de' => [
            'language' => 'German',
            'country' => 'GERMANY'
        ]
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['multimedia'], 'string'],
            [['cansend', 'record_status'], 'integer'],
            [['entity_id'], 'required'],
            [['date_modification'], 'safe'],
            [['type'], 'string', 'max' => 32],
            [['icon', 'name', 'description', 'file'], 'string', 'max' => 255],
            [['entity_id'], 'string', 'max' => 64],
            [['name', 'description', 'file'], 'required', 'on' => ['create']],
            [['name', 'description', 'multimedia'], 'required', 'on' => ['create-gallery']],
            ['icon', 'compare', 'compareValue' => 'custom', 'operator' => '!=',
                'when' => function ($model) {
                    if ($model->icon == 'custom')
                        return true;
                },
                'whenClient' => "function (attribute, value) {
                    if($('input[name=\"Media[icon]\"]:checked').val()=='custom'){
                        return true;
                    }
                    }",
                'on' => ['create', 'create-gallery'],
                'message' => 'When set Custom, you have to upload icon'] //compare validator
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['name', 'description', 'cansend', 'icon', 'file', 'type'];
        $scenarios['create-gallery'] = ['name', 'description', 'cansend', 'icon', 'multimedia', 'type'];

        return $scenarios;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_media' => 'Id Media',
            'type' => 'Type',
            'icon' => 'Icon',
            'name' => 'Name',
            'description' => 'Description',
            'file' => 'File',
            'multimedia' => 'Multimedia',
            'cansend' => 'Cansend',
            'entity_id' => 'Entity ID',
            'date_modification' => 'Date Modification',
            'record_status' => 'Record Status',
        ];
    }

    public function getMediaLinks()
    {
        return $this->hasMany(MediaLinks::className(), ['id_media' => 'id_media'])->andOnCondition(['zse_v1_media_links.context' => 4]);
    }

    public function getFolders()
    {
        return $this->hasMany(ReportsTree::className(), ['id_report_tree' => 'id_container'])
            ->via('mediaLinks');
    }
    
    public function getMediaTr()
    {
        return $this->hasMany(MediaTr::className(), ['id_media' => 'id_media']);
    }
    
    public function getRepositoryFile()
    {
        return $this->hasOne(RepositoryFiles::className(), ['name' => 'file']);
    }
    
    /**
     * Get media' file name
     * if type==gallery => return media's name
     * else => return repository_file's name
     * @return type
     */
    public function getFileName()
    {
        if (isset($this->repositoryFile)) { //calculate size for this media
            return $this->repositoryFile->name;
        } 
        // type = gallery
        return $this->name;
    }

    /**
     * Get media's size depending on the type of
     */
    public function getSize()
    {
        if (isset($this->repositoryFile)) { //calculate size for this media
            return $this->repositoryFile->size;
        } 
        elseif ($this->type === 'gallery') { //calculate size for gallery item
            $galleryItems = explode("\n", $this->multimedia);
            $gallerySize = 0;
            foreach ($galleryItems as $item) {
                $itemSize = \app\modules\reports\models\RepositoryFiles::find()->select('size')->where(['name' => $item])->scalar();
                $gallerySize += $itemSize;
            }
            return $gallerySize;
        }
        return 0;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->loadDefaultValues(); // load default attributes from db table
                if ($this->type == 'file') { //if type file, try to find type for media by 'repository_files'.'mimetype'
                    if ($this->repositoryFile) {
                        $detectedType = SearchRepositoryFiles::getTypeByMimeType($this->repositoryFile->mimetype);
                        if ($detectedType) $this->type = $detectedType;
                    }
                }
                $this->entity_id = ($this->idContainer) ? sha1(mt_rand(10000, 99999) . time() . $this->idContainer) : 0; // set idContainer
            } else { // update

            }
            return true;
        } else
            return false;
    }
    
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->mediaTr as $mediaTr) {
                $mediaTr->delete(); // delete related media translation models
            }
            foreach ($this->mediaLinks as $link) {
                $link->delete(); // delete related media translation models
            }
            return true;
        } else {
            return false;
        }
    }

    public function setIdContainer($idContainer)
    {
        $this->idContainer = $idContainer;
    }

    public function getIdContainer()
    {
        return $this->idContainer;
    }

    /**
     * Get languages relations array
     * @return type
     */
    public static function getLanguages()
    {
        return self::$translationLanguages;
    }

}
