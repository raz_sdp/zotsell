<?php

namespace app\modules\contacts\models;

use Yii;

/**
 * This is the model class for table "zse_v1_accounts_services_licenses".
 *
 * @property integer $lid
 * @property string $id_owner
 * @property string $id_account
 * @property integer $id_record
 * @property integer $type
 * @property string $serial
 * @property string $usercode
 * @property string $userzone
 * @property string $key
 * @property string $expiration
 * @property integer $connection
 * @property integer $renewal
 * @property integer $visibility
 */
class AccountsServicesLicenses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zse_v1_accounts_services_licenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_owner', 'type', 'serial'], 'required'],
            [['id_owner', 'id_account', 'id_record', 'type', 'connection', 'renewal', 'visibility'], 'integer'],
            [['expiration'], 'safe'],
            [['serial', 'usercode', 'userzone', 'key'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lid' => 'Lid',
            'id_owner' => 'Id Owner',
            'id_account' => 'Id Account',
            'id_record' => 'Id Record',
            'type' => 'Type',
            'serial' => 'Serial',
            'usercode' => 'Usercode',
            'userzone' => 'Userzone',
            'key' => 'Key',
            'expiration' => 'Expiration',
            'connection' => 'Connection',
            'renewal' => 'Renewal',
            'visibility' => 'Visibility',
        ];
    }
}
