<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SearchZseV1Records */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zse-v1-records-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_record') ?>

    <?= $form->field($model, 'id_locality') ?>

    <?= $form->field($model, 'id_classification') ?>

    <?= $form->field($model, 'code') ?>

    <?= $form->field($model, 'code_destination') ?>

    <?php // echo $form->field($model, 'business_name') ?>

    <?php // echo $form->field($model, 'legal_status') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'mobile') ?>

    <?php // echo $form->field($model, 'inserted_by') ?>

    <?php // echo $form->field($model, 'assigned_to') ?>

    <?php // echo $form->field($model, 'origin') ?>

    <?php // echo $form->field($model, 'is_contact') ?>

    <?php // echo $form->field($model, 'is_customer') ?>

    <?php // echo $form->field($model, 'is_supplier') ?>

    <?php // echo $form->field($model, 'is_prospect') ?>

    <?php // echo $form->field($model, 'is_destination') ?>

    <?php // echo $form->field($model, 'is_reseller') ?>

    <?php // echo $form->field($model, 'is_intrested') ?>

    <?php // echo $form->field($model, 'authorize_send') ?>

    <?php // echo $form->field($model, 'coordinates') ?>

    <?php // echo $form->field($model, 'external_code') ?>

    <?php // echo $form->field($model, 'icon') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'province') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'postal_code') ?>

    <?php // echo $form->field($model, 'vat_code') ?>

    <?php // echo $form->field($model, 'fiscal_code') ?>

    <?php // echo $form->field($model, 'pricelist') ?>

    <?php // echo $form->field($model, 'options') ?>

    <?php // echo $form->field($model, 'person_in_charge') ?>

    <?php // echo $form->field($model, 'role') ?>

    <?php // echo $form->field($model, 'direct_mail') ?>

    <?php // echo $form->field($model, 'direct_phone') ?>

    <?php // echo $form->field($model, 'date_modification') ?>

    <?php // echo $form->field($model, 'record_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
