$(document).ready(function () {
    $("#modalSubmit").click(function(e) {
        e.preventDefault();
        var form = $("#formMoveFiles");
        $.ajax({
            url: "/documents/folder/save-moved-files",
            type: "POST",
            data: form.serialize(),
            success: function (result) {
                console.log('response='+$.parseJSON(result));
                var parseResponse = $.parseJSON(result);
                var modalContainer = $('#modal');
                var modalBody = modalContainer.find('#modalContent');
                var insidemodalBody = modalContainer.find('#formMoveFiles');

                if (parseResponse == true) {
                    insidemodalBody.html(result).hide(); // 
                    //$('#my-modal').modal('hide');
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').append("<strong>Files Moved successfully</strong>");
                    $('#success > .alert-success').append('</div>');


                    setTimeout(function () { // close modal after 2 seconds
                        $("#modal").modal('hide');
                        //console.log('nestable '+nestableFolders);
                        $("#nestableFolders").load(location.href + " #nestableFolders");
                        //document.location.reload();
                    }, 1000);
                }
                else {
                    modalBody.html(result).hide().fadeIn();
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').append("<strong>"+parseResponse+"</strong>");
                    $('#success > .alert-danger').append('</div>');

                    setTimeout(function () { // close modal after 2 seconds
                        $("#modal").modal('hide');
                        //console.log('nestable '+nestableFolders);
                        $("#nestableFolders").load(location.href + " #nestableFolders");
                        //document.location.reload();
                    }, 1000);

                }
            }
        });
        //$("#myForm").submit();
    });

});

