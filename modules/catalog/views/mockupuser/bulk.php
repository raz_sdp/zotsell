<?php
    $this->title = 'Contacts for %username%';
    $this->params['breadcrumbs'][] = $this->title;
?>


  <h1>Associate Items  to Users</h1>
  <p class="lead">Bulk Associate  Items to %username%</p>
  
  
  

<div class="row">
<div class="col-md-6">
    <div id="w0" class="grid-view"><table class="table table-striped  table-condensed"><thead>
<tr><th><input type="checkbox" class="select-on-check-all" name="selection_all" value="1"></th><th><a href="/zot/web/documents/folder/bulk?id=1&amp;media-all-sort=name" data-sort="name">Name</a></th><th><a href="/zot/web/documents/folder/bulk?id=1&amp;media-all-sort=type" data-sort="type">Type</a></th></tr><tr id="w0-filters" class="filters"><td>&nbsp;</td><td><input type="text" class="form-control" name="MediaSearchNotCurrent[name]"></td><td><input type="text" class="form-control" name="MediaSearchNotCurrent[type]"></td></tr>
</thead>
<tbody>
<tr data-key="0"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>prospect</td></tr>
<tr data-key="1"><td><input type="checkbox" name="selection[]" value="3" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>prospect</td></tr>
<tr data-key="2"><td><input type="checkbox" name="selection[]" value="6" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>prospect</td></tr>
<tr data-key="3"><td><input type="checkbox" name="selection[]" value="7" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="4"><td><input type="checkbox" name="selection[]" value="32" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="5"><td><input type="checkbox" name="selection[]" value="51" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="6"><td><input type="checkbox" name="selection[]" value="70" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="7"><td><input type="checkbox" name="selection[]" value="89" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="8"><td><input type="checkbox" name="selection[]" value="90" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="9"><td><input type="checkbox" name="selection[]" value="92" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="10"><td><input type="checkbox" name="selection[]" value="96" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="11"><td><input type="checkbox" name="selection[]" value="115" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="12"><td><input type="checkbox" name="selection[]" value="134" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="13"><td><input type="checkbox" name="selection[]" value="155" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="14"><td><input type="checkbox" name="selection[]" value="176" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="15"><td><input type="checkbox" name="selection[]" value="201" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>

</tbody></table></div>    <div class="pull-right">
        <button id="addButton" class="bulk-actions-btn btn btn-success btn-small">Add selected</button>    </div>
</div>

<div class="col-md-6">
    <div id="w1" class="grid-view"><table class="table table-striped  table-condensed"><thead>
<tr><th><input type="checkbox" class="select-on-check-all" name="selection_all" value="1"></th><th><a href="/zot/web/documents/folder/bulk?id=1&amp;media-cur-sort=name" data-sort="name">Name</a></th><th><a href="/zot/web/documents/folder/bulk?id=1&amp;media-cur-sort=type" data-sort="type">Type</a></th></tr><tr id="w1-filters" class="filters"><td>&nbsp;</td><td><input type="text" class="form-control" name="MediaSearchCurrent[name]"></td><td><input type="text" class="form-control" name="MediaSearchCurrent[type]"></td></tr>
</thead>
<tbody>
<tr data-key="9"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>prospect</td></tr>
<tr data-key="91"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>prospect</td></tr>
<tr data-key="197"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="198"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="199"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>customer</td></tr>
<tr data-key="200"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
<tr data-key="223"><td><input type="checkbox" name="selection[]" value="1" onclick="console.log(&#039;clicked:&#039;+this.checked);"></td><td>Nome contatto</td><td>destination</td></tr>
</tbody></table></div>    <!--<input type="button" class="btn btn-info" value="Multiple Delete" id="MyButton" >-->
    <div class="pull-right">
    <button id="removeButton" class="bulk-actions-btn btn btn-danger btn-small">Remove selected</button>    </div>
</div>




    </div>
