<?php
/**
 * Created by PhpStorm.
 * User: Rahul
 * Date: 2/27/16
 * Time: 12:22 PM
 */
?>
<div class="pull-right text-muted m-l-lg">
    Active
</div>
<h3> Documents</h3>
<hr/>
<div class="note-content">
    <div class="row">
        <div class="col-lg-12">
            <table id="example2"
                   class="table table-striped table-bordered table-hover js-view-document-table custom_table_brake">
                <thead>
                <tr>
                    <th>Icon</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Sharable</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>


                    