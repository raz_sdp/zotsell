<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 12:23 AM
 */?>
<div class="color-line">
</div>
<div id="logo" class="light-version">
        <span>
            Zot admin
        </span>
</div>
<nav role="navigation">
    <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
    <div class="small-logo">
        <span class="text-primary">Zot Admin</span>
    </div>
    <form role="search" class="navbar-form-custom" method="post" action="#">
        <div class="form-group">
            <input type="text" placeholder="Search something special" class="form-control" name="search">
        </div>
    </form>
    <div class="mobile-menu">
        <button type="button" class="navbar-toggle mobile-menu-toggle" data-toggle="collapse"
                data-target="#mobile-collapse">
            <i class="fa fa-chevron-down"></i>
        </button>
        <div class="collapse mobile-navbar" id="mobile-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a class="" href="login.html">Login</a>
                </li>
                <li>
                    <a class="" href="login.html">Logout</a>
                </li>
                <li>
                    <a class="" href="profile.html">Profile</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="navbar-right">
        <ul class="nav navbar-nav no-borders">
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="pe-7s-speaker"></i>
                </a>
                <ul class="dropdown-menu hdropdown notification animated flipInX">
                    <li>
                        <a>
                            <span class="label label-success">NEW</span> It is a long established.
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="label label-warning">WAR</span> There are many variations.
                        </a>
                    </li>
                    <li>
                        <a>
                            <span class="label label-danger">ERR</span> Contrary to popular belief.
                        </a>
                    </li>
                    <li class="summary"><a href="#">See all notifications</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                    <i class="pe-7s-keypad"></i>
                </a>

                <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                    <table>
                        <tbody>
                        <tr>
                            <td>
                                <a href="admin/index.html">
                                    <i class="pe pe-7s-config text-info"></i>
                                    <h5>Admin</h5>
                                </a>
                            </td>
                            <td>
                                <a href="super_user/index.html">
                                    <i class="pe pe-7s-users"></i>
                                    <h5>Superuser</h5>
                                </a>
                            </td>
                            <td>
                                <a href="user/index.html">
                                    <i class="pe pe-7s-user text-success"></i>
                                    <h5>User</h5>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="forum.html">
                                    <i class="pe pe-7s-help2 text-info"></i>
                                    <h5>Help</h5>
                                </a>
                            </td>
                            <td>
                                <a href="analytics.html">
                                    <i class="pe pe-7s-cart text-danger"></i>
                                    <h5>Licence Store</h5>
                                </a>
                            </td>
                            <td>
                                <a href="file_manager.html">
                                    <i class="pe pe-7s-box1 text-success"></i>
                                    <h5>Files</h5>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle label-menu-corner" href="#" data-toggle="dropdown">
                    <i class="pe-7s-mail"></i>
                    <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu hdropdown animated flipInX">
                    <div class="title">
                        You have 4 new messages
                    </div>
                    <li>
                        <a>
                            It is a long established.
                        </a>
                    </li>
                    <li>
                        <a>
                            There are many variations.
                        </a>
                    </li>
                    <li>
                        <a>
                            Lorem Ipsum is simply dummy.
                        </a>
                    </li>
                    <li>
                        <a>
                            Contrary to popular belief.
                        </a>
                    </li>
                    <li class="summary"><a href="#">See All Messages</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="login.html">
                    <i class="pe-7s-upload pe-rotate-90"></i>
                </a>
            </li>
        </ul>
    </div>
</nav>