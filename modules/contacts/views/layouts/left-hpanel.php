<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 2/24/16
 * Time: 1:38 AM
 */
?>
<div class="hpanel">
    <div class="panel-body">
        <div class="dropdown">
            <a class="dropdown-toggle btn btn-success btn-block" href="#" data-toggle="dropdown">
                NEW
            </a>
            <ul class="dropdown-menu filedropdown m-l">
                <li><a href="<?php echo Yii::$app->homeUrl?>contacts/records/company_create?type=1"><i class="fa fa-building-o"></i> Company</a></li>
                <li><a href="<?php echo Yii::$app->homeUrl?>contacts/records/company_create?type=2"><i class="fa fa-truck"></i> Destination</a></li>
                <li><a href="<?php echo Yii::$app->homeUrl?>contacts/records/company_create?type=3"><i class="fa fa-user-secret"></i> Prospect</a></li>
                <li><a href="<?php echo Yii::$app->homeUrl?>contacts/records/company_create?type=4"><i class="fa fa-user"></i> Person</a></li>

            </ul>
        </div>

        <ul class="h-list m-t">
            <?php $action = Yii::$app->controller->action->id; ?>
            <li <?php if($action=="list") echo "class=\"active\"";?>><a href="<?php echo Yii::$app->homeUrl?>contacts/records/list"><i class="fa fa-list"></i> List</a></li>
            <li <?php if($action=="folder") echo "class=\"active\"";?>><a href="<?php echo Yii::$app->homeUrl?>contacts/records/folder"><i class="fa fa-folder"></i> Folders</a></li>
            <li <?php if($action=="flow") echo "class=\"active\"";?>><a href="<?php echo Yii::$app->homeUrl?>contacts/records/flow"><i class="fa fa-th"></i> Flow</a></li>
        </ul>
    </div>
</div>