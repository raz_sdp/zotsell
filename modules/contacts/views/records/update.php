<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\contacts\models\Records */

$this->title = 'Update Records: ' . ' ' . $model->id_record;
$this->params['breadcrumbs'][] = ['label' => 'Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_record, 'url' => ['view', 'id' => $model->id_record]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="records-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
